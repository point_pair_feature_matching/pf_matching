#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase


# script settings
# TODO: change
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2S_extended3'


PRIMITIVES_SETTINGS = {'matcher_settings': {'use_rotational_symmetry': True,
                                       'collapse_symmetric_models': True,
                                       'publish_equivalent_poses': True,
                                       'rs_maxThresh': 0.4,
                                       'rs_pose_weight_thresh': 0.3,
                                       'publish_n_best_poses': 2
                                       },
                       'preprocessor_settings': {'remove_largest_plane': True,
                                                 'remove_plane_first': True,
                                                'plane_inlier_threshold': 0.006,
                                                'plane_inlier_threshold_is_relative': False
                                            }
                       }

# info about the dataset: path, matcher_settings, scene preprocessor settings
primitives_normal = PRIMITIVES_SETTINGS.copy()
primitives_normal.update({'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py'})
primitives_noiseless = PRIMITIVES_SETTINGS.copy()
primitives_noiseless.update({'path': '~/ROS/datasets/geometric_primitives_noiseless/blensor_dataset.py'})
primitives_low_noise = PRIMITIVES_SETTINGS.copy()
primitives_low_noise.update({'path': '~/ROS/datasets/geometric_primitives_low_noise/blensor_dataset.py'})
DATASET_DICTS = [primitives_normal,
                 # primitives_noiseless,
                 # primitives_low_noise,
                 {'path': '~/ROS/datasets/household_objects_multi_instance/blensor_dataset.py',
                  'matcher_settings': {'use_rotational_symmetry': False,
                                       'publish_n_best_poses': 2
                                       },
                  'preprocessor_settings': {'remove_largest_plane': True,
                                            'remove_plane_first': True,
                                            'plane_inlier_threshold': 0.006,
                                            'plane_inlier_threshold_is_relative': False
                                            }
                  }
                ]

# [((featureA, featureB, ...), ((weightA0, weightB0, ...),
#                               (weightA1, weightB1, ...))),
#  ...]
# S2S combinations
# FEATURE_COMBINATIONS = [(('S2S', 'B2B'), ((1, 20),
#                                           (1, 40),  # range circa 30 - 120
#                                           (1, 60),
#                                           (1, 80),
#                                           (1, 100),
#                                           (1, 120),
#                                           (1, 140),
#                                           (1, 180)
#                                           )),
#                         (('S2S', 'S2B'), ((1, 6),   # range 5 - 15
#                                           (1, 9),
#                                           (1, 12),
#                                           (1, 15),
#                                           (1, 18),
#                                           (1, 21),
#                                           (1, 24)
#                                           )),
#                         (('S2S', 'B2S'), ((1, 2),   # range 2 - 10
#                                           (1, 4),
#                                           (1, 6),
#                                           (1, 8),
#                                           (1, 10),
#                                           (1, 12)
#                                           )),
#                         ]

# FEATURE_COMBINATIONS = [(('S2B', 'B2S'), ((1, 1),   # range 5 - 15
#                                           (1, 2),
#                                           (1, 3),
#                                           (2, 1),
#                                           (3, 1)
#                                           )),
#                         (('S2B', 'B2B'), ((1, 1),
#                                           (1, 2),
#                                           (1, 3),   # range 2 - 10
#                                           (1, 6),
#                                           (1, 9),
#                                           (1, 12)
#                                           )),
#                         ]

# FEATURE_COMBINATIONS = [(('S2B', 'B2S'), ((3, 1),
#                                           (4, 1),
#                                           (5, 1)
#                                           )),
#                         (('S2B', 'B2B'), ((1, 1),
#                                           (1, 2),
#                                           )),
#                         ]

FEATURE_COMBINATIONS = [(('S2S', 'S2B'), ((1, 31),
                                          ))
                        ]

EDGE_THRESH_LOW = 0.1
EDGE_THRESH_HIGH = 0.2

FEATURE_TYPES = ['S2S', 'B2B', 'S2B', 'B2S']


def generate_all_features_enabled_settings():
    """
    generate a dict with settings for all features enabled and with a weight of 1.0
    @return: dict with the above settings
    """
    settings = {}
    for feature_name in FEATURE_TYPES:
        settings['match_' + feature_name] = True
        settings[feature_name + '_weight'] = 1
    return settings


def generate_all_features_disabled_settings():
    """
    generate a dict with settings for all features disabled and with a weight of 1.0
    @return: dict with the above settings
    """
    settings = {}
    for feature_name in FEATURE_TYPES:
        settings['match_' + feature_name] = False
        settings[feature_name + '_weight'] = 1
    return settings


class FeatureTypesCombinationExperiment(ExperimentBase):
    """
    experiment for checking the performance of combining two feature types during matching
    """
    def __init__(self, data_storage, save_path_wo_extension, dataset_dicts, feature_combinations):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)

        # copy dataset settings and import the datasets into the structure
        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])

        self.feature_combinations = feature_combinations

    def setup(self):

        # set up pipeline
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True,
                                       'extract_boundary': False,
                                       'extract_curvature_edges': True,
                                       'curvature_lower_thresh': EDGE_THRESH_LOW,
                                       'curvature_upper_thresh': EDGE_THRESH_HIGH
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'extract_boundary': True,
                                       'extract_curvature_edges': True,
                                       'curvature_lower_thresh': EDGE_THRESH_LOW,
                                       'curvature_upper_thresh': EDGE_THRESH_HIGH
                                       }

        matcher_settings = {'show_results': False,
                            'publish_pose_cluster_weights': False,
                            'd_dist': 0.05,
                            'refPointStep': 2,
                            'd_dist_is_relative': True,
                            'maxThresh': 1.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

    def looping(self):

        # calculate number of runs and set up runtime estimator
        n_matches = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_matches += dataset.get_number_of_models() * dataset.get_number_of_scenes()

        n_data_points = 0
        for feature_combination in self.feature_combinations:
            n_data_points += len(feature_combination[1])  # number of different weight ratios

        n_runs = n_matches * n_data_points
        re = ros_interface.RuntimeEstimator(n_runs)

        # loop over datasets
        self.interface.new_run()
        for dataset_dict in self.dataset_dicts:

            # extract and set up dataset
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)

            # set up preprocessor and matcher for dataset
            self.interface.set_dyn_reconfigure_parameters('matcher',
                                                          dataset_dict['matcher_settings'])
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                          dataset_dict['preprocessor_settings'])

            # loop over combinations of feature types
            for (features, weights_list) in self.feature_combinations:
                print('feature combination: ', features)

                for weights in weights_list:

                    print('%s: %s with weights %s' % (dataset.get_name(),
                                                      '_'.join(features),
                                                      ':'.join(str(w) for w in weights)))

                    # add not for easier visualization
                    self.interface.add_note_to_current_run('feature_types', features)

                    # set up matcher with feature types and weight
                    current_matcher_settings = generate_all_features_disabled_settings()
                    for feature, weight in zip(features, weights):
                        current_matcher_settings['match_' + feature] = True
                        current_matcher_settings[feature + '_weight'] = weight
                    self.interface.set_dyn_reconfigure_parameters('matcher',
                                                                  current_matcher_settings)

                    # loop models and scenes of current dataset
                    self.loop_models_and_scenes(dataset, re)


if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = FeatureTypesCombinationExperiment(data_storage,
                                                   SAVE_FILE_WO_EXTENSION,
                                                   dataset_dicts=DATASET_DICTS,
                                                   feature_combinations=FEATURE_COMBINATIONS)
    experiment.run(wait_before_looping=True)
