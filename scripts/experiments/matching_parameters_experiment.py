#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '/home/xaver/ROS/datasets/Mian_Bennamoun_Owens/dataset.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/matching_parameters/MBO_d_dist_rp_step'
D_DIST_VALUES = [0.025, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.125, 0.15]
RP_STEP_VALUES = [1, 2, 5, 10, 20, 40]


class MatchingParametersExperiment(ExperimentBase):
    """
    experiment matching models from datsets to themselves to find rotational symmetries
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension, d_dist_values,
                 rp_step_values):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)
        self.dataset = imp.load_source('dataset', dataset_path).get_dataset()

        self.d_dist_values = d_dist_values
        self.ref_point_step_values = rp_step_values

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points_is_relative': True
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': False,
                                       'remove_plane_first': True
                                       }

        matcher_settings = {'show_results': False,
                            'd_dist_is_relative': True,
                            'maxThresh': 0.8
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() * self.dataset.get_number_of_scenes() *\
                 len(self.d_dist_values) * len(self.ref_point_step_values)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over ref_point steps
        for rp_step in self.ref_point_step_values:

            # iterate over d_dist values
            for d_dist in self.d_dist_values:

                # set up matcher with parameter combination
                matcher_settings = {'refPointStep': rp_step,
                                    'd_dist': d_dist,
                                    'd_dist_is_relative': True}
                self.interface.set_dyn_reconfigure_parameters('/matcher', matcher_settings)

                # set up model preprocessor with parameter combination
                model_preprocessor_settings = {"d_points": d_dist,
                                               "d_points_is_relative": True}
                self.interface.set_dyn_reconfigure_parameters("/model_preprocessor",
                                                              model_preprocessor_settings)

                # iterate over models and scenes
                self.loop_models_and_scenes(self.dataset, re)


if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = MatchingParametersExperiment(data_storage,
                                              DATASET_PATH,
                                              SAVE_FILE_WO_EXTENSION,
                                              d_dist_values=D_DIST_VALUES,
                                              rp_step_values=RP_STEP_VALUES)
    experiment.run(wait_before_looping=True)
