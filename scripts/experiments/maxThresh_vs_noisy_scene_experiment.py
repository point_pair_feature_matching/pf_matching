#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
import numpy as np

# other script settings
TIME_OUT_S = 5 * 60

D_DIST = 0.05
RP_STEP = 2
MAX_THRESH_STEPS = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
NOISE_LEVELS = [0, 0.5, 1, 1.5, 2, 3, 4]  # as percent of model diameter
NOISE_LEVELS.reverse()  # start with most calculation intensive run

class MaxThreshNoisySceneExperiment(ExperimentBase):
    """
    experiment matching models from datsets to themselves to find rotational symmetries
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension, max_thresh_values,
                 noise_levels_relative, plane_removal_params):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)
        self.dataset = self.load_dataset(dataset_path)

        self.max_thresh_values = max_thresh_values
        self.noise_levels_relative = noise_levels_relative
        self.plane_removal_params = plane_removal_params

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect(scene_noisifer=True,
                                pkg_launch_file_tuples=[("pf_matching_tools",
                                                         "pipeline_noisy_as_nodes.launch")])

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,   # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': D_DIST,
                                       'd_points_is_relative': True
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       }
        # scene_preprocessor_settings.update(self.plane_removal_params)

        matcher_settings = {'show_results': False,
                            'd_dist_is_relative': True,
                            'd_dist': D_DIST,
                            'refPointStep': RP_STEP
                            }

        scene_noisifier_settings = {"noisification_method": 2}  # DEPTH NOISE

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.interface.set_dyn_reconfigure_parameters("scene_noisifier", scene_noisifier_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() * self.dataset.get_number_of_scenes() *\
                 len(self.max_thresh_values) * len(self.noise_levels_relative)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over models
        for model_index in range(self.dataset.get_number_of_models()):

            model_d_dist_abs = self.add_model(self.dataset, model_index)
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                          {'d_points': model_d_dist_abs,
                                                           'd_points_is_relative': False})
            model_diameter = model_d_dist_abs / D_DIST
            print ('model: %s' % self.dataset.get_model_name(model_index))

            # iterate over noise levels
            for noise_level in self.noise_levels_relative:

                print ('noise level (%% of object diameter): %f' % noise_level)

                # set up the noisifier with the correct amount of noise relative to the model's
                # diameter
                noise_sd = model_diameter * noise_level / 100  # convert from percent to fraction
                scene_noisifier_settings = {"noise_sd": noise_sd,
                                            "sd_is_relative": False}
                self.interface.set_dyn_reconfigure_parameters("scene_noisifier",
                                                              scene_noisifier_settings)
                self.interface.add_note_to_current_run('noise_level_relative_percent',
                                                       float(noise_level))

                #
                plane_removal_settings = self.plane_removal_params.copy()
                plane_removal_settings['plane_inlier_threshold'] =\
                    max(plane_removal_settings['plane_inlier_threshold'],
                        noise_sd * 2)
                self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                              plane_removal_settings)

                # iterate over maxThresh parameter values
                for max_thresh in self.max_thresh_values:

                    print ('maxThresh: %f' % max_thresh)

                    matcher_settings = {'maxThresh': max_thresh}
                    self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)

                    self.loop_scenes(self.dataset, re, [model_index])


if __name__ == '__main__':

    # # MBO
    # DATASET_PATH = '~/ROS/datasets/Mian_Bennamoun_Owens/dataset.py'
    # SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/maxThresh_vs_noise/MBO_maxThresh_vs_noisy_scene'
    # PLANE_REMOVAL_PARAMS = {'remove_largest_plane': False,
    #                         'plane_inlier_threshold': 0}
    #
    # data_storage = data_structure.ExperimentDataStructure()
    # experiment = MaxThreshNoisySceneExperiment(data_storage,
    #                                            DATASET_PATH,
    #                                            SAVE_FILE_WO_EXTENSION,
    #                                            max_thresh_values=MAX_THRESH_STEPS,
    #                                            noise_levels_relative=NOISE_LEVELS,
    #                                            plane_removal_params=PLANE_REMOVAL_PARAMS)
    # experiment.run(wait_before_looping=True)

    # household objects multi #####################################################
    DATASET_PATH = '~/ROS/datasets/household_objects_multi_instance/blensor_dataset.py'
    SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/maxThresh_vs_noise/HH_maxThresh_vs_noisy_scene'
    PLANE_REMOVAL_PARAMS = {'remove_largest_plane': True,
                            'remove_plane_first': True,
                            'plane_inlier_threshold': 0.005,
                            'plane_inlier_threshold_is_relative': False}

    data_storage = data_structure.ExperimentDataStructure()
    experiment = MaxThreshNoisySceneExperiment(data_storage,
                                               DATASET_PATH,
                                               SAVE_FILE_WO_EXTENSION,
                                               max_thresh_values=MAX_THRESH_STEPS,
                                               noise_levels_relative=NOISE_LEVELS,
                                               plane_removal_params=PLANE_REMOVAL_PARAMS)
    experiment.run(wait_before_looping=True)
