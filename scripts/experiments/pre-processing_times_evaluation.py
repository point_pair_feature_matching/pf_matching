#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
from math import pi
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from collections import OrderedDict

# ####################### script settings ############################################
FILE_PATH_NO_BOUNDARY = '~/ROS/experiment_data/weighting/weighting_by_frequency.bin'
FILE_PATH_WITH_BOUNDARY = '~/ROS/experiment_data/feature_types/performance_by_dataset_and_type_new.bin'
PLT_DIR = '~/ROS/experiment_data/pre-processing_times'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

DATASET_NAME_REPLACEMENTS = {'MBO': 'MBO',
                             'geometric_primitives': 'Primitives',
                             'household_objects_multi_instance': 'Household Objects'}


def extract_times(statistics_dict, parameter_ending='_time_ms'):
    """
    extract only time measurements form a node's statistics dict (i.e. everything ending on
    the parameter ending indicating a time)
    @param statistics_dict: dictionary with statistics
    @param parameter_ending: timing statistics parameters must end on this, all others will be
                             deleted
    @return: dict with only timing parameters, parameter names will have the parameter_ending removed
    """

    new_dict = {}
    for parameter_name in statistics_dict.keys():
        if parameter_name.endswith(parameter_ending):
            new_name = parameter_name[:-len(parameter_ending)]
            new_dict[new_name] = statistics_dict[parameter_name]

    return new_dict

def horizontal_stacked_bar_plot_by_dataset(ax, nested_data, dataset_name_replacements,
                                           data_name_replacements, bar_height=0.2, bar_spacing=0.4,
                                           colors=('r', 'g', 'b', 'y', 'gray', 'w')):
    """
    @param ax: axis to plot to
    @type ax: Axes
    @param nested_data: of form [(dataset_name, {data_name1: data1, data_name2: data2, ...}), (), ...]
    @param dataset_name_replacements: dict of form {old_name: new_name} that contains entries for
                                      all contained dataset names
    @param data_name_replacements: dict of form {old_str: new_str} to apply to all names of data
                                   field
    @param bar_height: height of one bar in plot units
    @param bar_spacing: vertical distance between two bars in plot units
    @param colors: colors to apply to bars
    """

    ds_names = []
    ds_positions = []

    for i, (ds_name, ds_data_dict) in enumerate(nested_data):

        previous_right = 0
        ds_names.append(dataset_name_replacements[ds_name])
        ds_positions.append(i * bar_spacing + bar_height * 0.5)

        for j, data_name in enumerate(sorted(ds_data_dict.keys())):

            width = ds_data_dict[data_name]

            for old in data_name_replacements.keys():
                data_name = data_name.replace(old, data_name_replacements[old])

            ax.barh(bottom=i * bar_spacing, width=width, height=bar_height, left=previous_right,
                    color=colors[j], label=data_name)

            previous_right += width

    ax.set_yticks(ds_positions)
    ax.set_yticklabels(ds_names)

    ax.set_ylim(top=(ds_positions[-1] + bar_spacing + bar_height))

    handles, labels = ax.get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    ax.legend(by_label.values(), by_label.keys(), ncol=3)


def plot_pre_processing_times(experiment_data, ax, which, dataset_name_replacements, plane_removal_wrong_position=False):
    """
    plot the average pre-processing times per dataset as horizontal bars

    only extracts data for first model / scene if multiple were pre-processed in a run (should
    not be the case)

    @param experiment_data: data to evaluate
    @param ax: axis to plot to
    @param which: 'model' or 'scene'
    @param dataset_name_replacements: dict of form {old_name: new_name} that contains entries for
                                      all contained dataset names
    @param plane_removal_wrong_position: if true, also consider timings that are not nested correctly
                                         in old experiment data due to a (now fixed) bug
    """

    if which == 'model':
        preprocessor_name = '/model_preprocessor'
        print('pre-processing of models:')
    elif which == 'scene':
        preprocessor_name = '/scene_preprocessor'
        print('pre-processing of scenes:')
    else:
        raise Exception('Input "%s" for parameter "which" unknown.' % which)

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [],
                                     [['statistics', preprocessor_name], ['datasetName']])

    # replace nested statistics dicts for model / scene with dict of timings only
    for i, entry in enumerate(basic_data):
        statistics = entry[iDict[preprocessor_name]]

        if plane_removal_wrong_position:
            non_nested_timings = extract_times(statistics)
        else:
            non_nested_timings = {}

        nested_timings = {}
        for x in statistics.values():
            if type(x) == dict:
                nested_timings = extract_times(x)
                break

        timings = {}
        timings.update(non_nested_timings)
        timings.update(nested_timings)

        entry_lst = list(entry)
        entry_lst[iDict[preprocessor_name]] = timings
        basic_data[i] = tuple(entry_lst)

    iDict['timings'] = iDict[preprocessor_name]

    # nest by dataset and average
    average_timings =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['timings']],
                                                   data_extraction.average_dict_entries)

    horizontal_stacked_bar_plot_by_dataset(ax, average_timings,
                                           dataset_name_replacements=dataset_name_replacements,
                                           data_name_replacements={'_': ' '})

    # print pre-processing times to console
    for ds, timings_dict in average_timings:
        print(ds)
        for name, time in timings_dict.items():
            print('%-20s: %6.2f ms' % (name, time))

    ax.set_xlabel('cumulative pre-processing time in ms')


if __name__ == '__main__':

    data_no_boundary = ExperimentDataStructure()
    data_no_boundary.from_pickle(FILE_PATH_NO_BOUNDARY)
    post_processing.compute_recognition(data_no_boundary,
                                        max_translation_error_relative,
                                        max_rotation_error)

    data_with_boundary = ExperimentDataStructure()
    data_with_boundary.from_pickle(FILE_PATH_WITH_BOUNDARY)
    post_processing.compute_recognition(data_no_boundary,
                                        max_translation_error_relative,
                                        max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # plots without boundary extraction
    print('plots without boundary extraction\n')
    fig_no_boundary_scene, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(16, 5))
    fig_no_boundary_model, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(16, 5))
    plot_pre_processing_times(data_no_boundary, fig_no_boundary_model.axes[0],
                              'model', DATASET_NAME_REPLACEMENTS, plane_removal_wrong_position=True)
    plot_pre_processing_times(data_no_boundary, fig_no_boundary_scene.axes[0],
                              'scene', DATASET_NAME_REPLACEMENTS, plane_removal_wrong_position=True)

    # plots with boundary extraction
    print('plots with boundary extraction\n')
    fig_with_boundary_scene, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(16, 4.1))
    fig_with_boundary_model, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(16, 4.1))
    plot_pre_processing_times(data_with_boundary, fig_with_boundary_model.axes[0],
                              'model', DATASET_NAME_REPLACEMENTS, plane_removal_wrong_position=True)
    plot_pre_processing_times(data_with_boundary, fig_with_boundary_scene.axes[0],
                              'scene', DATASET_NAME_REPLACEMENTS, plane_removal_wrong_position=True)

    # save figures
    if SAVE_PLOTS:
        plt_dir = PLT_DIR
        plotting.save_figure(fig_no_boundary_model, plt_dir, 'pre-process_times_models_no_boundary')
        plotting.save_figure(fig_no_boundary_scene, plt_dir, 'pre-process_times_scenes_no_boundary')
        plotting.save_figure(fig_with_boundary_model, plt_dir, 'pre-process_times_models_with_boundary')
        plotting.save_figure(fig_with_boundary_scene, plt_dir, 'pre-process_times_scenes_with_boundary')

    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
