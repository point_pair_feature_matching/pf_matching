#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase


# script settings
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/feature_types/performance_by_dataset_and_type_new'


PRIMITIVES_SETTINGS = {'matcher_settings': {'use_rotational_symmetry': True,
                                            'collapse_symmetric_models': True,
                                            'publish_equivalent_poses': True,
                                            'rs_maxThresh': 0.4,
                                            'rs_pose_weight_thresh': 0.3,
                                            'publish_n_best_poses': 2
                                            },
                       'preprocessor_settings': {'remove_largest_plane': True,
                                                 'remove_plane_first': True,
                                                 'plane_inlier_threshold': 0.006,
                                                 'plane_inlier_threshold_is_relative': False
                                                 }
                       }

# info about the dataset: path, matcher_settings, scene preprocessor settings
primitives_normal = PRIMITIVES_SETTINGS.copy()
primitives_normal.update({'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py'})
primitives_noiseless = PRIMITIVES_SETTINGS.copy()
primitives_noiseless.update({'path': '~/ROS/datasets/geometric_primitives_noiseless/blensor_dataset.py'})
primitives_low_noise = PRIMITIVES_SETTINGS.copy()
primitives_low_noise.update({'path': '~/ROS/datasets/geometric_primitives_low_noise/blensor_dataset.py'})
DATASET_DICTS = [primitives_normal,
                 # primitives_noiseless,
                 # primitives_low_noise,
                 {'path': '~/ROS/datasets/household_objects_multi_instance/blensor_dataset.py',
                  'matcher_settings': {'use_rotational_symmetry': False,
                                       'publish_n_best_poses': 2
                                       },
                  'preprocessor_settings': {'remove_largest_plane': True,
                                            'remove_plane_first': True,
                                            'plane_inlier_threshold': 0.006,
                                            'plane_inlier_threshold_is_relative': False
                                            }
                  }
                 ]

FEATURES_DICT = {'S2S': 'match_S2S',
                 'B2B': 'match_B2B',
                 'S2B': 'match_S2B',
                 'B2S': 'match_B2S'
                 }

EDGE_THRESH_LOW = 0.1
EDGE_THRESH_HIGH = 0.2


class FeatureTypesExperiment(ExperimentBase):
    """
    experiment to check the performance of different feature types
    """

    def __init__(self, data_storage, save_path_wo_extension, dataset_dicts, features_dicts):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)

        # copy dataset settings and import the datasets into the structure
        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])

        self.features_dicts = features_dicts  # name of feature, parameter to enable for matching

    def setup(self):

        # sets pipeline
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True,
                                       'extract_boundary': False,       # no discontinuity boundary for models
                                       'extract_curvature_edges': True,
                                       'curvature_lower_thresh': EDGE_THRESH_LOW,
                                       'curvature_upper_thresh': EDGE_THRESH_HIGH
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'extract_boundary': True,
                                       'extract_curvature_edges': True,
                                       'curvature_lower_thresh': EDGE_THRESH_LOW,
                                       'curvature_upper_thresh': EDGE_THRESH_HIGH
                                       }

        matcher_settings = {'show_results': False,
                            'publish_pose_cluster_weights': False,
                            'd_dist': 0.05,
                            'refPointStep': 2,
                            'd_dist_is_relative': True,
                            'maxThresh': 1.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

    def looping(self):

        # calculate number of runs and set up runtime estimator
        n_matches = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_matches += dataset.get_number_of_models() * dataset.get_number_of_scenes()

        n_data_points = len(self.features_dicts)

        n_runs = n_matches * n_data_points
        re = ros_interface.RuntimeEstimator(n_runs)

        # loop over datasets
        self.interface.new_run()
        for dataset_dict in self.dataset_dicts:

            # extract and set up dataset
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)

            print(dataset.get_name())

            # set up preprocessor and matcher for dataset
            self.interface.set_dyn_reconfigure_parameters('matcher',
                                                          dataset_dict['matcher_settings'])
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                          dataset_dict['preprocessor_settings'])

            # loop over matching of feature types
            for current_feature, current_feature_setting in self.features_dicts.items():
                print(current_feature)

                # add note for easy evaluation
                self.interface.add_note_to_current_run('feature_type', current_feature)

                # activate only a single feature
                current_matcher_settings = {}
                for setting_name in ['match_S2S', 'match_B2B', 'match_S2B', 'match_B2S']:
                    current_matcher_settings[setting_name] = False
                current_matcher_settings[current_feature_setting] = True
                self.interface.set_dyn_reconfigure_parameters('matcher', current_matcher_settings)

                # match model-scene combinations
                self.loop_models_and_scenes(dataset, re)


if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = FeatureTypesExperiment(data_storage,
                                        SAVE_FILE_WO_EXTENSION,
                                        dataset_dicts=DATASET_DICTS,
                                        features_dicts=FEATURES_DICT)
    experiment.run(wait_before_looping=True)
