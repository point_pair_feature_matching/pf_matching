#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
import time
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase

# other script settings
D_DIST = 0.05
TIME_OUT_S = 5 * 60
NOISE_LEVEL = 0.0
MAX_THRESH = 1.0
DATASET_PATHS = ['/home/xaver/ROS/datasets/Mian_Bennamoun_Owens/dataset.py',
                 '/home/xaver/ROS/datasets/household_objects_multi_instance/blensor_dataset.py',
                 '/home/xaver/ROS/datasets/geometric_primitives/blensor_dataset.py']
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/self_matching/self_matching_maxT_1_0'

FEATURES_TO_USE = {'match_S2S': True,
                   'match_B2B': False,
                   'match_S2B': False,
                   'match_B2S': False}

class SelfMatchingExperiment(ExperimentBase):
    """
    experiment matching models from datsets to themselves to find rotational symmetries
    """
    def __init__(self, data_storage, save_path_wo_extension, dataset_paths, features_to_use):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)
        self.datasets = [self.load_dataset(path) for path in dataset_paths]
        self.features_to_use = features_to_use

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect(pkg_launch_file_tuples=(('pf_matching_tools',
                                                         'pipeline_noisy_after_preprocessing.launch'),))

        # initial setup of dynamic reconfigure parameters
        preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                 'post_downsample_method': 1,  # VG
                                 'normal_estimation_method': 0,  # NONE
                                 'remove_NaNs': True,
                                 'flip_normals': False,
                                 'd_points': D_DIST,
                                 'd_points_is_relative': True}
        if(self.features_to_use['match_B2B'] or
           self.features_to_use['match_S2B'] or
           self.features_to_use['match_B2S']):
            preprocessor_settings.update({'extract_boundary': True,
                                          'extract_curvature_edges': True,
                                          'curvature_lower_thresh': 0.1,
                                          'curvature_upper_thresh': 0.2})

        matcher_settings = {'publish_pose_cluster_weights': True,
                            'd_dist': D_DIST,
                            'd_dist_is_relative': True,
                            'refPointStep': 1,  # ALWAYS FOR SYMMETRY ANALYSIS
                            'maxThresh': MAX_THRESH,
                            'publish_n_best_poses': 5,
                            'show_results': False
                            }
        matcher_settings.update(self.features_to_use)

        loader_settings = {'recenter_cloud': True}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', loader_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', loader_settings)


        self.interface.setup_dyn_reconfigure_clients(['/pc_noisification/surface_noisifier',
                                                      '/pc_noisification/edge_noisifier'])
        noisifier_settings = {'noise_sd': NOISE_LEVEL * D_DIST,
                              'sd_is_relative': True,
                              'noisification_method': 1  # ANY DIRECTION
                              }

        self.interface.set_dyn_reconfigure_parameters('/pc_noisification/surface_noisifier',
                                                      noisifier_settings)
        self.interface.set_dyn_reconfigure_parameters('/pc_noisification/edge_noisifier',
                                                      noisifier_settings)

    def looping(self):

        # calculate number of runs to perform
        n_runs = 0
        for dataset in self.datasets:
            n_runs += dataset.get_number_of_models()

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # match all the models to themselves
        for dataset in self.datasets:

            self.interface.add_note_to_current_run('datasetName', dataset.get_name())

            for model_index in range(dataset.get_number_of_models()):

                re.run_start()

                # add an construct model as model
                self.add_model(dataset, model_index)

                # add model as scene and match
                scene_loader_settings = {'frame_id': dataset.get_model_name(model_index)}
                self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_settings)
                self.interface.publish_scene(*dataset.get_model_name_and_path(model_index))
                self.interface.wait_for_statistics('matcher', 'matched_models',
                                                   time_out_time=TIME_OUT_S)
                # wrap up the run
                time.sleep(1)  # wait for remaining statistics messages to arrive
                re.run_end()
                self.interface.new_run()

    def end(self):
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        print ('Binary saving done.')
        self.data.to_yaml(self.save_path_wo_extension + '.yaml')
        print ('YAML-saving done.')


if __name__ == '__main__':
    # structure for data_storage saving
    data_storage = data_structure.ExperimentDataStructure()
    experiment = SelfMatchingExperiment(data_storage, SAVE_FILE_WO_EXTENSION, DATASET_PATHS, FEATURES_TO_USE)
    experiment.run(wait_before_looping=True)
