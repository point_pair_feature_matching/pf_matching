#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.evaluation import reusable_plots

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import copy
import numpy as np

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/feature_types/performance_by_dataset_and_type_new.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad




def plot_recog_rates(data, ax, dataset_name, poses='first', show_legend=True):
    """
    plot the recognition rates per matching mode and model as bar-plots
    @type data: ExperimentDataStructure
    @param data: experiment tmp_data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    @param poses: 'first' to only plot recognition rate of first cluster or 'all' to calculate
                  recognition rate for all returned clusters
    """

    if poses == 'first':
        eval_function = data_extraction.recognition_rate_best_poses
        y_label = 'recognition rate (1 instance)'
    elif poses == 'all':
        eval_function = data_extraction.recog_rate_total
        y_label = 'recognition rate (2 instance)'
    else:
        raise Exception('poses-parameter must be "first" or "all", got "%s".' % poses)

    # extract tmp_data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['datasetName'],
                                      ['notes', 'feature_type']])

    relevant_data =\
        [entry for entry in basic_data if entry[iDict['datasetName']] == dataset_name]

    # print overall recognition rates
    print('total recognition rates %s pose(s) on %s' % (poses, dataset_name))
    rr_first_poses_per_mode =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['recognition']],
                                                   eval_function)
    for feature, rr in rr_first_poses_per_mode:
        print('feature: %s, recognition rate %s pose cluster(s): %f' % (feature, poses, rr))

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   eval_function)

    reusable_plots.bar_by_group(rr_first_poses, ax, 'feature types', show_legend=show_legend)
    ax.set_ylabel(y_label)
    ax.set_ylim(0, 1)
    #ax.set_title(dataset_name)

    if show_legend:
        ax.legend(loc='lower right')


def plot_recog_rate_as_matrix(data, dataset_name):
    """
    plot matrices that shows for each model in which scenes it was recognized for which feature types
    (note: currently shows all-red instead of all green if the model was always recognized)

    @param data: experiment data structure to evaluate
    @param dataset_name: name of the dataset to evaluate, must exist in experiment data
    @return: figure with the matrix
    """

    # extract tmp_data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['datasetName'],
                                      ['notes', 'feature_type']])

    relevant_data =\
        [entry for entry in basic_data if entry[iDict['datasetName']] == dataset_name]

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['model'],
                                                    iDict['feature_type'],
                                                    iDict['scene'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    # assemble matrix for plotting
    fig, axes = plt.subplots(len(rr_first_poses), 1, figsize=plotting.cm2inch(16, 16))

    for i, (model_name, model_data) in enumerate(rr_first_poses):
        ax = fig.axes[i]
        n_features = len(model_data)
        n_scenes = len(model_data[0][1])
        matrix = np.zeros(shape=(n_features, n_scenes))

        feature_names = []

        for j, (feature_name, feature_data) in enumerate(model_data):
            feature_names.append(feature_name)
            for k, (scene_name, recog_rate) in enumerate(feature_data):
                matrix[j, k] = recog_rate

        ax.imshow(matrix, interpolation='nearest', cmap=plt.get_cmap('RdYlGn'))
        ax.set_xlabel('scene')
        ax.set_ylabel('feature')
        ax.set_yticks(np.arange(0,len(feature_names)))
        ax.set_yticklabels(list(feature_names))
        ax.set_title(model_name)

    return fig

def plot_average_matching_time(data, ax, dataset_name, show_legend=True, y_top_lim=None):
    """
    plot the average matching time per model and feature type as bar-plots
    @type data: ExperimentDataStructure
    @param data the experiment data
    @type ax: Axes
    @param ax: axis to plot to
    @param dataset_name: name of the dataset to evaluate
    @param show_legend: only display legend if this parameter is true
    @param y_top_lim: explicitly set the max value of the y-axis or set to None for automatic setting
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['datasetName'],
                                      ['notes', 'feature_type']])

    relevant_data =\
        [entry for entry in basic_data if entry[iDict['datasetName']] == dataset_name]

    average_match_time_per_mode =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per feature type for dataset %s' % dataset_name)
    for feature_type, match_time in average_match_time_per_mode:
        print('feature: %s, time: %f ms' % (feature_type, match_time))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    reusable_plots.bar_by_group(average_match_time_per_model, ax, 'feature types', show_legend=show_legend)
    ax.set_ylim(top=y_top_lim)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')

    #ax.set_title(dataset_name)
    if show_legend:
       ax.legend(ncol=2, loc='center left')


def plot_average_weight_best_pose_cluster(data, ax, dataset_name, show_legend=True):
    """
    plot the average weight of the highest weight pose clusters per model and feature type as bar-plots
    @type data: ExperimentDataStructure
    @param data the experiment data
    @type ax: Axes
    @param ax: axis to plot to
    @param dataset_name: name of the dataset to evaluate
    @param show_legend: only display legend if this parameter is true
    """


    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['datasetName'],
                                      ['notes', 'feature_type']])

    relevant_data = \
        [entry for entry in basic_data if entry[iDict['datasetName']] == dataset_name]

    average_weight_per_feature_type =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['poses']],
                                                   data_extraction.average_weight_best_pose_cluster)

    print('average weight of best pose cluster per feature for dataset %s' % dataset_name)
    for feature_type, weight in average_weight_per_feature_type:
        print('feature: %s, time: %d' % (feature_type, weight))

    average_weight_per_model =\
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['feature_type'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_weight_best_pose_cluster)

    reusable_plots.bar_by_group(average_weight_per_model, ax, 'feature types', show_legend=show_legend)
    ax.set_ylabel('best cluster average weight')
    # ax.set_title(dataset_name)


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    figures = []
    dataset_names = data_extraction.get_unique_run_data(data, ['datasetName'])

    primitives_name = 'geometric_primitives'
    HH_objects_name = 'household_objects_multi_instance'

    # do the plotting
    fig, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_recog_rates(data, fig.axes[0], primitives_name, poses='first', show_legend=False)
    plot_recog_rates(data, fig.axes[1], HH_objects_name, poses='first')

    fig0, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_recog_rates(data, fig0.axes[0], primitives_name, poses='all', show_legend=False)
    plot_recog_rates(data, fig0.axes[1], HH_objects_name, poses='all')

    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    plot_average_matching_time(data, fig1.axes[0], primitives_name, show_legend=False)
    plot_average_matching_time(data, fig1.axes[1], HH_objects_name, y_top_lim=4000)

    fig2, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_average_weight_best_pose_cluster(data, fig2.axes[0], primitives_name, show_legend=False)
    plot_average_weight_best_pose_cluster(data, fig2.axes[1], HH_objects_name)

    matrices = []
    dataset_names = data_extraction.get_unique_run_data(data, ['datasetName'])
    for dataset_name in dataset_names:
        matrices.append(plot_recog_rate_as_matrix(data, dataset_name))

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig,  plt_dir, 'feature_types_recog_rates_best_pose')
        plotting.save_figure(fig0, plt_dir, 'feature_types_recog_rates_best_2_poses')
        plotting.save_figure(fig1, plt_dir, 'feature_types_matching_times')
        plotting.save_figure(fig2, plt_dir, 'feature_types_average_weight_best_cluster')

    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
