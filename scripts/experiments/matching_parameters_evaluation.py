#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/matching_parameters/MBO_d_dist_rp_step.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def myround(x, prec=1, base=2.5):
    """
    rounds x in steps of base with a precision of prec
    """
    return round(base * round(float(x) / base), prec)


def plot_recog_rate_over_d_dist(experiment_data, ax):
    """
    plot the recognition rate over the d_dist value for different values of ref_point_step
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-object the function plotted in
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['recognition']],
                                     [["dynamicParameters", "/matcher", "d_dist"],
                                      ["dynamicParameters", "/matcher", "refPointStep"]])

    rr_per_rp_step_per_dd = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['refPointStep'],
                                                    iDict['d_dist'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # set up plot styles
    ps = plotting.PlotStyle(len(rr_per_rp_step_per_dd),
                            additional_plot_settings={"markerfacecolor": "None"})

    # plot one graph per reference point step
    for rp_step, plot_data in rr_per_rp_step_per_dd:
        ax.plot(*zip(*plot_data), label=rp_step, **ps.next())

    # labels etc.
    ax.legend(loc="upper right", title=r"$s_{rp}$")
    ax.set_xlabel(r"$d_{dist}$ / $diam(M)$")
    ax.set_ylabel("recognition rate (1 instance)")

    ax.grid()
    ax.set_ylim(0, 1.05)

    return ax


def plot_recog_rate_over_doo(experiment_data, ax):
    """
    plot the recognition rate over the degree of occlusion
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-object the function plotted in
    """

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['recognition'], ["degreeOfOcclusion"]],
                                     [["dynamicParameters", "/matcher", "d_dist"],
                                      ["dynamicParameters", "/matcher", "refPointStep"]])

    # determine which parameter combinations to plot (tuples of d_dist and ref point step)
    graph_parameters = [(0.025, 1), (0.05, 2), (0.05, 5), (0.05, 10)]

    # set up plot styles
    ps = plotting.PlotStyle(len(graph_parameters),
                            additional_plot_settings={"markerfacecolor": "None"})

    # plot one graph for each parameter combination
    for d_dist, rp_step in graph_parameters:

        # relevant_data = [entry for entry in basic_data if entry[iDict['d_dist']] == d_dist and
        #                 entry[iDict['refPointStep']] == rp_step]

        relevant_data = []
        for entry in basic_data:
            if entry[iDict['d_dist']] == d_dist and entry[iDict['refPointStep']] == rp_step:
                entry_as_list = list(entry)
                try:
                    # extract doo from dictionary and round in 2.5 steps
                    entry_as_list[iDict['degreeOfOcclusion']] = \
                        myround(entry_as_list[iDict['degreeOfOcclusion']][0], prec=1, base=2.5)

                    relevant_data.append(tuple(entry_as_list))
                except KeyError:
                    pass

        rr_per_doo = \
            data_extraction.nest_and_process_by_values(relevant_data,
                                                       [iDict['degreeOfOcclusion'],
                                                        iDict['recognition']],
                                                       data_extraction.recognition_rate_best_poses)

        ax.plot(*zip(*rr_per_doo), label="$s_{rp}$ %2d, $d_{dist}$ %4.3f" % (rp_step, d_dist),
                **ps.next())

    ax.legend(loc="lower left", title="parameters")
    ax.set_xlabel("degree of occlusion in %")
    ax.set_ylabel("recognition rate (1 instance)")

    ax.set_ylim(0, 1.05)
    ax.grid()

    return ax


def plot_matching_time_over_scene_points(experiment_data, ax):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-object the function plotted in
    """

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [["dynamicParameters", "/matcher", "refPointStep"],
                                      ['statistics', '/scene_preprocessor']])

    values = [(entry[iDict['refPointStep']],
               myround(
                   entry[iDict['/scene_preprocessor']][entry[iDict['scene']]]['n_points_surface'],
                   prec=0, base=200),
               entry[iDict['/matcher']]['match_time_ms'])
              for entry in basic_data]
    match_time_over_n_points_per_rp_step = \
        data_extraction.nest_and_process_by_values(values,
                                                   [0, 1, 2], np.mean)

    ps = plotting.PlotStyle(len(match_time_over_n_points_per_rp_step),
                            additional_plot_settings={"markerfacecolor": "None"})
    for rp_step, plot_data in match_time_over_n_points_per_rp_step:
        ax.semilogy(*zip(*plot_data), label=rp_step, **ps.next())

    ax.set_xlabel('number of scene points')
    ax.set_ylabel('average matching time in ms')
    ax.grid(which='both')
    ax.legend(loc='lower right', title='$s_{rp}$')
    ax.set_ylim(1, 2 * 10 ** 5)

    return ax


def plot_matching_time_over_d_dist(experiment_data, ax):
    """
    plot the time required for matching over the d_dist values for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-object the function plotted in
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [["dynamicParameters", "/matcher", "refPointStep"],
                                      ["dynamicParameters", "/matcher", "d_dist"]])

    average_match_time_per_rp_step_per_dd = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['refPointStep'],
                                                    iDict['d_dist'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    ps = plotting.PlotStyle(len(average_match_time_per_rp_step_per_dd),
                            additional_plot_settings={"markerfacecolor": "None"})
    for rp_data in average_match_time_per_rp_step_per_dd:
        ax.semilogy(*zip(*rp_data[1]), label=rp_data[0], **ps.next())

    ax.grid(which='both')
    ax.legend(loc="upper right", title="$s_{rp}$")
    ax.set_xlabel("$d_{dist}$ / $diam(M)$")
    ax.set_ylabel("average matching time in ms")
    ax.set_ylim(1, 2 * 10 ** 5)

    return ax


def plot_precisions_of_recognized_poses(experiment_data, ax_distance, ax_angle):
    """
    plot the remaining errors of recognized poses
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [["dynamicParameters", "/matcher", "d_dist"],
                                      ["dynamicParameters", "/matcher", "refPointStep"]])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose in entry[iDict['poses']].values():
            if pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    average_distance_error_per_d_dist_per_rp_step = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['refPointStep'],
                                                    iDict['d_dist'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)

    average_angle_error_per_d_dist_per_rp_step = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['refPointStep'],
                                                    iDict['d_dist'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    # plot distance errors
    ps = plotting.PlotStyle(len(average_distance_error_per_d_dist_per_rp_step),
                            additional_plot_settings={"markerfacecolor": "None"})
    for rp_step, plot_data in average_distance_error_per_d_dist_per_rp_step:
        ax_distance.plot(*zip(*plot_data), label=rp_step, **ps.next())
    ax_distance.set_xlabel('$d_{dist}$ / $diam(M)$')
    ax_distance.set_ylabel('translation error / $diam(M)$')
    ax_distance.grid()
    ax_distance.legend(loc="upper left", title="$s_{rp}$")

    # plot angle errors
    ps = plotting.PlotStyle(len(average_angle_error_per_d_dist_per_rp_step),
                            additional_plot_settings={"markerfacecolor": "None"})
    for rp_step, plot_data in average_angle_error_per_d_dist_per_rp_step:
        ax_angle.plot(*zip(*plot_data), label=rp_step, **ps.next())
    ax_angle.set_xlabel('$d_{dist}$ / $diam(M)$')
    ax_angle.set_ylabel('orientation error in rad')
    ax_angle.grid()
    ax_angle.legend(loc="lower right", title="$s_{rp}$", ncol=2)


if __name__ == '__main__':

    # get / prepare data
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # recognition rates
    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    plot_recog_rate_over_d_dist(data, fig1.axes[0])
    plot_recog_rate_over_doo(data, fig1.axes[1])

    # matching times
    fig2, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    plot_matching_time_over_scene_points(data, fig2.axes[0])
    plot_matching_time_over_d_dist(data, fig2.axes[1])

    # pose precisions
    fig3, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    plot_precisions_of_recognized_poses(data, fig3.axes[0], fig3.axes[1])

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig1, plt_dir, 'recog_rates')
        plotting.save_figure(fig2, plt_dir, 'matching_times')
        plotting.save_figure(fig3, plt_dir, 'precision')
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
