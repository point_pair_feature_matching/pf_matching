#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '/home/xaver/ROS/datasets/Mian_Bennamoun_Owens/dataset.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/d_points/d_points_variation'
D_DIST = 0.05
RP_STEP_VALUES = [1, 2, 5, 10]
D_POINTS_VALUES_RELATIVE = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.3, 1.5, 2.0, 2.5]
MAX_THRESH = 1.0


class IndependentPreprocessingExperiment(ExperimentBase):
    """
    experiment varying the sub-sampling distance during pre-processing independent of d_dist
    parameter of the model
    """

    def __init__(self, data_storage, dataset_path, save_path_wo_extension, rp_step_values,
                 d_points_values_relative):

        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)
        self.dataset = imp.load_source('dataset', dataset_path).get_dataset()

        self.ref_point_step_values = rp_step_values
        self.d_points_values_relative = d_points_values_relative

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points_is_relative': True,
                                       "d_points": D_DIST,
                                       "d_points_is_relative": True
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': False,
                                       'remove_plane_first': True
                                       }

        matcher_settings = {'show_results': False,
                            'maxThresh': MAX_THRESH,
                            'd_dist': D_DIST,
                            'd_dist_is_relative': True
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() * self.dataset.get_number_of_scenes() * \
                 len(self.ref_point_step_values) * len(self.d_points_values_relative)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over models, use one at a time
        for model_index in range(self.dataset.get_number_of_models()):
            model_d_dist_abs = self.add_model(self.dataset, model_index)

            # iterate over d_point settings
            for d_points_relative in self.d_points_values_relative:

                scene_preprocessor_settings = {'d_points': model_d_dist_abs * d_points_relative,
                                               'd_points_is_relative': False}
                self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                              scene_preprocessor_settings)
                self.interface.add_note_to_current_run('d_points_relative', d_points_relative)

                # iterate over ref_point steps
                for rp_step in self.ref_point_step_values:
                    self.interface.set_dyn_reconfigure_parameters('/matcher',
                                                                  {'refPointStep': rp_step})

                    self.loop_scenes(self.dataset, re, [model_index], scene_indices=None)


if __name__ == '__main__':
    data_storage = data_structure.ExperimentDataStructure()
    experiment = IndependentPreprocessingExperiment(data_storage,
                                                    DATASET_PATH,
                                                    SAVE_FILE_WO_EXTENSION,
                                                    rp_step_values=RP_STEP_VALUES,
                                                    d_points_values_relative=
                                                        D_POINTS_VALUES_RELATIVE)
    experiment.run(wait_before_looping=True)
