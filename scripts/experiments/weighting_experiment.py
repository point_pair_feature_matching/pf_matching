#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase


# script settings
# TODO: change
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/weighting/weighting_by_frequency'

# DATASET_PATH = '/home/xaver/ROS/datasets/household_objects_multi_instance/blensor_dataset.py'
# SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/weighting_parameter_variations_HH_multi'
#
# REMOVE_PLANE = True
# N_POSES = 2

# info about the dataset: path, matcher_settings, scene preprocessor settings
DATASET_DICTS = [{'path': '/home/xaver/ROS/datasets/Mian_Bennamoun_Owens/dataset.py',
                  'matcher_settings': {'use_rotational_symmetry': False,
                                       'publish_n_best_poses': 1
                                       },
                  'preprocessor_settings': {'remove_largest_plane': False,
                                            }
                  },
                 {'path': '~/ROS/datasets/household_objects_multi_instance/blensor_dataset.py',
                  'matcher_settings': {'use_rotational_symmetry': False,
                                       'publish_n_best_poses': 2
                                       },
                  'preprocessor_settings': {'remove_largest_plane': True,
                                            'remove_plane_first': True,
                                            'plane_inlier_threshold': 0.006,
                                            'plane_inlier_threshold_is_relative': False
                                            }
                  },
                 {'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py',
                  'matcher_settings': {'use_rotational_symmetry': True,
                                       'collapse_symmetric_models': True,
                                       'publish_equivalent_poses': True,
                                       'rs_maxThresh': 0.4,
                                       'rs_pose_weight_thresh': 0.3,
                                       'publish_n_best_poses': 2
                                       },
                  'preprocessor_settings': {'remove_largest_plane': True,
                                            'remove_plane_first': True,
                                            'plane_inlier_threshold': 0.006,
                                            'plane_inlier_threshold_is_relative': False
                                            }
                 }
                ]

# weighting by feature distance
# WEIGHTING_DICTS = [{'strategy': 'discard_features_by_distance',
#                     'parameter_name': 'distance_discarding_thresh',
#                     'parameter_values': [0, 0.05, 0.1, 0.15, 0.2, 0.3, 0.4]
#                     },
#                    {'strategy': 'weight_features_by_distance',
#                     'parameter_name': 'distance_weighting_power',
#                     'parameter_values': [0, 0.5, 1, 1.5]
#                     }]

#  weighting by feature frequency
WEIGHTING_DICTS = [{'strategy': 'weight_features_by_frequency',
                    'parameter_name': 'frequency_weighting_power',
                    'parameter_values': [0, 0.2, 0.4, 0.5, 0.6, 0.8,  1]
                    },
                   {'strategy': 'discard_features_by_frequency',
                    'parameter_name': 'frequency_discarding_remaining_ratio',
                    'parameter_values': [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2]
                    }
                   ]





class SeparateWeightsExperiment(ExperimentBase):
    """
    experiment for checking recognition for different weighting strategies separately on different
    datasets
    """
    def __init__(self, data_storage, save_path_wo_extension, dataset_dicts, weighting_dicts):
        # TODO: doc
        ExperimentBase.__init__(self, data_storage, save_path_wo_extension)

        # copy dataset settings and import the datasets into the structure
        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])

        self.weighting_dicts = weighting_dicts

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       }

        matcher_settings = {'show_results': False,
                            'publish_pose_cluster_weights': False,
                            'd_dist': 0.05,
                            'refPointStep': 2,
                            'd_dist_is_relative': True,
                            'maxThresh': 1.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)



    def looping(self):

        # calculate number of runs and set up runtime estimator
        n_matches = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_matches += dataset.get_number_of_models() * dataset.get_number_of_scenes()

        n_data_points = 0
        for weighting_dict in self.weighting_dicts:
            n_data_points += len(weighting_dict['parameter_values'])

        n_runs = n_matches * n_data_points
        re = ros_interface.RuntimeEstimator(n_runs)

        # loop over datasets
        self.interface.new_run()
        for dataset_dict in self.dataset_dicts:

            # extract and set up dataset
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)

            # set up preprocessor and matcher for dataset
            self.interface.set_dyn_reconfigure_parameters('matcher',
                                                          dataset_dict['matcher_settings'])
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                          dataset_dict['preprocessor_settings'])

            # loop over weighting strategies
            for weighting_dict in self.weighting_dicts:

                # add note for easier evaluation
                self.interface.add_note_to_current_run('weighting_strategy',
                                                       weighting_dict['strategy'])

                # loop over parameter settings
                for parameter_value in weighting_dict['parameter_values']:

                    print('strategy: %s, parameter: %s, value: %f' %
                          (weighting_dict['strategy'],
                           weighting_dict['parameter_name'],
                           parameter_value
                           ))

                    # set weighting parameters in matcher
                    weighting_settings = {'discard_features_by_frequency': False,
                                          'discard_features_by_distance': False,
                                          'weight_features_by_distance': False,
                                          'weight_features_by_frequency': False}
                    weighting_settings[weighting_dict['strategy']] = True
                    weighting_settings[weighting_dict['parameter_name']] = parameter_value
                    self.interface.set_dyn_reconfigure_parameters('matcher',
                                                                  weighting_settings)

                    # match all models of the dataset
                    self.loop_models_and_scenes(dataset, re, model_indices=None, scene_indices=None) # TODO: change back to all



if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = SeparateWeightsExperiment(data_storage,
                                           SAVE_FILE_WO_EXTENSION,
                                           dataset_dicts=DATASET_DICTS,
                                           weighting_dicts=WEIGHTING_DICTS)
    experiment.run(wait_before_looping=True)
