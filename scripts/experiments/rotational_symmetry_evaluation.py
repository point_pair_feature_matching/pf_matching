#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/rotational_symmetry/welzl_bounding_sphere/rotational_symmetry.bin'
#FILE_PATH = '~/ROS/experiment_data/rotational_symmetry/epos6_bounding_sphere/my_version_of_code/rotational_symmetry_CMPH.bin'
#FILE_PATH = '~/ROS/experiment_data/rotational_symmetry/epos6_bounding_sphere/xavers_original_version_of_code/rotational_symmetry.bin'

SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']

            matching_mode = None
            if used_rs and collapsed_points:
                matching_mode = 'rs reduced'
            elif used_rs and not collapsed_points:
                matching_mode = 'rs'
            elif not used_rs:
                matching_mode = 'original'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n')):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width = 0.2
    colors = ['#c62828', '#33691e', '#1a237e', '#ffb300'] #['r', 'g', 'b', 'k', 'c']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        plt_tuples = [(group_dict[entry[0]] + i * bar_width, entry[1]) for entry in group_data]

        ax.bar(*zip(*plt_tuples), width=bar_width, color=colors[i], label=bar_names, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    ax.set_xticks(group_loc + n_bars / 2 * bar_width)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.tick_params(axis='x', top=False)
    ax.set_xticklabels(group_names, fontsize=7)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_average_matching_time(data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_match_time_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per matching mode')
    for mode, match_time in average_match_time_per_mode:
        print('mode: %s, time: %f ms' % (mode, match_time))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    bar_by_model(average_match_time_per_model, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylim(0, 80000)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')


def plot_recog_rates_first_pose(data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # print overall recognition rates
    rr_first_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    for mode, rr in rr_first_poses_per_mode:
        print('mode: %s, recognition rate best pose cluster: %f' % (mode, rr))

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    bar_by_model(rr_first_poses, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylabel('recognition rate (1 instance)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='lower right')



def plot_recog_rates_all_poses(data, ax, show_legend=True):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment tmp_data for plotting
    @type ax: Axes
    @param ax: axis to plot recognition rate for all returned poses to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate for all models
    recog_rate_all_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    bar_by_model(recog_rate_all_poses, ax, 'matching modes')
    ax.set_ylabel('recognition rate (2 instances)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='lower right')

    # print overall recognition rates
    rr_first_2_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    for mode, rr in rr_first_2_poses_per_mode:
        print('mode: %s, recognition rate best 2 pose cluster: %f' % (mode, rr))


def plot_precisions_of_recognized_poses(experiment_data, ax_distance, ax_angle):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose in entry[iDict['poses']].values():
            if pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # plot distance error
    average_distance_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    bar_by_model(average_distance_error, ax_distance, 'matching modes')
    ax_distance.set_ylabel('distance error / model diameter')

    # plot angle error
    average_angle_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    bar_by_model(average_angle_error, ax_angle, 'matching modes')
    ax_angle.set_ylabel('angle error in rad')


if __name__ == '__main__':

    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH) 
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    add_matching_mode(data)


    # plot_average_matching_time(tmp_data, None)

    # plot
    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    plot_recog_rates_first_pose(data, fig1.axes[0], show_legend=False)
    plot_recog_rates_all_poses(data, fig1.axes[1])
    fig1.tight_layout()

    fig2, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(9, 6))
    plot_average_matching_time(data, fig2.axes[0], show_legend=True)
    fig2.tight_layout()

    fig3, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    plot_precisions_of_recognized_poses(data, *fig3.axes)
    fig3.tight_layout()

    fig4, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    plot_recog_rates_first_pose(data, fig4.axes[1], show_legend=True)
    plot_average_matching_time(data, fig4.axes[0], show_legend=False)
    fig4.tight_layout()

    fig5, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(9, 6))
    plot_recog_rates_all_poses(data, fig5.axes[0])
    fig5.tight_layout()

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig1, plt_dir, 'rot_sym_recognition_rates')
        plotting.save_figure(fig2, plt_dir, 'rot_sym_matching_times')
        plotting.save_figure(fig3, plt_dir, 'rot_sym_precisions')
        plotting.save_figure(fig4, plt_dir, 'rot_sym_matching_time_recog_rate')
        plotting.save_figure(fig5, plt_dir, 'rot_sym_recognition_rates_all_poses')
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
