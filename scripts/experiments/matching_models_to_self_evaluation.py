#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# script settings
FILE_PATH_LEFT = '~/ROS/experiment_data/self_matching/self_matching_maxT_1_0.bin'
FILE_PATH_RIGHT = '~/ROS/experiment_data/self_matching/self_matching_maxT_0_4.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
THRESHOLD = 0.3
N_CLUSTERS = 25

DATASET_DICT = {'MBO': 'MBO',
                'geometric_primitives': 'Primitives',
                'household_objects_multi_instance': 'Household Objects'}


def plot_pose_cluster_weights(experiment_data, axes, n, threshold, add_legend, add_ylabels,
                              dataset_dict=DATASET_DICT):
    """
    plot the weights of the first n pose clusters, each dataset is plotted in one axis

    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment left_data
    @type axes: list of Axes
    @param axes: axes to plot to
    @param n: number of pose clusters to plot
    @param threshold: draw a threshold line at this x-value or no line if None is given
    @param add_legend: if True, add a legend with dataset name and model names
    @param add_ylabels: if True, add y-axis labels to plots
    @param dataset_dict: dict of form {old: new} to replace dataset names in the legend, must
                         must contain all dataset names as keys
    @return: ax-object the function plotted in
    """

    # extract relevant left_data
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/matcher']],
                                                     [['notes', 'datasetName']])
    iDict['matcherStatistics'] = iDict.pop('/matcher')

    # extract maxThresh levels
    maxThreshLevels = \
        data_extraction.get_unique_run_data(experiment_data,
                                            ['dynamicParameters', '/matcher', 'maxThresh'],
                                            copy_missing_from_previous=True)
    assert len(maxThreshLevels) == 1, 'found more than one maxThresh-value in dataset'
    maxThreshLevel = maxThreshLevels[0]

    # extract dataset names and check
    dataset_names = data_extraction.get_unique_run_data(experiment_data, ['notes', 'datasetName'])
    assert len(dataset_names) == len(axes), 'Number of datasets does not match number of axes' \
                                            ' for plotting'

    # nest left_data for plotting
    def get_first_dict_pose_cluster_weights(dicts):
        return dicts[0]['pose_cluster_weights']
    pcws_by_dataset_by_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['matcherStatistics']],
                                                   get_first_dict_pose_cluster_weights)

    # plot all
    for i, (dataset_name, model_data) in enumerate(pcws_by_dataset_by_model):

        ax = axes[i]

        # set up plot styles
        # ps = plotting.PlotStyle(len(model_data),
        #                         additional_plot_settings={"markerfacecolor": "None"})

        # plotting
        for model_name, pcws in model_data:
            pcw = np.array(pcws)
            pcw /= pcw[0]
            ax.plot(pcw, label=model_name, marker='x')  # **ps.next()

        # labels, titles etc.
        ax.set_xlim(0, n)
        ax.set_ylim(0, 1.05)

        if add_ylabels:
            ax.set_ylabel('normalized weight')
        ax.grid()

        if add_legend:
            ax.legend(title=dataset_dict[dataset_name])

        if threshold is not None:
            ax.plot([0, n], [threshold, threshold], 'r--', linewidth=1)

    # add column header and labels
    axes[0].set_title(r'$t_{max} = %1.2f$' % maxThreshLevel)
    axes[-1].set_xlabel('pose clusters')


if __name__ == '__main__':

    # get / prepare data
    left_data = ExperimentDataStructure()
    left_data.from_pickle(FILE_PATH_LEFT)

    right_data = ExperimentDataStructure()
    right_data.from_pickle(FILE_PATH_RIGHT)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # plot all
    fig, axes = plt.subplots(nrows=3, ncols=2, sharex=True, sharey=True,
                             figsize=plotting.cm2inch(16, 12))
    plot_pose_cluster_weights(left_data, axes[:, 0], N_CLUSTERS, None,
                              add_legend=True, add_ylabels=True)
    plot_pose_cluster_weights(right_data, axes[:, 1], N_CLUSTERS, THRESHOLD,
                              add_legend=False, add_ylabels=False)

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_LEFT)
        plotting.save_figure(fig, plt_dir, 'symmetries_tmax_variation')
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
