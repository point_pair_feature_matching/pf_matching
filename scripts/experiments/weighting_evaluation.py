#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import copy
import numpy as np

# ####################### script settings ############################################
FREQUENCY_FILE_PATH = '~/ROS/experiment_data/weighting/weighting_by_frequency.bin'
DISTANCE_FILE_PATH = '~/ROS/experiment_data/weighting/weighting_by_distance.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

dataset_name_replacements = {'MBO': 'MBO',
                             'geometric_primitives': 'Primitives',
                             'household_objects_multi_instance': 'Household Objects'}

strategy_param_list = [('weight_features_by_frequency', 'frequency_weighting_power'),
                       ('discard_features_by_frequency', 'frequency_discarding_remaining_ratio'),
                       ('discard_features_by_distance', 'distance_discarding_thresh'),
                       ('weight_features_by_distance', 'distance_weighting_power')]

PRECISION_LIMITS_DISTANCE = (0, 0.05)
PRECISION_LIMITS_ANGLE = (0, 0.1)


def make_single_numeric_ticklabel_bold(ax, axis, value):
    """
    does nothing if the value is not one of the tick labels
    @param ax: matplotlib Axes object to modify
    @param axis: axis for the tick labels, must be 'x' or 'y'
    @param value: the value to print bold
    """

    # draw the figure to set labels (otherwise text will be empty)
    ax.figure.canvas.draw()

    # get labels from specified axis
    if axis == 'x':
        labels = ax.get_xticklabels()
    elif axis == 'y':
        labels = ax.get_yticklabels()
    else:
        raise Exception('axis not defined, must be "x" or "y"')

    # find the label and make it bold
    for label in labels:
        if float(label.get_text()) == value:
            label.update({'weight': 'heavy'})
            break


def plot_recog_rate_over_weighting_parameter(data, ax, strategy_name, parameter_name, show_legend,
                                             legend_replacement_dict=dataset_name_replacements):
    """
    plot the recognition rate by dataset over a specified weighting parameter
    @type data: ExperimentDataStructure
    @type ax: Axes
    """

    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'weighting_strategy'],
                                      ['datasetName'],
                                      ['dynamicParameters', '/matcher', strategy_name],
                                      ['dynamicParameters', '/matcher', parameter_name]])

    strategy_data = [entry for entry in basic_data if entry[iDict[strategy_name]]]
    recog_rates = \
        data_extraction.nest_and_process_by_values(strategy_data,
                                                   [iDict['datasetName'],
                                                    iDict[parameter_name],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # set up plot styles
    ps = plotting.PlotStyle(len(recog_rates),
                            additional_plot_settings={"markerfacecolor": "None"})

    for dataset_name, plot_data in recog_rates:
        ax.plot(*zip(*plot_data), label=legend_replacement_dict[dataset_name], **ps.next())
    ax.grid()

    # ax.set_xlabel(parameter_name)
    # ax.set_title(strategy_name)
    if show_legend:
        ax.legend(title='datasets', ncol=2, loc='best')

    ax.set_ylabel('recognition rate (1 instance)')
    ax.set_ylim(0, 1)


def recog_rate_weight_by_frequency(data, ax, show_legend):
    plot_recog_rate_over_weighting_parameter(data, ax,
                                             strategy_name='weight_features_by_frequency',
                                             parameter_name='frequency_weighting_power',
                                             show_legend=show_legend)
    ax.set_xlabel(r'$p_f$ for $w=n_f^{-p_f}$')
    make_single_numeric_ticklabel_bold(ax, 'x', 0)


def recog_rate_discard_by_frequency(data, ax, show_legend):
    plot_recog_rate_over_weighting_parameter(data, ax,
                                             strategy_name='discard_features_by_frequency',
                                             parameter_name='frequency_discarding_remaining_ratio',
                                             show_legend=show_legend)
    ax.set_xlabel('$k_f$ (fraction of remaining features)')
    make_single_numeric_ticklabel_bold(ax, 'x', 1)


def recog_rate_weight_by_distance(data, ax, show_legend):
    plot_recog_rate_over_weighting_parameter(data, ax,
                                             strategy_name='weight_features_by_distance',
                                             parameter_name='distance_weighting_power',
                                             show_legend=show_legend)
    ax.set_xlabel(r'$p_d$ for $w=\left( \frac{\left|d\right|}{diam(M)} \right)^{p_d}$')
    make_single_numeric_ticklabel_bold(ax, 'x', 0)


def recog_rate_discard_by_distance(data, ax, show_legend):
    plot_recog_rate_over_weighting_parameter(data, ax,
                                             strategy_name='discard_features_by_distance',
                                             parameter_name='distance_discarding_thresh',
                                             show_legend=show_legend)
    ax.set_xlabel(r'$k_d$ to discard features with $\frac{\left|d\right|}{diam(M)}<k_d$')
    ax.set_xlim(0, 0.4)
    step = 0.1
    ax.xaxis.set_ticks(np.arange(ax.get_xlim()[0], ax.get_xlim()[1] + step, step=step))
    make_single_numeric_ticklabel_bold(ax, 'x', 0)


def plot_precisions_of_recognized_poses(experiment_data, ax_distance, ax_angle, strategy_name,
                                        parameter_name,
                                        legend_replacement_dict=dataset_name_replacements):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'weighting_strategy'],
                                      ['datasetName'],
                                      ['dynamicParameters', '/matcher', strategy_name],
                                      ['dynamicParameters', '/matcher', parameter_name]])

    # filter out only first poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for i, pose in entry[iDict['poses']].items():
            if pose['correct'] and i == 0:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(copy.deepcopy(entry_list)))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # plot distance error
    average_distance_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['datasetName'],
                                                    iDict[parameter_name],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)

    # set up plot styles
    ps = plotting.PlotStyle(len(average_distance_error),
                            additional_plot_settings={"markerfacecolor": "None"})

    for dataset_name, plot_data in average_distance_error:
        ax_distance.plot(*zip(*plot_data), label=legend_replacement_dict[dataset_name], **ps.next())
    ax_distance.grid()
    # ax_distance.set_xlabel(parameter_name)
    # ax_distance.set_title(strategy_name)

    ax_distance.legend(title='datasets', ncol=2, loc='upper center')

    ax_distance.set_ylim(0)
    ax_distance.set_ylabel('translation error / $diam(M)$')

    # plot angle error
    average_angle_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['datasetName'],
                                                    iDict[parameter_name],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    # set up plot styles
    ps = plotting.PlotStyle(len(average_angle_error),
                            additional_plot_settings={"markerfacecolor": "None"})

    for dataset_name, plot_data in average_angle_error:
        ax_angle.plot(*zip(*plot_data), label=legend_replacement_dict[dataset_name], **ps.next())
    ax_angle.grid()

    ax_angle.set_ylim(0)
    ax_angle.set_ylabel('orientation error in rad')


def precision_weight_by_frequency(data, ax_distance, ax_angle):
    """
    @type ax_distance: Axes
    @type ax_angle: Axes
    """
    plot_precisions_of_recognized_poses(data, ax_distance, ax_angle,
                                        strategy_name='weight_features_by_frequency',
                                        parameter_name='frequency_weighting_power')
    for ax in (ax_distance, ax_angle):
        ax.set_xlabel(r'$p_f$ for $w=n_f^{-p_f}$')
        make_single_numeric_ticklabel_bold(ax, 'x', 0)
    ax_distance.set_ylim(*PRECISION_LIMITS_DISTANCE)
    ax_angle.set_ylim(*PRECISION_LIMITS_ANGLE)


def precision_discard_by_frequency(data, ax_distance, ax_angle):
    """
    @type ax_distance: Axes
    @type ax_angle: Axes
    """
    plot_precisions_of_recognized_poses(data, ax_distance, ax_angle,
                                        strategy_name='discard_features_by_frequency',
                                        parameter_name='frequency_discarding_remaining_ratio')
    for ax in (ax_distance, ax_angle):
        ax.set_xlabel('$k_f$ (fraction of remaining features)')
        make_single_numeric_ticklabel_bold(ax, 'x', 1)
    ax_distance.set_ylim(*PRECISION_LIMITS_DISTANCE)
    ax_angle.set_ylim(*PRECISION_LIMITS_ANGLE)


def precision_weight_by_distance(data, ax_distance, ax_angle):
    """
    @type ax_distance: Axes
    @type ax_angle: Axes
    """
    plot_precisions_of_recognized_poses(data, ax_distance, ax_angle,
                                        strategy_name='weight_features_by_distance',
                                        parameter_name='distance_weighting_power')
    for ax in (ax_distance, ax_angle):
        ax.set_xlabel(r'$p_d$ for $w=\left( \frac{\left|d\right|}{diam(M)} \right)^{p_d}$')
        make_single_numeric_ticklabel_bold(ax, 'x', 0)
    ax_distance.set_ylim(*PRECISION_LIMITS_DISTANCE)
    ax_angle.set_ylim(*PRECISION_LIMITS_ANGLE)


def precision_discard_by_distance(data, ax_distance, ax_angle):
    """
    @type ax_distance: Axes
    @type ax_angle: Axes
    """
    plot_precisions_of_recognized_poses(data, ax_distance, ax_angle,
                                        strategy_name='discard_features_by_distance',
                                        parameter_name='distance_discarding_thresh')
    for ax in (ax_distance, ax_angle):
        ax.set_xlabel(r'$k_d$ to discard features with $\frac{\left|d\right|}{diam(M)}<k_d$')
        ax.set_xlim(0, 0.4)
        step = 0.1
        ax.xaxis.set_ticks(np.arange(ax.get_xlim()[0], ax.get_xlim()[1] + step, step=step))
        make_single_numeric_ticklabel_bold(ax, 'x', 0)
    ax_distance.set_ylim(*PRECISION_LIMITS_DISTANCE)
    ax_angle.set_ylim(*PRECISION_LIMITS_ANGLE)

def print_matching_times(data, strategy_name, parameter_name):


    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'weighting_strategy'],
                                      ['datasetName'],
                                      ['dynamicParameters', '/matcher', strategy_name],
                                      ['dynamicParameters', '/matcher', parameter_name]])

    strategy_data = [entry for entry in basic_data if entry[iDict[strategy_name]]]
    match_times_per_ds = \
        data_extraction.nest_and_process_by_values(strategy_data,
                                                   [iDict['datasetName'],
                                                    iDict[parameter_name],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    for dataset, match_times in match_times_per_ds:
        print(dataset)
        for p_value_time in match_times:
            print('value: %f, time: %f ms' % p_value_time)


if __name__ == '__main__':

    frequency_data = ExperimentDataStructure()
    frequency_data.from_pickle(FREQUENCY_FILE_PATH)
    post_processing.compute_recognition(frequency_data,
                                        max_translation_error_relative,
                                        max_rotation_error)

    distance_data = ExperimentDataStructure()
    distance_data.from_pickle(DISTANCE_FILE_PATH)
    post_processing.compute_recognition(distance_data,
                                        max_translation_error_relative,
                                        max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # plots for frequency
    fig_frequency_recog_rates, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    fig_frequency_weighting_prec, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    fig_frequency_discard_prec, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))

    recog_rate_weight_by_frequency(frequency_data, fig_frequency_recog_rates.axes[0], True)
    recog_rate_discard_by_frequency(frequency_data, fig_frequency_recog_rates.axes[1], False)

    precision_weight_by_frequency(frequency_data, *fig_frequency_weighting_prec.axes)
    precision_discard_by_frequency(frequency_data, *fig_frequency_discard_prec.axes)

    # plots for distance
    fig_distance_recog_rates, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    fig_distance_weighting_prec, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    fig_distance_discard_prec, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))

    recog_rate_weight_by_distance(distance_data, fig_distance_recog_rates.axes[0], True)
    recog_rate_discard_by_distance(distance_data, fig_distance_recog_rates.axes[1], False)

    precision_weight_by_distance(distance_data, *fig_distance_weighting_prec.axes)
    precision_discard_by_distance(distance_data, *fig_distance_discard_prec.axes)

    # times for discarding by distance to check speed up
    print_matching_times(distance_data, strategy_name='discard_features_by_distance',
                                        parameter_name='distance_discarding_thresh')

    # save figures
    if SAVE_PLOTS:
        pass
        plt_dir = os.path.dirname(FREQUENCY_FILE_PATH)
        # base_name, ext = os.path.splitext(os.path.basename(FREQUENCY_FILE_PATHFILE_PATH))
        plotting.save_figure(fig_frequency_recog_rates, plt_dir,
                             'weighting_frequency_recog_rates')
        plotting.save_figure(fig_frequency_weighting_prec, plt_dir,
                             'weighting_frequency_weighting_precision')
        plotting.save_figure(fig_frequency_discard_prec, plt_dir,
                             'weighting_frequency_discard_precision')

        plotting.save_figure(fig_distance_recog_rates, plt_dir,
                             'weighting_distance_recog_rates')
        plotting.save_figure(fig_distance_weighting_prec, plt_dir,
                             'weighting_distance_weighting_precision')
        plotting.save_figure(fig_distance_discard_prec, plt_dir,
                             'weighting_distance_discard_precision')
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
