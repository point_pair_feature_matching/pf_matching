#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path


# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/d_points/d_points_variation.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def plot_recog_rate_over_d_points(experiment_data, ax):
    """
    plot the recognition rate over the d_points value for different ref-point steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot to
    """

    # make sure we used exactly one d_dist setting
    d_dist = data_extraction.get_unique_run_data(experiment_data,
                                                 ["dynamicParameters", "/matcher", "d_dist"])
    assert len(d_dist) == 1, 'More than one d_dist value in experiment data, expected exactly one.'
    d_dist = d_dist[0]
    print('d_dist used is %f' % d_dist)

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['recognition']],
                                     [['notes', 'd_points_relative'],
                                      ["dynamicParameters", "/matcher", "refPointStep"]])

    # nest data for plotting
    rr_per_rp_step_per_dp =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['refPointStep'],
                                                    iDict['d_points_relative'],
                                                    iDict['recognition']
                                                    ],
                                                   data_extraction.recognition_rate_best_poses)
    # set up plot styles
    ps = plotting.PlotStyle(len(rr_per_rp_step_per_dp),
                            additional_plot_settings={"markerfacecolor": "None"})

    # do plotting
    for rp_step, plot_data in rr_per_rp_step_per_dp:
        ax.plot(*zip(*plot_data), label=rp_step, **ps.next())

    # set axes, labels etc
    ax.legend(loc="upper right", title=r"$s_{rp}$")
    ax.set_xlabel(r"$d_{points,S}$ / $d_{dist}$")
    ax.set_ylabel("recognition rate")

    ax.grid()
    ax.set_ylim(0, 1.05)

if __name__ == '__main__':

    # get / prepare data
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # recognition rates
    fig, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(12, 7))
    plot_recog_rate_over_d_points(data, fig.axes[0])

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        base_name, ext = os.path.splitext(os.path.basename(FILE_PATH))
        plotting.save_figure(fig, plt_dir, base_name)
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
