#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/maxThresh_vs_noise/MBO_maxThresh_vs_noisy_scene.bin'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def plot_recog_rate_over_d_dist(data, ax):
    """
    plot the recognition rate over the d_dist value for different values of ref_point_step
    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'noise_level_relative_percent'],
                                      ["dynamicParameters", "/matcher", "maxThresh"]])

    rr_per_nl_per_mt =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['maxThresh'],
                                                    iDict['noise_level_relative_percent'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # set up plot styles
    ps = plotting.PlotStyle(len(rr_per_nl_per_mt),
                            additional_plot_settings={"markerfacecolor": "None"})

    # plot one graph per reference point step
    for max_thresh, plot_data in rr_per_nl_per_mt:
        ax.plot(*zip(*plot_data), label=max_thresh, **ps.next())

    # labels etc.

    ax.legend(loc="lower left", title=r"$t_{max}$", ncol=2)
    ax.set_xlabel("$\sigma$ of added scene noise \n in % of model diameter")
    ax.set_ylabel("recognition rate (1 instance)")

    ax.grid()
    ax.set_ylim(0, 1.0)

    return ax


def plot_matching_time_vs_max_thresh(data, ax):
    """
    plot the average matching time for each parameter value of maxThresh
    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'noise_level_relative_percent'],
                                      ["dynamicParameters", "/matcher", "maxThresh"]])

    average_match_time_per_max_thresh =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['maxThresh'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    # set up plot styles
    ps = plotting.PlotStyle(len(average_match_time_per_max_thresh),
                            additional_plot_settings={"markerfacecolor": "None"})

    # plot time
    ax.plot(*zip(*average_match_time_per_max_thresh), **ps.next())

    ax.set_xlabel("$t_{max}$")
    ax.set_ylabel("average matching time in ms")

    ax.set_xlim(None, 1)
    #ax.set_ylim(0, 9000)

    ax.grid()

    return ax


if __name__ == '__main__':

    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # plot
    fig, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 7))
    plot_matching_time_vs_max_thresh(data, fig.axes[0])
    plot_recog_rate_over_d_dist(data, fig.axes[1])

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        base_name, ext = os.path.splitext(os.path.basename(FILE_PATH))
        plotting.save_figure(fig, plt_dir, base_name)
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
