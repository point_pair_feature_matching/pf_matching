#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.evaluation import reusable_plots

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path

# ####################### script settings ############################################
FILE_PATHS = ['~/ROS/experiment_data/feature_types_combinations/combinations_with_S2S.bin',
              '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2B.bin',
              '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2B_extended.bin',
              '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2S_extended.bin',
              '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2S_extended2.bin',
              '~/ROS/experiment_data/feature_types_combinations/combinations_with_S2S_extended3.bin']
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

dataset_name_replacements = {'MBO': 'MBO',
                             'geometric_primitives': 'Primitives',
                             'household_objects_multi_instance': 'Household Objects'}


def plot_recog_rate_for_two_feature_combination(data_lst, ax, const_weight_feature,
                                                varied_weight_feature, dataset_name_replacements,
                                                show_legend = True):
    """
    plot the recognition rates for a combination of two features over the relative weight of both
    @param data_lst: list with experiment data from multiple runs
    @type ax: Axes
    @param ax: axis to plot to
    @param const_weight_feature: name of feature who's weight should be constant in the plot
    @param varied_weight_feature: name of feature who's weight should be varied in the plot
    @param dataset_name_replacements: dict of form {old_name: new_name} that contains entries for
                                      all contained dataset names
    @param show_legend: only display legend if this parameter is true
    @return:
    """

    # generate names of settings for value retrieval
    const_weight_name = const_weight_feature + '_weight'
    varied_weight_name = varied_weight_feature + '_weight'

    # get the data
    basic_data = []
    iDict = {}
    for data in data_lst:
        iDict, part_data = \
            data_extraction.extract_data(data,
                                         [['recognition']],
                                         [['datasetName'],
                                          ['notes', 'feature_types'],
                                          ['dynamicParameters', '/matcher', const_weight_name],
                                          ['dynamicParameters', '/matcher', varied_weight_name]])
        basic_data += part_data

    # only use combination of specified feature types
    relevant_data = \
        [entry for entry in basic_data if
         set(entry[iDict['feature_types']]) == {const_weight_feature, varied_weight_feature}]
    assert len(relevant_data) > 0, 'No data for feature combination %s - %s.' %\
                                   (const_weight_feature, varied_weight_feature)

    # normalize weights
    for i, tupl in enumerate(relevant_data):
        lst = list(tupl)
        lst[iDict[varied_weight_name]] /= lst[iDict[const_weight_name]]
        relevant_data[i] = tuple(lst)

    # nest tmp_data
    recog_rates = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['datasetName'],
                                                    iDict[varied_weight_name],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # set up plot styles
    ps = plotting.PlotStyle(len(recog_rates),
                            additional_plot_settings={"markerfacecolor": "None"})

    # plot
    for dataset_name, plot_data in recog_rates:
        ax.plot(*zip(*plot_data), label=dataset_name_replacements[dataset_name], **ps.next())

    # add plot elements
    ax.set_xlabel(r'$w_{%s}/w_{%s}$' % (varied_weight_feature, const_weight_feature))
    ax.set_ylabel('recognition rate (1 instance)')

    ax.set_ylim(0.7, 1.0)
    ax.grid()

    if show_legend:
        ax.legend(loc='lower center')


def add_line(ax, y, color, linestyle='dashed'):
    """
    add a horizontal line to a plot
    @type ax: Axes
    @param ax: axis to plot to
    @param y: y-position of line
    @param color: color of line
    @param linestyle: matplotlib style of line
    """

    x_lims = ax.get_xlim()
    ax.plot(x_lims, [y, y], linestyle=linestyle, color=color)


def add_max_lines(ax):
    """
    add lines for the maximum recognition rate of the primitives and household objects dataset
    of old experiments to a plot
    @param ax: axis to plot to
    """

    # primitives dataset, blue
    add_line(ax, 0.87, color='b')

    # household objects, green
    add_line(ax, 0.96, color=(0, 1, 0))


def plot_matching_times_for_two_feature_combination(data_lst, ax, feature_pairs, dataset_name_replacements):
    """
    plot the matching times for a combination of two features over the relative weight of both
    @param data_lst: list with experiment data from multiple runs
    @type ax: Axes
    @param ax: axis to plot to
    @param feature_pairs: list of pairs with feature types, e.g. [('S2S', 'S2B'), ('S2S', 'B2B'), ...]
    @param dataset_name_replacements: dict of form {old_name: new_name} that contains entries for
                                      all contained dataset names
    """

    match_time_per_features_per_ds = []

    for feature_0, feature_1 in feature_pairs:
        # feature_0_param = feature_0 + '_weight'
        # feature_1_param = feature_1 + '_weight'

        basic_data = []
        iDict = {}
        for data in data_lst:
            iDict, part_data = \
                data_extraction.extract_data(data,
                                             [['statistics', '/matcher']],
                                             [['datasetName'],
                                              ['notes', 'feature_types']])
            basic_data += part_data

        # only use combination of specified feature types
        relevant_data = \
            [entry for entry in basic_data if
             set(entry[iDict['feature_types']]) == {feature_0, feature_1}]
        assert len(relevant_data) > 0, 'No data for feature combination %s - %s.' % \
                                       (feature_0, feature_1)

        # calculate time
        match_time = \
            data_extraction.nest_and_process_by_values(relevant_data,
                                                       [iDict['datasetName'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_match_time)

        match_time_per_features_per_ds.append(('%s-%s' % (feature_0, feature_1),
                                               match_time))

    # manually add times from old experiments
    match_time_per_features_per_ds.append(('S2S only', [('geometric_primitives', 8781),
                                                        ('household_objects_multi_instance', 4093)]))
    match_time_per_features_per_ds.append(('S2B only', [('geometric_primitives', 9712),
                                                        ('household_objects_multi_instance', 616)]))

    reusable_plots.bar_by_group(match_time_per_features_per_ds, ax, legend_title=None, bar_width=0.1,
                                colors=('r', 'g', 'b', 'k', 'y', 'gray', 'w'),
                                group_name_replacements=dataset_name_replacements.items())

    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')


if __name__ == '__main__':

    data_lst = []
    for file_path in FILE_PATHS:
        tmp_data = ExperimentDataStructure()
        tmp_data.from_pickle(file_path)
        post_processing.compute_recognition(tmp_data, max_translation_error_relative,
                                            max_rotation_error)
        data_lst.append(tmp_data)


    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    fig0, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_recog_rate_for_two_feature_combination(data_lst, fig0.axes[0], 'S2S', 'B2S', dataset_name_replacements)
    plot_recog_rate_for_two_feature_combination(data_lst, fig0.axes[1], 'S2S', 'B2B', dataset_name_replacements, show_legend=False)
    add_max_lines(fig0.axes[0])
    add_max_lines(fig0.axes[1])

    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_recog_rate_for_two_feature_combination(data_lst, fig1.axes[0], 'S2S', 'S2B', dataset_name_replacements)
    plot_matching_times_for_two_feature_combination(data_lst, fig1.axes[1], [('S2S', 'B2S'), ('S2S', 'B2B'), ('S2B', 'B2S'), ('S2B', 'B2B'), ('S2S', 'S2B')], dataset_name_replacements)
    add_max_lines(fig1.axes[0])

    fig2, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(16, 6))
    plot_recog_rate_for_two_feature_combination(data_lst, fig2.axes[0], 'S2B', 'B2S', dataset_name_replacements)
    plot_recog_rate_for_two_feature_combination(data_lst, fig2.axes[1], 'S2B', 'B2B', dataset_name_replacements, show_legend=False)
    add_max_lines(fig2.axes[0])
    add_max_lines(fig2.axes[1])


    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATHS[0])
        plotting.save_figure(fig0, plt_dir, 'recog_rates_S2S_combinations')
        plotting.save_figure(fig1, plt_dir, 'combination_times+recog_rates_S2S-S2B')
        plotting.save_figure(fig2, plt_dir, 'recog_rates_S2B_combinations')

    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
