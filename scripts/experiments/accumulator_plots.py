#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from pf_matching.evaluation import plotting


# others
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
ROOT_DIR = '~/ROS/experiment_data/accumulator_images'
CMAP_NAME =  'YlOrRd'
ARROW_COLOR = (0, 0, 0)
SAVE_PLOTS = True
SHOW_PLOTS = True
ADDITIONAL_NAME_STRING = '_' + CMAP_NAME


def plot_accumulator_array(fname, ax, cmap='gnuplot', show_xlabel=True):
    """
    plot the contents of an accumulator array as an image
    @param fname: file name to load from, can contain '~'
    @type ax: Axes
    @param ax: axis to plot to
    @param cmap: matplotlib colormap name to use
    @param show_xlabel: only show label of x-axis if true
    @return: the plotted image
    """

    # load image of accumulator array
    fname = os.path.expanduser(fname)
    img = mpimg.imread(fname)
    assert (len(img.shape) <= 2)  # contains only height and width

    # plot
    ret_img = ax.imshow(img, interpolation="none", cmap=cmap, aspect='auto')

    # set labels
    ax.tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    ylims = ax.get_ylim()
    ax.set_yticks(ylims + (np.mean(ylims),))
    ax.set_yticklabels(["$\pi$", "$-\pi$", "0"])

    if show_xlabel:
        ax.set_xlabel(r'index of model reference point $i_{m_r}$')
    ax.set_ylabel(r"$\alpha$")

    return ret_img


def add_arrow(ax, x, y, length, angle, offset=0, color=(1, 1, 1)):
    """
    add an arrow to a plot, might still be buggy
    @type ax: Axes
    @param ax: axis to add to
    @param x: x location of point of interest
    @param y: y location of point of interest
    @param length: length of arrow
    @param angle: algle of the arrow in rad, 0 points to the right, pi/2 upwards
    @param offset: space between arrow tip and point of interest
    @param color: color of arrow
    """
    tip_location = np.array((-np.cos(angle) * offset,
                             np.sin(angle) * offset)) + (x, y)

    start_location = np.array((-np.cos(angle) * (length + offset),
                               np.sin(angle) * (length + offset))) + (x, y)

    arrowprops = {'arrowstyle': "->",
                  'connectionstyle': "arc3",
                  'color': color}
    ax.annotate('',
                xy=tip_location, xytext=start_location, arrowprops=arrowprops)


def add_cbar(fig, img):
    """
    add a color bar at the right of a figure with 2 subplots
    @param fig: figure to add colorbar to
    @param img: image to derive colorbar from
    """

    fig.set_tight_layout(False)
    fig.tight_layout()
    cbax = fig.colorbar(img, orientation='vertical', ax=[fig.axes[0], fig.axes[1]], fraction=0.05)

    limits = cbax.get_clim()
    cbax.set_ticks(limits)
    cbax.set_ticklabels(["0", "max"])


if __name__ == '__main__':

    # prepare for plotting
    mpl.rcParams.update(plotting.thesis_default_settings)

    # plot comparison of MBO and Houshold Objects
    fig1, axes = plt.subplots(2, 1, figsize=plotting.cm2inch(16, 5), sharex=True)

    path = os.path.join(ROOT_DIR, 'MBO_T-rex_rs3/feature_1__refP_16__all.png')
    img = plot_accumulator_array(path, fig1.axes[0], show_xlabel=False, cmap=CMAP_NAME)
    add_arrow(fig1.axes[0], 98, 0, length=20, angle=np.pi/4, color=ARROW_COLOR)

    path = os.path.join(ROOT_DIR, 'HH_clorix_s0s2/feature_1__refP_6__all.png')
    plot_accumulator_array(path , fig1.axes[1], cmap=CMAP_NAME)
    add_arrow(fig1.axes[1],137, 7, length=20, angle=np.pi / 4, color=ARROW_COLOR)

    add_cbar(fig1, img)

    # plot comparison of geometric primitive with / without model reduction
    fig, axes = plt.subplots(2, 1, figsize=plotting.cm2inch(16, 5), sharex=True)

    path = os.path.join(ROOT_DIR, 'Primitives_pyramid_s0s3_with_rs/feature_1__refP_2__all.png')
    plot_accumulator_array(path, fig.axes[1], cmap=CMAP_NAME)
    add_arrow(fig.axes[1], 1, 7, length=20, angle=np.pi * 3 / 4, color=ARROW_COLOR)

    path = os.path.join(ROOT_DIR, 'Primitives_pyramid_s0s3_without_rs/feature_1__refP_2__all.png')
    img = plot_accumulator_array(path, fig.axes[0], show_xlabel=False, cmap=CMAP_NAME)
    add_arrow(fig.axes[0], 638, 6, length=20, angle=np.pi / 4, color=ARROW_COLOR)
    add_arrow(fig.axes[0], 12, 7, length=20, angle=np.pi * 3 / 4, color=ARROW_COLOR)
    add_arrow(fig.axes[0], 42, 7, length=20, angle=np.pi * 3 / 4, color=ARROW_COLOR)
    add_arrow(fig.axes[0], 366, 6, length=20, angle=np.pi / 4, color=ARROW_COLOR)

    add_cbar(fig, img)


    # save figures
    if SAVE_PLOTS:
        pass
        plotting.save_figure(fig1, ROOT_DIR, 'accumulator_array_MBO_vs_houshold_objects' + ADDITIONAL_NAME_STRING)
        plotting.save_figure(fig, ROOT_DIR, 'accumulator_array_pyramid_with_without_rs' + ADDITIONAL_NAME_STRING)
    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
