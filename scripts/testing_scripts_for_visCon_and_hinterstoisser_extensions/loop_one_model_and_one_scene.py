# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure

# global script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
# TODO: choose the absolute step-size d_dist_abs that is used for discretizing the pair feature  
D_DIST_ABS = 0.02 # 0.016 --> 176 Punkte im Modell "Cuboid". Das schafft der RAM noch. 
		  # 0.008 --> >800 Punkte im Modell "Cuboid". xavers alter wert --> 5% des Modell-Durchmessers
		  #	      aber achtung! Xaver hat refPoint-Step 2 genommen. d.h. es sind dann effektiv weniger PPFs
		  # 0.02  --> 104 Punkte im Modell "Cuboid". Das schafft der RAM noch. 
		  # 0.01  --> 432 Punkte im Modell "Cuboid". Das schafft der RAM nicht mehr. 	
		  # 0.015 --> 192 Punkte im Modell "Cuboid". Das schafft der RAM noch.
		  # 0.014 --> 208 Punkte 
		  # 0.012 --> 312 Punkte ist (mit Eclipse offen) zu groß für den RAM.
				   # der Modell-Durchmesser schwankt beim Cuboid zwischen 0.15 und 0.17, dementsprechend entspricht der  
				   # D_DIST_ABS-Wert 0.019 etwa 11% bis 12% des Modell-Durchmessers

class loopOverOneSceneToGetCalculationTimesForAveraging(ExperimentBase): # this class is a child of the parent-class ExperimentBase
    """
    Simple script to train one model (cuboid) and match it to one scene (scene_0_sensor_0) using VisCon.
    Goal: Estimate a typical occupation level for the Hinterstoißer Flag-Array.
    """

    def __init__(self, dataset_path, d_dist_abs): # constructor
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure() # set up a dictionary that contains information about the experiment
        ExperimentBase.__init__(self, data_storage) 

        self.dataset = self.load_dataset(dataset_path)
        self.d_dist_abs = d_dist_abs

    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
                            'publish_pose_cluster_weights': True,
                            'd_dist': 0.05,
                            'refPointStep': 2,
                            'd_dist_is_relative': True,
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 2,
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,

			    'use_rotational_symmetry': True,      # match without collapsing models
                            'collapse_symmetric_models': False,
                            'publish_equivalent_poses': True,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

        #self.wait_for_enter('-' * 40 + '\n'
        #                    'Pipeline started and configured.\n'
        #                    'View / change parameters manually through "rosrun rqt_reconfigure rqt_reconfigure".\n'
        #                    'Show pipeline setup via "rqt_graph".\n'
        #                    'Press enter to start matching...')

    def looping(self):

        # model training
        print ('-' * 40 + '\n' + 'Adding model cuboid...')
	model_index = 0 # only the cuboid model, which should be the first model that the search for models returns
        self.add_model(self.dataset, model_index, clear_matcher=False)
        print('Cuboid model added.')

        # matching scene_0_sensor_0 'numberOfLoops'-times, to get calculate the mean of voting-time 
	# (times are stored in a file, see PFmatcher code) 
        scene_index = 0
	numberOfLoops = 5
	for loop in range(0, numberOfLoops+1):

        	self.interface.new_run()
		string = '-' * 40 + '\n' + 'Performing ' + str(loop) + ' of ' + str(numberOfLoops) +' ...'
		print (string)        
    		self.add_scene(self.dataset, scene_index)
		

    def end(self):
	self.wait_for_enter('-' * 40 + '\n' +
                    	    'Matching done. Press enter and the demo will clean up...')


if __name__ == '__main__':

    demo = loopOverOneSceneToGetCalculationTimesForAveraging(DATASET_PATH, D_DIST_ABS)
    demo.run(wait_before_looping=False)
