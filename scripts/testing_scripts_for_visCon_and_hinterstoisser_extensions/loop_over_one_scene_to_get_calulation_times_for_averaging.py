# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure

# global script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
# TODO: choose the absolute step-size d_dist_abs that is used for discretizing the pair feature  
D_DIST_ABS = 0.02 # 0.016 --> 176 Punkte im Modell "Cuboid". Das schafft der RAM noch. 
		  # 0.008 --> >800 Punkte im Modell "Cuboid". xavers alter wert --> 5% des Modell-Durchmessers
		  #	      aber achtung! Xaver hat refPoint-Step 2 genommen. d.h. es sind dann effektiv weniger PPFs
		  # 0.02  --> 104 Punkte im Modell "Cuboid". Das schafft der RAM noch. 
		  # 0.01  --> 432 Punkte im Modell "Cuboid". Das schafft der RAM nicht mehr. 	
		  # 0.015 --> 192 Punkte im Modell "Cuboid". Das schafft der RAM noch.
		  # 0.014 --> 208 Punkte 
		  # 0.012 --> 312 Punkte ist (mit Eclipse offen) zu groß für den RAM.
				   # der Modell-Durchmesser schwankt beim Cuboid zwischen 0.15 und 0.17, dementsprechend entspricht der  
				   # D_DIST_ABS-Wert 0.019 etwa 11% bis 12% des Modell-Durchmessers

class loopOverOneSceneToGetCalculationTimesForAveraging(ExperimentBase): # this class is a child of the parent-class ExperimentBase
    """
    Simple script to train one model (cuboid) and match it to one scene (scene_0_sensor_0) using VisCon.
    Goal: Estimate a typical occupation level for the Hinterstoißer Flag-Array.
    """

    def __init__(self, dataset_path, d_dist_abs): # constructor
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure() # set up a dictionary that contains information about the experiment
        ExperimentBase.__init__(self, data_storage) 

        self.dataset = self.load_dataset(dataset_path)
        self.d_dist_abs = d_dist_abs

    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
                            'd_dist': self.d_dist_abs,
                            'd_dist_is_relative': False,
			    'match_S2S': False,
			    'match_B2B': False,
			    'match_S2B': False,
			    'match_B2S': False,
			    'model_hash_table_type': 2, # MH_CMPH_CHD
			    'match_S2SVisCon': True,                            
			    'refPointStep': 1.0,
                            'maxThresh': 1.0,
                            'use_rotational_symmetry': False, #True
                            'collapse_symmetric_models': False,
			    'publish_clustered_poses': False, #True
			    'publish_n_best_poses': 1.0,

			    # debugging
			    #'HS_save_dir': '/home/markus/Documents',

			    # visibility context:
			    'd_VisCon': self.d_dist_abs,
			    'd_VisCon_is_relative': False,
			    'voxelSize_intersectDetect': 0.008,
			    'ignoreFactor_intersectClassific': 0.9,
			    'gapSizeFactor_allCases_intersectClassific': 1.0,
			    'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
			    'advancedIntersectionClassification': False,
			    'alongSurfaceThreshold_intersectClassific': 10.0,
			    'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
			    'visualizeVisibilityContextFeature': False,

			    # hinterstoisser extensions:
			    'use_neighbour_PPFs_for_training': False,
			    'use_neighbour_PPFs_for_matching': False,
			    'use_voting_balls': True,
			    'use_hinterstoisser_clustering': False,
			    'use_hypothesis_verification_with_visibility_context': False,
			    'vote_for_adjacent_rotation_angles': True,
			    'flag_array_hash_table_type': 4, # FA_CMPH_CHD
			    'flag_array_quantization_steps': 32.0,
                            }

                            #'d_dist': self.d_dist_abs,
                            #'d_dist_is_relative': False,
			    #'d_VisCon': self.d_dist_abs,
			    #'d_VisCon_is_relative': False,
			    #'voxelSize_intersectDetect': 0.008,
			    #'ignoreFactor_intersectClassific': 0.9,
			    #'gapSizeFactor_allCases_intersectClassific': 1.0,
			    #'gapSizeFactor_surfaceCase_intersectClassific': 0.0, #1.9
			    #'advancedIntersectionClassification': False,
			    #'visualizeVisibilityContextFeature': False,
			    #'use_hinterstoisser_clustering': False,
			    #'use_voting_balls': False,
			    #'use_neighbour_PPFs': True,
			    #'use_hypothesis_verification_with_visibility_context': False,
			    #'vote_for_adjacent_rotation_angles': True,
			    #'use_flag_array': True,
			    #'flag_array_quantization_steps': 32.0,
			    #'match_S2S': False,
			    #'match_B2B': False,
			    #'match_S2B': False,
			    #'match_B2S': False,
			    #'match_S2SVisCon': True,
                            #'refPointStep': 1.0,
                            #'maxThresh': 1.0,
                            #'use_rotational_symmetry': False,
                            #'collapse_symmetric_models': False,
                            #'publish_clustered_poses': False,
			    #'publish_n_best_poses': 1.0,
                            #}

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

        #self.wait_for_enter('-' * 40 + '\n'
        #                    'Pipeline started and configured.\n'
        #                    'View / change parameters manually through "rosrun rqt_reconfigure rqt_reconfigure".\n'
        #                    'Show pipeline setup via "rqt_graph".\n'
        #                    'Press enter to start matching...')

    def looping(self):

        # model training
        print ('-' * 40 + '\n' + 'Adding model cuboid...')
	model_index = 0 # only the cuboid model, which should be the first model that the search for models returns
        self.add_model(self.dataset, model_index, clear_matcher=False)
        print('Cuboid model added.')

        # matching scene_0_sensor_0 'numberOfLoops'-times, to get calculate the mean of voting-time 
	# (times are stored in a file, see PFmatcher code) 
        scene_index = 0
	numberOfLoops = 2
	for loop in range(0, numberOfLoops+1):

        	self.interface.new_run()
		string = '-' * 40 + '\n' + 'Performing ' + str(loop) + ' of ' + str(numberOfLoops) +' ...'
		print (string)        
    		self.add_scene(self.dataset, scene_index)
		

    def end(self):
	self.wait_for_enter('-' * 40 + '\n' +
                    	    'Matching done. Press enter and the demo will clean up...')


if __name__ == '__main__':

    demo = loopOverOneSceneToGetCalculationTimesForAveraging(DATASET_PATH, D_DIST_ABS)
    demo.run(wait_before_looping=False)
