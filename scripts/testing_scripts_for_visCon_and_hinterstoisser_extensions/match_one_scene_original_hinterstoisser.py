# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

import time

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure

# global script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'


class matchOneSceneForTestingTheAlgorithm(ExperimentBase): # this class is a child of the parent-class ExperimentBase
    """
    Simple script to train one model (cuboid) and match it to one scene (scene_0_sensor_0) using VisCon.
    Goal: Estimate a typical occupation level for the Hinterstoißer Flag-Array.
    """

    def __init__(self, dataset_path): # constructor
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure() # set up a dictionary that contains information about the experiment
        ExperimentBase.__init__(self, data_storage) 

        self.dataset = self.load_dataset(dataset_path)

    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
			    'match_S2S': True,
			    'match_B2B': False,
			    'match_S2B': False,
			    'match_B2S': False,
			    'model_hash_table_type': 2, # STL
			    'match_S2SVisCon': False,                            
			    'refPointStep': 1.0,
                            'maxThresh': 1.0,
                            'use_rotational_symmetry': True, #True
                            'collapse_symmetric_models': True,
			    'publish_clustered_poses': False, #True
			    'publish_n_best_poses': 1.0,

			    # debugging
			    #'HS_save_dir': '/home/markus/Documents',

			    # visibility context:
			    'd_VisCon': 0.05,
			    'd_VisCon_is_relative': False,
			    'voxelSize_intersectDetect': 0.008,
			    'ignoreFactor_intersectClassific': 0.9,
			    'gapSizeFactor_allCases_intersectClassific': 1.0,
			    'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
			    'advancedIntersectionClassification': False,
			    'alongSurfaceThreshold_intersectClassific': 10.0,
			    'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
			    'visualizeVisibilityContextFeature': False,

			    # hinterstoisser extensions:
			    'use_neighbour_PPFs_for_training': True,
			    'use_neighbour_PPFs_for_matching': False,
			    'use_voting_balls': True,
			    'use_hinterstoisser_clustering': True,
			    'use_hypothesis_verification_with_visibility_context': False,
			    'vote_for_adjacent_rotation_angles': True,
			    'flag_array_hash_table_type': 4, # FA_STL_PREBUILT
			    'flag_array_quantization_steps': 32.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

	# Cuboid #0 
	#   diameter: 0.195062
	#   d_dist:   0.005 --> 0.0097531
	# Hexagonal frustum #3 
	#   diameter: 0.109906
	#   d_dist:   0.005 --> 0.0054953
	# Pyramid #4 
	#   diameter: 0.157026
	#   d_dist:   0.005 --> 0.0078513        

	model_indices = [0,3,4]
	d_dist = [0.0097531, 0.0054953, 0.0078513] #absolute values
	model_diameters = [0.195062, 0.109906, 0.157026]	
	
	for i, model_index in enumerate(model_indices):        
		
		# adjust model specific parameters:
		model_preprocessor_settings = {'d_points': d_dist[i],
					       'd_points_is_relative': False}
		scene_preprocessor_settings = {'d_points': d_dist[i],
					       'd_points_is_relative': False}
		matcher_settings = {'d_dist': 1/((2/(d_dist[i]/model_diameters[i]))+1), # Nyquist-Criterion
                            	    'd_dist_is_relative': True}		
		
		# update parameter-server:
		self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
 		self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
 		self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)

		# model training
		print ('-' * 40 + '\n' + 'Adding model ...')
		self.add_model(self.dataset, model_index, clear_matcher=True)
		print('model added.')

		# matching scene_0_sensor_0, displaying results 
		scene_index = 0
		self.interface.new_run()  
	    	self.add_scene(self.dataset, scene_index)
		

    def end(self):
	print('-' * 40 + '\n' + 'Matching done. Cleaning up...')


if __name__ == '__main__':

    demo = matchOneSceneForTestingTheAlgorithm(DATASET_PATH)
    demo.run(wait_before_looping=False)
