#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors


# other script settings
TIME_OUT_S = 5 * 60
DATASET_DICTS = [{'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py',
                  'scene_preprocessor_settings' : {'d_points': 0.008,
                                                   'd_points_is_relative': False,
                                                   'remove_largest_plane': True
                                                   },
                  'matcher_settings': {'use_rotational_symmetry': True,
                                       'publish_equivalent_poses': True
                                        }
                  },
                 {'path': '~/ROS/datasets/household_objects_multi_instance/blensor_dataset.py',
                  'scene_preprocessor_settings' : {'d_points': 0.015,
                                                   'd_points_is_relative': False,
                                                   'remove_largest_plane': True
                                                   },
                  'matcher_settings' : {'use_rotational_symmetry': False,
                                        'publish_equivalent_poses': False
                                        }
                  },
                 {'path': '~/ROS/datasets/Mian_Bennamoun_Owens/dataset.py',
                  'scene_preprocessor_settings' : {'d_points': 10,
                                                   'd_points_is_relative': False,
                                                   'remove_largest_plane': False
                                                   },
                  'matcher_settings' : {'use_rotational_symmetry': False,
                                        'publish_equivalent_poses': False
                                        }
                  }
                 ]
#DATASET_PATH = '~/Datasets/household_objects_multi_instance/blensor_dataset.py'
#DATASET_PATH = '~/Datasets/MBO_converted2/mbodataset.py'

SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/pose_verifying/publish_n_best_poses/publish_n_best_poses'
#SAVE_FILE_WO_EXTENSION = '~/experiment_results/pose_verifying/household'
#SAVE_FILE_WO_EXTENSION = '~/experiment_results/pose_verifying/mbo'

class PublishNBestPosesExperiment(ExperimentBase):
    """
    experiment to analyse the mu_v and mu_p values relating to true/false pose
    """
    def __init__(self, data_storage, dataset_dicts, save_path_wo_extension):
    
        ExperimentBase.__init__(self, data_storage)
        
        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])
        self.save_path_wo_extension = save_path_wo_extension
        
    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()
        
        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        """
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False
                                       }

        matcher_settings = {'show_results': True,
                            'd_dist': self.d_dist_abs,
                            'd_dist_is_relative': False,
                            'refPointStep': 1
                            }
        
        """
        model_preprocessor_settings = {'pre_downsample_method': 0,  # VG
                                       'normal_estimation_method': 0,  # 0 NONE
                                       'remove_NaNs': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'd_points': 10,
                                       'd_points_is_relative': False,}

        matcher_settings = {'show_results': False,
                            #'publish_pose_cluster_weights': True,
                            'publish_n_best_poses': 50,
                            }
        

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
    def looping(self):
        
        # calculate number of runs to perform
        n_runs = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_runs += dataset.get_number_of_models() * dataset.get_number_of_scenes()
        
        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()
        
        # match all models of the datasets
        for dataset_dict in self.dataset_dicts:
            
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)
            
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                         dataset_dict['scene_preprocessor_settings'])
            self.interface.set_dyn_reconfigure_parameters('matcher',
                                                         dataset_dict['matcher_settings'])
            
            print('dataset: %s' %(dataset.get_name()))
            
            self.loop_models_and_scenes_with_constant_d_points(dataset, re)
            
            
            
    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

        print('Computing pose errors for all runs...')
        compute_pose_errors(self.data)
        
        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        self.data.to_yaml(self.save_path_wo_extension + '.yaml')
        
if __name__ == '__main__':
    
    data_storage = data_structure.ExperimentDataStructure()
    experiment = PublishNBestPosesExperiment(data_storage,
                                             DATASET_DICTS,
                                             SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=True)
