#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from twisted.protocols.amp import ListOf

input = raw_input
range = xrange

# project
from math import floor, ceil, pi, sqrt, pow
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/hinterstoisser_pose_verifying/hinterstoisser_searchradius_and_angleDiffThresh/hinterstoisser_searchradius_and_angleDiffThresh.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15 # rad
max_x = 1.0
stepSize = 100
searchradius_values = [0.5, 1.0, 2.0, 3.0, 5.0]
angleDiffThresh_in_degree_values = [3, 6, 12, 18, 45, 180]
precisions_weight = 0.2
angleDiffThresh_in_pi_values = [a/180 for a in angleDiffThresh_in_degree_values]
datasetNames = [#'MBO', 'household_objects_multi_instance',
     'geometric_primitives']
datasetSceneCount = {#'MBO': 50, 'household_objects_multi_instance': 15,
     'geometric_primitives': 20}
datasetSynonyms = [#'MBO', 'Haushaltsgegenst\\"ande',
 'primitive Gegenst\\"ande']

def get_TPs(experiment_data):
    """
    get the numbers of true positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: true positives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])
        
    tps = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_true_positives)
    #print(tps)
    return tps

def get_FPs(experiment_data):
    """
    get the numbers of false positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: false positives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])
        
    fps = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_false_positives)
    #print(fps)
    return fps
    
def get_Relevants(experiment_data):
    """
    get the numbers of false negatives + true positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: false negatives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])
        
    relevants = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_relevants) # the sum of relevant poses (tp + fn) for a list of dictionaries
    #print(relevants)
    return relevants
    
def compute_precisions_recalls(tp, fp, last_tp, last_fp, relevants):
    if fp > 0: #fp > 0 and tp >= 0 ==> tp + fp > 0
        precision = tp / (tp + fp)
    else:
        precision = 1 #limes n->0 n/n = 1
        
    if relevants > 0: #tp + fn > 0 ==> tp > 0 oder fn > 0
        recall = tp / (relevants)
    else: #fn = 0 und tp = 0
        recall = 1 #limes fp->0 tp/tp = 1

    # linear interpolation of the missing tp-steps
    
    listOfInterpolatedPR = []
    if last_tp is not None and last_fp is not None and tp != last_tp:
        x_list = range(1, abs(tp - last_tp))
        negativesPerPositive = (fp - last_fp) / (tp - last_tp)
        
        
        for x in x_list:
            interpolated_tp = tp + x
            interpolated_fp = fp + negativesPerPositive*x
            
            if relevants > 0:
                interpolated_recall = interpolated_tp / relevants
            else:
                interpolated_recall = 1
            if interpolated_fp > 0: #fp > 0 and tp >= 0 ==> tp + fp > 0
                interpolated_precision = interpolated_tp / (interpolated_tp + interpolated_fp)
            else:
                interpolated_precision = 1 #limes n->0 n/n = 1
            
            listOfInterpolatedPR.append((interpolated_recall, interpolated_precision))
    
    listOfPR = listOfInterpolatedPR + [(recall, precision)]
    
    return precision, recall, listOfPR

    

def plot_precision_recall_overV_constAngleDiffThresh(experiment_data, dataset_names_list, 
                                                     p_to_use=1, 
                                                     angleDiffThresh_list=[1], 
                                                     show_legend=False):
    v_steps_list = [v_step/stepSize for v_step in range(int(stepSize * max_x) +1)]
    
    true_positives_data_dict = {a: 
                                {dSname: 
                                 {r: 
                                  {v: None for v in v_steps_list} 
                                  for r in searchradius_values} 
                                 for dSname in dataset_names_list} 
                                for a in angleDiffThresh_list}
    false_positives_data_dict = {a: 
                                 {dSname: 
                                  {r: 
                                   {v: None for v in v_steps_list} 
                                   for r in searchradius_values} 
                                  for dSname in dataset_names_list} 
                                 for a in angleDiffThresh_list}

    relevants_data_dict = {a: 
                           {dSname: 
                            {r: None for r in searchradius_values} 
                            for dSname in dataset_names_list} 
                           for a in angleDiffThresh_list}
    
    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)
    
    relevants_list = get_Relevants(experiment_data)
    
    # TODO debugging, remove afterwards
    wait = input("PRESS ENTER TO CONTINUE.")
    print (relevants_list)
    wait = input("PRESS ENTER TO CONTINUE.")
    
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_values:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_list:
                        relevants_data_dict[a][datasetName][r] = relevants
                        
    # TODO debugging, remove afterwards
    wait = input("PRESS ENTER TO CONTINUE.")
    print (relevants_data_dict)
    wait = input("PRESS ENTER TO CONTINUE.")
    
    print("getting Data from the experiments, v running from 0 to 1")
    display_steps = [i/10 for i in range (10+1)]
    for v in v_steps_list:
        #print(v)
        post_processing.compute_verified_through_mu(data, v, p_to_use)
        post_processing.compute_verification(data)
        
        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)
        
        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_values:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_list:
                            #print(tp)
                            true_positives_data_dict[a][datasetName][r][v] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_values:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_list:
                            #print(fp)
                            false_positives_data_dict[a][datasetName][r][v] = fp
        
        progress = floor(v * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)
            
    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {a:
                           {dS:
                            {r: [] for r in searchradius_values}
                            for dS in extendedDatasetNames_list}
                           for a in angleDiffThresh_list}
    recall_data_dict = {a:
                        {dS:
                         {r: [] for r in searchradius_values}
                         for dS in extendedDatasetNames_list}
                        for a in angleDiffThresh_list}
    
    precision_recall_data_dict = {a:
                                  {dS:
                                   {r: [] for r in searchradius_values}
                                   for dS in extendedDatasetNames_list}
                                  for a in angleDiffThresh_list}
    
    e_data_dict = {a:
                   {dS:
                    {r: [] for r in searchradius_values}
                    for dS in extendedDatasetNames_list}
                   for a in angleDiffThresh_list}
                   

    print("calculating precision and recall values")
    for r in searchradius_values:
        for a in angleDiffThresh_list:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for v in v_steps_list:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[a][datasetName][r]
                    tp = true_positives_data_dict[a][datasetName][r][v]
                    fp = false_positives_data_dict[a][datasetName][r][v]
                    
                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum([x for name, x in datasetSceneCount.items() if name != datasetName]) 
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants
                    
                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                    
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                    else:
                        e = 1
                    e_data_dict[a][datasetName][r].append((v, e))

                    precision_data_dict[a][datasetName][r].append((v, precision)) 
                    recall_data_dict[a][datasetName][r].append((v, recall))
                    precision_recall_data_dict[a][datasetName][r] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp
                    
                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp/productOfScenes)
                fp = int(sum_fp/productOfScenes)
                relevants = int(sum_relevants/productOfScenes)
                
                precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)

                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                else:
                    e = 1
                e_data_dict[a][datasetName][r].append((v, e))

                precision_data_dict[a][datasetName][r].append((v, precision)) 
                recall_data_dict[a][datasetName][r].append((v, recall))
                precision_recall_data_dict[a][datasetName][r] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp
                

    precision_recall_auc_dict = {a:
                                     {dS:
                                      {r: 0 for r in searchradius_values}
                                      for dS in extendedDatasetNames_list}
                                     for a in angleDiffThresh_list}

    print("computing area under curves")                            
    for a, precision_recall_by_angle in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_angle.items():
            for r, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                area = 0
                #print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        #print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[a][datasetName][r] += 0.5 * (recall - last_recall) * (precision + last_precision)
                        #print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall
                
    dictOfReturnFiguresA = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    #dictOfReturnFiguresB = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    print("generating plots...")
    for a, precision_recall_auc_by_angle in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_angle.items():
            dictOfReturnFiguresA[a][datasetName] = plt.figure(figsize=plotting.cm2inch(15,10))
            
            precision_list = sorted(precision_data_dict[a][datasetName].items())
            recall_list = sorted(recall_data_dict[a][datasetName].items())
            e_list = sorted(e_data_dict[a][datasetName].items())
        
            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for r, precision in precision_list:
                if a !=1:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_precision.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for r, recall in recall_list:
                if a !=1:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_recall.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_e = plotting.PlotStyle(len(e_list),
                                          additional_plot_settings={"markerfacecolor": "None",
                                                                              "marker": "None"})
            ax = plt.subplot(223)
            for r, e in e_list:
                if a !=1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_e.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$'%precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0., title='Parameter')

            
            
            """
            dictOfReturnFiguresB[a][datasetName], ax = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5,6))
    
            precision_recall_list = sorted(precision_recall_data_dict[a][datasetName].items())
            
            ps_precisionOverRecall = plotting.PlotStyle(len(precision_recall_list),
                                                        additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
            for r, precision_recall in precision_recall_list:
                auc = precision_recall_auc_dict[a][datasetName][r]
                ax.plot(*zip(*sorted(precision_recall, key=lambda x: (x[0], -x[1]))), label='$%.1f$ $($AUC: $%.2f)$'%(r, auc), **ps_precisionOverRecall.next())
                
            ax.set_xlabel('Trefferquote')
            ax.set_ylabel('Genauigkeit')
            
            ax.grid()
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            
            if show_legend:
                if a != 1:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                              title='$r_s$ mit $\\alpha=\\frac{1}{%d}\\pi$'%(1/a))
                else:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=\\pi$')
            
            # just make these graphics if they are needed
            for alpha in prweights:
                dictOfReturnFiguresC[a][datasetName][alpha], ax = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5,6))

                f_list = sorted(f_data_dict[a][datasetName][alpha].items())
                
                ps_f = plotting.PlotStyle(len(f_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
                for r, f in f_list:
                    ax.plot(*zip(*f), label='$%.1f$'%r, **ps_f.next())
                    
                ax.set_xlabel('$\mathcal{V}$')
                ax.set_ylabel('$F_{%.1f}$'%alpha)
                
                ax.grid()
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                
                if show_legend:
                    ax.legend(loc='upper right', title='$r_s$')
            """
                
           
    return dictOfReturnFiguresA

def plot_precision_recall_overP_constAngleDiffThresh(experiment_data, dataset_names_list,
                                                     v_to_use=0, 
                                                     angleDiffThresh_list=[1], 
                                                     show_legend=False):
    p_steps_list = [(p_step-1)/stepSize for p_step in range(int(stepSize * max_x) +1)]
    
    true_positives_data_dict = {a: 
                                {dSname: 
                                 {r: 
                                  {p: None for p in p_steps_list} 
                                  for r in searchradius_values} 
                                 for dSname in dataset_names_list} 
                                for a in angleDiffThresh_list}
    false_positives_data_dict = {a: 
                                 {dSname: 
                                  {r: 
                                   {p: None for p in p_steps_list} 
                                   for r in searchradius_values} 
                                  for dSname in dataset_names_list} 
                                 for a in angleDiffThresh_list}

    relevants_data_dict = {a: 
                           {dSname: 
                            {r: None for r in searchradius_values} 
                            for dSname in dataset_names_list} 
                           for a in angleDiffThresh_list}
    
    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)
    
    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_values:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_list:
                        #print(relevants)
                        relevants_data_dict[a][datasetName][r] = relevants
    
    print("getting Data from the experiments, p running from 0 to 1")
    display_steps = [i/10 for i in range (10+1)]
    for p in p_steps_list:
        #print(p)
        post_processing.compute_verified_through_mu(data, v_to_use, p)
        post_processing.compute_verification(data)
        
        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)
        
        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_values:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_list:
                            #print(tp)
                            true_positives_data_dict[a][datasetName][r][p] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_values:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_list:
                            #print(fp)
                            false_positives_data_dict[a][datasetName][r][p] = fp
        
        progress = floor((p*stepSize+1)/(stepSize*max_x +1) * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)
            
    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {a:
                           {dS:
                            {r: [] for r in searchradius_values}
                            for dS in extendedDatasetNames_list}
                           for a in angleDiffThresh_list}
    recall_data_dict = {a:
                        {dS:
                         {r: [] for r in searchradius_values}
                         for dS in extendedDatasetNames_list}
                        for a in angleDiffThresh_list}
    
    precision_recall_data_dict = {a:
                                 {dS:
                                  {r: [] for r in searchradius_values}
                                  for dS in extendedDatasetNames_list}
                                 for a in angleDiffThresh_list}
    
    e_data_dict = {a:
                   {dS:
                    {r: [] for r in searchradius_values}
                    for dS in extendedDatasetNames_list}
                   for a in angleDiffThresh_list}

    print("calculating precision and recall values")
    for r in searchradius_values:
        for a in angleDiffThresh_list:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for p in p_steps_list[::-1]:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[a][datasetName][r]
                    tp = true_positives_data_dict[a][datasetName][r][p]
                    fp = false_positives_data_dict[a][datasetName][r][p]
                    
                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum([x for name, x in datasetSceneCount.items() if name != datasetName]) 
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants
                    
                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                    
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                    else:
                        e = 1
                    e_data_dict[a][datasetName][r].append((p, e))

                    precision_data_dict[a][datasetName][r].append((p, precision)) 
                    recall_data_dict[a][datasetName][r].append((p, recall))
                    precision_recall_data_dict[a][datasetName][r] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp
                    
                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp/productOfScenes)
                fp = int(sum_fp/productOfScenes)
                relevants = int(sum_relevants/productOfScenes)
                
                precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                
                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                else:
                    e = 1
                e_data_dict[a][datasetName][r].append((p, e))

                precision_data_dict[a][datasetName][r].append((p, precision)) 
                recall_data_dict[a][datasetName][r].append((p, recall))
                precision_recall_data_dict[a][datasetName][r] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {a:
                                     {dS:
                                      {r: 0 for r in searchradius_values}
                                      for dS in extendedDatasetNames_list}
                                     for a in angleDiffThresh_list}

    print("computing area under curves")                            
    v = 0
    for a, precision_recall_by_angle in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_angle.items():
            for r, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                #print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        #print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[a][datasetName][r] += 0.5 * (recall - last_recall) * (precision + last_precision)
                        #print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall
                
    dictOfReturnFiguresA = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    #dictOfReturnFiguresB = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    print("generating plots...")
    for a, precision_recall_auc_by_angle in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_angle.items():
            dictOfReturnFiguresA[a][datasetName] = plt.figure(figsize=plotting.cm2inch(15,10))
            
            precision_list = sorted(precision_data_dict[a][datasetName].items())
            recall_list = sorted(recall_data_dict[a][datasetName].items())
            e_list = sorted(e_data_dict[a][datasetName].items())
        
            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for r, precision in precision_list:
                if a !=1:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_precision.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for r, recall in recall_list:
                if a !=1:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_recall.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_e = plotting.PlotStyle(len(e_list),
                                          additional_plot_settings={"markerfacecolor": "None",
                                                                              "marker": "None"})
            ax = plt.subplot(223)
            for r, e in e_list:
                if a !=1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_e.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$'%precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0., title='Parameter')
            
            """
            dictOfReturnFiguresB[a][datasetName], ax = plt.subplots(1,1, figsize=plotting.cm2inch(7.5,6))
    
            precision_recall_list = sorted(precision_recall_data_dict[a][datasetName].items())
            
            ps_precisionOverRecall = plotting.PlotStyle(len(precision_recall_list),
                                                        additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
            for r, precision_recall in precision_recall_list:
                auc = precision_recall_auc_dict[a][datasetName][r]
                ax.plot(*zip(*sorted(precision_recall, key=lambda x: (x[0], -x[1]))), label='$%.1f$ $($AUC: $%.2f)$'%(r, auc), **ps_precisionOverRecall.next())
                
            ax.set_xlabel('Trefferquote')
            ax.set_ylabel('Genauigkeit')
            
            ax.grid()
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
                
            if show_legend:
                if a != 1:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=\\frac{1}{%d}\\pi$'%(1/a))
                else:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=pi$')
            """
                
    return dictOfReturnFiguresA

def plot_precision_recall_overV_constSearchradius(experiment_data, dataset_names_list, 
                                                  p_to_use=1, 
                                                  searchradius_list=[1], 
                                                  show_legend=False):
    v_steps_list = [v_step/stepSize for v_step in range(int(stepSize * max_x) +1)]
    
    true_positives_data_dict = {r: 
                                {dSname: 
                                 {a: 
                                  {v: None for v in v_steps_list} 
                                  for a in angleDiffThresh_in_pi_values} 
                                 for dSname in dataset_names_list} 
                                for r in searchradius_list}
    false_positives_data_dict = {r: 
                                 {dSname: 
                                  {a: 
                                   {v: None for v in v_steps_list} 
                                   for a in angleDiffThresh_in_pi_values} 
                                  for dSname in dataset_names_list} 
                                 for r in searchradius_list}

    relevants_data_dict = {r: 
                           {dSname: 
                            {a: None for a in angleDiffThresh_in_pi_values} 
                            for dSname in dataset_names_list} 
                           for r in searchradius_list}
    
    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)
    
    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_list:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_in_pi_values:
                        relevants_data_dict[r][datasetName][a] = relevants
    
    print("getting Data from the experiments, v running from 0 to 1")
    display_steps = [i/10 for i in range (10+1)]
    for v in v_steps_list:
        #print(v)
        post_processing.compute_verified_through_mu(data, v, p_to_use)
        post_processing.compute_verification(data)
        
        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)
        
        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_list:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            #print(tp)
                            true_positives_data_dict[r][datasetName][a][v] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_list:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            #print(fp)
                            false_positives_data_dict[r][datasetName][a][v] = fp
        
        progress = floor(v * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)
            
    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {r:
                           {dS:
                            {a: [] for a in angleDiffThresh_in_pi_values}
                            for dS in extendedDatasetNames_list}
                           for r in searchradius_list}
    recall_data_dict = {r:
                        {dS:
                         {a: [] for a in angleDiffThresh_in_pi_values}
                         for dS in extendedDatasetNames_list}
                        for r in searchradius_list}
    
    precision_recall_data_dict = {r:
                                 {dS:
                                  {a: [] for a in angleDiffThresh_in_pi_values}
                                  for dS in extendedDatasetNames_list}
                                 for r in searchradius_list}
    
    e_data_dict = {r:
                   {dS:
                    {a: [] for a in angleDiffThresh_in_pi_values}
                    for dS in extendedDatasetNames_list}
                   for r in searchradius_list}

    print("calculating precision and recall values")
    for r in searchradius_list:
        for a in angleDiffThresh_in_pi_values:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for v in v_steps_list:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[r][datasetName][a]
                    tp = true_positives_data_dict[r][datasetName][a][v]
                    fp = false_positives_data_dict[r][datasetName][a][v]
                    
                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum([x for name, x in datasetSceneCount.items() if name != datasetName]) 
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants

                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                    
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                    else:
                        e = 1
                    e_data_dict[r][datasetName][a].append((v, e))
                    
                    precision_data_dict[r][datasetName][a].append((v, precision)) 
                    recall_data_dict[r][datasetName][a].append((v, recall))
                    precision_recall_data_dict[r][datasetName][a] += listOfPR
                    
                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp
                    
                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp/productOfScenes)
                fp = int(sum_fp/productOfScenes)
                relevants = int(sum_relevants/productOfScenes)
                
                precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                
                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                else:
                    e = 1
                e_data_dict[r][datasetName][a].append((v, e))

                precision_data_dict[r][datasetName][a].append((v, precision)) 
                recall_data_dict[r][datasetName][a].append((v, recall))
                precision_recall_data_dict[r][datasetName][a] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp
                      

    precision_recall_auc_dict = {r:
                                     {dS:
                                      {a: 0 for a in angleDiffThresh_in_pi_values}
                                      for dS in extendedDatasetNames_list}
                                     for r in searchradius_list}

    print("computing area under curves")                            
    for r, precision_recall_by_radius in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_radius.items():
            for a, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                #print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        #print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[r][datasetName][a] += 0.5 * (recall - last_recall) * (precision + last_precision)
                        #print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall
                
    dictOfReturnFiguresA = {r: {dSname: None for dSname in extendedDatasetNames_list} for r in searchradius_list}
    print("generating plots...")
    for r, precision_recall_auc_by_radius in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_radius.items():
            dictOfReturnFiguresA[r][datasetName] = plt.figure(figsize=plotting.cm2inch(15,10))
            
            precision_list = sorted(precision_data_dict[r][datasetName].items())
            recall_list = sorted(recall_data_dict[r][datasetName].items())
            e_list = sorted(e_data_dict[r][datasetName].items())
        
            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for a, precision in precision_list:
                if a !=1:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_precision.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for a, recall in recall_list:
                if a !=1:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_recall.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_e = plotting.PlotStyle(len(e_list),
                                          additional_plot_settings={"markerfacecolor": "None",
                                                                              "marker": "None"})
            ax = plt.subplot(223)
            for a, e in e_list:
                if a !=1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_e.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$'%precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0., title='Parameter')
        
    return dictOfReturnFiguresA, e_data_dict

def plot_precision_recall_overP_constSearchradius(experiment_data, dataset_names_list,
                                                  v_to_use=0, 
                                                  searchradius_list=[1], 
                                                  show_legend=False):
    p_steps_list = [(p_step-1)/stepSize for p_step in range(int(stepSize * max_x) +1)]
    
    true_positives_data_dict = {r: 
                                {dSname: 
                                 {a: 
                                  {p: None for p in p_steps_list} 
                                  for a in angleDiffThresh_in_pi_values} 
                                 for dSname in dataset_names_list} 
                                for r in searchradius_list}
    false_positives_data_dict = {r: 
                                 {dSname: 
                                  {a: 
                                   {p: None for p in p_steps_list} 
                                   for a in angleDiffThresh_in_pi_values} 
                                  for dSname in dataset_names_list} 
                                 for r in searchradius_list}

    relevants_data_dict = {r: 
                           {dSname: 
                            {a: None for a in angleDiffThresh_in_pi_values} 
                            for dSname in dataset_names_list} 
                           for r in searchradius_list}
    
    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)
    
    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_list:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_in_pi_values:
                        #print(relevants)
                        relevants_data_dict[r][datasetName][a] = relevants
    
    print("getting Data from the experiments, p running from 0 to 1")
    display_steps = [i/10 for i in range (10+1)]
    for p in p_steps_list:
        #print(p)
        post_processing.compute_verified_through_mu(data, v_to_use, p)
        post_processing.compute_verification(data)
        
        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)
        
        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_list:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            #print(tp)
                            true_positives_data_dict[r][datasetName][a][p] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_list:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            #print(fp)
                            false_positives_data_dict[r][datasetName][a][p] = fp
        
        progress = floor((p*stepSize+1)/(stepSize*max_x +1) * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)
            
    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {r:
                           {dS:
                            {a: [] for a in angleDiffThresh_in_pi_values}
                            for dS in extendedDatasetNames_list}
                           for r in searchradius_list}
    recall_data_dict = {r:
                        {dS:
                         {a: [] for a in angleDiffThresh_in_pi_values}
                         for dS in extendedDatasetNames_list}
                        for r in searchradius_list}
    
    precision_recall_data_dict = {r:
                                 {dS:
                                  {a: [] for a in angleDiffThresh_in_pi_values}
                                  for dS in extendedDatasetNames_list}
                                 for r in searchradius_list}
    
    e_data_dict = {r:
                   {dS:
                    {a: [] for a in angleDiffThresh_in_pi_values}
                    for dS in extendedDatasetNames_list}
                   for r in searchradius_list}

    print("calculating precision and recall values")
    for r in searchradius_list:
        for a in angleDiffThresh_in_pi_values:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for p in p_steps_list[::-1]:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[r][datasetName][a]
                    tp = true_positives_data_dict[r][datasetName][a][p]
                    fp = false_positives_data_dict[r][datasetName][a][p]
                    
                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum([x for name, x in datasetSceneCount.items() if name != datasetName]) 
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants
                    
                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                    #in preselection the recall is more important, than the precision
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                    else:
                        e = 1
                    e_data_dict[r][datasetName][a].append((p, e))
                    
                    precision_data_dict[r][datasetName][a].append((p, precision)) 
                    recall_data_dict[r][datasetName][a].append((p, recall))
                    precision_recall_data_dict[r][datasetName][a] += listOfPR
                    
                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp
                    
                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp/productOfScenes)
                fp = int(sum_fp/productOfScenes)
                relevants = int(sum_relevants/productOfScenes)
                
                precision, recall, listOfPR = compute_precisions_recalls(tp, fp, last_tp[datasetName], last_fp[datasetName], relevants)
                
                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                else:
                    e = 1
                e_data_dict[r][datasetName][a].append((p, e))

                precision_data_dict[r][datasetName][a].append((p, precision)) 
                recall_data_dict[r][datasetName][a].append((p, recall))
                precision_recall_data_dict[r][datasetName][a] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {r:
                                     {dS:
                                      {a: 0 for a in angleDiffThresh_in_pi_values}
                                      for dS in extendedDatasetNames_list}
                                     for r in searchradius_list}

    print("computing area under curves")                            
    v = 0
    for r, precision_recall_by_radius in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_radius.items():
            for a, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                #print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data):
                    if last_precision is not None and last_recall is not None:
                        #print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[r][datasetName][a] += 0.5 * (recall - last_recall) * (precision + last_precision)
                        #print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall
                
    dictOfReturnFiguresA = {r: {dSname: None for dSname in extendedDatasetNames_list} for r in searchradius_list}
    print("generating plots...")
    for r, precision_recall_auc_by_radius in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_radius.items():
            dictOfReturnFiguresA[r][datasetName] = plt.figure(figsize=plotting.cm2inch(15,10))
            
            precision_list = sorted(precision_data_dict[r][datasetName].items())
            recall_list = sorted(recall_data_dict[r][datasetName].items())
            e_list = sorted(e_data_dict[r][datasetName].items())
        
            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for a, precision in precision_list:
                if a !=1:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_precision.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for a, recall in recall_list:
                if a !=1:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_recall.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            ps_e = plotting.PlotStyle(len(e_list),
                                          additional_plot_settings={"markerfacecolor": "None",
                                                                              "marker": "None"})
            ax = plt.subplot(223)
            for a, e in e_list:
                if a !=1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(r, 1/a), **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$'%r, **ps_e.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$'%precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)
            
            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0., title='Parameter')
        
    return dictOfReturnFiguresA, e_data_dict

def plot_relevant_model_points_over_searchradius(experiment_data, ax, datasets, show_legend=False):
    """
    plot the number of relevant model points over the searchradius
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier', 'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]
    average_model_points_time_per_angleDiffThresh_per_searchradius = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['searchradius'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_model_points)
        
    ps = plotting.PlotStyle(len(average_model_points_time_per_angleDiffThresh_per_searchradius),
                            additional_plot_settings={"markerfacecolor": "None"})
    ax.plot(*zip(*average_model_points_time_per_angleDiffThresh_per_searchradius), label='Anzahl der Modellpunkte', **ps.next())
        
    ax.set_xlabel('$r_s$')
    ax.set_ylabel('Anzahl der\nrelevanten Modellpunkte')
    ax.grid(which='both')
    
    if show_legend:
        ax.legend(loc='lower right')
        
    ax.set_xlim(0,max(searchradius_values))
    ax.set_ylim(0, 500)

    return ax
    
def plot_mu_time_over_angleDiffThresh(experiment_data, ax, datasets):
    """
    plot the time required for calculating mu time over the angleDiffThresh
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi'],
                                                      ['dynamicParameters', '/verifier', 'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]
    
    average_muvalue_time_per_searchradius_per_angleDiffThresh = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)
        
    ps = plotting.PlotStyle(len(average_muvalue_time_per_searchradius_per_angleDiffThresh),
                            additional_plot_settings={"markerfacecolor": "None"})
    for r, plot_data in average_muvalue_time_per_searchradius_per_angleDiffThresh:
        ax.plot(*zip(*plot_data), label='$%.1f$'%(r), **ps.next())
        
    ax.set_xlabel('$\\alpha$')
    ax.set_ylabel('mittlere Berechnungszeit\nf\\"ur $\\mu$ in ms')
    ax.grid(which='both')
    ax.legend(loc='upper right', title='$r_s$')
    ax.set_xlim(0,max(angleDiffThresh_in_pi_values))
    #ax.set_ylim(1, 1800)

    return ax

def plot_mu_time_over_searchradius(experiment_data, ax, datasets):
    """
    plot the time required for calculating mu time over the searchradius
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi'],
                                                      ['dynamicParameters', '/verifier', 'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]
    
    average_muvalue_time_per_angleDiffThresh_per_searchradius = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['angleDiffThresh_in_pi'],
                                                    iDict['searchradius'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)
        
    ps = plotting.PlotStyle(len(average_muvalue_time_per_angleDiffThresh_per_searchradius),
                            additional_plot_settings={"markerfacecolor": "None"})
    for a, plot_data in average_muvalue_time_per_angleDiffThresh_per_searchradius:
        if a != 1:
            ax.plot(*zip(*plot_data), label='$\\frac{1}{%d}\\pi$'%(1/a), **ps.next())
        else:
            ax.plot(*zip(*plot_data), label='$\\pi$', **ps.next())
        
    ax.set_xlabel('$r_s$')
    ax.set_ylabel('mittlere Berechnungszeit\nf\\"ur $\\mu$ in ms')
    ax.grid(which='both')
    ax.legend(loc='upper left', title='$\\alpha$')
    ax.set_xlim(0,max(searchradius_values))
    #ax.set_ylim(1, 1800)

    return ax
    

if __name__ == '__main__':
    
    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_correctness_from_verifier(data, max_translation_error_relative, max_rotation_error)
    #post_processing.compute_verified(data)
    
    mpl.rcParams.update(plotting.krone_thesis_default_settings)

    print("this may take a while...")
    
    print("generating one graphic per angleDiffThresh")
    figA = plot_precision_recall_overV_constAngleDiffThresh(data, datasetNames, angleDiffThresh_list=[1], show_legend=True)
    if SAVE_PLOTS:
        print("saving plotted figures...")
        plt_dir = os.path.dirname(FILE_PATH)
        for a, fig_by_ds in figA.items():
            for dataset, figure in fig_by_ds.items():
                plotting.save_figure(figure, 
                                     plt_dir, 
                                     '%s_pUr_V_angle_%s' %(dataset, int(a*180)))
                
                plt.close(figure)
    
    figA = plot_precision_recall_overP_constAngleDiffThresh(data, datasetNames, angleDiffThresh_list=[1], show_legend=True)
    if SAVE_PLOTS:
        print("saving plotted figures...")
        plt_dir = os.path.dirname(FILE_PATH)
        for a, fig_by_ds in figA.items():
            for dataset, figure in fig_by_ds.items():
                plotting.save_figure(figure, 
                                     plt_dir, 
                                     '%s_pUr_P_angle_%s' %(dataset, int(a*180)))
                
                plt.close(figure)
    
    selected_searchradius = {#datasetNames[0]: 0.5,
                                #datasetNames[1]: 0.5,
                                datasetNames[0]: 0.5,#1.0
                                'all': 1.0} # selections made because of constant anglediffthresh
   
    print("generating one graphic per searchradius")
    figA, e_dictV = plot_precision_recall_overV_constSearchradius(data, datasetNames, searchradius_list=[0.5, 1], show_legend=True)
    if SAVE_PLOTS:
        print("saving plotted figures...")
        plt_dir = os.path.dirname(FILE_PATH)
        
        for r, fig_by_ds in figA.items():
            for dataset, figure in fig_by_ds.items():
                if selected_searchradius[dataset] == r:
                    r_as_string = '%.1f' %r
                    r_for_saving = r_as_string.replace('.', '-')
                    plotting.save_figure(figure, 
                                         plt_dir, 
                                         '%s_pUr_V_radius_%s' %(dataset, r_for_saving))
                    
                plt.close(figure)
    
    figA, e_dictP = plot_precision_recall_overP_constSearchradius(data, datasetNames, searchradius_list=[0.5, 1], show_legend=True)
    if SAVE_PLOTS:
        print("saving plotted figures...")
        plt_dir = os.path.dirname(FILE_PATH)
        
        for r, fig_by_ds in figA.items():
            for dataset, figure in fig_by_ds.items():
                if selected_searchradius[dataset] == r:
                    r_as_string = '%.1f' %r
                    r_for_saving = r_as_string.replace('.', '-')
                    plotting.save_figure(figure, 
                                         plt_dir, 
                                         '%s_pUr_P_radius_%s' %(dataset, r_for_saving))
                
                plt.close(figure)
    
    selected_angleDiffThresh = {#datasetNames[0]: 1/15,
                                #datasetNames[1]: 1/10,
                                datasetNames[0]: 1/15,#1/10
                                'all': 1/15} # selections made because of constant searchradius
    
    extendeddataset_dict = {name: [name] for name in datasetNames}
    extendeddataset_dict['all'] = datasetNames
    
    for dsname, dslist in extendeddataset_dict.items():
        listOfVEs = e_dictV[selected_searchradius[dsname]][dsname][selected_angleDiffThresh[dsname]]
        sortedVEs = sorted(listOfVEs, key=lambda x: (x[1], x[0]))
        bestV, bestVsE = sortedVEs[0]
        listOfPEs = e_dictP[selected_searchradius[dsname]][dsname][selected_angleDiffThresh[dsname]]
        sortedPEs = sorted(listOfPEs, key=lambda x: (x[1], -x[0]))
        bestP, bestPsE = sortedPEs[0]
        
        print('For the selected searchradius %.1f and angleDiffThresh 1/%d of Dataset \'%s\'\n'\
              'the best e for v is %.2f (v=%.2f) and the best e for p is %.2f (p=%.2f)'\
              %(selected_searchradius[dsname], 1/selected_angleDiffThresh[dsname], dsname,\
                bestVsE, bestV, bestPsE, bestP))
        
    for dsname, dslist in extendeddataset_dict.items():
        
        fig, axes = plt.subplots(1,2, figsize=plotting.cm2inch(15, 7))
        plot_relevant_model_points_over_searchradius(data, fig.axes[0], dslist)
        plot_mu_time_over_searchradius(data, fig.axes[1], dslist)
    
        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH)
            plotting.save_figure(fig, plt_dir, 'mu_times_%s' %dsname)
            
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
    