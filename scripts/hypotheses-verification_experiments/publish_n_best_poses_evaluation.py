#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/pose_verifying/publish_n_best_poses/publish_n_best_poses.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15 # rad
epsilonband_values = [ 0.02]
datasetNames = ['MBO', 'household_objects_multi_instance', 'geometric_primitives']
datasetSynonyms = ['MBO', 'Haushaltsgegenst\\"ande', 'primitive Gegenst\\"ande']

datasetDictionary = {dsName: datasetSynonyms[i] for i, dsName in enumerate(datasetNames)}

def plot_mu_v_over_mu_p(experiment_data, ax, show_legend, best_n_poses):
    """
    plot mu_v over mu_p for the given epsilonband value
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: eperiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    @type espilonband: float
    @param epsilonband: plot the values for the given epsilonband
    @type best_n_poses: int
    @param best_n_poses: plot the values for the given best_n_poses
    """ 
    muv_true = []
    muv_false = []
    mup_true = []
    mup_false = []
    print("epsilonband: %s" %(epsilonband))
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['datasetName']])
    #relevant_data =\
    #    [entry for entry in basic_data if entry[iDict['epsilonband']] == epsilonband]
        
    for entry in basic_data:
        for i in entry[iDict['poses']]:
            pose = entry[iDict['poses']][i]
            if i in range(best_n_poses):
                #print("muv: %s" % (pose['mu']['mu_v']))
                if pose['correct']:
                    muv_true.append(pose['mu']['mu_v'])
                    mup_true.append(pose['mu']['mu_p'])
                else:
                    muv_false.append(pose['mu']['mu_v'])
                    mup_false.append(pose['mu']['mu_p'])
    #print("mu_true: %s" % (mu_true))
    #print("mu_false: %s" % (mu_false))
    
    ax.grid()
    ax.plot(mup_false, muv_false, 'r.', label = r"verworfen")
    ax.plot(mup_true, muv_true, 'g.', label = r"verifiziert")
    ax.axis([0, 0.05, 0, 0.4])
    ax.set_xlabel(r'$\mu_\mathcal{P}$')
    ax.set_ylabel(r'$\mu_\mathcal{V}$')
    if show_legend:
        ax.legend(loc="upper right", title =r"Pose")
                    

def plot_correct_pose_barchart(experiment_data, datasetName, best_n_range, instances_per_scene, ax, show_legend):
    """
    at which position of the 'n_best_poses' are the correct poses. plot the number of poses over position
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print("Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" %(datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['datasetName']])
    
    relevant_data = \
        [entry for entry in base_data if entry[iDict['datasetName']] == datasetName]
    
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range
    
    # do the counting
    for posenumber in range(best_n_range):
        for entry in relevant_data:
            #print("entry-->poses: %s" % (len(entry)))
            for i in entry[iDict['poses']]:
                if i == posenumber:
                    pose = entry[iDict['poses']][i]
                    if pose['correct']:
                        #print("i: %s; pose->correct: %s" % (i, True))
                        list[i] += 1
                        
    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number/len(relevant_data))
        #print("i: %s, number: %s" % (i, number))
    ax.set_xlabel('$i$-te Lagehypothese')
    #ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0,1)
    datasetNameForChart = datasetDictionary[datasetName]
    ax.set_title(datasetNameForChart)
    if show_legend:
        ax.legend(loc="upper right", title ="Pose")
    

if __name__ == '__main__':
    
    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)
    
    # prepare
    mpl.rcParams.update(plotting.krone_thesis_default_settings)
    
    print("plotting bar chart of correct poses")
    bars, axes = plt.subplots(1,3, figsize=plotting.cm2inch(16,7), sharey=True)
    bars.axes[0].set_ylabel("Anteil korrekter Hypothesen")
    
    plot_correct_pose_barchart(data, 'MBO', 25, 1, bars.axes[0], show_legend=False)
    #plot_correct_pose_barchart(data, 'MBO', 10, 1, bars.axes[3], show_legend=False)
    plot_correct_pose_barchart(data, 'household_objects_multi_instance', 25, 2, bars.axes[1], show_legend=False)
    #plot_correct_pose_barchart(data, 'household_objects_multi_instance', 10, 2, bars.axes[4], show_legend=False)
    plot_correct_pose_barchart(data, 'geometric_primitives', 25, 2, bars.axes[2], show_legend=False)
    #plot_correct_pose_barchart(data, 'geometric_primitives', 20, 2, bars.axes[5], show_legend=False)
    bars.tight_layout()
    
    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(bars, plt_dir, 'barcharts_correct_poses')
        
    # show figures for checking Layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
    
