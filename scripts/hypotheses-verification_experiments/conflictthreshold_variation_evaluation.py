#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/hinterstoisser_pose_verifying/hinterstoisser_conflictthreshold/hinterstoisser_conflictthreshold.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
precisions_weight = 0.5

max_translation_error_relative = 0.1
max_rotation_error = pi / 15 # rad

datasetNames = [#'MBO', 'household_objects_multi_instance',
     'geometric_primitives']
datasetSynonyms = [#'MBO', 'Haushaltsgegenst\\"ande',
                   'primitive Gegenst\\"ande']
datasetDictionary = {dsName: datasetSynonyms[i] for i, dsName in enumerate(datasetNames)}

if __name__ == '__main__':
    
    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_correctness_from_verifier(data, max_translation_error_relative, max_rotation_error)
    post_processing.compute_verified(data)
    post_processing.compute_verification(data)
    
    mpl.rcParams.update(plotting.krone_thesis_default_settings)

    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "conflictthreshold"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ["dynamicParameters", "/verifier", "searchradius"],
                                      ['datasetName']])
        
    precisions = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['conflictthreshold'],
                                                    iDict['verification']],
                                                   data_extraction.average_precision)
        
    recalls = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['conflictthreshold'],
                                                    iDict['verification']],
                                                   data_extraction.average_recall)
        
    figures = plt.figure(figsize=plotting.cm2inch(15,10))
    ps_precision = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
    ps_recall = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
    ps_e = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
    ps_rr = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
    
    precision_dict = {ds:
                      {sr:
                       {a:
                        {cth: value for cth, value in precision_by_a}
                        for a, precision_by_a in precision_by_sr}
                       for sr, precision_by_sr in precision_by_ds}
                      for ds, precision_by_ds in precisions}
    
    recall_dict = {ds:
                      {sr:
                       {a:
                        {cth: value for cth, value in recall_by_a}
                        for a, recall_by_a in recall_by_sr}
                       for sr, recall_by_sr in recall_by_ds}
                      for ds, recall_by_ds in recalls}
    
    e_dict = {ds:
              {sr:
               {a: [] for a, recall_by_a in recall_by_sr}
               for sr, recall_by_sr in recall_by_ds}
              for ds, recall_by_ds in recalls}
    
    print("precision")
    ax = plt.subplot(221)
    for prec_dataset, precisions_by_dataset in precisions:
        for searchradius, precisions_by_searchradius in precisions_by_dataset:
            for angleDiffThresh, precisions_by_angleDiffThresh in precisions_by_searchradius:
                print("%s (%s, %s)"%(prec_dataset, searchradius, angleDiffThresh))
                for conflictthreshold, precision in precisions_by_angleDiffThresh:
                    precision_dict[prec_dataset][searchradius][angleDiffThresh][conflictthreshold] = precision
                ax.plot(*zip(*precisions_by_angleDiffThresh), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(searchradius, 1/angleDiffThresh), **ps_precision.next())
    
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    ax.grid()
    ax.set_xlabel('Konfliktgrenzwert')
    ax.set_ylabel('Genauigkeit')
    
    print("recall")
    ax_rec = plt.subplot(222)
    ax_e = plt.subplot(223)
    for rec_dataset, recalls_by_dataset in recalls:
        for searchradius, recalls_by_searchradius in recalls_by_dataset:
            for angleDiffThresh, recalls_by_angleDiffThresh in recalls_by_searchradius:
                print("%s (%s, %s)"%(rec_dataset, searchradius, angleDiffThresh))
                e_list = []
                for conflictthreshold, recall in recalls_by_angleDiffThresh:
                    precision = precision_dict[rec_dataset][searchradius][angleDiffThresh][conflictthreshold]
                    recall_dict[rec_dataset][searchradius][angleDiffThresh][conflictthreshold] = recall
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight/precision + (1-precisions_weight)/recall)
                    else:
                        e = 1
                    e_list.append((conflictthreshold, e))
                sorted_e_list = sorted(e_list, key=lambda x: (x[1], x[0]))
                print('the best effectivness is achieved with a conflictthreshold of %.2f (e: %.2f with precision=%.2f and recall=%.2f).'\
                       %(sorted_e_list[0][0],sorted_e_list[0][1],
                         precision_dict[rec_dataset][searchradius][angleDiffThresh][sorted_e_list[0][0]],
                         recall_dict[rec_dataset][searchradius][angleDiffThresh][sorted_e_list[0][0]]))
                ax_rec.plot(*zip(*recalls_by_angleDiffThresh), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$'%(searchradius, 1/angleDiffThresh), **ps_recall.next())
                ax_e.plot(*zip(*e_list), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$, %s'\
                          %(searchradius, 1/angleDiffThresh, datasetDictionary[rec_dataset]), **ps_e.next())
                    
    ax_rec.set_xlim(0,1)
    ax_rec.set_ylim(0,1)
    ax_rec.grid()
    ax_rec.set_xlabel('Konfliktgrenzwert')
    ax_rec.set_ylabel('Trefferquote')
    
    ax_e.set_xlim(0,1)
    ax_e.set_ylim(0,0.5)
    ax_e.grid()
    ax_e.set_xlabel('Konfliktgrenzwert')
    ax_e.set_ylabel('$E$ mit $\\beta=%.1f$'%precisions_weight)
        
    ax_e.legend(bbox_to_anchor=(1.15, 1), loc='upper left', borderaxespad=0., title='Parameter')
    
    truepositives = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['conflictthreshold'],
                                                    iDict['verification']],
                                                   data_extraction.verification_true_positives)

    instances_per_model_per_scene = {#datasetNames[0]: 1,
                                     #datasetNames[1]: 2,
                                     datasetNames[0]: 2}
    scenes_per_dataset = {#datasetNames[0]: 50,
                          #datasetNames[1]: 15,
                          datasetNames[0]: 20}
    models_per_scene = {#datasetNames[0]: 4,
                        #datasetNames[1]: 5,
                        datasetNames[0]: 5}
    
    fig_rr, ax = plt.subplots(1, 1, figsize=plotting.cm2inch(15,10))
    
    for dataset, tp_by_dataset in truepositives:
        print(dataset)
        recognizable = scenes_per_dataset[dataset] * models_per_scene[dataset] * instances_per_model_per_scene[dataset]
        for sr, tp_by_sr in tp_by_dataset:
            for a, tp_by_a in tp_by_sr:
                print('(%s, %s)' %(sr, a))
                rr_list = []
                for conflict_th, tps in tp_by_a:
                    recognition_rate = tps/recognizable
                    rr_list.append((conflict_th, recognition_rate))
                ax.plot(*zip(*rr_list), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$, %s'\
                          %(sr, 1/a, datasetDictionary[dataset]), **ps_rr.next())
    ax.set_ylim(0,3)
    ax.set_ylabel('Erkennungsrate')
    ax.set_xlabel('Konfliktgrenzwert')
    ax.grid()
    ax.legend(loc='lower right', title='Parameter')
    
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(figures, plt_dir, 'pUr_conflictthreshold')
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates')
        
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
    