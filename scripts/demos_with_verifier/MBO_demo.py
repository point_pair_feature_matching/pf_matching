# -*- coding: utf-8 -*-

"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure

# script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/Mian_Bennamoun_Owens/dataset.py'
D_DIST_ABS = 10


class MBODemoExperiment(ExperimentBase):
    """
    simple demo to show matching algorithm in action
    """

    def __init__(self, dataset_path, d_dist_abs):
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure()
        ExperimentBase.__init__(self, data_storage)

        self.dataset = self.load_dataset(dataset_path)
        self.d_dist_abs = d_dist_abs

    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect_with_verifier()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
                            'refPointStep': 1,
                            'publish_n_best_poses': 5,
                            }

        verifier_settings = {'show_results': True,
                             'publish_statistics': True,
                             'publish_mu_values': False,
                             'd_dist': 0.05,
                             'searchradius': 0.5,
                             'searchradius_is_relative': True,
                             'scenezbufferingthreshold': 0.5,
                             'zbufferingthreshold_is_relative': True,
                             'supportthreshold': 0.07,
                             'penaltythreshold': 0.13,
                             'use_verification_of_normals': True,
                             'angleDiffThresh_in_pi': 1/15,
                             'zbufferingresolution': 25,
                             'use_conflict_analysis': 0.01
                             }
        

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.wait_for_enter('-' * 40 + '\n'
                            'Pipeline started and configured.\n'
                            'View / change parameters manually through "rosrun rqt_reconfigure rqt_reconfigure".\n'
                            'Show pipeline setup via "rqt_graph".\n'
                            'Press enter to start matching...')

    def looping(self):

        # model training
        print ('-' * 40 + '\n' + 'Adding models...')
        for model_index in range(self.dataset.get_number_of_models()):
            self.add_model_for_verification(self.dataset, model_index, clear_matcher=False, clear_verifier=False)
        print('All models added.')

        # matching in scenes
        for scene_index in range(self.dataset.get_number_of_scenes()):

            self.interface.new_run()

            try:
                inp = input('-' * 40 + '\n' +
                            'Enter "x" to exit or any other string to match the next scene...')
                if 'x' in inp:
                    break
            except SyntaxError:
                pass

            self.add_scene_for_verification(self.dataset, scene_index)

    def end(self):
        self.wait_for_enter('-' * 40 + '\n' +
                            'Matching done. Press enter and the demo will clean up...')


if __name__ == '__main__':

    demo = MBODemoExperiment(DATASET_PATH, D_DIST_ABS)
    demo.run(wait_before_looping=False)
