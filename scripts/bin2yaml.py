#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

# other imports
import os
import tkFileDialog
import pf_matching.data_structure as ds


class Bin2YamlConverter(object):
    """
    convert the binary files with experiment data to yaml-style human readable form
    """

    @staticmethod
    def get_bin_files():
        """
        @brief ask user for the file to convert
        @return file names
        """

        options = {'defaultextension': '.bin',
                   'filetypes': [('all files', '.*'), ('binary experiment results', '.bin')],
                   'title': 'Select the bin-files to convert'}

        files = tkFileDialog.askopenfilenames(**options)
        if files:
            return files
        else:
            return None

    @staticmethod
    def convert_files(files):
        """
        convert the files from bin to yaml
        @param files: list of full file paths to convert
        @return: None
        """

        for i, load_file in enumerate(files):
            print('converting file %d of %d...' % (i + 1, len(files)))
            save_file = os.path.splitext(load_file)[0] + '.yaml'
            d = ds.ExperimentDataStructure()
            d.from_pickle(load_file)
            d.to_yaml(save_file)

        print('All conversions done.')

if __name__ == '__main__':
    converter = Bin2YamlConverter()
    files = converter.get_bin_files()
    if files is not None:
        converter.convert_files(files)
    else:
        print('No valid file names.')
