# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *

# other imports
import os
import subprocess
import tkFileDialog


class Model_converter(object):
    """
    @brief Converts meshes of models to a form that can be used for pair-feature matching,
           using the provided Meshlab-script

    @note This is just a quick-and-dirty implementation not tested for a lot of wrong inputs
          or failiure cases.

    The conversion via Meshlab:
    * converts the file to .ply
    * samples points on all the mesh's faces
    * deletes the mesh
    * calculates and saves normals
    * Edits the ply-file's header, so that it's readable with the PCL:io functions
    """

    def __init__(self):
        """
        @brief construction, nothing special
        """

        # get the script's directory
        self._last_dir = os.path.dirname(os.path.realpath(__file__))

    def convertModels(self):
        """
        @brief Do the conversion process!
        """

        # ask for all required files / dirs
        script_file = self._getMeshlabScript()
        if not script_file:
            raise IOError('No script selected.')
        model_files = self._getSourceFiles()
        if not model_files:
            raise IOError('No model files selected.')
        save_dir = self._getSaveDir()
        if not save_dir:
            raise IOError('No save directory selected.')

        # convert models
        for model_file in model_files:

            # construct file names
            base_name = os.path.splitext(os.path.basename(model_file))[0]
            save_name = os.path.join(save_dir, base_name + "_with_normals.ply")

            # process model via meshlab
            # LC_ALL=C overrides the language environment, otherwise, there might
            # be loading problems with 0 loaded faces / vertices because
            # meshlabserver expects a ',' instead of a '.' as the decimal divider
            meshlab_command = "LC_ALL=C meshlabserver -i %s -o %s -s %s -om vn" %\
                              (model_file, save_name, script_file)
            p = subprocess.Popen(meshlab_command, shell=True)
            p.wait()

            # make sure the script ran through properly
            assert(os.path.exists(save_name))

            #replace nx with normal_x etc. in file header
            replacements = {'nx': 'normal_x', 'ny': 'normal_y', 'nz': 'normal_z'}
            lines = []
            replacements_done = 0
            with open(save_name) as infile:
                for line in infile:
                    if replacements_done < 3:
                        for src, target in replacements.iteritems():
                            line = line.replace(src, target)
                            replacements_done += 1
                            break
                    lines.append(line)
            with open(save_name, 'w') as outfile:
                for line in lines:
                    outfile.write(line)
            print("Normal names replaced.")

            print("Done.")

    def _getSourceFiles(self):
        """
        @brief ask user for the meshes to be converted
        @return tuple of filenames or None
        """
        options = {}
        options['defaultextension'] = '.ply'
        options['filetypes'] = [('all files', '.*'), ('PLY-files', '.ply')]
        options['initialdir'] = self._last_dir
        options['title'] = 'Please select the models to be processed.'
        files = tkFileDialog.askopenfilenames(**options)
        if files:
            self._last_dir = os.path.dirname(files[0])
            return files
        else:
            return None

    def _getSaveDir(self):
        """
        @brief ask user for the directory to save point clouds to
        @return directory name
        """
        options = {}
        options['initialdir'] = self._last_dir
        options['mustexist'] = False
        options['title'] = "Please select the directory for saving processed models to."
        save_dir = tkFileDialog.askdirectory(**options)
        self._last_dir = save_dir
        return save_dir

    def _getMeshlabScript(self):
        """
        @brief ask the user for the meshlab script file for conversion
        @return tuple of 1 script file, with return[0] being the script name
        """
        options = {}
        options['defaultextension'] = '.mlx'
        options['filetypes'] = [('Meshlab script', '.mlx')]
        options['initialdir'] = self._last_dir
        options['title'] = 'Please select the Meshlab script for processing.'
        filename = tkFileDialog.askopenfilename(**options)
        self._last_dir = os.path.dirname(filename)
        return filename

if __name__ == "__main__":
    m = Model_converter()
    m.convertModels()
