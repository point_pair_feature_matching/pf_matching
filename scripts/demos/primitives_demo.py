# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure

# script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
D_DIST_ABS = 0.008


class PrimitivesDemoExperiment(ExperimentBase):
    """
    simple demo to show matching algorithm in action including rotational symmetry
    """

    def __init__(self, dataset_path, d_dist_abs):
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure()
        ExperimentBase.__init__(self, data_storage)

        self.dataset = self.load_dataset(dataset_path)
        self.d_dist_abs = d_dist_abs

    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'remove_NaNs': True,
                                       'd_points': self.d_dist_abs,
                                       'd_points_is_relative': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': True,
                            'd_dist': 0.05,
                            'd_dist_is_relative': True,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'use_rotational_symmetry': True,
                            'collapse_symmetric_models': True
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

        self.wait_for_enter('-' * 40 + '\n'
                            'Pipeline started and configured.\n'
                            'View / change parameters manually through "rosrun rqt_reconfigure rqt_reconfigure".\n'
                            'Show pipeline setup via "rqt_graph".\n'
                            'Press enter to start matching...')

    def looping(self):



        # model training
        print ('-' * 40 + '\n' + 'Adding models...')
        for model_index in range(self.dataset.get_number_of_models()):
            self.add_model(self.dataset, model_index, clear_matcher=False)
        print('All models added.')

        # matching in scenes
        for scene_index in range(self.dataset.get_number_of_scenes()):

            self.interface.new_run()

            try:
                inp = input('-' * 40 + '\n' +
                            'Enter "x" to exit or any other string to match the next scene...')
                if 'x' in inp:
                    break
            except SyntaxError:
                pass

            self.add_scene(self.dataset, scene_index)

    def end(self):
        self.wait_for_enter('-' * 40 + '\n' +
                            'Matching done. Press enter and the demo will clean up...')


if __name__ == '__main__':

    demo = PrimitivesDemoExperiment(DATASET_PATH, D_DIST_ABS)
    demo.run(wait_before_looping=False)
