#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler


# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Watchdog script to check if specified python process is still running. 
                 Shuts down computer when process has finished.
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

complete_processname="python $1" # this "python" word here is crucial! if one wants to watch other 
                                 # types of scripts, one would have to change it accordingly.
                                 
if  [  -n "$1" ]; then # if the first argument exists
	
	echo " The process to watch is: $complete_processname "
	status=$(pgrep -af "$complete_processname")
	echo " The process with name $complete_processname has the process-id: $status"
	
	while [ -n "$status" ];	do # as long as status exists, i.e. as long as pgrep does return an ID
		sleep 60		
		echo " Process is still running. Check status of process again in 60 seconds."
		status=$(pgrep -af "$complete_processname")
	done; 
	
	echo " Process has finished."
	echo "${red} ${bold}Shut down computer.${reset}"
	sudo poweroff
		
else
	echo "Argument is missing. Whatchdog will not be initialized."
fi
