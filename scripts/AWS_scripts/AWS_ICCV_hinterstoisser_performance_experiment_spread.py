# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
import sys

# import from project
from pf_matching.experiments.aws_experiment_interface import AWSExperimentInterface_spreaded
import instance_config

SAVE_FILE_NAME = 'ICCV_hinterstoisser_performance.bin'     # TODO: this separation of file-name and file-directory is due to a big in 'fabric 2.3.1'. bug is fixed and the next release will contain the fix
SAVE_DIRECTORY_REMOTE = '/home/ubuntu/ROS/experiment_data/ICCV_hinterstoisser_performance/' # TODO: always give absolute path, no ~
SAVE_DIRECTORY_LOCAL = '~/ROS/experiment_data/ICCV_hinterstoisser_performance/'             # TODO: here we can use ~ to specify path, because we resolve that later.
EXPERIMENT_SCRIPT_PATH_REMOTE = '~/ROS/catkin_workspace/src/pf_matching/scripts/visCon_and_hinterstoisser_experiments/ICCV_hinterstoisser_performance_experiment.py' # TODO: here we can use ~ to specify path, because we resolve that later.
PATH_TO_SSH_PRIVATE_KEY_FILE_LOCAL = instance_config.key
INSTANCE_ID = instance_config.instance_id_list_verifier_tuning                      # TODO: instance ID of the AMI
USERNAME = 'ubuntu'                                                                 # TODO: change username if necessary. depends on the AMI you are using
WATCHDOG_FILE_NAME = 'AWS_self_shutdown.sh'
WATCHDOG_PATH_REMOTE = '/home/ubuntu/'                                              # TODO: always give absolute path, no ~
WATCHDOG_PATH_LOCAL = '~/ROS/catkin_workspace/src/pf_matching/scripts/AWS_scripts/' # TODO: here we can use ~ to specify path, because we resolve that later.
 
    
if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] =="merge_downloaded_files":
        e = AWSExperimentInterface_spreaded(EXPERIMENT_SCRIPT_PATH_REMOTE,
                                            INSTANCE_ID,
                                            PATH_TO_SSH_PRIVATE_KEY_FILE_LOCAL,
                                            USERNAME,
                                            SAVE_DIRECTORY_REMOTE,
                                            SAVE_DIRECTORY_LOCAL,
                                            SAVE_FILE_NAME,
                                            WATCHDOG_FILE_NAME,
                                            WATCHDOG_PATH_REMOTE,
                                            WATCHDOG_PATH_LOCAL)
        mode = sys.argv[1]
        if mode=="merge_downloaded_files":
            e.merge_downloaded_files_of_experiment_parts()
        else:
            print ("Unkown argument!")
    else:
        if len(sys.argv) != 3:
            print ("\nArgument is missing, or is too much. Submit TWO arguments. Choose from\n\
                    - start + machine_index --> start the experiment and subsequently logout\n\
                    - kill + machine_index --> abort the experiment.\n\
                    - download + machine_index --> try downloading experiment data.\n\
                    - stop_ami + machine_index --> check if experiment is still running, otherwise stop AMI\n\
                    - update_ami + machine_index --> update git repo on instance and compile\n\
                    - htop + machine_index --> show htop performance monitor of instance\n\
                    - tail + machine_index --> show console-output of experiment. Not in realtime!\n\
                    - show_ini + machine_index --> show spreading_info.ini file on instance to check if job_id equals machine_index\n\
                    The machine_index starts at 0!\n\
                    For merging the downlaoded data-files use argument 'merge_downloaded_files'")


        else:
            e = AWSExperimentInterface_spreaded(EXPERIMENT_SCRIPT_PATH_REMOTE,
                                                INSTANCE_ID,
                                                PATH_TO_SSH_PRIVATE_KEY_FILE_LOCAL,
                                                USERNAME,
                                                SAVE_DIRECTORY_REMOTE,
                                                SAVE_DIRECTORY_LOCAL,
                                                SAVE_FILE_NAME,
                                                WATCHDOG_FILE_NAME,
                                                WATCHDOG_PATH_REMOTE,
                                                WATCHDOG_PATH_LOCAL)

            mode = sys.argv[1]
            machine_idx = int(sys.argv[2])
            if mode == 'start':
                e.startExperiment(machine_idx)
            # elif mode == 'check':
            #     e.checkoutExperiment()
            # elif mode == 'track':
            #     e.trackExperiment()
            elif mode == 'stop_ami':
                e.stopAMI(machine_idx)
            elif mode == 'kill':
                e.killExperiment(machine_idx)
            elif mode == 'download':
                e.downloadExperimentData(machine_idx)
            # elif mode == 'start_watchdog':
            #     e.startRemoteWatchdog(True)
            # elif mode == 'stop_watchdog':
            #     e.killRemoteWatchdog(True)
            elif mode=='update_ami':
                e.updateAWSinstance(machine_idx)
            elif mode == 'htop':
                e.htop(machine_idx)
            elif mode=='tail':
                e.tail(machine_idx)
            elif mode=='show_ini':
                e.show_ini(machine_idx)
            elif mode=="merge_downloaded_files":
                e.merge_downlaoded_files_of_experiment_parts()
            else:
                print ("Unkown argument!")
