
*********************************************************************
                    purpose of the AWS scripts:
*********************************************************************

They provide an easy CLI to start, track, stop, ... experiments on an AWS EC2 instancs (remote computer).

The AWS-scripts basically start the "real" experiment-scripts form the folder "visCon_and_hinterstoisser_experiments"
on the remote computer, a.k.a. the AWS EC2 instance. For that, they establish ssh connections. 

Alternatively, you could also start the scripts located in "visCon_and_hinterstoisser_experiments" manually on the
remote computer. But be aware, that you have to download the result-files manually as well. Even more important:
Make sure you stop the EC2 instance, when an experiment is done. Otherwise you will generate unnecessary costs.

*********************************************************************
            prerequisites for the AWS scripts to work:
*********************************************************************

We need to create the credentials-file and the config file under:

		mkdir ~/.aws
		touch ~/.aws/credentials
		touch ~/.aws/config

	the content of these files is:

	#credentials-file:
	[default]
	aws_access_key_id = 
	aws_secret_access_key = 

	#config-file:
	[default]
	region=eu-central-1 #OR region=us-east-1 #depending on the region you've chosen

	To get the values for access_key_id and secret_access_key, we have to create them on the AWS account-website first.
	Clicking on "My Security Credentials" and confirming "Continue to Security Credentials". We then need to create a 
        new "access keys"-pair. We download that .csv-file and store it in a secure place. This file contains both of the 
	above needed values for the credentials-file.

The next step is to install pip:

	sudo apt-get install python-pip

Having installed that, we are now able to install the "Boto3" python package, which is the AWS python SDK.

	sudo pip install boto3

This SDK enables us to start and stop the AWS instance from within python code.

*********************************************************************
                             IMPORTANT!
*********************************************************************

1. Modify the instance_config.py in the AWS_scripts directory! It is the config-file that all AWS-scripts use to establish
   an ssh connection to your EC2 instance.

2. In the AWS-scripts, edit all the first lines that are marked with "TODO". This is necessary especially if your installation-path
   differs from the standard, which is "~/ROS/catkin_workspace/src/"
