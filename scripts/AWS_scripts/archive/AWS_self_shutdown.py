# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *

import time
import sys
import os
from subprocess import check_output, CalledProcessError


def getProcessIDs(script_name):
    # separate the name of the script from the path
    if script_name.count('/') > 0:
        script_name = script_name.rsplit('/',1)[1]
    # in parts from https://stackoverflow.com/a/35938503
    try:
        # get output of 'pgrep -f' of remote system to get the process-id
        result = check_output(["pgrep", script_name])
        result = result.strip()
        process_ids = list(map(int, result.split()))
    except  CalledProcessError:
        process_ids = []
    print ('List of PIDs associated with process-name = ' + script_name +': ' + ', '.join(str(e) for e in process_ids))
    return process_ids

def checkIfExperimentIsStillRunning(script_name):
    process_ids = getProcessIDs(script_name)
    if len(process_ids) == 0:
        print("Zero processes with name %s were found. Experiment might be done." %script_name)
        return 0 #false
    elif len(process_ids) >= 2:
        print("Too many processes with name %s were found" %script_name)
        return -1
    else:    
        # process is found, it has an id. ergo it has not finished yet.
        print("Process with name %s was found. Experiment is still running!" %script_name)
        return 1 #true

###############################################################################
#                                main part
###############################################################################
if __name__ == '__main__':
    """
    This srcipt lets the EC2 instance stop itself, after the specified experiment-script has finished.
    I.e. this has to be run on the EC2 instance, not locally.
    
    This conducts a STOP by default, NO TERMINATION, if instance is not configured accordignly. 
    for termination configure --instance-initiated-shutdown-behavior or the equivalent on the 
    AWS console or API call.
    
    Use like this: Start the experiment on the remote from the local computer. Then, if wanted, start 
                   this script from the local cumputer on the remote computer as well. Mind passing 
                   the correct name of the experiment script, otherwise the shutdown will fail. 
    """

    if len(sys.argv) != 2:
        print ('\nArgument is missing, or is too much.\nGive path_of_script as argument,' + 
               'after which the EC2 instance shall shut itself down.\n')
    
    else:
        script_name = sys.argv[1]
        status = checkIfExperimentIsStillRunning(script_name)
        
        # check status til experiment is done.
        while status == 1 or status == -1:
            time.sleep(60) # wait x seconds
            status = checkIfExperimentIsStillRunning(script_name)
        
        # stop the instance, given that it costs money each second it is running
        os.system("sudo /home/ubuntu/stop_instance.sh")
        