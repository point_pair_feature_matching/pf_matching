

import sys
import boto3
from botocore.exceptions import ClientError

instance_id = sys.argv[2]
action = sys.argv[1].upper()

client = boto3.client('ec2')

# from https://boto3.readthedocs.io/en/latest/guide/ec2-example-managing-instances.html
if action == 'ON':
    # Do a dryrun first to verify permissions
    try:
        client.start_instances(InstanceIds=[instance_id], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, run start_instances without dryrun
    try:
        response = client.start_instances(InstanceIds=[instance_id], DryRun=False)
        print(response)
    except ClientError as e:
        print(e)
else:
    # Do a dryrun first to verify permissions
    try:
        client.stop_instances(InstanceIds=[instance_id], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, call stop_instances without dryrun
    try:
        response = client.stop_instances(InstanceIds=[instance_id], DryRun=False)
        print(response)
    except ClientError as e:
        print(e)

# from https://stackoverflow.com/a/46956969
if action == 'ON':
	waiter = client.get_waiter('instance_status_ok')
	waiter.wait(InstanceIds=[instance_id])
	print("The instance is now up and running.")
else:
	waiter = client.get_waiter('instance_stopped')
	waiter.wait(InstanceIds=[instance_id])
	print("The instance is now stopped.")
