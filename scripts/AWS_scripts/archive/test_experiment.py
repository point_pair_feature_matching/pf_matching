import time

print ('Hello World! This is a script that runs for a specified amount of time.')

# set goal of total amount of time the script shall run:
minutes = 0.5
t_start = time.time()
t_end = t_start + 60 * minutes
i = 1
while time.time() < t_end:
	
	if time.time() == (t_start + (10 * i)):
		elapsed = i/6.0
		print ("Script has been running for %f minutes now. "\
		       "Script will finish after a total amount of %s minutes."%(elapsed, minutes))
		
		i = i+1
