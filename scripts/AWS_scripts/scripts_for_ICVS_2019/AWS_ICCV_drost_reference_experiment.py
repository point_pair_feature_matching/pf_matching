# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
import time
import sys
import os

# import from project
from pf_matching.experiments.aws_experiment_interface import AWSExperimentInterface
import instance_config

SAVE_FILE_NAME = 'ICCV_drost_reference_SIXD_config.bin'     # TODO: this separation of file-name and file-directory is due to a big in 'fabric 2.3.1'. bug is fixed and the next release will contain the fix
SAVE_DIRECTORY_REMOTE = '/home/ubuntu/ROS/experiment_data/ICVS_2019/ICCV_drost_reference/' # TODO: always give absolute path, no ~
SAVE_DIRECTORY_LOCAL = '~/ROS/experiment_data/ICVS_2019/ICCV_drost_reference/'             # TODO: here we can use ~ to specify path, because we resolve that later.
EXPERIMENT_SCRIPT_PATH_REMOTE = '~/ROS/catkin_workspace/src/pf_matching/scripts/visCon_and_hinterstoisser_experiments/scripts_for_ICVS_19_paper/ICCV_drost_reference_experiment.py' # TODO: here we can use ~ to specify path, because we resolve that later.
PATH_TO_SSH_PRIVATE_KEY_FILE_LOCAL = instance_config.key
INSTANCE_ID = instance_config.instance_id_viscon_hinterstoisser            	        # TODO: instance ID of the AMI
USERNAME = 'ubuntu'                                                                 # TODO: change username if necessary. depends on the AMI you are using
WATCHDOG_FILE_NAME = 'AWS_self_shutdown.sh'
WATCHDOG_PATH_REMOTE = '/home/ubuntu/'                                              # TODO: always give absolute path, no ~
WATCHDOG_PATH_LOCAL = '~/ROS/catkin_workspace/src/pf_matching/scripts/AWS_scripts/' # TODO: here we can use ~ to specify path, because we resolve that later.
 
    
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("\nArgument is missing, or is too much. Choose ONE from\n\
                - start --> start the experiment and subsequently logout\n\
                - check --> check if the experiment is still running\n\
                - track --> log in into the remote console PERMANENTLY to keep track detailed status\n\
                - kill  --> abort the experiment.\n\
                - download --> try downloading experiment data.\n\
                - stop_ami --> check if experiment is still running, otherwise stop AMI\n\
                - start_watchdog --> start watchdog on remote computer manually, in case it is not running\n\
                - stop_watchdog  --> stop watchdog on remote computer manually\n\
                - update_ami --> update git repo on instance and compile\n\
                - htop --> show htop performance monitor of instance\n\
                - tail --> show console-output of experiment. Not in realtime!")
    else:
        e = AWSExperimentInterface(EXPERIMENT_SCRIPT_PATH_REMOTE,
                                   INSTANCE_ID,
                                   PATH_TO_SSH_PRIVATE_KEY_FILE_LOCAL,
                                   USERNAME,
                                   SAVE_DIRECTORY_REMOTE,
                                   SAVE_DIRECTORY_LOCAL,
                                   SAVE_FILE_NAME,
                                   WATCHDOG_FILE_NAME,
                                   WATCHDOG_PATH_REMOTE,
                                   WATCHDOG_PATH_LOCAL)

        mode = sys.argv[1]
        if mode == 'start':
            e.startExperiment()
        elif mode == 'check':
            e.checkoutExperiment()
        elif mode == 'track':
            e.trackExperiment()
        elif mode == 'stop_ami':
            e.stopAMI()
        elif mode == 'kill':
            e.killExperiment()
        elif mode == 'download':
            e.downloadExperimentData()
        elif mode == 'start_watchdog':
            e.startRemoteWatchdog(True)
        elif mode == 'stop_watchdog':
            e.killRemoteWatchdog(True)
        elif mode == 'update_ami':
            e.updateAWSinstance()
        elif mode == 'htop':
            e.htop()
        elif mode == 'tail':
            e.tail()
        else:
            print("Unkown argument!")
