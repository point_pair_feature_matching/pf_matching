# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler


# TODO: modify these two variables accordingly
# ID of the AWS EC2 instance:
# ICCV drost reference experiment: 'i-03f31ff4805c039be' 
# ICCV hintstoisser publish n best poses experiment: 'i-0500e002fe914ba7a' 
# for ICCV hinterstoisser with d_dist = 0.025, i.e. large RAM 'i-0bcdd81e1ed0cd66f' 
instance_id = 'i-018461e2d6ff499f1'
# path to the private ssh-key that matches the public ssh-key on the EC2 instance:
key = '~/.ssh/MasterarbeitAWSkeyNvirginia.pem' #'~/.ssh/MasterarbeitAWSkey.pem'

instance_id_hinterstoisser_influence_of_extension_packages = 'i-0500e002fe914ba7a' #m5.large

# you can use a whole set of IDs to spread an experiment across them
# instance-list for ICCV Hodans subset verifier tuning with more RAM (t3.medium)
instance_id_list_verifier_tuning = ['i-018461e2d6ff499f1',
                                    'i-02bdb38554cf7ac97',
                                    'i-08d303a8ed34ac468',
                                    'i-0a0d9f0710a2138f6',
                                    'i-0bce17047d6960f03',
                                    'i-0f94ebc814e075ead']

# instance-list for VisCon Parameter Tuning (t3.small)
instance_id_list_viscon_tuning = ['i-0596556671c42b889',
                                   'i-07b589070e56b7872',
                                   'i-0982aa3168ab92321',
                                   'i-0c03b703c3eb88a8f',
                                   'i-0dadb92e23e7c74f3']

# instance for VisCon+Hinterstoisser Performance (r5.2xlarge)
instance_id_viscon_hinterstoisser = 'i-0596556671c42b889' # for cuboid and elliptic_prism and pyramid
#instance_id_viscon_hinterstoisser = 'i-07b589070e56b7872' # for cylinder, hex-frustum
