# you need to specify the following variables for your case:
dataset="geometric_primitives" #"OcclusionChallengeICCV2015" #"geometric_primitives" # dataset you want to copy to the AWS machine

## Prerequesites: 
# - do the prerequisites in dataset_installation_script_local_side and run that script.
# - then run this script here on the remote computer.

# 1. change file permissions for all of the datasets' sub-folders
sudo chmod -R a+x+w+r /home/ubuntu/ROS/datasets/"$dataset"/



