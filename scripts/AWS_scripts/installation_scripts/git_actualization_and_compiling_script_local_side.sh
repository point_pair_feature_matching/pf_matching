# you need to specify the following variables for your case:
remote_ip="34.238.176.89"               		# public ip of the AWS machine
sshkey="/home/markus/.ssh/MasterarbeitAWSkeyNvirginia.pem"  	# private key file that matches the public key file on your AWS machine
# you also need to have the metslib files on your local machine. specify their path on your local machine:
metslib_files="/usr/include/pcl-1.7/pcl/recognition/3rdparty/"  # you need them for a successful compilation, but unfortunately
								# they are not included in the ros-perception package
								# you can download them from the git repo of PCL

## Prerequisites: 
# - create AWS ubuntu 14.04 server Instance and then enable internet access
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html?icmpid=docs_ec2_console
# https://docs.aws.amazon.com/de_de/vpc/latest/userguide/VPC_Internet_Gateway.html
# - you can use this script manually, or you can start the updating process with the 
#   aws-experiment-interface-class function updateAMIinstance()

# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


# 2. execute remote-side-script via ssh session
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 copying remote-side-scripts to remote computer to the home directory
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo scp -i $sshkey git_actualization_and_compiling_script_remote_side.sh ubuntu@"$remote_ip":/home/ubuntu/

# 2. execute remote-side-script via ssh session
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 establishing ssh-connection to remote computer 
 next step: manually start git_actualization_and_compiling_script_remote_side.sh script
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo ssh -i $sshkey ubuntu@"$remote_ip"

