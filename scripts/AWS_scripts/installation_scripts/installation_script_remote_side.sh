# you need to specify the following variables for your case:
bitbucket_username="phil_rudd"          		# username of your bitbucket accout, from where to download the git-repositories
git_username="Markus Ziegler"				# for the git config. not so important.
git_useremail="markus.f.ziegler@campus.tu-berlin.de"  	# for the git config. not so important.


## Prerequisites: execute installation_script_local_side on your local machine
## that script will store this script on the remote computer.
## then you have to open a ssh session and start this script.

# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


# 1. installing ros indigo as discribed in:	
# http://wiki.ros.org/indigo/Installation/Ubuntu
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing ros indigo
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

#	1.1 Configure Ubuntu repositories to include universe, multiverse and restricted:
sudo add-apt-repository 'deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse' 

#	1.2 setup the sources.list:
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

#	1.3 setup the keys:
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

#	1.4 update the Debian package index:
sudo apt-get update

#   	1.5 you want to install ROS on remote cloud computer, i.e. don't need GUIs:
		
sudo apt-get install ros-indigo-ros-base
sudo apt-get install ros-indigo-perception-pcl
	
#   	1.6 initialize rosdep:
sudo rosdep init
rosdep update
	
#   	1.7 set up environment, so that bash can find the ros-commands:
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
source ~/.bashrc

#   	1.8 bashrc is not loaded when logging in via ssh. instead source bashrc in profile
echo "if [ -f ~/.bashrc ]; then" >> ~/.profile
echo "  . ~/.bashrc" >> ~/.profile
echo "fi" >> ~/.profile
# this might have the problem, that bashrc is run, but it might not run till the end of the file.
# and all my lines I added to bashrc, are appended to the file's end.
# so: instead: put the required line upfront with vim in ex-mode: https://superuser.com/a/1063882
# ex -sc '1i|task goes here' -cx todo.txt
# I am prepending all the lines, that are appended to the .bashrc during this scripts execution, 
# to the .bashrc at the end of this script in step #16 ---> NO! doing it right now:
ex -sc '4i|' -cx ~/.bashrc
ex -sc '5i|# ROS stuff:' -cx ~/.bashrc
ex -sc '6i|source /opt/ros/indigo/setup.bash' -cx ~/.bashrc
ex -sc '7i|source ~/ROS/catkin_workspace/devel/setup.bash' -cx ~/.bashrc
ex -sc '8i|export LC_ALL="C"' -cx ~/.bashrc
#ex -sc '9i|rospack profile' -cx ~/.bashrc
ex -sc '10i|export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/' -cx ~/.bashrc
ex -sc '11i|' -cx ~/.bashrc

# 2. install cmph
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing cmph
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
mkdir ~/Downloads
cd ~/Downloads
wget "https://sourceforge.net/projects/cmph/files/cmph/cmph-2.0.tar.gz"
gunzip cmph-2.0.tar.gz 
tar xvf cmph-2.0.tar
cd cmph-2.0
./configure
make
make check
sudo make install
sudo make installcheck
make clean
make distclean
cd ..
rm -r cmph-2.0 cmph-2.0.tar 
cd ~
echo '# bugfix for CMPH lib:' >> ~/.profile
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/' >> ~/.profile # to prevent error "could not find libcmph.so.O"

# 3. install git and make some config-changes:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing git
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo apt-get install git
git config --global user.name $git_username
git config --global user.email $git_useremail

# 4. create a swap file of size 4096 MB to prevent processes from stopping due to a lack of RAM:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 instanciate a swap file
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo swapoff -a
sudo dd if=/dev/zero of=/var/swapfile bs=1M count=4096 &&
sudo chmod 600 /var/swapfile &&
sudo mkswap /var/swapfile && 
echo /var/swapfile none swap defaults 0 0 | sudo tee -a /etc/fstab && # make sure the swapfile is recognized after each instance-reboot
sudo swapon -a

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 about to log out ...
 next step: please re-login via ssh and execute the script 
            "installation_script_remote_side_part_2.sh" manually now!
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo chmod a+x+w+r installation_script_remote_side_part_2.sh
logout
#./installation_script_remote_side_part_2.sh
