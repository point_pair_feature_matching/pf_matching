# you need to specify the following variables for your case:
remote_ip="18.233.150.51"               		# public ip of the AWS machine
sshkey="/home/markus/.ssh/MasterarbeitAWSkeyNvirginia.pem"  	# private key file that matches the public key file on your AWS machine
dataset="geometric_primitives" #"OcclusionChallengeICCV2015" #"geometric_primitives"				# dataset you want to copy to the AWS machine

## Prerequesites: 
# - create new EBS volume to hold the dataset-files
#   https://docs.aws.amazon.com/de_de/AWSEC2/latest/UserGuide/ebs-creating-volume.html
# - attach the EBS volume to the EC2 instance
#   https://docs.aws.amazon.com/de_de/AWSEC2/latest/UserGuide/ebs-attaching-volume.html
# - mount the EBS volume with the mount-point:
#   /home/ubuntu/ROS/datasets/
#   https://docs.aws.amazon.com/de_de/AWSEC2/latest/UserGuide/ebs-using-volumes.html
# - make sure you have the dataset you want to copy stored locally under ~/ROS/datasets/

# 1. upload dataset files to remote computer
scp -i $sshkey -r  ~/ROS/datasets/"$dataset" ubuntu@"$remote_ip":/home/ubuntu/ROS/datasets/"$dataset"/

# 1.a alternatively run this command if the filetransfer was interrupted, e.g. the ssh-connection was lost.
#     In this case you want to use rsync, because it skipps already existing files. "scp" always overwrites every file.
#     For very large datasets with a slow upload-rate, using "scp" would be insane. But rsync seems to be only half as
#     fast as scp regarding the upload-rate. :'-(
#rsync -av --ignore-existing -Pav -e "ssh -i $sshkey" ~/ROS/datasets/"$dataset"/* ubuntu@"$remote_ip":/home/ubuntu/ROS/datasets/"$dataset"/

# 2. to modify the file permissions, upload the remote-script: 
scp -i $sshkey ./dataset_installation_script_remote_side.sh ubuntu@"$remote_ip":/home/ubuntu/ROS/datasets/"$dataset"/

# 3. then open ssh-connection
ssh -i $sshkey ubuntu@"$remote_ip"

# 4. manually start the associated shell-script on the remote computer to modify the permissions
