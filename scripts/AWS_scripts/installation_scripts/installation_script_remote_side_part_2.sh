# you need to specify the following variables for your case:
bitbucket_username="phil_rudd"          		# username of your bitbucket accout, from where to download the git-repositories
git_username="Markus Ziegler"				# for the git config. not so important.
git_useremail="markus.f.ziegler@campus.tu-berlin.de"  	# for the git config. not so important.


## Prerequisites: execute installation_script_local_side on your local machine
## that script will store this script on the remote computer.
## This script "part_2" will be automatically called by the "part_2" script.
## You do not need to execute this script by yourself.

# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


## Continuing with part 2 of the installation script. this splitting is necessary to call the bash again.

# 5. create the "catkin_workspace" workspace, which will contain all the source-code from the ROS-programming-project:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 creating the 'catkin workspace'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
#	4.1 create the folder-structure. src is the place, where the git-code is cloned to:	
mkdir -p ~/ROS/catkin_workspace/src 

#	4.2 get to the src folder		
cd ~/ROS/catkin_workspace/src/

#	4.3 init the current path as the current (?) workspace. as a result, a CMakeLists.txt link shows up (don't know where exactly):
catkin_init_workspace

# 6. clone the pf_matching_core repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downloading 'pf_matching_core'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching_core.git
cd ~/ROS/catkin_workspace/src/pf_matching_core/
git checkout matching_of_geometrically_simple_objects_PCL_1.7.2

# 7. move the missing metslib files from home directory to their real destination
sudo mv ~/3rdparty /usr/include/pcl-1.7/pcl/recognition/
sudo rm -r ~/3rdparty 

# 8. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace":
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching_core'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/ 
catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS_TYPE=-O2


# 9. add the commands to the bash permanently, so that they are available from every terminal right from the start:
echo "source ~/ROS/catkin_workspace/devel/setup.bash" >> ~/.bashrc 
echo "echo 'catkin environment sourced'" >> ~/.bashrc

# 10. clone the pf_matching_tools repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downloading 'pf_matching_tools'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/src/
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching_tools.git
cd ~/ROS/catkin_workspace/src/pf_matching_tools/
git checkout matching_of_geometrically_simple_objects

# 11. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace":
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching_tools'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/
catkin_make
source ~/ROS/catkin_workspace/devel/setup.bash 
export LC_ALL="C"   #this is needed for rospack profile command to work within ssh session
rospack profile

# 12. clone the pf_matching repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downlaoding 'pf_matching'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/src/
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching.git
cd ~/ROS/catkin_workspace/src/pf_matching/
git checkout matching_of_geometrically_simple_objects

# 13. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace"
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/ 
catkin_make 
source ~/ROS/catkin_workspace/devel/setup.bash 
export LC_ALL="C"
rospack profile

# 14. add lines to bashrc to enable autocompletion for ros
echo 'export LC_ALL="C"' >> ~/.bashrc
#echo "rospack profile" >> ~/.bashrc

# 15. prepend all previously to .bashrc appended lines to .bashrc
# this is a pretty quick and dirty hack. because 'source /opt/ros/indigo/setup.bash'
# will generate a lot of output. and this output will be generated every time a fabric2::run()
# is ececuted. console gets pretty polluted with that output.
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 doing some additional stuff
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
#ex -sc '4i|' -cx ~/.bashrc
#ex -sc '5i|# ROS stuff:' -cx ~/.bashrc
#ex -sc '6i|source /opt/ros/indigo/setup.bash' -cx ~/.bashrc
#ex -sc '7i|source ~/ROS/catkin_workspace/devel/setup.bash' -cx ~/.bashrc
#ex -sc '8i|export LC_ALL="C"' -cx ~/.bashrc
##ex -sc '9i|rospack profile' -cx ~/.bashrc
#ex -sc '10i|export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/' -cx ~/.bashrc
#ex -sc '11i|' -cx ~/.bashrc

sudo rosdep fix-permissions

# 16. make some addtional directories
mkdir /home/ubuntu/Documents/
mkdir /home/ubuntu/ROS/datasets/
sudo apt-get install htop
sudo apt-get install python-pip
pip2 install --user configparser
# 17. shut down
#sudo poweroff
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 PPF-matching system installed.
 next step: upload the dataset-files 
            or mount an existing EBS-volume containing the datasets
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
