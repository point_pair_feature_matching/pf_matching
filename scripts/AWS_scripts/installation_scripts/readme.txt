Make shure to ajust all the variables in the shell scripts before using them!

The "local" scripts will usually upload their "remote" counterparts to the AWS instance
and afterwards establish a ssh connection to this instance.
You then have to use this ssh-conntection to execute the uploaded "remote" scripts via ssh manually.
The "remote" scripts will always be located in the home directory of the AWS instance.
