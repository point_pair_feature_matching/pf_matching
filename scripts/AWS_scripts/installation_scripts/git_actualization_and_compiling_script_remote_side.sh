# you need to specify the following variables for your case:
bitbucket_username="phil_rudd"          		# username of your bitbucket accout, from where to download the git-repositories
git_username="Markus Ziegler"				# for the git config. not so important.
git_useremail="markus.f.ziegler@campus.tu-berlin.de"  	# for the git config. not so important.


## Prerequisites: execute git_actualization_local_side.sh on your local machine
## that script will store this script on the remote computer.
## You need to execute this script by yourself.

# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


## updating the instance's git repos and do compiling
## install some other stuff if needed


echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 updating the 'catkin workspace' repository
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

# get to the repos and update them	
cd ~/ROS/catkin_workspace/src/pf_matching/
git checkout -- scripts/visCon_and_hinterstoisser_experiments/ICCV_hinterstoisser_searchradius_angleDiffThresh_variation_experiment.py # this file is currently always ahead-->reset it
git pull
cd ../pf_matching_core
git status
git checkout -- launch/preprocesing_and_matching_as_nodes.launch # this file is currently always ahead-->reset it
git pull


echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building the 'catkin workspace' repository
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

# build the catkin_workspace
cd ~/ROS/catkin_workspace/ 
catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS_TYPE=-O2

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 doing some additional stuff
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

sudo apt-get update
sudo apt-get install htop
sudo apt-get install python-pip
pip2 install --user configparser

# 17. shut down
#sudo poweroff

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 PPF-matching system updated.
 next step: ready to start an experiment, e.g. with one of the 'AWS_ .py' scripts
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
