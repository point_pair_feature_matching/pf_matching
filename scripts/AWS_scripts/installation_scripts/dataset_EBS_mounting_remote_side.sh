# you need to specify the following variables for your case:
UUID="86036617-2f4f-4b43-b478-1aa8f2e62267" # UUID of the EBS volume to mount, use e.g. ls -al /dev/disk/by-uuid/ to get it.

## this script simply mounts an existing EBS volume - that shall contain the datasets OR 
#   already contains the datasets - to the path ~/ROS/datasets/ 
#   This script needs to be run on the remote computer
## Note: it will be uploaded to the remote computer by the installation_script_local_side.sh


# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


# 1. make backup of original fstab file
sudo cp /etc/fstab /etc/fstab.orig &&

# 2. add line to fstab file for permanent mounting even after reboot
echo UUID="$UUID" /home/ubuntu/ROS/datasets ext4 defaults,nofail 0 2 | sudo tee -a /etc/fstab &&

# 3. mount all devices (checking for errors)
sudo mount -a &&
lsblk

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Successfully mounted the EBS volume with UUID="$UUID".
 You can now upload the dataset files to the mounting path of this volume. 
 OR if the volume already contains the datasets, you can start experiments now.
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

# 4. now you could upload the dataset files to the volume using the path /home/ubuntu/datasets/
