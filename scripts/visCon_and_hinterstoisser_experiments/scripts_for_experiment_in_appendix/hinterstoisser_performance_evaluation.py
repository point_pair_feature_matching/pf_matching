#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/hinterstoisser_performances/hinterstoisser_performance.bin'
BASELINE_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
precisions_weight = 0.5
use_Krones_evaluation = True

max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

# TODO: give the recognition rates from results of searchradius_angleDiffThresh_variation_evaluation:
RESULT_ONE_INSTANCE_DETECTION = [("\\textsc{Hinterstoisser}'s extensions without verifier", [('cuboid', 0.7), ('cylinder', 0.75), ('elliptic_prism', 0.95), ('hexagonal_frustum', 0.75), ('pyramid', 0.95)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=1.0$, without conflict-detection", [('cuboid', 0.7), ('cylinder', 0.85), ('elliptic_prism', 1.0), ('hexagonal_frustum', 0.8), ('pyramid', 0.85)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=0.35$, without conflict-detection", [('cuboid', 0.7), ('cylinder', 0.85), ('elliptic_prism', 1.0), ('hexagonal_frustum', 0.8), ('pyramid', 0.85)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=0.35$, conflict threshold = 0.08",[('cuboid', 0.7), ('cylinder', 0.85), ('elliptic_prism', 1.0), ('hexagonal_frustum', 0.8), ('pyramid', 0.85)])]

RESULT_TWO_INSTANCE_DETECTION = [("\\textsc{Hinterstoisser}'s extensions without verifier", [('cuboid', 0.425), ('cylinder', 0.4), ('elliptic_prism', 0.775), ('hexagonal_frustum', 0.375), ('pyramid', 0.475)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=1.0$, without conflict-detection", [('cuboid', 0.1), ('cylinder', 0.1), ('elliptic_prism', 0.35), ('hexagonal_frustum', 0.0), ('pyramid', 0.0)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=0.35$, without conflict-detection", [('cuboid', 0.05), ('cylinder', 0.0), ('elliptic_prism', 0.25), ('hexagonal_frustum', 0.0), ('pyramid', 0.0)]),
                                 ("\\textsc{Hinterstoisser}'s extensions with $r_\\mathrm{s}=0.5$ and $\\alpha=0.033\\pi$, $\\mathcal{V}=0.0$, $\\mathcal{P}=0.35$, conflict threshold = 0.08", [('cuboid', 0.25), ('cylinder', 0.4), ('elliptic_prism', 0.8),('hexagonal_frustum', 0.2), ('pyramid', 0.5)])]


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if used_rs and collapsed_points and not used_vb and not used_n_PPFs and not used_hc and\
                    not used_fa and not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_vb and used_n_PPFs and used_hc and used_fa \
                    and voted_for_adj_rot_angl:
                matching_mode = "\\textsc{Hinterstoisser}'s extensions\nwith verifier"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    #colors_six_bars = ['#c62828', '#ff5f52',  # red
    #                   '#33691e', '#629749',  # green
    #                   '#1a237e', '#534bae',  # blue
    #                   '#ffb300', '#ffe54c',  # yellow
    #                   ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300', '#7b1fa2']
    colors_six_bars = colors_three_bars # the six'th color is not used here, hence it's non-definition

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_recognition_rate_one_instance(data, baseline_data, ax, show_legend=False):

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # ******************************************************************************************************************
    # extensions
    # ******************************************************************************************************************

    # extract data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['poses']],
                                                    [["dynamicParameters", "/verifier",
                                                      "conflictthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "supportthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "penaltythreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "angleDiffThresh_in_pi"],
                                                     ["dynamicParameters", "/verifier",
                                                      "searchradius"],
                                                     ['notes', 'matching_mode']])

    # dict for storing the best setups
    best_setup_dict = {'searchradius': -1, 'angleDiffThresh_in_pi': -1, 'recognition_rate': -1,
                       'supportthreshold': -1, 'penaltythreshold': -1, 'conflictthreshold': -1}

    # get the current setup
    for run in base_data:
        best_setup_dict['conflictthreshold'] = run[iDict['conflictthreshold']]
        best_setup_dict['searchradius'] = run[iDict['searchradius']]
        best_setup_dict['angleDiffThresh_in_pi'] = run[iDict['angleDiffThresh_in_pi']]
        best_setup_dict['supportthreshold'] = run[iDict['supportthreshold']]
        best_setup_dict['penaltythreshold'] = run[iDict['penaltythreshold']]
        break

    # calculate recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info_one_instance = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    # calculate recognition rate for each model of the datasets:
    recognition_rate_with_verifier_info_one_instance = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    print('\n(One instance detection) The recognition rate is:', recognition_rate_with_verifier_info_one_instance[0][1])

    # ******************************************************************************************************************
    # do plotting --- barplot
    # ******************************************************************************************************************

    # merge baseline-data and experiment data and data of previous experiments
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0]) # add "baseline"-mode
    for data in RESULT_ONE_INSTANCE_DETECTION:
        recognition_rate_per_model.append(data)
    recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions with "\
                                       "$r_\\mathrm{s}=%s$ and $\\alpha=%s\\pi$, "\
                                       "$\\mathcal{V}=%s$, $\\mathcal{P}=%s$, "\
                                       "conflict threshold = %s"\
                                       %(best_setup_dict['searchradius'],
                                         round(best_setup_dict['angleDiffThresh_in_pi'],3),
                                         best_setup_dict['supportthreshold'],
                                         best_setup_dict['penaltythreshold'],
                                         best_setup_dict['conflictthreshold']),
                                        recognition_rate_per_model_with_verifier_info_one_instance[0][1]))

    print('\n(One instance detection) The recognition rates for each model are:')
    for x in recognition_rate_per_model:
        print(x)

    # plot and plot-settings
    bar_by_model(recognition_rate_per_model, ax, 'matching mode',
                 show_legend=show_legend, show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if (show_legend):
        ax.legend(loc='upper right')

    return best_setup_dict


def plot_recognition_rate_two_instances(data, baseline_data, best_setup_dict, ax, show_legend=False):

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [["dynamicParameters", "/verifier",
                                       "conflictthreshold"],
                                      ["dynamicParameters", "/verifier",
                                       "supportthreshold"],
                                      ["dynamicParameters", "/verifier",
                                       "penaltythreshold"],
                                      ["dynamicParameters", "/verifier",
                                       "angleDiffThresh_in_pi"],
                                      ["dynamicParameters", "/verifier",
                                       "searchradius"],
                                      ['notes', 'matching_mode']])

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    print('\n(One instance detection) The recognition rate is', recognition_rate_with_verifier_info[0][1])

    # ******************************************************************************************************************
    # do plotting --- barplot
    # ******************************************************************************************************************

    # merge baseline-data and experiment data and data of previous experiments
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])  # add "baseline"-mode
    for data in RESULT_TWO_INSTANCE_DETECTION:
        recognition_rate_per_model.append(data)
    recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions with "\
                                       "$r_\\mathrm{s}=%s$ and $\\alpha=%s\\pi$, "\
                                       "$\\mathcal{V}=%s$, $\\mathcal{P}=%s$, "\
                                       "conflict threshold = %s"\
                                       %(best_setup_dict['searchradius'],
                                         round(best_setup_dict['angleDiffThresh_in_pi'],3),
                                         best_setup_dict['supportthreshold'],
                                         best_setup_dict['penaltythreshold'],
                                         best_setup_dict['conflictthreshold']),
                                       recognition_rate_per_model_with_verifier_info[0][1]))

    print('\n(Two-instance-detection) Best recognition rates for each model are:')
    for x in recognition_rate_per_model:
        print(str(x))

    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'matching mode', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (two instances)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_pose_errors(data)
    post_processing.compute_correctness_from_verifier(data, max_translation_error_relative,
                                                      max_rotation_error)
    post_processing.compute_verified(data) # adds verificationWeight and verified-field to each pose
    post_processing.compute_verification(data)  # computes tp's, fp's, fn's

    # for comparison reasons:
    print('Loading Baseline-data...')
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)
    post_processing.compute_pose_errors(baseline_data)
    post_processing.compute_recognition(baseline_data, max_translation_error_relative,
                                        max_rotation_error)

    # mark which run belongs to which matching mode
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # mark every pose as verified, i.e. set min_mu_v = 0  and max_mu_p = 1:
    post_processing.assign_mu_to_poses(data)
    post_processing.compute_verified_through_mu(data, 0.0, 0.35)
    # add the 'weight' in the 'verified_poses' dict as 'verificationWeight' to the 'poses' dict.
    post_processing.add_verifiedWeight_to_poses(data)


    # plot recognition rate for one-instance detection:
    fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 9), sharey=True)
    fig_rr.subplots_adjust(bottom=5/17)
    best_setup_dict = plot_recognition_rate_one_instance(data, baseline_data,
                                                         fig_rr.axes[0],
                                                         show_legend=False)
    # plot recognition rate for two-instance detection:
    plot_recognition_rate_two_instances(data, baseline_data,
                                        best_setup_dict,
                                        fig_rr.axes[1],
                                        show_legend=False)

    fig_rr.axes[0].legend(bbox_to_anchor=(0.5, 0.00), bbox_transform=fig_rr.transFigure,
                        loc='lower center', borderaxespad=0., title='matching modes', ncol=1)
    fig_rr.tight_layout(rect=[0, 5/17, 1, 1])

    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates_for_one_and_two_instance_dete')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
