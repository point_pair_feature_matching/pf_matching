#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors, get_model_diameters
import pf_matching.pose_tools as pt

# project
from math import floor, ceil, pi, sqrt, pow
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
import tf.transformations as tft



# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'#'~/ROS/datasets/geometric_primitives/blensor_dataset.py'
FILE_PATH_RECENTERED = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_publish_n_best_poses/ICCV_hinterstoisser_publish_n_best_poses.bin'
#'~/ROS/experiment_data/ICCV_drost_reference_Plane_inlier_threshold_variation/ICCV_drost_reference_Plane_inlier_threshold_variation_part_2.bin' #'~/ROS/experiment_data/hinterstoisser_pose_verifying/hinterstoisser_publish_n_best_poses/hinterstoisser_publish_n_best_poses.bin'
FILE_PATH_NOT_RECENTERED = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_publish_n_best_poses/ICCV_hinterstoisser_publish_n_best_poses.bin'
#'~/ROS/experiment_data/ICCV_drost_reference_Plane_inlier_threshold_variation/ICCV_drost_reference_Plane_inlier_threshold_variation_part_2.bin'

VISUALIZE_I_TH_POSES = 1


def compute_and_compare_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data_recentered, data_not_recentered, dataset):

    diameters_dict = get_model_diameters(data_recentered)

    for i, run_id in enumerate(sorted(data_recentered["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data_recentered.get_nested(["runData", run_id, "scene"])
            scene_transform = data_recentered.get_nested(["runData", run_id, "loadTransform",
                                                         scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names_recentered = data_recentered.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue
        try:
            model_names_not_recentered = data_not_recentered.get_nested(
                ["runData", run_id, "poses"]).keys()
        except KeyError:
            print('No models matched in run %d, skipping.' % run_id)
            continue

        # both data-elements need to have the same model name, because i don't check it here
        for model_name in model_names_recentered:

            try:
                model_load_transforms_recentered = \
                    dict(data_recentered.get_run_data([["loadTransforms", model_name]],
                                                      copy_missing_from_previous=True))
                model_transform_recentered = model_load_transforms_recentered[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform_recentered = tft.identity_matrix()
            try:
                model_load_transforms_not_recentered = \
                    dict(data_not_recentered.get_run_data([["loadTransforms", model_name]],
                                                      copy_missing_from_previous=True))
                model_transform_not_recentered = model_load_transforms_not_recentered[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform_not_recentered = tft.identity_matrix()

            # assuming that each data-element has the same equivalent poses
            try:
                model_equivalent_pose_transforms = \
                    dict(data_recentered.get_run_data([['equivalentPoses', model_name]],
                                                      copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                print('got no model equivalent poses')
                model_equivalent_poses = {}


            model_diameter = diameters_dict[model_name]

            i_th_pose = 0
            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data_recentered.get_nested(poses_path).keys():
                i_th_pose += 1
                model_pose_recentered = data_recentered.get_nested(poses_path + [pose_index])
                model_pose_not_recentered = data_not_recentered.get_nested(poses_path + [pose_index])
                print('model pose (recentered experiment):\n', pt.pose_dict2matrix(model_pose_recentered))
                print('model pose (not-recentered experiment):\n', pt.pose_dict2matrix(model_pose_not_recentered))

                # go through all ground truth poses
                gts_path = ["runData", run_id, "groundTruth", model_name]
                for gt_index in data_recentered.get_nested(gts_path).keys():
                    gt_pose_recentered = data_recentered.get_nested(gts_path + [gt_index])
                    gt_pose_not_recentered = data_not_recentered.get_nested(gts_path + [gt_index])

                    print('ground truth pose (recentered experiment) %i:' %(gt_index), gt_pose_recentered,'\n', pt.pose_dict2matrix(gt_pose_recentered))
                    print('model load transform (recentered experiment):', model_transform_recentered,'\n', pt.pose_dict2matrix(model_transform_recentered))
                    print('ground truth pose (not-recentered experiment) %i:' % (gt_index), gt_pose_not_recentered, '\n', pt.pose_dict2matrix(gt_pose_not_recentered))
                    print('model load transform (not-recentered experiment):', model_transform_not_recentered, '\n', pt.pose_dict2matrix(model_transform_not_recentered))
                    print('scene load transform:\n', scene_transform)

                    gt_pose_transformed_recentered = pt.transform_gt(gt_pose_recentered, model_transform_recentered, scene_transform)
                    print('*transformed ground truth pose (recentered experiment) %i:\n' %(gt_index), gt_pose_transformed_recentered)

                    gt_pose_transformed_not_recentered = pt.transform_gt(gt_pose_not_recentered, model_transform_not_recentered, scene_transform)
                    print('*transformed ground truth pose (not-recentered experiment) %i:\n' %(gt_index), gt_pose_transformed_not_recentered)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose_recentered = pt.concatenate_transforms(model_pose_recentered, equivalent_pose)
                        model_test_pose_not_recentered = pt.concatenate_transforms(model_pose_not_recentered, equivalent_pose)

                        print('equivalent pose:\n', equivalent_pose)
                        print('*model test pose (recentered experiment):\n', model_test_pose_recentered)
                        print('*model test pose (not-recentered experiment):\n', model_test_pose_not_recentered)

                        display_pose_errors_by_hinterstoisser_lepetit_et_al_2013(model_test_pose_recentered, model_test_pose_not_recentered,
                                                                                gt_pose_transformed_recentered, gt_pose_transformed_not_recentered,
                                                                                pt.pose_dict2matrix(model_transform_recentered), pt.pose_dict2matrix(model_transform_not_recentered),
                                                                                dataset, model_name, scene_name, i_th_pose)


def display_pose_errors_by_hinterstoisser_lepetit_et_al_2013(pose_recentered, pose_not_recentered, gt_recentered, gt_not_recentered,
                                                                model_load_transform_recentered, model_load_transform_not_recentered,
                                                                dataset, model_name, scene_name, i_th_pose):

    import pcl

    # check if any of the inputs is None
    if None in [pose_recentered, pose_not_recentered, gt_recentered, gt_not_recentered]:
        return {'average distance': None}

    # extract pose values
    if type(pose_recentered) is dict:
        pose_recentered = pt.pose_dict2matrix(pose_recentered)
    if type(pose_not_recentered) is dict:
        pose_not_recentered = pt.pose_dict2matrix(pose_not_recentered)

    # extract ground truth values
    if type(gt_recentered) is dict:
        gt_recentered = pt.pose_dict2matrix(gt_recentered)
    if type(gt_not_recentered) is dict:
        gt_not_recentered = pt.pose_dict2matrix(gt_not_recentered)

    # all values exist, calculate errors

    # get model load path and model scale factor
    model_absolute_load_path = ""
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_name(model_idx) == model_name:
            model_absolute_load_path = dataset.get_model_path(model_idx)
            break
    if not model_absolute_load_path:
        return {'average distance': None}
    # get scene load path
    scene_absolute_load_path = ""
    for scene_idx in range(dataset.get_number_of_scenes()):
        if dataset.get_scene_name(scene_idx) == scene_name:
            scene_absolute_load_path = dataset.get_scene_path(scene_idx)
            break
    if not scene_absolute_load_path:
        return {'average distance': None}

    # load the model as a pcl point cloud
    gt_model_recentered = pcl.load_XYZI(model_absolute_load_path)
    gt_model_not_recentered = pcl.load_XYZI(model_absolute_load_path)
    pose_model_recentered = pcl.load_XYZI(model_absolute_load_path)
    pose_model_not_recentered = pcl.load_XYZI(model_absolute_load_path)
    scene = pcl.load_XYZI(scene_absolute_load_path)

    # store the model points in np array of tuples(?) [(x1,y1,z1), (x2, y2,z2), ...]
    #gt_model_points = np.zeros((gt_model.size, 4), dtype=np.float32)
    #pose_model_points = np.zeros((pose_model.size, 4), dtype=np.float32)
    gt_model_points_recentered = gt_model_recentered.to_array()
    gt_model_points_not_recentered = gt_model_not_recentered.to_array()
    pose_model_points_recentered = pose_model_recentered.to_array()
    pose_model_points_not_recentered = pose_model_not_recentered.to_array()

    # scale model and transform into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)))
    # first the recentered model:
    for i in range(0, gt_model_recentered.size):
        gt_model_points_recentered[i][0] *= model_scale_factor
        gt_model_points_recentered[i][1] *= model_scale_factor
        gt_model_points_recentered[i][2] *= model_scale_factor
        gt_model_points_recentered[i][3] = 1
        #print('gt point after scaling:', gt_model_points[i])
        # transform model clouds into gt
        # recenter model, because we loaded the original model above
        gt_model_points_recentered[i] = np.dot(pt.inverse(model_load_transform_recentered), gt_model_points_recentered[i])
        #print('gt point after recentering:', gt_model_points[i])
        # apply gt-pose of the recentered model to the recentered model
        gt_model_points_recentered[i] = np.dot(gt_recentered, gt_model_points_recentered[i])
        #print('gt point after applying gt-pose:', gt_model_points[i])
        # rotate gt-pose by 180° about the cameras x-axis, because originally the gt-pose
        # was determined assuming the cameras view-direction was the negative z-axis.
        gt_model_points_recentered[i] = np.dot(R, gt_model_points_recentered[i])
        #print('gt point after rotation about 180°:', gt_model_points[i])
        #input('press enter to contiune')

    # now the not-recentered model:
    for i in range(0, gt_model_not_recentered.size):
        gt_model_points_not_recentered[i][0] *= model_scale_factor
        gt_model_points_not_recentered[i][1] *= model_scale_factor
        gt_model_points_not_recentered[i][2] *= model_scale_factor
        gt_model_points_not_recentered[i][3] = 1
        #print('gt point after scaling:', gt_model_points[i])
        # transform model clouds into gt
        # recenter model, because we loaded the original model above
        gt_model_points_not_recentered[i] = np.dot(pt.inverse(model_load_transform_not_recentered), gt_model_points_not_recentered[i])
        #print('gt point after recentering:', gt_model_points[i])
        # apply gt-pose of the recentered model to the recentered model
        gt_model_points_not_recentered[i] = np.dot(gt_not_recentered, gt_model_points_not_recentered[i])
        #print('gt point after applying gt-pose:', gt_model_points[i])
        # rotate gt-pose by 180° about the cameras x-axis, because originally the gt-pose
        # was determined assuming the cameras view-direction was the negative z-axis.
        gt_model_points_not_recentered[i] = np.dot(R, gt_model_points_not_recentered[i])
        #print('gt point after rotation about 180°:', gt_model_points[i])
        #input('press enter to contiune')

    # scale model and tranform into pose-hypothesis
    # first the recentered model:
    for i in range(0, pose_model_recentered.size):
        pose_model_points_recentered[i][0] *= model_scale_factor
        pose_model_points_recentered[i][1] *= model_scale_factor
        pose_model_points_recentered[i][2] *= model_scale_factor
        pose_model_points_recentered[i][3] = 1
        # transform model clouds into pose
        # recenter model, because we loaded the original model above
        pose_model_points_recentered[i] = np.dot(pt.inverse(model_load_transform_recentered), pose_model_points_recentered[i])
        # apply pose-hypothesis of the recentered model to the recentered model
        pose_model_points_recentered[i] = np.dot(pose_recentered, pose_model_points_recentered[i])

    # then the not-recentered model:
    for i in range(0, pose_model_not_recentered.size):
        pose_model_points_not_recentered[i][0] *= model_scale_factor
        pose_model_points_not_recentered[i][1] *= model_scale_factor
        pose_model_points_not_recentered[i][2] *= model_scale_factor
        pose_model_points_not_recentered[i][3] = 1
        # transform model clouds into pose
        # recenter model, because we loaded the original model above
        pose_model_points_not_recentered[i] = np.dot(pt.inverse(model_load_transform_not_recentered), pose_model_points_not_recentered[i])
        # apply pose-hypothesis of the recentered model to the recentered model
        pose_model_points_not_recentered[i] = np.dot(pose_not_recentered, pose_model_points_not_recentered[i])

    # save transformed poses in the original point clouds
    gt_model_recentered.from_array(gt_model_points_recentered)
    gt_model_not_recentered.from_array(gt_model_points_not_recentered)
    pose_model_recentered.from_array(pose_model_points_recentered)
    pose_model_not_recentered.from_array(pose_model_points_not_recentered)

    # calculate distance between gt's of recentered and not-recentered models
    sum_distance = 0
    for i in range(0, gt_model_recentered.size):
        sum_distance += np.linalg.norm(
            gt_model_points_recentered[i] - gt_model_points_not_recentered[i])
    print('distance between gt-poses of recentered and not-recentered experiments =', sum_distance)

    # calculate distance between poses of recentered and not-recentered models
    sum_distance = 0
    for i in range(0, pose_model_recentered.size):
        sum_distance += np.linalg.norm(
            pose_model_points_recentered[i] - pose_model_points_not_recentered[i])
    print('distance between poses of recentered and not-recentered experiments =', sum_distance)


    # calculate distance between model points of gt and pose of recentered models
    sum_distance = 0
    for i in range(0, pose_model_recentered.size):
        sum_distance += np.linalg.norm(gt_model_points_recentered[i] - pose_model_points_recentered[i])
    average_distance_recentered = sum_distance / pose_model_recentered.size
    print('average distance (recentered experiment) =', average_distance_recentered)

    # calculate distance between model points of gt and pose of not-recentered models
    sum_distance = 0
    for i in range(0, pose_model_not_recentered.size):
        sum_distance += np.linalg.norm(gt_model_points_not_recentered[i] - pose_model_points_not_recentered[i])
    average_distance_not_recentered = sum_distance / pose_model_not_recentered.size
    print('average distance (not-recentered experiment) =', average_distance_not_recentered)


    # if (i_th_pose == VISUALIZE_I_TH_POSES):
    #     # visulalize the model poses:
    #     import pcl.pcl_visualization
    #     visual = pcl.pcl_visualization.CloudViewing()
    #     #visual.ShowGrayCloud(gt_model_recentered, b'gt_recentered')
    #     visual.ShowGrayCloud(gt_model_not_recentered, b'gt_not_recentered')
    #     #visual.ShowGrayCloud(pose_model_recentered, b'pose_recentered')
    #     visual.ShowGrayCloud(pose_model_not_recentered, b'pose_not_recentered')
    #     visual.ShowGrayCloud(scene, b'scene')
    #
    #     flag = True
    #     while flag:
    #         flag != visual.WasStopped()
    #     end


if __name__ == '__main__':
    # load data and do post-processing
    print('Loading Experiment-data...')
    data_recentered = ExperimentDataStructure()
    data_not_recentered = ExperimentDataStructure()
    data_recentered.from_pickle(FILE_PATH_RECENTERED)
    data_not_recentered.from_pickle(FILE_PATH_NOT_RECENTERED)
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    print('Computing pose errors with error-metric from [Hinterstoisser, Lepetit et al. 2013] for all runs...')
    #compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data_recentered, dataset)
    #compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data_not_recentered, dataset)
    compute_and_compare_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data_recentered, data_not_recentered, dataset)

    #post_processing.compute_correctness_from_verifier(data, max_translation_error_relative,
    #                                                  max_rotation_error)
    # post_processing.compute_verified(data)