#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke, Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/hinterstoisser_influence_of_extension_packages/hinterstoisser_influence_of_extension_packages.bin'
BASELINE_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist.bin'
SAVE_PATH_WO_EXTENSION = '~/ROS/experiment_data/hinterstoisser_influence_of_extension_packages/hinterstoisser_influence_of_extension_packages'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if used_rs and collapsed_points and not used_vb and not used_n_PPFs and not used_hc and not used_fa and\
                not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_vb and not used_n_PPFs and not used_hc and not used_fa and\
                not voted_for_adj_rot_angl:
                matching_mode = "smart sampling of point pairs, without \\textsc{Hinterstoisser}'s clustering"
            elif used_rs and collapsed_points and used_vb and not used_n_PPFs and used_hc and not used_fa and\
                not voted_for_adj_rot_angl:
                matching_mode = "smart sampling of point pairs, with \\textsc{Hinterstoisser}'s clustering"
            elif used_rs and collapsed_points and not used_vb and used_n_PPFs and not used_hc and used_fa and\
                voted_for_adj_rot_angl:
                matching_mode = "accounting for sensor noise, without \\textsc{Hinterstoisser}'s clustering"
            elif used_rs and collapsed_points and not used_vb and used_n_PPFs and used_hc and used_fa and\
                voted_for_adj_rot_angl:
                matching_mode = "accounting for sensor noise, with \\textsc{Hinterstoisser}'s clustering"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['k','#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300', '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['k', # black
                       '#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    #print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i], label=bar_names, log=logarithmic_y_axis)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i], label=bar_names, log=logarithmic_y_axis)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_average_matching_time(baseline_data, experiment_data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @type baseline_data: ExperimentDataStructure
    @parameter data: experiment data
    @parameter baseline_data: experiment data of baseline algorithm for comparison
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_match_time_per_mode_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('\nbaseline algorithm: average matching time per matching mode')
    for mode, match_time in average_match_time_per_mode_baseline:
        print('mode: %s, time: %f ms' % (mode, match_time))

    average_match_time_per_model_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    #print('average_match_time_per_model_baseline = ', average_match_time_per_model_baseline)

    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_match_time_per_mode_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('extensions: average matching time per matching mode')
    for mode, match_time in average_match_time_per_mode_extensions:
        print('mode: %s, time: %f ms' % (mode, match_time))

    average_match_time_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    #print('average_match_time_per_model_extensions',average_match_time_per_model_extensions)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/verifier']],
                                     [['notes', 'matching_mode']])

    average_verification_time_per_mode_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)

    print('extensions: average verification time per matching mode')
    for mode, verification_time in average_verification_time_per_mode_extensions:
        print('mode: %s, time: %f ms' % (mode, verification_time))

    average_verification_time_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)

    #print('average_verification_time_per_model_extensions', average_verification_time_per_model_extensions)

    average_match_and_verification_time_per_mode_extensions = []
    for v_mode, verification_time in average_verification_time_per_mode_extensions:
        for m_mode, matching_time in average_match_time_per_mode_extensions:
            if v_mode == m_mode:
                average_match_and_verification_time_per_mode_extensions.append([v_mode, matching_time+verification_time])

    print('extensions: average matching+verification time per matching mode')
    for mode, time in average_match_and_verification_time_per_mode_extensions:
        print('mode: %s, time: %f ms' % (mode, time))

    average_match_and_verification_time_per_model_extensions = []
    for v_mode, verification_times_by_model in average_verification_time_per_model_extensions:
        for v_model, verification_time in verification_times_by_model:
            for m_mode, matching_times_by_model in average_match_time_per_model_extensions:
                for m_model, matching_time in matching_times_by_model:
                    if v_mode == m_mode and m_model == v_model:
                        mode_exists = False
                        for stored_v_mode, stored_times_by_mode in average_match_and_verification_time_per_model_extensions:
                            if stored_v_mode == v_mode:
                                mode_exists = True
                                stored_times_by_mode.append([v_model, matching_time+verification_time])
                        if not mode_exists:
                            average_match_and_verification_time_per_model_extensions.append([v_mode, [[v_model, matching_time+verification_time]]])

    #print('average_match_and_verification_time_per_model =',average_match_and_verification_time_per_model)

    # add baseline-data to extension-data
    overall_matching_times_per_model = []
    # convert into list of lists, because mode-names in list of tuples can't be edited
    average_match_time_per_model_extensions = [list(elem) for elem in
                                               average_match_time_per_model_extensions]
    overall_matching_times_per_model.append(average_match_time_per_model_baseline[0])
    for mode_results in average_match_and_verification_time_per_model_extensions:
        mode_results[0] = mode_results[0] + ', with verifier'
        overall_matching_times_per_model.append(mode_results)
    for mode_results in average_match_time_per_model_extensions:
        mode_results[0] = mode_results[0] + ', without verifier'
        overall_matching_times_per_model.append(mode_results)

    # plotting
    bar_by_model(overall_matching_times_per_model, ax, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylim(0, 210000)
    ax.set_yticks([y for y in range(0,210001,1000*30)])
    scaled_labels = [float(y) / 1000 for y in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')


def plot_average_model_building_time(baseline_data, experiment_data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @type baseline_data: ExperimentDataStructure
    @parameter data: experiment data
    @parameter baseline_data: experiment data of baseline algorithm for comparison
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    # **********************************************************************************************
    # baseline
    # **********************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_model_building_time_per_mode_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('\nbaseline algorithm: average model building time per matching mode')
    for mode, model_building_time in average_model_building_time_per_mode_baseline:
        print('mode: %s, time: %f ms' % (mode, model_building_time))

    average_model_building_time_per_model_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('baseline algorithm: average_model_building_time_per_model:')
    for mode, model_building_time in average_model_building_time_per_model_baseline:
        print('mode: %s, time: %s ms' % (mode, model_building_time))

    # **********************************************************************************************
    # extensions - with and without verifier (does not matter)
    # **********************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_model_building_time_per_mode_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('extensions: average model building time per matching mode')
    for mode, model_building_time in average_model_building_time_per_mode_extensions:
        print('mode: %s, time: %f ms' % (mode, model_building_time))

    average_model_building_time_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('extensions: average model building time per model')
    for mode, time in average_model_building_time_per_model_extensions:
        print('mode: %s, time: %s ms' % (mode, time))

    # add baseline-data to extension-data
    plot_data = []
    plot_data.append(average_model_building_time_per_model_baseline[0])
    # convert into list of lists, because mode-names in list of tuples can't be edited
    average_model_building_time_per_model_extensions = [list(elem) for elem in
                                                        average_model_building_time_per_model_extensions]
    plot_data.append(("accounting for sensor noise,\nwith \\textsc{Hinterstoisser}'s\nclustering",
                      average_model_building_time_per_model_extensions[0][1]))
    plot_data.append(("accounting for sensor noise,\nwithout \\textsc{Hinterstoisser}'s\nclustering",
                      average_model_building_time_per_model_extensions[1][1]))
    plot_data.append(("smart sampling of point pairs,\nwith \\textsc{Hinterstoisser}'s\nclustering",
                      average_model_building_time_per_model_extensions[2][1]))
    plot_data.append(("smart sampling of point pairs,\nwithout \\textsc{Hinterstoisser}'s\nclustering",
                      average_model_building_time_per_model_extensions[3][1]))

    # plotting
    bar_by_model(plot_data, ax, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=True)
    ax.set_ylim(1000, 1000000)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average model building time in s')


def plot_average_number_of_scene_points(data_1, data_2, ax, show_legend):
    """
    comparing the average number of scene points between two Experiments,
    namely the rotational_symmetry_experiment.py of Kroischke and rotational_symmetry_nyquist_experiment.py
    The difference between both experiments lies not only in the experiment-script-code but also in the matcher-code.
    Main difference: Both point clouds, model and scene, will be either downsampled with slightly differing values for
    d_points_scene and d_points_model or with with the exact same values d_points_scene = d_points_model.
    @type data_1: ExperimentDataStructure
    @type data_2: ExperimentDataStructure
    @param data_1: data of experiment No. 1
    @param data_2: data of experiment No. 2
    @note data: both datas have to be retrieved from different .bin-files! That means one of them needs to have its origin
    in another experiment that has already been done.
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict_1, basic_data_1 =\
        data_extraction.extract_data(data_1,
                                     None,
                                     [['statistics', '/scene_preprocessor']])

    iDict_2, basic_data_2 =\
        data_extraction.extract_data(data_2,
                                     None,
                                     [['statistics', '/scene_preprocessor']])

    average_number_of_scene_points_per_model_1 =\
        data_extraction.nest_and_process_by_values(basic_data_1,
                                                   [iDict_1['model'],
                                                    iDict_1['/scene_preprocessor']],
                                                   data_extraction.average_number_of_cloud_points)

    average_number_of_scene_points_per_model_2 = \
        data_extraction.nest_and_process_by_values(basic_data_2,
                                                   [iDict_2['model'],
                                                    iDict_2['/scene_preprocessor']],
                                                   data_extraction.average_number_of_cloud_points)

    #print("average number of scenes per model =",average_number_of_scene_points_per_model_1)
    #print("average number of scenes per model =",average_number_of_scene_points_per_model_2)

    # create: [( 'd_points_scene = d_dist_abs',[(),(),(),...] ), ( 'd_points_scene = d_points_model', [(),(),(),...] )]
    average_number_of_scene_points_per_model = [('d_points_scene = d_dist_abs',     average_number_of_scene_points_per_model_2),
                                                ('d_points_scene = d_points_model', average_number_of_scene_points_per_model_1)]

    bar_by_model(average_number_of_scene_points_per_model,
                  ax, 'downsampling modes', show_legend=show_legend, show_top_ticks=False)
    ax.set_ylim(0, 16000)
    scaled_labels = [float(x) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average number of scene points')

def plot_model_diameter(data_1, data_2, ax, show_legend):
    """
    comparing the model diameters between two Experiments,
    namely the rotational_symmetry_experiment.py of Kroischke and rotational_symmetry_nyquist_experiment.py
    The difference between both experiments lies not only in the experiment-script-code but also in the matcher-code.
    Main difference: Both point clouds, model and scene, will be either downsampled with slightly differing values for
    d_points_scene and d_points_model or with with the exact same values d_points_scene = d_points_model.
    @type data_1: ExperimentDataStructure
    @type data_2: ExperimentDataStructure
    @param data_1: data of experiment No. 1
    @param data_2: data of experiment No. 2
    @note data: both datas have to be retrieved from different .bin-files! That means one of them needs to have its origin
    in another experiment that has already been done.
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict_1, basic_data_1 = \
        data_extraction.extract_data(data_1,
                                     [['statistics', '/model_preprocessor']],
                                     [['notes', 'matching_mode']])

    iDict_2, basic_data_2 = \
        data_extraction.extract_data(data_2,
                                     [['statistics', '/model_preprocessor']],
                                     [['notes', 'matching_mode']])

    model_diameters_1 = \
        data_extraction.nest_and_process_by_values_advanced(basic_data_1,
                                                            [iDict_1['model'],
                                                             iDict_1['/model_preprocessor']],
                                                            data_extraction.average_key_figure,
                                                            'cloud_diameter')

    model_diameters_2 = \
        data_extraction.nest_and_process_by_values_advanced(basic_data_2,
                                                            [iDict_2['model'],
                                                             iDict_2['/model_preprocessor']],
                                                            data_extraction.average_key_figure,
                                                            'cloud_diameter')

    # print("average number of scenes per model =",average_number_of_scene_points_per_model_1)
    # print("average number of scenes per model =",average_number_of_scene_points_per_model_2)

    # create: [( 'd_points_scene = d_dist_abs',[(),(),(),...] ), ( 'd_points_scene = d_points_model', [(),(),(),...] )]
    model_diameters_per_model = [('epos6 algorithm', model_diameters_2),
                                 ('welzl algorithm', model_diameters_1)]

    bar_by_model(model_diameters_per_model,
                 ax, 'bounding sphere algorithms', show_legend=show_legend)
    ax.set_ylim(0, 0.2)
    scaled_labels = [float(x) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('model diameter [m]')


def plot_key_figure_of_voting_ball(data, ax, show_legend, key_figure, y_max, voting_ball_idx, logarithmic_y_axis = False):
    """
    plot pose-key-figures that are stored in a list, containing one value for each voting ball, such as
    - n_raw_poses,
    - n_pose_clusters,
    - raw_poses_max_weight,
    - raw_poses_median_weight
    per model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    @param key_figure: name of the key figure
    @type key_figure: string
    @param y_max: max value for y-axis
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    # plot key figure per model as bar-plot
    key_figures_list =\
        data_extraction.nest_and_process_by_values_advanced(basic_data,
                                                            [iDict['matching_mode'],
                                                             iDict['model'],
                                                             iDict['/matcher']],
                                                            data_extraction.average_key_figure_per_voting_ball,
                                                            key_figure)

    #print('key figures list =', key_figures_list)

    n_voting_balls = len(key_figures_list[0][1][0][1])
    #print('n_voting_balls =',n_voting_balls, 'content =', key_figures_list[0][1][0][1])

    import copy
    key_figures_list_per_voting_ball = [ copy.deepcopy(key_figures_list) for _ in xrange(n_voting_balls) ]
    for voting_ball_idx in range(n_voting_balls):
        for mode_idx in range(len(key_figures_list)):
            for model_idx in range(len(key_figures_list[mode_idx][1])):
                #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])
                key_figure_of_voting_ball = key_figures_list[mode_idx][1][model_idx][1][voting_ball_idx]
                key_figures_list_per_voting_ball[voting_ball_idx][mode_idx][1][model_idx][1] = key_figure_of_voting_ball
                #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])

    #print('key figures list voting_balls =', key_figures_list_per_voting_ball)
    #print('key figures list =', key_figures_list)

    bar_by_model(key_figures_list_per_voting_ball[voting_ball_idx], ax, 'matching modes', show_legend=show_legend, logarithmic_y_axis=logarithmic_y_axis)
    ax.set_ylabel('average '+ key_figure )
    ax.set_ylim(0, y_max)
    if show_legend:
        ax.legend(loc='upper right')


def plot_key_figure(data, ax, show_legend, key_figure, y_max, logarithmic_y_axis = False):
    """
    version for data from Kroischkes original System (--> no voting balls --> no lists of values).
    plot pose-key-figures such as
    - n_raw_poses,
    - n_pose_clusters,
    - raw_poses_max_weight,
    - raw_poses_median_weight
    per model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    @param key_figure: name of the key figure
    @type key_figure: string
    @param y_max: max value for y-axis
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    # plot key figure per model as bar-plot
    key_figures_list =\
        data_extraction.nest_and_process_by_values_advanced(basic_data,
                                                            [iDict['matching_mode'],
                                                             iDict['model'],
                                                             iDict['/matcher']],
                                                            data_extraction.average_key_figure,
                                                            key_figure)

    #print('key figures list =', key_figures_list)

    #n_voting_balls = len(key_figures_list[0][1][0][1])
    #print('n_voting_balls =',n_voting_balls, 'content =', key_figures_list[0][1][0][1])

    #import copy
    #key_figures_list_per_voting_ball = [ copy.deepcopy(key_figures_list) for _ in xrange(n_voting_balls) ]
    #for voting_ball_idx in range(n_voting_balls):
    #    for mode_idx in range(len(key_figures_list)):
    #        for model_idx in range(len(key_figures_list[mode_idx][1])):
    #            #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])
    #            key_figure_of_voting_ball = key_figures_list[mode_idx][1][model_idx][1][voting_ball_idx]
    #            key_figures_list_per_voting_ball[voting_ball_idx][mode_idx][1][model_idx][1] = key_figure_of_voting_ball
    #            #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])

    #print('key figures list voting_balls =', key_figures_list_per_voting_ball)
    #print('key figures list =', key_figures_list)

    bar_by_model(key_figures_list, ax, 'matching modes', show_legend=show_legend, logarithmic_y_axis=logarithmic_y_axis)
    ax.set_ylabel('average '+ key_figure )
    ax.set_ylim(0, y_max)
    if show_legend:
        ax.legend(loc='upper right')


def plot_recog_rates_first_pose(baseline_data, experiment_data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type baseline_data: ExperimentDataStructure
    @param baseline_data: experiment data for comparison. for plotting
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    #print('baseline data =', baseline_data)
    #print('data =', data)
    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    recognition_rate_per_mode_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print ('\nbaseline aglorithm: average recognition rate per mode (one instance)')
    for mode, rr in recognition_rate_per_mode_baseline:
        print('mode:' , mode, 'recognition rate (one instance): ',rr)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                    [['poses']],
                                                    [['notes', 'matching_mode']])

    # calculate recognition rate with the pose that got the highest verificationWeight from the verifier.
    # for each model:
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    recognition_rate_per_mode_with_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    print('extensions: with verifier: average recognition rate per mode (one instance)')
    for mode, rr in recognition_rate_per_mode_with_verifier_info:
        print('mode:' , mode, 'recognition rate (one instance): ',rr)

    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    recognition_rate_per_model_without_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    recognition_rate_per_mode_without_verifier_info = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print('extensions: without verifier: average recognition rate per mode (one instance)')
    for mode, rr in recognition_rate_per_mode_without_verifier_info:
        print('mode:', mode, 'recognition rate (one instance): ', rr)

    # add baseline-data to extension-data
    recognition_rates_per_model = []
    # convert into list of lists, because mode-names in list of tuples can't be edited
    recognition_rate_per_model_with_verifier_info = [list(elem) for elem in recognition_rate_per_model_with_verifier_info]
    recognition_rate_per_model_without_verifier_info =[list(elem) for elem in recognition_rate_per_model_without_verifier_info]
    recognition_rates_per_model.append(recognition_rate_per_model_baseline[0])
    for mode_results in recognition_rate_per_model_with_verifier_info:
        mode_results[0] = mode_results[0] + ', with verifier'
        recognition_rates_per_model.append(mode_results)
    for mode_results in recognition_rate_per_model_without_verifier_info:
        mode_results[0] = mode_results[0] + ', without verifier'
        recognition_rates_per_model.append(mode_results)

    # plot and plot-settings
    bar_by_model(recognition_rates_per_model, ax, 'datasets', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if (show_legend):
        ax.legend(loc='upper right')


def plot_recog_rates_all_poses(data, ax, show_legend=True):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment tmp_data for plotting
    @type ax: Axes
    @param ax: axis to plot recognition rate for all returned poses to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate for all models
    recog_rate_all_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    bar_by_model(recog_rate_all_poses, ax, 'matching modes')
    ax.set_ylabel('recognition rate (2 instances)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='upper right')

    # print overall recognition rates
    rr_first_2_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    for mode, rr in rr_first_2_poses_per_mode:
        print('mode: %s, recognition rate best 2 pose cluster: %f' % (mode, rr))


def plot_precisions_of_recognized_poses(baseline_data, experiment_data, ax_distance, ax_angle, show_legends = True):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type baseline_data: ExperimentDataStructure
    @param baseline_data: experiment data of baseline algorithm for comparison
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose_id, pose in sorted(entry[iDict['poses']].items()):
            if pose_id == 0 and pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(baseline_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # get distance error
    average_distance_error_baseline = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error
    average_angle_error_baseline = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    # get distance error per matching mode
    average_distance_error_per_mode_baseline = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error per matching mode
    average_angle_error_per_mode_baseline = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    print('\nbaseline algorithm: average distance error per mode (one instance)')
    for mode, rr in average_distance_error_per_mode_baseline:
        print('mode:', mode, 'distance error (one instance): ', rr)
    print('baseline algorithm: average angle error per mode (one instance)')
    for mode, rr in average_angle_error_per_mode_baseline:
        print('mode:', mode, 'angle error (one instance): ', rr)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        # an entry is a run
        # get the highest verified pose
        max_idx = -1
        max_weight = -1  # because not-verified poses have verifictationWeight = 0
        for pose_idx, pose in sorted(entry[iDict['poses']].items()):
            if pose['verificationWeight'] > max_weight:
                max_idx = pose_idx
                max_weight = pose['verificationWeight']
        # only store highest verified pose if it is verified and correct
        if entry[iDict['poses']][max_idx]['correct'] and entry[iDict['poses']][max_idx]['verified']:
            entry_list = list(entry)
            entry_list[iDict['poses']] = entry[iDict['poses']][max_idx]
            recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # get distance error
    average_distance_error_extensions_with_verifier_info = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error
    average_angle_error_extensions_with_verifier_info = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    # get distance error per matching mode
    average_distance_error_extensions_with_verifier_info_per_mode = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error per matching mode
    average_angle_error_extensions_with_verifier_info_per_mode = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    print('extensions: with verifier: average distance error per mode (one instance)')
    for mode, rr in average_distance_error_extensions_with_verifier_info_per_mode:
        print('mode:', mode, 'distance error (one instance): ', rr)
    print('extensions: with verifier: average angle error per mode (one instance)')
    for mode, rr in average_angle_error_extensions_with_verifier_info_per_mode:
        print('mode:', mode, 'angle error (one instance): ', rr)

    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose_idx, pose in sorted(entry[iDict['poses']].items()):
            if pose_idx == 0 and pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # get distance error
    average_distance_error_extensions_without_verifier_info = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error
    average_angle_error_extensions_without_verifier_info = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    # get distance error per matching mode
    average_distance_error_extensions_without_verifier_info_per_mode = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    # get angle error per matching mode
    average_angle_error_extensions_without_verifier_info_per_mode = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    print('extensions: without verifier: average distance error per mode (one instance)')
    for mode, rr in average_distance_error_extensions_without_verifier_info_per_mode:
        print('mode:', mode, 'distance error (one instance): ', rr)
    print('extensions: without verifier: average angle error per mode (one instance)')
    for mode, rr in average_angle_error_extensions_without_verifier_info_per_mode:
        print('mode:', mode, 'angle error (one instance): ', rr)

    # merge all data
    average_distance_error_per_model = []
    average_angle_error_per_model =[]
    # convert into list of lists, because mode-names in list of tuples can't be edited
    average_angle_error_extensions_with_verifier_info = [list(elem) for elem in
                                                         average_angle_error_extensions_with_verifier_info]
    average_distance_error_extensions_with_verifier_info = [list(elem) for elem in
                                                         average_distance_error_extensions_with_verifier_info]
    average_angle_error_extensions_without_verifier_info = [list(elem) for elem in
                                                            average_angle_error_extensions_without_verifier_info]
    average_distance_error_extensions_without_verifier_info = [list(elem) for elem in
                                                            average_distance_error_extensions_without_verifier_info]
    average_distance_error_per_model.append(average_distance_error_baseline[0])
    average_angle_error_per_model.append(average_angle_error_baseline[0])
    for mode_results in average_angle_error_extensions_with_verifier_info:
        mode_results[0] = mode_results[0] + ', with verifier'
        average_angle_error_per_model.append(mode_results)
    for mode_results in average_angle_error_extensions_without_verifier_info:
        mode_results[0] = mode_results[0] + ', without verifier'
        average_angle_error_per_model.append(mode_results)
    for mode_results in average_distance_error_extensions_with_verifier_info:
        mode_results[0] = mode_results[0] + ', with verifier'
        average_distance_error_per_model.append(mode_results)
    for mode_results in average_distance_error_extensions_without_verifier_info:
        mode_results[0] = mode_results[0] + ', without verifier'
        average_distance_error_per_model.append(mode_results)

    bar_by_model(average_distance_error_per_model, ax_distance, 'matching modes',
                 show_legend=show_legends, show_top_ticks=False)
    ax_distance.set_ylabel('distance error / model diameter')
    bar_by_model(average_angle_error_per_model, ax_angle, 'matching modes',
                 show_legend=show_legends, show_top_ticks=False)
    ax_angle.set_ylabel('angle error in rad')


if __name__ == '__main__':

    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    print('Computing pose errors for all runs...')
    post_processing.compute_pose_errors(data)
    print('Assigning µ values to poses for all runs...')
    post_processing.assign_mu_to_poses(data)
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)
    print('Adding verificationWeight and verified-field to each pose...')
    post_processing.compute_verified(data) # adds verificationWeight and verified-field to each pose


    # for comparison reasons:
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)
    post_processing.compute_pose_errors(baseline_data)
    post_processing.compute_recognition(baseline_data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    add_matching_mode(data)
    add_matching_mode(baseline_data)
    #data.to_yaml(SAVE_PATH_WO_EXTENSION + '.yaml')

    # # plot
    # fig1, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(15, 10)) #*****legend has no title******
    # plot_recog_rates_first_pose(baseline_data, data, fig1.axes[0], show_legend=True)
    # #plot_recog_rates_all_poses(data, fig1.axes[1])
    #
    # fig2, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(15, 10))
    # plot_average_matching_time(baseline_data, data, fig2.axes[0], show_legend=True)
    #
    # fig3, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 10))
    # plot_precisions_of_recognized_poses(baseline_data, data, *fig3.axes)
    #
    # fig4, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 10)) #*****legend has no title******
    # plot_recog_rates_first_pose(baseline_data, data, fig4.axes[1], show_legend=True)
    # plot_average_matching_time(baseline_data, data, fig4.axes[0], show_legend=False)

    fig12, axes = plt.subplots(2, 2, figsize=plotting.cm2inch(15, 16.5))
    fig12.subplots_adjust(bottom=0.27)
    plot_average_matching_time(baseline_data, data, fig12.axes[0], show_legend=False)
    plot_recog_rates_first_pose(baseline_data, data, fig12.axes[1], show_legend=False)
    plot_precisions_of_recognized_poses(baseline_data, data, fig12.axes[2], fig12.axes[3],
                                        show_legends = False)
    fig12.axes[1].legend(bbox_to_anchor=(0.5,0.01), loc='lower center', borderaxespad=0.,
                         title='matching modes', bbox_transform=fig12.transFigure,)
    fig12.tight_layout(rect=[0, 0.27, 1, 1])


    #fig5, axes = plt.subplots(1, 1)#, figsize=plotting.cm2inch(9, 6)) #*****legend has no title******
    #plot_recog_rates_all_poses(data, fig5.axes[0])

    # fig8 , axes = plt.subplots(1,2)#, figsize=plotting.cm2inch(16, 6))
    # plot_key_figure_of_voting_ball(data, fig8.axes[0], show_legend=True, key_figure='n_raw_poses',
    #                                y_max=16000, voting_ball_idx = 0)
    # plot_key_figure_of_voting_ball(baseline_data, fig8.axes[1], show_legend=True, key_figure='n_raw_poses',
    #                                y_max=16000)
    #
    # fig9, axes = plt.subplots(1, 2)#, figsize=plotting.cm2inch(16, 6))
    # plot_key_figure_of_voting_ball(data, fig9.axes[0], show_legend=True, key_figure='n_pose_clusters',
    #                                y_max=16000, voting_ball_idx=0)
    # plot_key_figure_of_voting_ball(baseline_data, fig9.axes[1], show_legend=True, key_figure='n_pose_clusters',
    #                                y_max=16000)
    #
    # fig10, axes = plt.subplots(1, 2)#, figsize=plotting.cm2inch(16, 6))
    # fig10.axes[0].set_yscale('log')
    # plot_key_figure_of_voting_ball(data, fig10.axes[0], show_legend=True, key_figure='raw_poses_max_weight',
    #                                y_max=10000, voting_ball_idx=0, logarithmic_y_axis = True)
    # plot_key_figure_of_voting_ball(baseline_data, fig10.axes[1], show_legend=True, key_figure='raw_poses_max_weight',
    #                                y_max=10000, logarithmic_y_axis = True)
    #
    # fig11, axes = plt.subplots(1, 2)#, figsize=plotting.cm2inch(16, 6))
    # fig11.axes[0].set_yscale('log')
    # plot_key_figure_of_voting_ball(data, fig11.axes[0], show_legend=True, key_figure='raw_poses_median_weight',
    #                                y_max=1000, voting_ball_idx=0, logarithmic_y_axis = True)
    # plot_key_figure_of_voting_ball(baseline_data, fig11.axes[1], show_legend=True, key_figure='raw_poses_median_weight',
    #                                y_max=1000, logarithmic_y_axis = True)

    fig13, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5 + 5, 5.8))
    fig13.subplots_adjust(right=0.4)
    plot_average_model_building_time(baseline_data, data, fig13.axes[0], show_legend=True)
    fig13.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                        title='matching modes', bbox_transform=fig13.transFigure)
    fig13.tight_layout(rect=[0, 0, 0.6, 1])

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        #plotting.save_figure(fig1, plt_dir, 'hinterstoisser_influence_of_extension_packages_recognition_rates')
        #plotting.save_figure(fig2, plt_dir, 'hinterstoisser_influence_of_extension_packages_matching_times')
        #plotting.save_figure(fig3, plt_dir, 'hinterstoisser_influence_of_extension_packages_precisions')
        #plotting.save_figure(fig4, plt_dir, 'hinterstoisser_influence_of_extension_packages_matching_time_recog_rate')
        plotting.save_figure(fig12, plt_dir, 'hinterstoisser_influence_of_extension_packages_matching_time_recog_rate_precisions')
        #plotting.save_figure(fig5, plt_dir, 'hinterstoisser_influence_of_extension_packages_recognition_rates_all_poses')
        #plotting.save_figure(fig8, plt_dir, 'hinterstoisser_influence_of_extension_packages_number_of_raw_poses')
        #plotting.save_figure(fig9, plt_dir, 'hinterstoisser_influence_of_extension_packages_number_of_pose_clusters')
        #plotting.save_figure(fig10, plt_dir, 'hinterstoisser_influence_of_extension_packages_average_raw_poses_max_weight')
        #plotting.save_figure(fig11, plt_dir, 'hinterstoisser_influence_of_extension_packages_average_raw_poses_median_weight')
        plotting.save_figure(fig13, plt_dir, 'hinterstoisser_influence_of_extension_packages_model_building_time')

    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
