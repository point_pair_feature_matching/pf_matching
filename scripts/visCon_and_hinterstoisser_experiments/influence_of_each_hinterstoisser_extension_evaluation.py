#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from IPython.utils.pickleutil import istype
import itertools

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/influence_of_each_Hinterstoisser_extension/influence_of_each_Hinterstoisser_extension_geometric_primitives.bin'
SAVE_PATH_WO_EXTENSION = '~/ROS/experiment_data/influence_of_each_Hinterstoisser_extension/influence_of_each_Hinterstoisser_extension_geometric_primitives'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15 # rad


def add_equivalent_poses_info(data):
    
    # load data hat contains information on the equivalent poses! 
    print ("Try loading equivalent poses information from rotational_symmetry_experiment.bin")
    equivalent_poses_data = ExperimentDataStructure()
    equivalent_poses_data.from_pickle('~/ROS/experiment_data/rotational_symmetry/'+
                                      'xavers_original_version_of_code/rotational_symmetry.bin')
    
    equivalent_poses = equivalent_poses_data.get_run_data(["equivalentPoses"], False, True) 
    
    # get the equivalent poses of all models and put them into a dictionary
    # this code works also for those cases, where one run contains the equivalent poses of ALL models
    raw_poses={};
    model_names = [];
    for equivalent_pose in equivalent_poses:
        if equivalent_pose[1] is not None:
            for model in equivalent_pose[1].keys(): 
                if model not in model_names:       
                    model_names.append(model)
                    raw_poses.update({model: equivalent_pose[1][model]})
              
    # we need to set the poses only once at the first run "0", compute_pose_errors will likely use that information
    if not raw_poses :
        print ("Error, no equivalent poses found!")
    else:
        print ("Found equivalent poses for the following models: ", model_names)
        for model in model_names:
            print (model)
            print (raw_poses[model])
    
    print ("Try adding equivalent poses information from rotational_symmetry_experiment.bin to the current data")
    models_done=[];
    for runID, run in data['runData'].items():
        if 'equivalentPoses' in run.keys():
            if not run['equivalentPoses']:
                print ("Error adding equivalent poses, run already contains equivalent poses-entry, but it's empty!")
                if 'models' in run.keys():
                    for model_name in run['models'].values():
                        if model_name not in models_done:
                            run['equivalentPoses'][model_name] = raw_poses[model_name]
                            models_done.append(model_name)
            else:
                print ("Error adding equivalent poses, run already contains equivalent pose:\n", run['equivalentPoses'])   
        else: 
            if 'models' in run.keys():
                for model_name in run['models'].values():
                    if model_name not in models_done:
                        run['equivalentPoses'] = {}
                        run['equivalentPoses'][model_name] = raw_poses[model_name]
                        models_done.append(model_name)
            
    print ("Successfully added the equivalent poses information for the following models:", models_done)
    
    print ("Now trying to recalculate the pose-errors!")
    from pf_matching.evaluation.data_post_processing import compute_pose_errors
    compute_pose_errors(data)
    print('Saving data to .bin...')
    data.to_pickle(SAVE_PATH_WO_EXTENSION + '_with_recalculated_pose_errors.bin')


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_neighbour_PPFs_for_training = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_voting_balls = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_hinterstoisser_clustering = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            voted_for_adjacent_rotation_angles = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']
            used_flag_array = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']                 
	    used_rotational_symmetry = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']            
       
            matching_mode = None
            if used_neighbour_PPFs_for_training:
                matching_mode = 'used neighbour-PPFs for training'
            elif used_voting_balls:
                matching_mode = 'used voting balls'
            elif used_hinterstoisser_clustering:
                matching_mode = 'used Hinterstoisser-Clustering'
            elif voted_for_adjacent_rotation_angles:
                matching_mode = 'voted for adjacent scene-rotation-angles'
            elif used_flag_array == 2:
                matching_mode = 'used flag array of type "STL prebuilt"'
	    elif used_rotational_symmetry:
		matching_mode = 'used rotational symmetry'
            else:
                matching_mode = 'original'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass

def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'), use_log_yscale=False):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width = 0.1
    colors = ['r', 'g', 'b', 'k', 'c', 'm', 'y']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        plt_tuples = [(group_dict[entry[0]] + i * bar_width, entry[1]) for entry in group_data]

        ax.bar(*zip(*plt_tuples), width=bar_width, color=colors[i], label=bar_names, log=use_log_yscale)

    # set group names under bars
    ax.set_xlim(0)
    ax.set_xticks(group_loc + n_bars / 2 * bar_width)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=8)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')

def listit(t):
    """
    @type t: tuple of tuples to be converted into list of lists
    @return list of lists
    @note from https://stackoverflow.com/questions/1014352/how-do-i-convert-a-nested-tuple-of-tuples-and-lists-to-lists-of-lists-in-python
    """
    return list(map(listit, t)) if isinstance(t, (list, tuple)) else t

def tupleit(l):
    """
    @type l: list of lists
    @return tulple of tuples
    """
    return tuple(map(tupleit, l)) if isinstance(l, (tuple, list)) else l

def plot_average_clustering_time(data, ax, show_legend, use_log_yscale=False):
    """

    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_clustering_time_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_clustering_time)
    print('average clustering time per matching mode')
    for mode, clustering_time in average_clustering_time_per_mode:
        print('mode: %s, time: %f ms' % (mode, clustering_time))

    average_clustering_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_clustering_time)

    bar_by_model(average_clustering_time_per_model, ax, 'matching modes', show_legend=show_legend, use_log_yscale=use_log_yscale)
    if use_log_yscale:
        ax.set_ylim(0.1, getMax(average_clustering_time_per_model))
    else:
        ax.set_ylim(0, getMax(average_clustering_time_per_model))
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average clustering time in s')

def plot_average_matching_time(data, ax, show_legend, use_log_yscale=False):
    """

    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_match_time_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per matching mode')
    for mode, match_time in average_match_time_per_mode:
        print('mode: %s, time: %f ms' % (mode, match_time))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    bar_by_model(average_match_time_per_model, ax, 'matching modes', show_legend=show_legend, use_log_yscale=use_log_yscale)
    if use_log_yscale:
        ax.set_ylim(0.1, getMax(average_match_time_per_model))
    else:
        ax.set_ylim(0, getMax(average_match_time_per_model))
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')


def getMax(tupleOfTuples):

    flat_tuple = [x for entry in tupleOfTuples for entry_2 in entry for entry_3 in entry_2 for x in entry_3]
    currentMax = 0.0
    for entry in flat_tuple:
        if type(entry) is not str and entry > currentMax:
            currentMax = entry
    return currentMax

def plot_average_voting_time(data, ax, show_legend, use_log_yscale=False):
    """

    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])
      
    ############ voting time with voting ball and with flag array instanciaion times ############
    
    average_voting_time_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_voting_time_w_VB_and_w_FA_building)
    print('average voting time per matching mode')
    for mode, voting_time in average_voting_time_per_mode:
        print('mode: %s, time: %f ms' % (mode, voting_time))
    
    average_voting_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_voting_time_w_VB_and_w_FA_building)

    bar_by_model(average_voting_time_per_model, ax, 'matching modes', show_legend=show_legend, use_log_yscale=use_log_yscale)
    
    ############ overlay with voting time without voting ball but with flag array instanciaion times ############
    
    average_voting_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_voting_time_wo_VB_and_w_FA_building)

    bar_by_model(average_voting_time_per_model, ax, 'matching modes', show_legend=show_legend, use_log_yscale=use_log_yscale)
    
    ############ overlay with voting time without voting ball or flag array instanciaion times ############
    
    average_voting_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_voting_time_wo_VB_and_wo_FA_building)


    bar_by_model(average_voting_time_per_model, ax, 'matching modes', show_legend=show_legend, use_log_yscale=use_log_yscale)
    if use_log_yscale:
        ax.set_ylim(0.1, getMax(average_voting_time_per_model))
    else:
        ax.set_ylim(0, getMax(average_voting_time_per_model))
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average voting time in s')


def plot_precisions_of_recognized_poses(experiment_data, ax_distance, ax_angle):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose in entry[iDict['poses']].values():
            if pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # plot distance error
    average_distance_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    bar_by_model(average_distance_error, ax_distance, 'matching modes')
    ax_distance.set_ylabel('distance error / model diameter')

    # plot angle error
    average_angle_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    bar_by_model(average_angle_error, ax_angle, 'matching modes')
    ax_angle.set_ylabel('angle error in rad')


def plot_recog_rates_first_pose(data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """
    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # print overall recognition rates
    rr_first_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    for mode, rr in rr_first_poses_per_mode:
        print('mode: %s, recognition rate best pose cluster: %f' % (mode, rr))

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    bar_by_model(rr_first_poses, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylabel('recognition rate (1 instance)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='upper right')

def plot_recog_rates_all_poses(data, ax, show_legend=True):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment tmp_data for plotting
    @type ax: Axes
    @param ax: axis to plot recognition rate for all returned poses to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate for all models
    recog_rate_all_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    bar_by_model(recog_rate_all_poses, ax, 'matching modes')
    ax.set_ylabel('recognition rate (2 instances)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='upper right')

    # print overall recognition rates
    rr_first_2_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    for mode, rr in rr_first_2_poses_per_mode:
        print('mode: %s, recognition rate best 2 pose cluster: %f' % (mode, rr))


if __name__ == '__main__':
    
     # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    
    # get equivalent pose information from rotatoinal_symmetry experiment and add it to the current data
    #if not os.path.exists('/home/markus/ROS/experiment_data/influence_of_each_Hinterstoisser_extension/'+
    #                      'influence_of_each_Hinterstoisser_extension_geometric_primitives_with_recalculated_pose_errors.bin'): #if .bin does not contain the equivalent poses, this file is missing ...
    #    print("equivalent poses might be missing...")
    #    data.from_pickle(FILE_PATH)
    #    add_equivalent_poses_info(data) # ... so let's generate it
        
    #else:
    #	 print("equivalent poses might NOT be missing, skip importing them...")
    #	 data.from_pickle(SAVE_PATH_WO_EXTENSION + '_with_recalculated_pose_errors.bin')
    
    # do post processing
    post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)

    
    # prepare
    mpl.rcParams.update(plotting.thesis_default_settings)
    
    add_matching_mode(data)

    # plot  
    print("calculating and plotting bar charts")
    fig1, axes = plt.subplots(1, 1)#, figsize=plotting.cm2inch(16, 6))
    plot_recog_rates_first_pose(data, fig1.axes[0], show_legend=True)
    
    fig4, axes = plt.subplots(1, 1)
    plot_recog_rates_all_poses(data, fig4.axes[0], show_legend=True)

    fig2, axes = plt.subplots(1, 1)#, figsize=plotting.cm2inch(9, 6))
    #plt.yscale('log')
    plot_average_matching_time(data, fig2.axes[0], show_legend=True, use_log_yscale=False)

    fig3, axes = plt.subplots(1, 2)#, figsize=plotting.cm2inch(16, 6))
    plot_precisions_of_recognized_poses(data, *fig3.axes)
    
    fig5, axes = plt.subplots(1,1)
    #plt.yscale('log')
    plot_average_voting_time(data, fig5.axes[0], show_legend = True, use_log_yscale=False)
    
    fig6, axes = plt.subplots(1,1)
    plot_average_clustering_time(data, fig6.axes[0], show_legend = True, use_log_yscale=False)
    
    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig1, plt_dir, 'hinterstoisser_extensions_recognition_rates_first_pose')
        plotting.save_figure(fig2, plt_dir, 'hinterstoisser_extensions_matching_times')
        plotting.save_figure(fig3, plt_dir, 'hinterstoisser_extensions_precisions')
        plotting.save_figure(fig4, plt_dir, 'hinterstoisser_extensions_recognition_rates_all_poses')
        plotting.save_figure(fig5, plt_dir, 'hinterstoisser_extensions_voting_times')
        plotting.save_figure(fig6, plt_dir, 'hinterstoisser_extensions_clustering_times')
    else:
        print('Plots are not saved.')
        
    # show figures for checking Layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
    
