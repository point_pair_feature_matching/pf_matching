# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction

# ####################### script settings ############################################
FILE_PATH = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_searchradius_angleDiffThresh/ICCV_hinterstoisser_searchradius_angleDiffThresh'
SAVE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_searchradius_angleDiffThresh/ICCV_hinterstoisser_searchradius_angleDiffThresh'



if __name__ == '__main__':

    print("loading data...")

    data_1 = ExperimentDataStructure()
    data_1.from_pickle(FILE_PATH + '_1.bin')

    data_2 = ExperimentDataStructure()
    data_2.from_pickle(FILE_PATH + '_2.bin')

    data_3 = ExperimentDataStructure()
    data_3.from_pickle(FILE_PATH + '_3.bin')

    data_4 = ExperimentDataStructure()
    data_4.from_pickle(FILE_PATH + '_4.bin')

    data_5 = ExperimentDataStructure()
    data_5.from_pickle(FILE_PATH + '_5.bin')

    data_6 = ExperimentDataStructure()
    data_6.from_pickle(FILE_PATH + '_6.bin')

    data_to_append = [data_2, data_3, data_4, data_5, data_6]

    print("removing runs with id '-1'...")
    # remove first (empty) run with id '-1'
    for data in data_to_append:
        del data['runData'][-1]

    print("getting information how many runs are available in each data-file...")
    highest_run_idx_of_data_1 = 0
    for run_idx in data_1['runData']:
        if highest_run_idx_of_data_1 < run_idx:
            highest_run_idx_of_data_1 = run_idx
    # increment by 1
    highest_run_idx_of_data_1 += 1
    print('data_1 has %s runs' %highest_run_idx_of_data_1)
    print("it is assumed, that all datafiles have the same number of runs!")

    print("merging data...")
    # concatenate runs
    # ATTENTION! it is assumed, that every data-part has the same number of runs!
    for idx, data in enumerate(data_to_append):
        for run_idx in data['runData']:
            data_1['runData'][run_idx+(idx*highest_run_idx_of_data_1)]=data['runData'][run_idx]

    print("checking index plausibility...")
    # checking plausibility, i.e. continuity of new run-ids
    indices_plausible = True
    prev_idx = -2
    for run_idx in sorted(data_1['runData']):
        print(run_idx)
        diff = run_idx - prev_idx
        if diff != 1:
            print ("ERROR: new run-indices are not continuous: %s and %s" %(prev_idx, run_idx))
            indices_plausible = False
            break
        else:
            prev_idx = run_idx

    if indices_plausible:
        print("All new run-indices are continuous. Everything should be fine.")
        input('Hit <ENTER> to SAVE merged data...')

        data_1.to_pickle(SAVE_PATH_WO_EXTENSION + '_merged.bin')
        data_1.to_yaml(SAVE_PATH_WO_EXTENSION + '_merged.yaml')
    else:
        print("indices were not plausible. Aborting...")




    # old code for merging with individual number_of_runs to append
    # for run_idx in data_3['runData']:
    #     print('old index in data_3', run_idx)
    #     print('new index in data_1', run_idx + 500)
    #
    #     data_1['runData'][run_idx + 500] = data_3['runData'][run_idx]

    # for run_idx in data_1['runData']:
    #    print(run_idx)
    # input('HIT ENTER TO CONTINUE...')
