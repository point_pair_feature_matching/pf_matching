#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_publish_n_best_poses/ICCV_hinterstoisser_publish_n_best_poses'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
BASELINE_FILE_PATH = '~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference_with_computed_errors.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too
max_rotation_error = pi / 15  # rad
THETA = 0.3 # max error score for error metric of [Hodan et al. 2018]
# Scene indices of benchmark by [Hodan et al. 2018]
SCENE_INDICES = [3, 8, 17, 27, 36, 38, 39, 41, 47, 58, 61, 62, 64, 65, 69, 72, 79, 89, 96, 97, 102,
                 107, 110, 115, 119, 124, 126, 136, 153, 156, 162, 166, 175, 176, 178, 203, 207,
                 217, 219, 221, 224, 243, 248, 249, 254, 258, 263, 266, 268, 277, 283, 307, 310,
                 322, 326, 338, 342, 356, 362, 365, 368, 387, 389, 402, 415, 417, 425, 428, 434,
                 435, 438, 442, 446, 453, 473, 474, 476, 477, 480, 491, 494, 499, 501, 503, 521,
                 527, 529, 532, 535, 540, 543, 549, 560, 563, 571, 575, 589, 603, 607, 611, 615,
                 625, 642, 648, 649, 650, 652, 667, 669, 679, 691, 695, 703, 708, 711, 727, 736,
                 737, 739, 740, 750, 754, 756, 757, 758, 761, 762, 764, 768, 769, 770, 773, 775,
                 785, 788, 791, 794, 801, 803, 804, 808, 809, 819, 821, 828, 837, 840, 844, 850,
                 856, 867, 871, 877, 883, 886, 894, 902, 903, 904, 907, 909, 918, 925, 934, 942,
                 952, 956, 961, 968, 969, 972, 982, 984, 991, 1001, 1012, 1038, 1050, 1057, 1061,
                 1069, 1071, 1087, 1098, 1099, 1103, 1107, 1117, 1123, 1131, 1144, 1148, 1151, 1157,
                 1168, 1169, 1176, 1180, 1199, 1212]

def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if not used_vb and not used_n_PPFs and not used_hc and\
                    not used_fa and not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = "\\textsc{Kroischke}'s S2S"
            elif used_vb and used_n_PPFs and used_hc and used_fa \
                    and voted_for_adj_rot_angl:
                matching_mode = "\\textsc{Hinterstoisser}'s extensions\nwithout verifier"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_at_least_one_correct_pose_barchart(experiment_data, best_n_range, instances_per_scene, ax,
        votingball_idx, title, show_legend):
    """
    plots the probability that there is at least one correct pose within the the first n poses
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart with the best %s poses and %s instances per scene" % (
            best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        # if posenumber != 0:
        #    list[posenumber] = list[posenumber-1]
        for entry in base_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball, start=0):
                if idx <= posenumber and pose['correct']:
                    list[posenumber] += 1
                    break

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(base_data), color='#33691e') #green
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def plot_at_least_one_false_pose_barchart(experiment_data, best_n_range, instances_per_scene, ax,
        votingball_idx, title, show_legend):
    """
    plots the probability that there is at least one false pose within the the first n poses
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart with the best %s poses and %s instances per scene" % (
            best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        # if posenumber != 0:
        #    list[posenumber] = list[posenumber-1]
        for entry in base_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball, start=0):
                if idx <= posenumber and not pose['correct']:
                    list[posenumber] += 1
                    break

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(base_data), color='#c62828') #red
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def plot_correct_pose_barchart(experiment_data, best_n_range, instances_per_scene, ax,
                               votingball_idx, title, show_legend):
    """
    at which position of the 'n_best_poses' are the correct poses. plot the number of poses over position
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart with the best %s poses and %s instances per scene" % (
        best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        for entry in base_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball):
                if idx == posenumber:
                    # pose = poses_of_votingball[idx]
                    if pose['correct']:
                        # print("idx: %s; pose->correct: %s" % (idx, True))
                        list[idx] += 1

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(base_data), color='#1a237e')
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def plot_recog_rates_first_pose(data, baseline_data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter basic_data to keep only the scenes used by [Hodan, Michel et al. 2018]
    filtered_basic_data = []
    for run_id, run_data in enumerate(basic_data):
        # get scene index from scene name
        scene_name = run_data[iDict['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if scene_idx in SCENE_INDICES:
            filtered_basic_data.append(run_data)

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # recognition rate per model
    recognition_rate_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    #recognition rate
    recognition_rate_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # add baseline-data to extension-data
    recognition_rates_per_model = []
    recognition_rates_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rates_per_model.append(recognition_rate_per_model_extensions[0])

    print("\nRecognition rates:")
    print(recognition_rate_baseline)
    print(recognition_rate_extensions)

    print("\nRecognition rates per model:")
    for x in recognition_rates_per_model:
        print(str(x))

    bar_by_model(recognition_rates_per_model, ax, 'matching mode', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')


if __name__ == '__main__':

    # load data and do post-processing
    print('Loading Experiment-data...')
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_is_necessary = False

    if compute_pose_errors_is_necessary:

        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

        # compute pose errors with Hinterstoisser's error metric
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative)

        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        #data.to_yaml(FILE_PATH_WO_EXTENSION + "_with_computed_errors.yaml")
    else:
        data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")

    # for comparison reasons:
    print('Loading Baseline-data...')
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)

    # mark which run belongs to which matching mode
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    # load plotting parameters
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    print("plotting bar chart of correct poses")
    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig1.axes[0].set_ylabel("rate of correct\nhypotheses at $i$'th pose")

    plot_correct_pose_barchart(data, 20, 2, fig1.axes[0], show_legend=False,
                               title='small voting ball', votingball_idx=1)
    plot_correct_pose_barchart(data, 20, 2, fig1.axes[1], show_legend=False,
                               title='large voting ball', votingball_idx=2)
    fig1.tight_layout()

    print("plotting bar chart probability of at least one correct pose")
    fig2, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig2.axes[0].set_ylabel("probability of having at\nleast one correct hypothesis\nwithin first $i$ poses")

    plot_at_least_one_correct_pose_barchart(data, 20, 2, fig2.axes[0],
                                            show_legend=False,
                                            title='small voting ball', votingball_idx=1)
    plot_at_least_one_correct_pose_barchart(data, 20, 2, fig2.axes[1],
                                            show_legend=False,
                                            title='large voting ball', votingball_idx=2)
    fig2.tight_layout()

    print("plotting bar chart probability of at least one false pose")
    fig4, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig4.axes[0].set_ylabel("probability of having at\nleast one false hypothesis\nwithin first $i$ poses")

    plot_at_least_one_false_pose_barchart(data, 20, 2, fig4.axes[0],
                                          show_legend=False,
                                          title='small voting ball', votingball_idx=1)
    plot_at_least_one_false_pose_barchart(data, 20, 2, fig4.axes[1],
                                          show_legend=False,
                                          title='large voting ball', votingball_idx=2)
    fig4.tight_layout()

    # plot recognition rates
    print("plotting bar chart recognition rates for one-instance-detection")
    fig3, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5 + 5, 5.8))  # sharey=True)
    fig3.subplots_adjust(right=0.4)
    plot_recog_rates_first_pose(data, baseline_data, fig3.axes[0], show_legend=True)
    fig3.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                        title='matching modes', bbox_transform=fig3.transFigure)
    fig3.tight_layout(rect=[0, 0, 0.6, 1])

    # plot another figure that contains only the big voting ball:
    fig5, axes = plt.subplots(1,2, figsize=plotting.cm2inch(15, 5.8))
    fig5.axes[0].set_ylabel("probability of having at\nleast one correct hypothesis\nwithin first $i$ poses")
    fig5.axes[1].set_ylabel("probability of having at\nleast one false hypothesis\nwithin first $i$ poses")
    plot_at_least_one_correct_pose_barchart(data, 20, 2, fig5.axes[0],
                                            show_legend=False,
                                            title='large voting ball', votingball_idx=2)
    plot_at_least_one_false_pose_barchart(data, 20, 2, fig5.axes[1],
                                          show_legend=False,
                                          title='large voting ball', votingball_idx=2)
    fig5.tight_layout()

    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig1, plt_dir, 'barcharts_correct_poses')
        plotting.save_figure(fig2, plt_dir, 'barcharts_at_least_one_correct_pose')
        plotting.save_figure(fig4, plt_dir, 'barcharts_at_least_one_false_pose')
        plotting.save_figure(fig3, plt_dir, 'recognition_rate_per_model' )
        plotting.save_figure(fig5, plt_dir, 'barcharts_at_least_one_correct_pose_and_at_least_one_false_pose')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
