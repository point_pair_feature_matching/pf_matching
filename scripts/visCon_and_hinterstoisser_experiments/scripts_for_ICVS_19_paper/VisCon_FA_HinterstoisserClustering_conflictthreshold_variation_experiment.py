#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_from_verifier, assign_mu_to_poses


# other script settings
TIME_OUT_S = 30
DATASET_DICTS = [{'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py',
                  'scene_preprocessor_settings' : {# do not do that 'd_points': 0.008,
                                                   'd_points_is_relative': False,
                                                   'remove_largest_plane': True
                                                   },
                  'verifier_settings' : {'zbufferingresolution': 75}, # Krone's favoured value
                  'changing_verifier_settings' : [ {'searchradius': 0.5, #TODO: set these values to the ones obtained in searchradius_angleDiffThres_evaluation!
                                                    'searchradius_is_relative': True,
                                                    'scenezbufferingthreshold': 0.5, #TODO: set these values to the ones obtained in searchradius_angleDiffThres_evaluation!
                                                    'zbufferingthreshold_is_relative': True,
                                                    'angleDiffThresh_in_pi': 1/15, # 1/30   equivalent to 6° #TODO: set these values to the ones obtained in searchradius_angleDiffThres_evaluation!
                                                    'supportthreshold': 0.01, # 0.0  TODO: set these values to the ones obtained in searchradius_angleDiffThres_evaluation!
                                                    'penaltythreshold': 0.2} # 0.35  TODO: set these values to the ones obtained in searchradius_angleDiffThres_evaluation!
                                                  ]
                  }
                 ]
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data//ICVS_2019/VisCon_FA_and_HintClustering/conflictthreshold/conflictthreshold'
FILE_PATH = '~/ROS/experiment_data/ICVS_2019/VisCon_FA_and_HintClustering/publish_n_best_poses/publish_n_best_poses.bin'
CONFLICTTHRESHOLDS = [0.01, 0.02, 0.04, 0.06, 0.08, 0.10, 0.15, 0.20, 0.30, 0.4, 0.5, 1.0]

class ConflictThresholdVariationExperiment(ExperimentBase):
    """
    experiment to analyse the mu_v and mu_p values relating to true/false pose
    """
    def __init__(self, input_data, data_storage, dataset_dicts, save_path_wo_extension, conflictthresholds):
    
        ExperimentBase.__init__(self, data_storage)

        self.input_data = input_data
        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])
        self.save_path_wo_extension = save_path_wo_extension
        self.conflictthresholds = conflictthresholds
        
    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=True,
                                         dyn_reconfigure_callbacks=True)
        self.launch_and_connect_with_verifier_without_matcher()
        
        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       # do not set d_points here!
                                       }
        
        verifier_settings = {'show_results': False,
                             'publish_statistics': True,
                             'publish_mu_values': False,
                             'use_conflict_analysis': True,
                             'use_verification_of_normals': True,
                             'filter_selfocclusion': False
                             }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
    def looping(self):
        
        # calculate number of runs to perform
        n_runs = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_runs += dataset.get_number_of_scenes() * len(dataset_dict['changing_verifier_settings']) * dataset.get_number_of_models()
        
        n_runs = n_runs * len(self.conflictthresholds)
        
        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()
        
        # iterate over epsilonband_relativ
        for dataset_dict in self.dataset_dicts:
            
            # extract and setup dataset
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)
            
            self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                          dataset_dict['scene_preprocessor_settings'])
            self.interface.set_dyn_reconfigure_parameters('verifier',
                                                          dataset_dict['verifier_settings'])

            
            print('dataset: %s' %(dataset.get_name()))
            
            verifier_settings_for_dataset_list = dataset_dict['changing_verifier_settings']
            for verifier_settings in verifier_settings_for_dataset_list:
                print("searchradius: %s; angleDiffThresh: 1/%d pi; V: %s; P: %s"\
                      %(verifier_settings['searchradius'], 1/verifier_settings['angleDiffThresh_in_pi'],\
                        verifier_settings['supportthreshold'], verifier_settings['penaltythreshold']))
                self.interface.set_dyn_reconfigure_parameters('verifier',
                                                              verifier_settings)
                
            
                for conflicthresh in self.conflictthresholds:
                    print("conflictthresholds: %s" %(conflicthresh))
                    verifier_settings = {'conflictthreshold': conflicthresh}
                    self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)

                    # simulate matching all models of the dataset and verifying them afterwards
                    # for verification model-specific down-sampled scenes are being used ( difference to Krone )
                    self.loop_scenes_with_one_model_for_verification_without_matcher(dataset, re, self.input_data, n_best_poses=16) # TODO use best value from "publish_n_best_poses_evaluation"

                    print('Saving temporary data for this setup and all previous setups in case something goes wrong...')
                    self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        
        print('Computing pose errors for all runs...')
        compute_pose_errors_from_verifier(self.data)
        
        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        #self.data.to_yaml(self.save_path_wo_extension + '.yaml')
        
if __name__ == '__main__':
    
    input_data = data_structure.ExperimentDataStructure()
    input_data.from_pickle(FILE_PATH)

    data_storage = data_structure.ExperimentDataStructure()
    experiment = ConflictThresholdVariationExperiment(input_data, 
                                                      data_storage,
                                                      DATASET_DICTS,
                                                      SAVE_FILE_WO_EXTENSION,
                                                      CONFLICTTHRESHOLDS)
    
    experiment.run(wait_before_looping=True)
