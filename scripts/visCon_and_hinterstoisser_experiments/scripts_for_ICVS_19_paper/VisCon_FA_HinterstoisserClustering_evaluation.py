#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

# ####################### script settings ############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICVS_2019/VisCon_FA_and_HintClustering/VisCon_FA_and_HintClustering/VisCon_FA_and_HintClustering'
BASELINE_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist.bin'
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

# TODO: add data of competing algorithms:
# from viscon_tuning experiment, using the faster setting with gapsizefactor = 0
VISCON_RECOG_RATES_ONE_INSTANCE = ("\\textsc{Kim}'s Visibility Context\nwith $\mathrm{VoxelSize_{idx}}=1$, "
                                   '$\mathrm{gapSizeFactor_{surface}}=0.0$ and '
                                   '$\mathrm{ignoreFactor}=0.5$',
                                    [('cuboid', 0.9), ('cylinder', 0.65), ('elliptic_prism', 0.95),
                                     ('hexagonal_frustum', 0.65), ('pyramid', 0.9)])
VISCON_RECOG_RATES_TWO_INSTANCES = ("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=1$, "
                                   '$\mathrm{gapSizeFactor_{surface}}=0.0$ and '
                                   '$\mathrm{ignoreFactor}=0.5$',
                                    [('cuboid', 0.59999999999999998),
                                     ('cylinder', 0.34999999999999998),
                                     ('elliptic_prism', 0.59999999999999998),
                                     ('hexagonal_frustum', 0.32500000000000001),
                                     ('pyramid', 0.45000000000000001)])
VISCON_MATCHINGTIME = ("\\textsc{Kim}'s Visibility Context\nwith $\mathrm{VoxelSize_{idx}}=1$, "
                       '$\mathrm{gapSizeFactor_{surface}}=0.0$ and '
                       '$\mathrm{ignoreFactor}=0.5$',
                       [('cuboid', 37711.82314453125), ('cylinder', 50321.6281640625),
                       ('elliptic_prism', 39790.82201171875), ('hexagonal_frustum', 59082.73109375),
                       ('pyramid', 33421.43079101563)])
VISCON_BUILDINGTIME = ("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=1$, "
                       '$\mathrm{gapSizeFactor_{surface}}=0.0$ and '
                       '$\mathrm{ignoreFactor}=0.5$',
                       [('cuboid', 4722.0412109375), ('cylinder', 24697.241796875),
                        ('elliptic_prism', 5386.0828125), ('hexagonal_frustum', 30241.670703125),
                        ('pyramid', 8415.887109375)])
# from conflict_threshold_tuning experiment:
HINTERSTOISSER_RECOG_RATES_ONE_INSTANCE = ("\\textsc{Hinterstoisser}'s extensions\nwith "
                                           "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, "
                                           "$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                                           "conflict threshold = 0.02",
                                           [('cuboid', 0), ('cylinder', 0), ('elliptic_prism', 0),
                                            ('hexagonal_frustum', 0), ('pyramid', 0)])
HINTERSTOISSER_RECOG_RATES_TWO_INSTANCES = ("\\textsc{Hinterstoisser}'s extensions with "
                                            "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$,"
                                            "\n$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                                            "with conflict threshold = 0.02",
                                            [('cuboid', 0), ('cylinder', 0), ('elliptic_prism', 0),
                                             ('hexagonal_frustum', 0), ('pyramid', 0)])
# from the hinterstoisser_hash_table_performances_evaluation:
# with CMPH model hash table and CMPH flag array and verifier
HINTERSTOISSER_MATCHINGTIME = ("\\textsc{Hinterstoisser}'s extensions\nwith "
                               "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, "
                               "$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                               "conflict threshold = 0.02",
                               [['cuboid', 1], ['cylinder', 1],
                                ['elliptic_prism', 1],
                                ['hexagonal_frustum', 1],
                                ['pyramid', 1]])
HINTERSTOISSER_BUILDINGTIME = ("\\textsc{Hinterstoisser}'s extensions with "
                                "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$,"
                                "\n$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                                "with conflict threshold = 0.02",
                                [('cuboid', 1), ('cylinder', 1),
                                 ('elliptic_prism', 1),
                                 ('hexagonal_frustum', 1), ('pyramid', 1)])

def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_visCon = run_data['dynamicParameters']['/matcher']['match_S2SVisCon']

            matching_mode = None
            if used_rs and collapsed_points and not used_visCon and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_visCon and d_dist == 0.05:
                matching_mode = "\\textsc{Kim}'s Visibility Context and \\textsc{Hinterstoisser}'s Flag Array and Clustering without verifier"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_four_bars = 0.2
    # colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    # colors_four_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_six_bars = ['k', '#c62828', '#33691e', '#ffb300', '#1a237e',]
    colors_four_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 4:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_four_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_four_bars, color=colors_four_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 4:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_four_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
        ax.set_xticklabels(ax.get_xticklabels(), ha='center')
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')

def plot_recognition_rate(data, baseline_data, ax_bar_plot, show_legend=False):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print("(baseline performance) Recognition rate (one instance detection):",recognition_rate_baseline)
    print("(baseline performance) Recognition rate for each model (one instance detection):",recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser + Verifier --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['poses']],
                                                       #data_extraction.recognition_rate_best_poses) # this line should be wrong, because this way
                                                                                                    # the verifier's info does not get used.
                                                       data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('(VisCon performance) Recognition rates per model for each combination:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates for each combination:",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser + Verifier --- get parameter combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations:
    best_setup_dict = {'voxelSize_intersectDetect': -1, # stores the voxelSize_index !
                       'ignoreFactor_intersectClassific': -1,
                       'gapSizeFactor_surfaceCase_intersectClassific': -1,
                       'recognition_rate':0.0}
    ignoreFactors = set()
    gapSizeFactors = set()
    for vs, rr_by_vs in recognition_rates_for_each_combination: # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs: # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf: # loop over gapSizeFactors
                if rr_by_gsf > best_setup_dict['recognition_rate']:
                    best_setup_dict['recognition_rate'] = rr_by_gsf
                    best_setup_dict['ignoreFactor_intersectClassific'] = igf
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] = gsf
                    best_setup_dict['voxelSize_intersectDetect'] = vs
                ignoreFactors.add(igf)
                gapSizeFactors.add(gsf)

    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate (one instance detection):", best_setup_dict)

    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                    best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters = rr_by_gsf

    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate for each model (one instance detection):",recognition_rate_per_model_best_parameters)


    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser without Verifier --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model_without_verifier = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recognition_rate_best_poses)

        recognition_rates_for_each_combination_per_model_without_verifier.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('(VisCon performance) Recognition rates per model for each combination:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination_without_verifier = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model_without_verifier:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination_without_verifier.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates for each combination:",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser without Verifier --- get parameter combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations:
    best_setup_dict = {'voxelSize_intersectDetect': -1, # stores the voxelSize_index !
                       'ignoreFactor_intersectClassific': -1,
                       'gapSizeFactor_surfaceCase_intersectClassific': -1,
                       'recognition_rate':0.0}
    ignoreFactors = set()
    gapSizeFactors = set()
    for vs, rr_by_vs in recognition_rates_for_each_combination_without_verifier: # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs: # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf: # loop over gapSizeFactors
                if rr_by_gsf > best_setup_dict['recognition_rate']:
                    best_setup_dict['recognition_rate'] = rr_by_gsf
                    best_setup_dict['ignoreFactor_intersectClassific'] = igf
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] = gsf
                    best_setup_dict['voxelSize_intersectDetect'] = vs
                ignoreFactors.add(igf)
                gapSizeFactors.add(gsf)

    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate without Verifier (one instance detection):", best_setup_dict)

    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model_without_verifier:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                    best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters_without_verifier = rr_by_gsf

    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate for each model without Verifier (one instance detection):",recognition_rate_per_model_best_parameters_without_verifier)


    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rate_per_model.append(HINTERSTOISSER_RECOG_RATES_ONE_INSTANCE)
    recognition_rate_per_model.append(VISCON_RECOG_RATES_ONE_INSTANCE)
    recognition_rate_per_model.append(("\\textsc{Kim}'s Visibility Context and \\textsc{Hinterstoisser}'s Flag Array and Clustering"
                                         "\nwith $\mathrm{VoxelSize_{idx}}=%s$, "
                                         "$\mathrm{gapSizeFactor_{surface}}=%s$ and "
                                         "$\mathrm{ignoreFactor}=%s$,\n without Verifier"
                                         % (best_setup_dict['voxelSize_intersectDetect'],
                                            best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                            best_setup_dict['ignoreFactor_intersectClassific']),
                                         recognition_rate_per_model_best_parameters_without_verifier))
    recognition_rate_per_model.append(("\\textsc{Kim}'s Visibility Context and \\textsc{Hinterstoisser}'s Flag Array and Clustering"
                                       "\nwith $\mathrm{VoxelSize_{idx}}=%s$, "
                                       "$\mathrm{gapSizeFactor_{surface}}=%s$ and "
                                       "$\mathrm{ignoreFactor}=%s$,\nwith "
                                       "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, "
                                       "$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                                       "conflict threshold = 0.02"
                                       %(best_setup_dict['voxelSize_intersectDetect'],
                                         best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                         best_setup_dict['ignoreFactor_intersectClassific']),
                                       recognition_rate_per_model_best_parameters))
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (one instance)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')

    return best_setup_dict


def plot_two_instance_recognition_rate(data, baseline_data, best_setup_dict, ax_bar_plot,
                                       show_legend=False):


    # **********************************************************************************************
    # baseline --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    print("(baseline performance) Recognition rate (two instance detection):",recognition_rate_baseline)
    print("(baseline performance) Recognition rate for each model (two instance detection):",recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser + Verifier --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['poses']],
                                                       #data_extraction.recog_rate_total)   # this should be wrong, because this does not use the verifier info
                                                       data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser + Verifier --- get recognition rate for best parameter combination
    # **********************************************************************************************

    # use best parameter combination of one-instance-detection:
    for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_best_parameters = rr_by_gsf
    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate (two instance detection):", recognition_rate_best_parameters)

    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters = rr_by_gsf


    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate for each model (two instance detection):",recognition_rate_per_model_best_parameters)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser w/o Verifier --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher',
                                       'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher',
                                       'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher',
                                       'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes', 'matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    # print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model_without_verifier = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][
                    voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:
                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict[
                                                            'gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recog_rate_total)

        recognition_rates_for_each_combination_per_model_without_verifier.append(
            (voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    # print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination_without_verifier = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model_without_verifier:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum(
                    rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf) / len(rr_by_gsf)
                recognition_rates_for_each_combination_without_verifier.append(
                    (voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    # print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context + Hinterstoisser w/o Verifier --- get recognition rate for best parameter combination
    # **********************************************************************************************

    # use best parameter combination of one-instance-detection:
    for vs, rr_by_vs in recognition_rates_for_each_combination_without_verifier:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                # print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                # mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    recognition_rate_best_parameters_without_verifier = rr_by_gsf
    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate without Verifier (two instance detection):",
          recognition_rate_best_parameters_without_verifier)

    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model_without_verifier:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                # print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                # mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    recognition_rate_per_model_best_parameters_without_verifier = rr_by_gsf

    print(
        "(VisCon+Hinterstoisser Flag Array and Clustering performance) Best Recognition Rate for each model without Verifier(two instance detection):",
        recognition_rate_per_model_best_parameters_without_verifier)



    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rate_per_model.append(HINTERSTOISSER_RECOG_RATES_TWO_INSTANCES)
    recognition_rate_per_model.append(VISCON_RECOG_RATES_TWO_INSTANCES)

    recognition_rate_per_model.append(("\\textsc{Kim}'s Visibility Context and \\{Hintestoisser}'s Flag Array and Clustering"
                                       "with $\mathrm{VoxelSize_{idx}}=%s$, "
                                       "$\mathrm{gapSizeFactor_{surface}}=%s$ and "
                                       "$\mathrm{ignoreFactor}=%s$ without Verifier"
                                       % (best_setup_dict['voxelSize_intersectDetect'],
                                          best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                          best_setup_dict['ignoreFactor_intersectClassific']),
                                       recognition_rate_per_model_best_parameters_without_verifier))
    recognition_rate_per_model.append(("\\textsc{Kim}'s Visibility Context and \\{Hintestoisser}'s Flag Array and Clustering "
                                       "with $\mathrm{VoxelSize_{idx}}=%s$, "
                                       "$\mathrm{gapSizeFactor_{surface}}=%s$ and "
                                       "$\mathrm{ignoreFactor}=%s$"
                                       %(best_setup_dict['voxelSize_intersectDetect'],
                                         best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                         best_setup_dict['ignoreFactor_intersectClassific']),
                                       recognition_rate_per_model_best_parameters))
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (two instances)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')


def plot_average_matching_time(data, baseline_data, best_setup_dict, ax_bar_plot, show_legend=False):
    """
    Plot the average matching time and compare Hinterstoisser's extensions with Kroischkes system

    Note: The time that the verifier needs to verify all poses is neglected here.
          It should not exceed a few seconds, Krone reports a few hundred ms. Therefore
          it is really small compared to the matching times.
    :param baseline_data: for comparison
    :param data:
    :param ax: ax of figure
    :param show_legend: boolean to toggle legend
    :return:
    """

    # ******************************************************************************************************************
    # baseline --- get average matching times
    # ******************************************************************************************************************

    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['datasetName'],
                                      ['notes','matching_mode']])

    # filter baseline-mode:
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter other matching-modes:
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    average_match_time_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['/matcher']],
                                                   data_extraction.average_match_time)

    print('(baseline performance) average matching time:',average_match_time_baseline, " ms")

    average_match_time_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['/matcher']],
                                                   data_extraction.average_match_time)
    print('(baseline performance) average match time per model: ', average_match_time_per_model_baseline, " ms")

    # ******************************************************************************************************************
    # using Visibility Context --- get average matching times
    # ******************************************************************************************************************

    iDict, base_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes', 'matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    # calculate matching time per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    average_match_time_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][
                    voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        average_match_time_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_match_time)

        average_match_time_per_model.append((voxelSize_idx, average_match_time_per_model_for_voxelSize_idx))

    # calculate matching time for all parameter combinations
    average_match_time = []
    for voxelSize_idx, mt_by_vs_idx in average_match_time_per_model:
        for igf, mt_by_igf in mt_by_vs_idx:
            for gsf, mt_by_gsf in mt_by_igf:
                average_mt = sum(
                    mt_by_model_name for model_name, mt_by_model_name in mt_by_gsf) / len(mt_by_gsf)
                average_match_time.append((voxelSize_idx, [(igf, [(gsf, average_mt)])]))

    #print('average matching time:', average_match_time)
    #print('average matching time per model: ', average_match_time_per_model)

    # **********************************************************************************************
    # using Visibility Context --- get matching time of combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_match_time_per_model:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_match_time_per_model_best_parameters = mt_by_gsf

    print("(VisCon+Hinterstoisser Flag Array and Clustering performance) average matching time:",
          average_match_time_per_model_best_parameters, " ms")

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_match_time:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_match_time_best_parameters = mt_by_gsf

    print(
        "(VisCon+Hinterstoisser Flag Array and Clustering performance) average matching time:",
        average_match_time_best_parameters, " ms")

    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # format data for plotting
    plot_data = [average_match_time_per_model_baseline[0],
                 HINTERSTOISSER_MATCHINGTIME,
                 VISCON_MATCHINGTIME,
                 ("\\textsc{Kim}'s Visibility Context and \\textsc{Hinterstoisser}'s Flag Array and Clustering"
                  "\nwith $\mathrm{VoxelSize_{idx}}=%s$, "
                  "$\mathrm{gapSizeFactor_{surface}}=%s$ and "
                  "$\mathrm{ignoreFactor}=%s$,\nwith "
                  "$r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, "
                  "$\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, "
                  "conflict threshold = 0.02"
                   %(best_setup_dict['voxelSize_intersectDetect'],
                     best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                     best_setup_dict['ignoreFactor_intersectClassific']),
                  average_match_time_per_model_best_parameters)]

    # plot and plot-settings
    bar_by_model(plot_data, ax_bar_plot, legend_title='matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=True)
    ax_bar_plot.set_ylim(1000,300001)
    #ax_bar_plot.set_yticks([t for t in range(0,250001,1000*30)])
    scaled_labels = [float(x) / 1000 for x in ax_bar_plot.get_yticks()]
    ax_bar_plot.set_yticklabels(scaled_labels)
    ax_bar_plot.set_ylabel('average matching time in s')
    if show_legend:
        ax_bar_plot.legend(loc='upper left')


def plot_average_model_building_time(baseline_data, experiment_data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @type baseline_data: ExperimentDataStructure
    @parameter data: experiment data
    @parameter baseline_data: experiment data of baseline algorithm for comparison
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    # **********************************************************************************************
    # baseline
    # **********************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_model_building_time_per_mode_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(baseline performance) average model building time per matching mode')
    for mode, model_building_time in average_model_building_time_per_mode_baseline:
        print('mode: %s, time: %f ms' % (mode, model_building_time))

    average_model_building_time_per_model_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(baseline performance) average_model_building_time_per_model:')
    for mode, model_building_time in average_model_building_time_per_model_baseline:
        print('mode: %s, time: %s ms' % (mode, model_building_time))

    # **********************************************************************************************
    # VisCon and Hintestoisser's Extensions
    # **********************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_model_building_time_per_mode_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(VisCon+Hinterstoisser Flag Array and Clustering performance) average model building time per matching mode')
    for mode, model_building_time in average_model_building_time_per_mode_extensions:
        print('mode: %s, time: %f ms' % (mode, model_building_time))

    average_model_building_time_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(VisCon+Hinterstoisser Flag Array and Clustering performance) average model building time per model')
    for mode, time in average_model_building_time_per_model_extensions:
        print('mode: %s, time: %s ms' % (mode, time))

    # add baseline-data to extension-data
    plot_data = []
    plot_data.append(average_model_building_time_per_model_baseline[0])
    plot_data.append(HINTERSTOISSER_BUILDINGTIME)
    plot_data.append(VISCON_BUILDINGTIME)
    # convert into list of lists, because mode-names in list of tuples can't be edited
    average_model_building_time_per_model_extensions = [list(elem) for elem in
                                                        average_model_building_time_per_model_extensions]
    plot_data.append(("\\textsc{Kim}'s Visibility Context with \\textsc{Hinterstoisser}'s\nFlag Array and Clustering",
                      average_model_building_time_per_model_extensions[0][1]))

    # plotting
    bar_by_model(plot_data, ax, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=True)
    ax.set_ylim(1000, 20000001)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average model building time in s')


if __name__ == '__main__':

    # load data and do post-processing
    print("Loading Experiment-Data and Baseline-Experiment-Data")
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_necessary = True

    if compute_pose_errors_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors and recognition-values for all runs...')
        post_processing.compute_pose_errors(data)

        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition(data, max_translation_error_relative,
                                            max_rotation_error)
        print('Saving Experiment-data with computed pose-errors...')
        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
    else:
        try:
            data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        except:
            input = raw_input("You specified that the pose errors were already calculated.\n"
                              "But there is no file named '%s_with_computed_errors.bin'.\n"
                              "Therefore it is assumed that the file '%s.bin' contains those errors.\n"
                              "Hit <ENTER> to continue..." % (
                              FILE_PATH_WO_EXTENSION, FILE_PATH_WO_EXTENSION))
            data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

    # assign the verification weight to the verified poses
    post_processing.compute_verified(data)

    # load baseline-data for comparison
    # note: errors and recognition numbers have to be already calculated
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)

    post_processing.compute_pose_errors(baseline_data)
    post_processing.compute_recognition(baseline_data, max_translation_error_relative, max_rotation_error)

    # add matiching modes
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    # set general graphic-settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    print("\nPlot recognition rates for one and two instance detection")
    # plot recognition rates
    fig_rr, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 10.3))
    fig_rr.subplots_adjust(bottom=0.42)
    best_setup = plot_recognition_rate(data, baseline_data,
                                            fig_rr.axes[0], show_legend=False)
    plot_two_instance_recognition_rate(data, baseline_data, best_setup,
                                       fig_rr.axes[1], show_legend=False)
    fig_rr.axes[0].legend(bbox_to_anchor=(0.5, 0.015),
                          bbox_transform=fig_rr.transFigure,
                          loc='lower center', borderaxespad=0., title='matching modes')
    fig_rr.tight_layout(rect=[0, 0.42, 1, 1])

    # save plots:
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates_one_and_two_instance_detection_barplot')

    print("\nPlot average matching time and average model building time")
    # plot average matching time:
    fig_mt, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 9.5))
    fig_mt.subplots_adjust(bottom=0.37)
    plot_average_matching_time(data, baseline_data, best_setup,
                               fig_mt.axes[0], show_legend=False)
    plot_average_model_building_time(baseline_data, data,
                                     fig_mt.axes[1], show_legend=False)
    fig_mt.axes[0].legend(bbox_to_anchor=(0.5, 0.02),
                          bbox_transform=fig_mt.transFigure,
                          loc='lower center', borderaxespad=0., title='matching modes')
    fig_mt.tight_layout(rect=[0, 0.37, 1, 1])

    # save plots:
    if SAVE_PLOTS:
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_and_average_model_building_time_barplot')


    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
