#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

# ####################### script settings ############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICVS_2019/VisCon_tuning_with_ADI_error_metric/viscon_tuning_merged'
BASELINE_FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist'
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # used for Drost's and Hinterstoisser's ADI error metric
max_rotation_error = pi / 15  # rad


# orthogonal projection for matplotlib 3D plot
# https://github.com/matplotlib/matplotlib/issues/537#issuecomment-2441206
from mpl_toolkits.mplot3d import proj3d
def orthogonal_proj(zfront, zback):
    a = (zfront + zback) / (zfront - zback)
    b = -2 * (zfront * zback) / (zfront - zback)
    return np.array([[1, 0, 0, 0.15],
                     [0, 1, 0, 0],
                     [0, 0, a, b],
                     [0, 0, 0, zback]])


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_visCon = run_data['dynamicParameters']['/matcher']['match_S2SVisCon']

            matching_mode = None
            if used_rs and collapsed_points and not used_visCon and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_visCon and d_dist == 0.05:
                matching_mode = "Kim's Visibility Context"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    colors_six_bars = ['k', # black
                       '#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       '#7b1fa2', '#af52d5',  # purple
                       '#795548', '#a98274',  # brown
                       ]
    colors_three_bars = ['k','#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    #print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i], label=bar_names, log=logarithmic_y_axis)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i], label=bar_names, log=logarithmic_y_axis)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def find_best_recognition_rate_only_3D_plots(data, baseline_data, ax_bar_plot, ax_4D_plot=None,
                                             show_legend=False, print_output=True):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    if print_output:
        print("(baseline performance) Recognition rate :",recognition_rate_baseline)
        print("(baseline performance) Recognition rate for each model:",recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recognition_rate_best_poses)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context --- get parameter combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations:
    best_setup_dict = {'voxelSize_intersectDetect': -1, # stores the voxelSize_index !
                       'ignoreFactor_intersectClassific': -1,
                       'gapSizeFactor_surfaceCase_intersectClassific': -1,
                       'recognition_rate':0.0}
    ignoreFactors = set()
    gapSizeFactors = set()
    for vs, rr_by_vs in recognition_rates_for_each_combination: # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs: # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf: # loop over gapSizeFactors
                if rr_by_gsf > best_setup_dict['recognition_rate']:
                    best_setup_dict['recognition_rate'] = rr_by_gsf
                    best_setup_dict['ignoreFactor_intersectClassific'] = igf
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] = gsf
                    best_setup_dict['voxelSize_intersectDetect'] = vs
                ignoreFactors.add(igf)
                gapSizeFactors.add(gsf)
    if print_output:
        print("(VisCon performance) Best Recognition Rate:", best_setup_dict)

    recognition_rate_best_parameters_faster_mt = []
    for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                # get recognition rates per model with gapSizeFactor = 0, because this is faster,
                # according to 'average_matching_time'-evaluation
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:
                    print("jölskdfjöaslkdf")
                    recognition_rate_best_parameters_faster_mt = rr_by_gsf

    if print_output:
        print("(VisCon performance) Best Recognition Rate for faster setting:", recognition_rate_best_parameters_faster_mt)

    recognition_rate_per_model_best_parameters = []
    recognition_rate_per_model_best_parameters_faster_mt = []
    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                    best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters = rr_by_gsf

                # get recognition rates per model with gapSizeFactor = 0, because this is faster,
                # according to 'average_matching_time'-evaluation
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:

                    recognition_rate_per_model_best_parameters_faster_mt = rr_by_gsf
    if print_output:
        print("(VisCon performance) Best Recognition Rate for each model:",recognition_rate_per_model_best_parameters)
        print("(VisCon performance) Best Recognition Rate for each model for faster setting:",recognition_rate_per_model_best_parameters_faster_mt)


    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = [recognition_rate_per_model_baseline[0],
                                  ["\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                   %(best_setup_dict['voxelSize_intersectDetect'],
                                     best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                   recognition_rate_per_model_best_parameters],
                                  (
                                  "\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                  % (best_setup_dict['voxelSize_intersectDetect'],
                                     0.0,
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                  recognition_rate_per_model_best_parameters_faster_mt)]
    print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (one instance)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')

    # **********************************************************************************************
    # do plotting --- 3d and 4d plots
    # **********************************************************************************************

    if ax_4D_plot is not None:

        # **********************************************************************************************
        # do plotting --- 4d plot (really confusing to read)
        # **********************************************************************************************

        from matplotlib import cm
        #get data-points:
        x=[]  # ignoreFactors
        y=[]  # gapSizeFactors
        z=[]  # voxelSize indices
        rr=[] # recognition rate
        for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                    z.append(vs)
                    x.append(igf)
                    y.append(gsf)
                    rr.append(rr_by_gsf)

        ax_4D_plot.scatter(x, y, z, c=rr,
                           s=10, cmap=cm.coolwarm)
        ax_4D_plot.set_xticks([x_ for x_ in sorted(ignoreFactors)])
        ax_4D_plot.set_yticks([y_ for y_ in sorted(gapSizeFactors)])
        ax_4D_plot.set_zticks([z_ for z_ in range(voxelsize_nsteps)])

        ax_4D_plot.set_xlabel('ignoreFactor')
        ax_4D_plot.set_ylabel('$\mathrm{gapSizeFactor_{surface}}$')
        ax_4D_plot.set_zlabel('$\mathrm{voxelSize_{idx}}$')

        ax_4D_plot.azim =  57.0
        ax_4D_plot.elev =  40.0

        ax_4D_plot.xaxis._axinfo['label']['space_factor'] = 2.8
        ax_4D_plot.yaxis._axinfo['label']['space_factor'] = 2.5
        ax_4D_plot.zaxis._axinfo['label']['space_factor'] = 2.0

        # make the panes transparent (r,g,b,alpha)
        ax_4D_plot.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines black (r,g,b,alpha)
        ax_4D_plot.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        # **********************************************************************************************
        # do plotting --- one 3d plot for every gap-size-factor
        #                 to visulalize that gap-size-factor has smallest influence (easy to read)
        # **********************************************************************************************

        # plot some point-clouds to see influence of each parameter better
        fig_3D_plots= plt.figure(figsize=plotting.cm2inch(15, 12))
        # first three subplots
        ax_3D_0 = fig_3D_plots.add_subplot(2, 2, 1, projection='3d')
        ax_3D_1 = fig_3D_plots.add_subplot(2, 2, 2, projection='3d')
        ax_3D_2 = fig_3D_plots.add_subplot(2, 2, 3, projection='3d')

        axes_3D = [ax_3D_0, ax_3D_1, ax_3D_2]
        gapSizeFactors_list = list(sorted(gapSizeFactors))

        for idx, ax_3D in enumerate(axes_3D):
            # ignoreFactors and voxelSize_indices for plotting
            xi = np.array(list(ignoreFactors), dtype=np.float)
            zi = np.array(range(voxelsize_nsteps), dtype=np.float)
            # generate 2D-meshgrids for the plot
            x_grid, z_grid = np.meshgrid(xi, zi)
            x_max=[]
            z_max=[]
            rr_max=[]
            for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
                for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                    for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                        if gsf == gapSizeFactors_list[idx]:
                            x_max.append(igf)
                            z_max.append(vs)
                            rr_max.append(rr_by_gsf)

            # interpolate recognition rate (rri) for the
            # ignoreFactors (xi) and voxelSize_indices (zi) from the real data (x,z,rr)
            #from scipy.interpolate import griddata
            #rri = griddata((x_max, z_max), rr_max, (x_grid, z_grid), method='cubic')
            rri = np.array(rr_max).reshape((xi.size,zi.size))

            # fourth dimension - colormap  --> not used, is confusing
            # create colormap according to rr-value
            color_dimension = rri  # change to desired fourth dimension
            minn, maxx = color_dimension.min(), color_dimension.max()
            norm = mpl.colors.Normalize(minn, maxx)
            m = plt.cm.ScalarMappable(norm=norm, cmap=cm.coolwarm)
            m.set_array([])
            fcolors = m.to_rgba(color_dimension)

            ax_3D.plot_surface(X=x_grid, Y=z_grid, Z=rri,
                               rstride=1, cstride=1,
                               cmap=cm.coolwarm,  linewidth=0.5,
                               #facecolors=fcolors,
                               antialiased=True,
                               vmin=minn, vmax=maxx, shade=False,
                               edgecolor='k', alpha=1)
            ax_3D.azim=57.0
            ax_3D.elev=40.0
            ax_3D.dist=11.0

            ax_3D.set_xlabel('ignoreFactor')
            ax_3D.set_ylabel('$\mathrm{voxelSize_{idx}}$')
            ax_3D.set_zlabel('recognition rate\n(one instance)')
            ax_3D.set_title("$\mathrm{gapSizeFactor_{surface}}=%s$" %gapSizeFactors_list[idx])
            ax_3D.set_xticks([x_ for x_ in xi])
            ax_3D.set_xticklabels(ax_3D.get_xticks(), rotation=-39, va='baseline', ha='left',
                                  rotation_mode='anchor')
            ax_3D.set_yticks([y_ for y_ in zi])
            scaled_labels = [int(t) for t in ax_3D.get_yticks()]
            ax_3D.set_yticklabels(scaled_labels, rotation=19, va='center', ha='right',
                                  rotation_mode=None)
            ax_3D.set_zlim([0,1])
            ax_3D.set_zticklabels(ax_3D.get_zticks(), va='center', ha='right')
            ax_3D.xaxis._axinfo['label']['space_factor'] = 2.8#2.8
            ax_3D.yaxis._axinfo['label']['space_factor'] = 2.5#2.5
            ax_3D.zaxis._axinfo['label']['space_factor'] = 3.3#2.5
            # make the panes transparent (r,g,b,alpha)
            ax_3D.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            ax_3D.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            ax_3D.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            # make the grid lines black (r,g,b,alpha)
            ax_3D.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
            ax_3D.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
            ax_3D.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        fig_3D_plots.tight_layout()

        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
            plotting.save_figure(fig_3D_plots, plt_dir, 'recognitionrates_dependencies_on_paramters_surface_plots')

    return (best_setup_dict, recognition_rate_per_model_best_parameters)


def find_best_recognition_rate_for_each_model(data, baseline_data, dataset, additional_plot_data,
                                              best_setup_dict_one_common_setting,
                                              ax_bar_plot, show_legend=False, print_output=True):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    if print_output:
        print("(baseline performance) Recognition rate :",recognition_rate_baseline)
        print("(baseline performance) Recognition rate for each model:",recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recognition_rate_best_poses)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context --- get for each model individually the parameter combination with
    #                              best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations:
    best_setup_dict = { dataset.get_model_name(model_idx): {'voxelSize_intersectDetect': -1, # stores the voxelSize_index !
                       'ignoreFactor_intersectClassific': -1,
                       'gapSizeFactor_surfaceCase_intersectClassific': -1,
                       'recognition_rate_for_this_model':0.0}
                        for model_idx in range(dataset.get_number_of_models())}
    ignoreFactors = set()
    gapSizeFactors = set()
    for model_idx in range(dataset.get_number_of_models()):
        model_name = dataset.get_model_name(model_idx)
        for vs, rr_by_vs in recognition_rates_for_each_combination_per_model: # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs: # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf: # loop over gapSizeFactors
                    for model, rr_by_model in rr_by_gsf:
                        if model == model_name:
                            if rr_by_model > best_setup_dict[model_name]['recognition_rate_for_this_model']:
                                best_setup_dict[model_name]['recognition_rate_for_this_model'] = rr_by_model
                                best_setup_dict[model_name]['ignoreFactor_intersectClassific'] = igf
                                best_setup_dict[model_name]['gapSizeFactor_surfaceCase_intersectClassific'] = gsf
                                best_setup_dict[model_name]['voxelSize_intersectDetect'] = vs
                            ignoreFactors.add(igf)
                            gapSizeFactors.add(gsf)
    if print_output:
        print("(VisCon performance) Best Recognition Rates with best individual setting for each model:")
        for model, rr_model_individual_setting in best_setup_dict.items():
            print (model,':' ,rr_model_individual_setting)

    recognition_rate_best_parameters = 0
    recognition_rate_per_model_best_parameters = []
    for model, model_data in best_setup_dict.items():
        recognition_rate_best_parameters += model_data['recognition_rate_for_this_model']
        recognition_rate_per_model_best_parameters.append((model, model_data['recognition_rate_for_this_model']))
    recognition_rate_best_parameters = recognition_rate_best_parameters / dataset.get_number_of_models()

    if print_output:
        print("(VisCon performance) Best Recognition Rate for each model when each model uses its individual best setting:",recognition_rate_per_model_best_parameters)
    if print_output:
        print("(VisCon performance) Best overall Recognition Rate when each model uses its individual best setting:",recognition_rate_best_parameters)

    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = [recognition_rate_per_model_baseline[0],
                                  ["\\textsc{Kim}'s Visibility Context with\n$\mathrm{VoxelSize_{idx}}=%s$,\n$\mathrm{gapSizeFactor_{surface}}=%s$\nand $\mathrm{ignoreFactor}=%s$"
                                   %(best_setup_dict_one_common_setting['voxelSize_intersectDetect'],
                                     best_setup_dict_one_common_setting['gapSizeFactor_surfaceCase_intersectClassific'],
                                     best_setup_dict_one_common_setting['ignoreFactor_intersectClassific']),
                                   additional_plot_data],
                                  ("\\textsc{Kim}'s Visibility Context with\nindividual model settings",
                                   recognition_rate_per_model_best_parameters)]
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (one instance)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')

    return best_setup_dict


def find_best_recognition_rate(data, baseline_data, ax_bar_plot, ax_4D_plot=None, show_legend=False,
                               print_output=True):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    if print_output:
        print("(baseline performance) Recognition rate :",recognition_rate_baseline)
        print("(baseline performance) Recognition rate for each model:",recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recognition_rate_best_poses)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context --- get parameter combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations:
    best_setup_dict = {'voxelSize_intersectDetect': -1, # stores the voxelSize_index !
                       'ignoreFactor_intersectClassific': -1,
                       'gapSizeFactor_surfaceCase_intersectClassific': -1,
                       'recognition_rate':0.0}
    ignoreFactors = set()
    gapSizeFactors = set()
    for vs, rr_by_vs in recognition_rates_for_each_combination: # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs: # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf: # loop over gapSizeFactors
                if rr_by_gsf > best_setup_dict['recognition_rate']:
                    best_setup_dict['recognition_rate'] = rr_by_gsf
                    best_setup_dict['ignoreFactor_intersectClassific'] = igf
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] = gsf
                    best_setup_dict['voxelSize_intersectDetect'] = vs
                ignoreFactors.add(igf)
                gapSizeFactors.add(gsf)
    if print_output:
        print("(VisCon performance) Best Recognition Rate:", best_setup_dict)

    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                    best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters = rr_by_gsf

                # get recognition rates per model with gapSizeFactor = 0, because this is faster,
                # according to 'average_matching_time'-evaluation
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:

                    recognition_rate_per_model_best_parameters_faster_mt = rr_by_gsf
    if print_output:
        print("(VisCon performance) Best Recognition Rate for each model:",recognition_rate_per_model_best_parameters)

    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = [recognition_rate_per_model_baseline[0],
                                  ("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                   %(best_setup_dict['voxelSize_intersectDetect'],
                                     best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                   recognition_rate_per_model_best_parameters),
                                  (
                                  "\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                  % (best_setup_dict['voxelSize_intersectDetect'],
                                     0.0,
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                  recognition_rate_per_model_best_parameters_faster_mt)]
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (one instance)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')

    # **********************************************************************************************
    # do plotting --- 3d coloured plot = 4d
    # **********************************************************************************************

    if ax_4D_plot is not None:

        # from https://stackoverflow.com/q/50467393
        from matplotlib import cm
        #get data-points:
        x=[]  # ignoreFactors
        y=[]  # gapSizeFactors
        z=[]  # voxelSize indices
        rr=[] # recognition rate
        for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                    z.append(vs)
                    x.append(igf)
                    y.append(gsf)
                    rr.append(rr_by_gsf)

        ax_4D_plot.scatter(x, y, z, c=rr,
                           s=10, cmap=cm.coolwarm)
        ax_4D_plot.set_xticks([x_ for x_ in sorted(ignoreFactors)])
        ax_4D_plot.set_yticks([y_ for y_ in sorted(gapSizeFactors)])
        ax_4D_plot.set_zticks([z_ for z_ in range(voxelsize_nsteps)])

        ax_4D_plot.set_xlabel('ignoreFactor')
        ax_4D_plot.set_ylabel('$\mathrm{gapSizeFactor_{surface}}$')
        ax_4D_plot.set_zlabel('$\mathrm{voxelSize_{idx}}$')

        ax_4D_plot.azim =  57.0
        ax_4D_plot.elev =  40.0

        ax_4D_plot.xaxis._axinfo['label']['space_factor'] = 2.8
        ax_4D_plot.yaxis._axinfo['label']['space_factor'] = 2.5
        ax_4D_plot.zaxis._axinfo['label']['space_factor'] = 2.0

        # make the panes transparent (r,g,b,alpha)
        ax_4D_plot.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines black (r,g,b,alpha)
        ax_4D_plot.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        # TODO: add a colorbar
        #m = cm.ScalarMappable(cmap=cm.coolwarm)
        #m.set_array(rr)
        #cbar = plt.colorbar(m)

        # plot some point-clouds to see influence of each parameter better
        fig_correlations= plt.figure(figsize=plotting.cm2inch(15, 12))
        # first three subplots
        ax_2D_1 = fig_correlations.add_subplot(2, 2, 1)
        ax_2D_2 = fig_correlations.add_subplot(2, 2, 2)
        ax_2D_3 = fig_correlations.add_subplot(2, 2, 3)
        # last subplot
        ax_3D = fig_correlations.add_subplot(2, 2, 4, projection='3d')

        # ignoreFactors and voxelSize_indices for plotting
        xi = np.array(list(ignoreFactors), dtype=np.float)
        zi = np.array(range(voxelsize_nsteps), dtype=np.float)
        # generate 2D-meshgrids for the plot
        x_grid, z_grid = np.meshgrid(xi, zi)
        x_max=[]
        z_max=[]
        rr_max=[]
        for vs, rr_by_vs in recognition_rates_for_each_combination:  # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                    if gsf == 3: # TODO: use gapSizeFactor that yields best recognition rate
                        x_max.append(igf)
                        z_max.append(vs)
                        rr_max.append(rr_by_gsf)

        # interpolate recognition rate (rri) for the
        # ignoreFactors (xi) and voxelSize_indices (zi) from the real data (x,z,rr)
        #from scipy.interpolate import griddata
        #rri = griddata((x_max, z_max), rr_max, (x_grid, z_grid), method='cubic')
        rri = np.array(rr_max).reshape((xi.size,zi.size))

        # fourth dimension - colormap
        # create colormap according to rr-value
        color_dimension = rri  # change to desired fourth dimension
        minn, maxx = color_dimension.min(), color_dimension.max()
        norm = mpl.colors.Normalize(minn, maxx)
        m = plt.cm.ScalarMappable(norm=norm, cmap=cm.coolwarm)
        m.set_array([])
        fcolors = m.to_rgba(color_dimension)

        ax_3D.plot_surface(X=x_grid, Y=z_grid, Z=rri,
                           rstride=1, cstride=1,
                           cmap=cm.coolwarm,  linewidth=0.5,
                           #facecolors=fcolors,
                           antialiased=True,
                           vmin=minn, vmax=maxx, shade=False,
                           edgecolor='k', alpha=1)
        ax_3D.azim=57.0
        ax_3D.elev=40.0

        ax_3D.set_xlabel('ignoreFactor')
        ax_3D.set_ylabel('$\mathrm{voxelSize_{idx}}$')
        ax_3D.set_zlabel('recognition rate')
        ax_3D.set_xticks([x_ for x_ in xi])
        ax_3D.set_yticks([y_ for y_ in zi])
        #ax_3D.set_zlim([0,1])
        ax_3D.xaxis._axinfo['label']['space_factor'] = 2.8
        ax_3D.yaxis._axinfo['label']['space_factor'] = 2.5
        ax_3D.zaxis._axinfo['label']['space_factor'] = 2.5
        #ax_3D.set_zticklabels(["       "+tl.get_text() for tl in ax_3D.get_zticklabels()])
        # make the panes transparent (r,g,b,alpha)
        ax_3D.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_3D.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_3D.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines black (r,g,b,alpha)
        ax_3D.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_3D.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_3D.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        fig_correlations.tight_layout()

        # TODO: remove after having found the best view
        axx = ax_3D.get_axes()
        azm = axx.azim
        ele = axx.elev
        dst = axx.dist
        print("the default camera perspective is: azimut:%s elevation:%s distance:%s" %(azm,ele,dst))

        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
            plotting.save_figure(fig_correlations, plt_dir, 'recognitionrates_dependencies_on_paramters_scatterplots')

    return best_setup_dict


def plot_two_instance_recognition_rate(data, baseline_data, best_setup_dict, ax_bar_plot, show_legend=False):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    print("(baseline performance) Recognition rate (two instance detection):",
          recognition_rate_baseline)
    print("(baseline performance) Recognition rate for each model (two instance detection):",
          recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recog_rate_total)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context --- plot recognition rate for best parameter combination
    # **********************************************************************************************

    # use best parameter combination of one-instance-detection:
    for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
        for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
            for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                #print("mode %s for loop with voxelsize %s, ignorefactor %s and gapsizefactor %s:" % (
                #mode_idx, vs, igf, gsf), rr_by_gsf)
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                    best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                    best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                    recognition_rate_per_model_best_parameters = rr_by_gsf

                # get recognition rates per model with gapSizeFactor = 0, because this is faster,
                # according to 'average_matching_time'-evaluation
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:

                    recognition_rate_per_model_best_parameters_faster_mt = rr_by_gsf

    recognition_rate_best_parameters = sum (rr_by_model for model, rr_by_model in recognition_rate_per_model_best_parameters)/dataset.get_number_of_models()
    recognition_rate_faster_mt = sum (rr_by_model for model, rr_by_model in recognition_rate_per_model_best_parameters_faster_mt)/dataset.get_number_of_models()
    print("(VisCon performance) Best Recognition Rate (two instance detection):",
          recognition_rate_best_parameters)
    print("(VisCon performance) Best Recognition Rate for faster setting (two instance detection):",
          recognition_rate_faster_mt)
    print("(VisCon performance) Best Recognition Rate for each model (two instance detection):",
          recognition_rate_per_model_best_parameters)
    print("(VisCon performance) Best Recognition Rate for each model for faster setting (two instance detection):",
          recognition_rate_per_model_best_parameters_faster_mt,'\n')

    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = [recognition_rate_per_model_baseline[0],
                                  ("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                   %(best_setup_dict['voxelSize_intersectDetect'],
                                     best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                   recognition_rate_per_model_best_parameters),
                                  (
                                  "\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                  % (best_setup_dict['voxelSize_intersectDetect'],
                                     0.0,
                                     best_setup_dict['ignoreFactor_intersectClassific']),
                                  recognition_rate_per_model_best_parameters_faster_mt)]
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (two instances)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')

    return recognition_rate_per_model_best_parameters


def plot_best_two_instance_recognition_rate_of_each_model(data, baseline_data, best_setup_dict,
                                                          best_setup_dict_common_setting,
                                                          additional_plot_data, ax_bar_plot,
                                                          show_legend=False):


    # ******************************************************************************************************************
    # baseline --- get data and calculate recognition rates
    # ******************************************************************************************************************

    # extract data
    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # filter baseline-data to sort out all other matching modes than 'baseline'
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter 'other matching modes'
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    # calculate recognition rate of baseline
    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    # caluclate recognition rate of baseline for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['recognition']],
                                                   data_extraction.recog_rate_total)

    print("\n(baseline performance) Recognition rate (two instance detection):",
          recognition_rate_baseline)
    print("(baseline performance) Recognition rate for each model (two instance detection):",
          recognition_rate_per_model_baseline)

    # **********************************************************************************************
    # using Visibility Context --- get data and calculate recognition rates
    # **********************************************************************************************

    # extract data
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['recognition'], ['poses']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes','matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    #print("Voxel Sizes of each model:", voxelSizes)

    # calculate recognition rates per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    recognition_rates_for_each_combination_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        recognition_rates_for_each_combination_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(filtered_base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['recognition']],
                                                       data_extraction.recog_rate_total)

        recognition_rates_for_each_combination_per_model.append((voxelSize_idx, recognition_rates_for_each_combination_per_model_for_voxelSize_idx))

    #print('recognition_rates_for_each_combination_per_model:',recognition_rates_for_each_combination_per_model)

    # calculate recogntion rates for all parameter combinations
    recognition_rates_for_each_combination = []
    for voxelSize_idx, rr_by_vs_idx in recognition_rates_for_each_combination_per_model:
        for igf, rr_by_igf in rr_by_vs_idx:
            for gsf, rr_by_gsf in rr_by_igf:
                average_rr = sum (rr_by_model_name for model_name, rr_by_model_name in rr_by_gsf)/len(rr_by_gsf)
                recognition_rates_for_each_combination.append((voxelSize_idx, [(igf, [(gsf, average_rr)])]))

    #print("(VisCon performance) Recognition rates :",recognition_rates_for_each_combination)

    # **********************************************************************************************
    # using Visibility Context --- plot recognition rate for best parameter combination
    # **********************************************************************************************

    recognition_rate_per_model_best_parameters = []
    for model_idx in range(dataset.get_number_of_models()):
        model_name = dataset.get_model_name(model_idx)
        # use best parameter combination of one-instance-detection:
        for vs, rr_by_vs in recognition_rates_for_each_combination_per_model:  # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                    for model, rr_by_model in rr_by_gsf:
                        if model_name == model and \
                                best_setup_dict[model_name]['voxelSize_intersectDetect'] == vs and \
                                best_setup_dict[model_name]['ignoreFactor_intersectClassific'] == igf and \
                                best_setup_dict[model_name]['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                            recognition_rate_per_model_best_parameters.append((model_name,rr_by_model))


    recognition_rate_best_parameters = sum (rr_by_model for model, rr_by_model in recognition_rate_per_model_best_parameters)/dataset.get_number_of_models()
    print("(VisCon performance) Best overall Recognition Rate when each model uses its individual best setting (two instance detection):",
          recognition_rate_best_parameters)
    print("(VisCon performance) Best Recognition Rate for each model when each model uses its individual best setting (two instance detection):",
          recognition_rate_per_model_best_parameters)

    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    recognition_rate_per_model = [recognition_rate_per_model_baseline[0],
                                  ["\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                                   % (best_setup_dict_common_setting['voxelSize_intersectDetect'],
                                      best_setup_dict_common_setting['gapSizeFactor_surfaceCase_intersectClassific'],
                                      best_setup_dict_common_setting['ignoreFactor_intersectClassific']),
                                      additional_plot_data],
                                  ("\\textsc{Kim}'s Visibility Context with individual model settings",
                                   recognition_rate_per_model_best_parameters)]
    #print("plot_data:", recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax_bar_plot, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    ax_bar_plot.set_ylabel('recognition rate (two instances)')
    ax_bar_plot.set_ylim(0, 1.0)
    if show_legend:
        ax_bar_plot.legend(loc='lower right')


def plot_average_matching_time(data, baseline_data, best_setup_dict, ax_bar_plot, ax_4D_plot=None,
                               best_individual_setup_dict=None, show_legend=False, print_output=True):
    """
    Plot the average matching time and compare Hinterstoisser's extensions with Kroischkes system

    Note: The time that the verifier needs to verify all poses is neglected here.
          It should not exceed a few seconds, Krone reports a few hundred ms. Therefore
          it is really small compared to the matching times.
    :param baseline_data: for comparison
    :param data:
    :param ax: ax of figure
    :param show_legend: boolean to toggle legend
    :return:
    """

    # ******************************************************************************************************************
    # baseline --- get average matching times
    # ******************************************************************************************************************

    iDict_baseline, base_data_baseline = \
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['datasetName'],
                                      ['notes','matching_mode']])

    # filter baseline-mode:
    filtered_base_data = []
    for run_id, run_data in enumerate(base_data_baseline):
        # filter other matching-modes:
        matching_mode = run_data[iDict_baseline['matching_mode']]
        if matching_mode == 'baseline':
            filtered_base_data.append(run_data)

    average_match_time_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['/matcher']],
                                                   data_extraction.average_match_time)
    if print_output:
        print('(baseline performance) average matching time:',average_match_time_baseline, " ms")

    average_match_time_per_model_baseline = \
        data_extraction.nest_and_process_by_values(filtered_base_data,
                                                   [iDict_baseline['matching_mode'],
                                                    iDict_baseline['model'],
                                                    iDict_baseline['/matcher']],
                                                   data_extraction.average_match_time)
    if print_output:
        print('(baseline performance) average match time per model: ', average_match_time_per_model_baseline, " ms")

    # ******************************************************************************************************************
    # using Visibility Context --- get average matching times
    # ******************************************************************************************************************

    iDict, base_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes', 'matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    # calculate matching time per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    average_match_time_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(base_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][
                    voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:

                    filtered_base_data.append(run_data)

        average_match_time_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(base_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_match_time)

        average_match_time_per_model.append((voxelSize_idx, average_match_time_per_model_for_voxelSize_idx))

    # calculate matching time for all parameter combinations
    average_match_time = []
    for voxelSize_idx, mt_by_vs_idx in average_match_time_per_model:
        for igf, mt_by_igf in mt_by_vs_idx:
            for gsf, mt_by_gsf in mt_by_igf:
                average_mt = sum(
                    mt_by_model_name for model_name, mt_by_model_name in mt_by_gsf) / len(mt_by_gsf)
                average_match_time.append((voxelSize_idx, [(igf, [(gsf, average_mt)])]))

    #print('average matching time:', average_match_time)
    #print('average matching time per model: ', average_match_time_per_model)

    # **********************************************************************************************
    # using Visibility Context --- get matching time of combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_match_time_per_model:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_match_time_per_model_best_parameters = mt_by_gsf

                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:
                    average_match_time_per_model_best_parameters_faster_mt = mt_by_gsf
    if print_output:
        print("(VisCon performance) average matching time for parameter combination with best recognition rate:",
              average_match_time_per_model_best_parameters, " ms")
        print(
            "(VisCon performance) average matching time for faster parameter combination with good recognition rate:",
            average_match_time_per_model_best_parameters_faster_mt, " ms")

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_match_time:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_match_time_best_parameters = mt_by_gsf

                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:
                    average_match_time_best_parameters_faster_mt = mt_by_gsf
    if print_output:
        print(
            "(VisCon performance) average matching time for parameter combination with best recognition rate:",
            average_match_time_best_parameters, " ms")
        print(
            "(VisCon performance) average matching time for faster parameter combination with good recognition rate:",
            average_match_time_best_parameters_faster_mt, " ms\n")

    if best_individual_setup_dict is not None:
        average_match_time_per_model_best_individual_parameters = []
        for model_idx in range(dataset.get_number_of_models()):
            model_name = dataset.get_model_name(model_idx)
            for vs, mt_by_vs in average_match_time_per_model:  # loop over voxel size indices
                for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
                    for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                        for model, mt_by_model in mt_by_gsf:
                            if model_name == model and \
                                    best_individual_setup_dict[model_name]['voxelSize_intersectDetect'] == vs and \
                                    best_individual_setup_dict[model_name]['ignoreFactor_intersectClassific'] == igf and \
                                    best_individual_setup_dict[model_name]['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:

                                average_match_time_per_model_best_individual_parameters.append((model_name,mt_by_model))
        print(
            "(VisCon performance) Average matching time for each model when each model uses its individual best setting:",
            average_match_time_per_model_best_individual_parameters)
        average_match_time_best_individual_parameters = sum(mt_by_model for model, mt_by_model in
                                                            average_match_time_per_model_best_individual_parameters) / dataset.get_number_of_models()
        print(
            "(VisCon performance) Average matching time when each model uses its individual best setting:",
            average_match_time_best_individual_parameters)


    # **********************************************************************************************
    # do plotting --- barplot
    # **********************************************************************************************

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # format data for plotting
    plot_data = [average_match_time_per_model_baseline[0]]
    if best_individual_setup_dict is None:
        plot_data.append(("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                           %(best_setup_dict['voxelSize_intersectDetect'],
                             best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                             best_setup_dict['ignoreFactor_intersectClassific']),
                          average_match_time_per_model_best_parameters))
        plot_data.append(("\\textsc{Kim}'s Visibility Context with $\mathrm{VoxelSize_{idx}}=%s$, $\mathrm{gapSizeFactor_{surface}}=%s$ and $\mathrm{ignoreFactor}=%s$"
                          %(best_setup_dict['voxelSize_intersectDetect'],
                            0.0,
                            best_setup_dict['ignoreFactor_intersectClassific']),
                          average_match_time_per_model_best_parameters_faster_mt))
    else:
        plot_data.append(("\\textsc{Kim}'s Visibility Context with\n$\mathrm{VoxelSize_{idx}}=%s$,\n$\mathrm{gapSizeFactor_{surface}}=%s$\nand $\mathrm{ignoreFactor}=%s$"
                         % (best_setup_dict['voxelSize_intersectDetect'],
                            best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                            best_setup_dict['ignoreFactor_intersectClassific']),
                         average_match_time_per_model_best_parameters))
        plot_data.append(("\\textsc{Kim}'s Visibility Context with\nindividual model settings",
                          average_match_time_per_model_best_individual_parameters))

    # plot and plot-settings
    bar_by_model(plot_data, ax_bar_plot, legend_title='matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=False)
    ax_bar_plot.set_ylim(1,150000)
    ax_bar_plot.set_yticks([t for t in range(0,150001,1000*30)])
    scaled_labels = [round(float(t) / 1000 / 60,2) for t in ax_bar_plot.get_yticks()]
    ax_bar_plot.set_yticklabels(scaled_labels)
    ax_bar_plot.set_ylabel('average matching time in min')
    if show_legend:
        ax_bar_plot.legend(loc='upper left')

    # **********************************************************************************************
    # do plotting --- 3d coloured plot = 4d
    # **********************************************************************************************

    if ax_4D_plot is not None:

        # **********************************************************************************************
        # do plotting --- 4d plot (really confusing to read)
        # **********************************************************************************************

        from matplotlib import cm
        #get data-points:
        x=[]  # ignoreFactors
        y=[]  # gapSizeFactors
        z=[]  # voxelSize indices
        mt=[] # matching time
        ignoreFactors = set()
        gapSizeFactors = set()
        for vs, rr_by_vs in average_match_time:  # loop over voxel size indices
            for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                    z.append(vs)
                    x.append(igf)
                    y.append(gsf)
                    mt.append(rr_by_gsf)
                    ignoreFactors.add(igf)
                    gapSizeFactors.add(gsf)

        ax_4D_plot.scatter(x, y, z, c=mt,
                           s=10, cmap=cm.coolwarm)
        ax_4D_plot.set_xticks([x_ for x_ in sorted(ignoreFactors)])
        ax_4D_plot.set_xticklabels(ax_4D_plot.get_xticks(), rotation=-39, va='baseline', ha='left',
                                   rotation_mode='anchor')
        ax_4D_plot.set_yticks([int(y_) for y_ in sorted(gapSizeFactors)])
        ax_4D_plot.set_yticklabels(ax_4D_plot.get_yticks(), rotation=19, va='center', ha='right',
                                   rotation_mode=None)
        ax_4D_plot.set_zticks([z_ for z_ in range(voxelsize_nsteps)])
        ax_4D_plot.set_zticklabels(ax_4D_plot.get_zticks(), va='center', ha='right')

        ax_4D_plot.set_xlabel('ignoreFactor')
        ax_4D_plot.set_ylabel('$\mathrm{gapSizeFactor_{surface}}$')
        ax_4D_plot.set_zlabel('$\mathrm{voxelSize_{idx}}$')

        ax_4D_plot.azim =  57.0
        ax_4D_plot.elev =  40.0

        ax_4D_plot.xaxis._axinfo['label']['space_factor'] = 2.8
        ax_4D_plot.yaxis._axinfo['label']['space_factor'] = 2.2
        ax_4D_plot.zaxis._axinfo['label']['space_factor'] = 2.0

        # make the panes transparent (r,g,b,alpha)
        ax_4D_plot.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_4D_plot.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines black (r,g,b,alpha)
        ax_4D_plot.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
        ax_4D_plot.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        # **********************************************************************************************
        # do plotting --- one 3d plot for every voxel-index
        #                 to visulalize that voxel-size has smallest influence (easy to read)
        # **********************************************************************************************

        # plot some point-clouds to see influence of each parameter better
        fig_3D_plots= plt.figure(figsize=plotting.cm2inch(15, 18))
        # first three subplots
        ax_3D_0 = fig_3D_plots.add_subplot(3, 2, 1, projection='3d')
        ax_3D_1 = fig_3D_plots.add_subplot(3, 2, 2, projection='3d')
        ax_3D_2 = fig_3D_plots.add_subplot(3, 2, 3, projection='3d')
        ax_3D_3 = fig_3D_plots.add_subplot(3, 2, 4, projection='3d')
        ax_3D_4 = fig_3D_plots.add_subplot(3, 2, 5, projection='3d')

        axes_3D = [ax_3D_0, ax_3D_1, ax_3D_2, ax_3D_3, ax_3D_4]
        voxelSizes_list = range(voxelsize_nsteps)

        for idx, ax_3D in enumerate(axes_3D):

            # ignoreFactors and gapSizeFactors for plotting
            xi = np.array(list(ignoreFactors), dtype=np.float)
            yi = np.array(list(gapSizeFactors), dtype=np.float)
            # generate 2D-meshgrids for the plot
            x_grid, y_grid = np.meshgrid(xi, yi)
            x_max=[]
            y_max=[]
            mt_max=[]
            for vs, rr_by_vs in average_match_time:  # loop over voxel size indices
                for igf, rr_by_igf in rr_by_vs:  # loop over ignoreFactors
                    for gsf, rr_by_gsf in rr_by_igf:  # loop over gapSizeFactors
                        if vs == voxelSizes_list[idx]:
                            x_max.append(igf)
                            y_max.append(gsf)
                            mt_max.append(rr_by_gsf)

            # interpolate average matching time (mti) for the
            # ignoreFactors (xi) and gapSizeFactors (yi) from the real data (x,y,mt)
            #from scipy.interpolate import griddata
            #rri = griddata((x_max, y_max), mt_max, (x_grid, y_grid), method='cubic')
            mti = (np.array(mt_max).reshape((xi.size,yi.size))).T

            # fourth dimension - colormap  --> not used, is confusing
            # create colormap according to rr-value
            color_dimension = mti  # change to desired fourth dimension
            minn, maxx = color_dimension.min(), color_dimension.max()
            norm = mpl.colors.Normalize(minn, maxx)
            m = plt.cm.ScalarMappable(norm=norm, cmap=cm.coolwarm)
            m.set_array([])
            fcolors = m.to_rgba(color_dimension)

            ax_3D.plot_surface(X=x_grid, Y=y_grid, Z=mti,
                               rstride=1, cstride=1,
                               cmap=cm.coolwarm,  linewidth=0.5,
                               #facecolors=fcolors,
                               antialiased=True,
                               vmin=minn, vmax=maxx, shade=False,
                               edgecolor='k', alpha=1)
            ax_3D.azim=57.0
            ax_3D.elev=40.0
            ax_3D.dist=11.0

            ax_3D.set_xlabel('ignoreFactor')
            ax_3D.set_ylabel('$\mathrm{gapSizeFactor_{surface}}$')
            ax_3D.set_zlabel('average matching time\nin min')
            ax_3D.set_title("$\mathrm{voxelSize_{idx}}=%s$" %voxelSizes_list[idx])
            ax_3D.set_xticks([x_ for x_ in xi])
            ax_3D.set_xticklabels(ax_3D.get_xticks(), rotation=-39, va='baseline', ha='left',
                                  rotation_mode='anchor')
            ax_3D.set_yticks([int(y_) for y_ in yi])
            ax_3D.set_yticklabels(ax_3D.get_yticks(), rotation=19, va='center', ha='right',
                                  rotation_mode=None)
            ax_3D.set_zlim(1, 150000)
            ax_3D.set_zticks([t for t in range(0, 150001, 1000 * 30)])
            scaled_labels = [round(float(t) / 1000 / 60, 2) for t in ax_3D.get_zticks()]
            ax_3D.set_zticklabels(scaled_labels, va='center', ha='right')
            ax_3D.xaxis._axinfo['label']['space_factor'] = 2.8
            ax_3D.yaxis._axinfo['label']['space_factor'] = 2.2
            ax_3D.zaxis._axinfo['label']['space_factor'] = 3.3
            # make the panes transparent (r,g,b,alpha)
            ax_3D.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            ax_3D.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            ax_3D.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
            # make the grid lines black (r,g,b,alpha)
            ax_3D.xaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
            ax_3D.yaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)
            ax_3D.zaxis._axinfo["grid"]['color'] = (189/255, 189/255, 189/255, 1)

        fig_3D_plots.tight_layout()

        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
            plotting.save_figure(fig_3D_plots, plt_dir, 'average_matching_time_dependencies_on_paramters_surface_plots')


def plot_average_model_building_time(baseline_data, experiment_data, ax, best_setup_dict,
                                     show_legend):
    """

    @type data: ExperimentDataStructure
    @type baseline_data: ExperimentDataStructure
    @parameter data: experiment data
    @parameter baseline_data: experiment data of baseline algorithm for comparison
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    # **********************************************************************************************
    # baseline
    # **********************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_model_building_time_per_mode_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(baseline performance) average model building time per matching mode')
    for mode, model_building_time in average_model_building_time_per_mode_baseline:
        print('mode: %s, time: %f ms' % (mode, model_building_time))

    average_model_building_time_per_model_baseline =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_model_building_time)

    print('(baseline performance) average_model_building_time_per_model:')
    for mode, model_building_time in average_model_building_time_per_model_baseline:
        print('mode: %s, time: %s ms' % (mode, model_building_time))

    # **********************************************************************************************
    # VisCon and Hintestoisser's Extensions
    # **********************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['statistics', '/matcher']],
                                     [['dynamicParameters', '/matcher', 'voxelSize_intersectDetect'],
                                      ['dynamicParameters', '/matcher', 'ignoreFactor_intersectClassific'],
                                      ['dynamicParameters', '/matcher', 'gapSizeFactor_surfaceCase_intersectClassific'],
                                      ['notes', 'matching_mode']])

    # TODO: specify values. use the ones from the experiment
    from math import sqrt
    voxelsize_nsteps = 5
    d_dist_rel = 0.05
    d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                 'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                 'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                 'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                 'pyramid': 0.64}  # with diam=156mm and d_min=100mm
    voxelSizes = {}
    for model_name in d_min_rel.keys():
        voxelSizes[model_name] = np.linspace(d_dist_rel / (sqrt(3)),  # min: voxeldiameter=d_points
                                             0.5 * d_min_rel[model_name],  # max: 0.5 * d_min_rel
                                             voxelsize_nsteps)

    # calculate matching time per model for all parameter combinations
    # because the voxelSizes vary for each model, we need to handle voxelSizes by their indices:
    average_building_time_per_model = []
    for voxelSize_idx in range(voxelsize_nsteps):
        filtered_base_data = []
        for model_name in d_min_rel.keys():
            for run_id, run_data in enumerate(basic_data):
                if run_data[iDict['voxelSize_intersectDetect']] == voxelSizes[model_name][
                    voxelSize_idx] \
                        and run_data[iDict['model']] == model_name:
                    filtered_base_data.append(run_data)

        average_match_time_per_model_for_voxelSize_idx = \
            data_extraction.nest_and_process_by_values(basic_data,
                                                       [iDict['ignoreFactor_intersectClassific'],
                                                        iDict['gapSizeFactor_surfaceCase_intersectClassific'],
                                                        iDict['model'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_model_building_time)

        average_building_time_per_model.append(
            (voxelSize_idx, average_match_time_per_model_for_voxelSize_idx))

    # calculate matching time for all parameter combinations
    average_building_time = []
    for voxelSize_idx, mt_by_vs_idx in average_building_time_per_model:
        for igf, mt_by_igf in mt_by_vs_idx:
            for gsf, mt_by_gsf in mt_by_igf:
                average_mt = sum(
                    mt_by_model_name for model_name, mt_by_model_name in mt_by_gsf) / len(mt_by_gsf)
                average_building_time.append((voxelSize_idx, [(igf, [(gsf, average_mt)])]))

    # print('average matching time:', average_match_time)
    # print('average matching time per model: ', average_match_time_per_model)

    # **********************************************************************************************
    # using Visibility Context --- get building time of combination with best recognition rate
    # **********************************************************************************************

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_building_time_per_model:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_building_time_per_model_best_parameters = mt_by_gsf

                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:
                    average_building_time_per_model_best_parameters_faster_mt = mt_by_gsf
    print(
        "(VisCon performance) average model building time for parameter combination with best recognition rate:",
        average_building_time_per_model_best_parameters, " ms")
    print(
        "(VisCon performance) average model building time for faster parameter combination with good recognition rate:",
        average_building_time_per_model_best_parameters_faster_mt, " ms")

    # loop over all parameter combinations to find the one that was marked as best:
    for vs, mt_by_vs in average_building_time:  # loop over voxel size indices
        for igf, mt_by_igf in mt_by_vs:  # loop over ignoreFactors
            for gsf, mt_by_gsf in mt_by_igf:  # loop over gapSizeFactors
                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'] == gsf:
                    average_building_time_best_parameters = mt_by_gsf

                if best_setup_dict['voxelSize_intersectDetect'] == vs and \
                        best_setup_dict['ignoreFactor_intersectClassific'] == igf and \
                        0.0 == gsf:
                    average_building_time_best_parameters_faster_mt = mt_by_gsf
    print(
        "(VisCon performance) average model building time for parameter combination with best recognition rate:",
        average_building_time_best_parameters, " ms")
    print(
        "(VisCon performance) average model building time for faster parameter combination with good recognition rate:",
        average_building_time_best_parameters_faster_mt, " ms\n")

    # add baseline-data to extension-data
    plot_data = []
    plot_data.append(average_model_building_time_per_model_baseline[0])

    # format data for plotting
    plot_data = [average_model_building_time_per_model_baseline[0],
                 ("\\textsc{Kim}'s Visibility Context with\n$\mathrm{VoxelSize_{idx}}=%s$,"
                  '\n$\mathrm{gapSizeFactor_{surface}}=%s$\nand $\mathrm{ignoreFactor}=%s$'
                   %(best_setup_dict['voxelSize_intersectDetect'],
                     best_setup_dict['gapSizeFactor_surfaceCase_intersectClassific'],
                     best_setup_dict['ignoreFactor_intersectClassific']),
                  average_building_time_per_model_best_parameters),
                 ("\\textsc{Kim}'s Visibility Context with\n$\mathrm{VoxelSize_{idx}}=%s$,"
                  '\n$\mathrm{gapSizeFactor_{surface}}=%s$\nand $\mathrm{ignoreFactor}=%s$'
                   %(best_setup_dict['voxelSize_intersectDetect'],
                     0.0,
                     best_setup_dict['ignoreFactor_intersectClassific']),
                  average_building_time_per_model_best_parameters_faster_mt)]

    # plotting
    bar_by_model(plot_data, ax, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=False)
    ax.set_ylim(0, 60000)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average model building time in s')


if __name__ == '__main__':

    # load data and do post-processing
    print("Loading Experiment-Data and Baseline-Experiment-Data")
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)
    baseline_data = ExperimentDataStructure()

    compute_pose_errors_necessary = True
    baseline_compute_pose_errors_necessary = True

    if compute_pose_errors_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors and recognition-values for all runs...')
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_ADI(data, dataset)

        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative)
        print('Saving Experiment-data with computed pose-errors...')
        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
    else:
        try:
            data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
        except:
            input = raw_input("You specified that the pose errors were already calculated.\n"
                              "But there is no file named '%s_with_computed_errors_hodan.bin'.\n"
                              "Therefore it is assumed that the file '%s.bin' contains those errors.\n"
                              "Hit <ENTER> to continue..." % (
                              FILE_PATH_WO_EXTENSION, FILE_PATH_WO_EXTENSION))
            data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

    if baseline_compute_pose_errors_necessary:
        baseline_data.from_pickle(BASELINE_FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors and recognition-values for all runs of baseline data...')
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_ADI(baseline_data, dataset)

        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(baseline_data, max_translation_error_relative)
        print('Saving Experiment-data with computed pose-errors of baseline data...')
        baseline_data.to_pickle(BASELINE_FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
    else:
        try:
            baseline_data.from_pickle(BASELINE_FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
        except:
            input = raw_input("You specified that the pose errors were already calculated for baseline data.\n"
                              "But there is no file named '%s_with_computed_errors_hodan.bin'.\n"
                              "Therefore it is assumed that the file '%s.bin' contains those errors.\n"
                              "Hit <ENTER> to continue..." % (
                              BASELINE_FILE_PATH_WO_EXTENSION, BASELINE_FILE_PATH_WO_EXTENSION))
            baseline_data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")


    # add matiching modes
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    # set general graphic-settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    # change 3D-perspective
    proj3d.persp_transformation = orthogonal_proj

    print("\nPlot recognition rates as coloured 3D plot (=4D) to show influence of all three variables:\n"
          " - voxelsize\n"
          " - gapsize_surface\n"
          " - ignorefactor\n")
    # plot recognition rates for all parameter-combinations (4d) and
    # a bar plot with the best recognition rate
    # add baseline-performance for easy comparison
    fig_rr = plt.figure(figsize=plotting.cm2inch(15, 6))
    # right subplot
    ax_2D_rr = fig_rr.add_subplot(1, 2, 2)
    # left subplot
    ax_4D_rr = fig_rr.add_subplot(1, 2, 1, projection='3d')
    # find best recognition rate
    results = find_best_recognition_rate_only_3D_plots(data, baseline_data,
                                                       ax_2D_rr, ax_4D_rr,
                                                       show_legend=False)
    fig_rr.tight_layout()

    # save plots:
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates_for_best_parameter_combination_barplot')

    print("\nPlot average matching time as coloured 3D plot (=4D) to show influence of all three variables:\n"
          " - voxelsize\n"
          " - gapsize_surface\n"
          " - ignorefactor\n")    # plot average matching time:
    fig_mt = plt.figure(figsize=plotting.cm2inch(15, 6))
    # right subplot
    ax_2D_mt = fig_mt.add_subplot(1, 2, 2)
    # left subplot
    ax_4D_mt = fig_mt.add_subplot(1, 2, 1, projection='3d')
    plot_average_matching_time(data,
                               baseline_data,
                               results[0],
                               ax_2D_mt,
                               ax_4D_mt,
                               show_legend=False)
    fig_mt.tight_layout()

    # another plot, that contains only the barplots of rr and average mathing time
    fig_mt_and_rr, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 7.5))
    fig_mt_and_rr.subplots_adjust(bottom=0.2)
    find_best_recognition_rate(data, baseline_data, fig_mt_and_rr.axes[1], print_output=False)
    plot_average_matching_time(data, baseline_data, results[0], fig_mt_and_rr.axes[0],
                               print_output=False)
    fig_mt_and_rr.axes[1].legend(bbox_to_anchor=(0.5, 0.00),
                                 bbox_transform=fig_mt_and_rr.transFigure,
                                 loc='lower center', borderaxespad=0.,title='matching modes')
    fig_mt_and_rr.tight_layout(rect=[0, 0.2, 1, 1])

    #another plot, showing best one- and associated two-instance-detection results:
    fig_mt_two_instances, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 7.5))
    fig_mt_two_instances.subplots_adjust(bottom=0.2)
    results_two_instances = plot_two_instance_recognition_rate(data, baseline_data,
                                                               results[0], axes[1])
    find_best_recognition_rate(data, baseline_data, axes[0], print_output=False)
    fig_mt_two_instances.axes[0].legend(bbox_to_anchor=(0.5, 0.00),
                                        bbox_transform=fig_mt_two_instances.transFigure,
                                        loc='lower center', borderaxespad=0.,title='matching modes')
    fig_mt_two_instances.tight_layout(rect=[0, 0.2, 1, 1])

    # plot a barplot of the (average) model builiding time.
    fig_bt, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 7.5))
    fig_bt.subplots_adjust(bottom=0.2)
    plot_average_matching_time(data, baseline_data, results[0], fig_bt.axes[0],
                               print_output=False)
    plot_average_model_building_time(baseline_data, data, fig_bt.axes[1], results[0],
                                     show_legend=False)
    fig_bt.axes[0].legend(bbox_to_anchor=(0.5, 0.00),
                                 bbox_transform=fig_bt.transFigure,
                                 loc='lower center', borderaxespad=0.,title='matching modes')
    fig_bt.tight_layout(rect=[0, 0.2, 1, 1])


    # find best model individual configuration
    fig3, axes = plt.subplots(1,2,figsize=plotting.cm2inch(15, 7.5))
    fig3.subplots_adjust(bottom=0.2)
    individual_best_setup_dict = find_best_recognition_rate_for_each_model(data, baseline_data,
                                                                           dataset, results[1],
                                                                           results[0], fig3.axes[0],
                                                                           show_legend=False)
    plot_best_two_instance_recognition_rate_of_each_model(data, baseline_data,
                                                          individual_best_setup_dict,
                                                          results[0],results_two_instances,
                                                          fig3.axes[1],show_legend=False)
    fig3.axes[1].legend(bbox_to_anchor=(0.5, 0.00),
                          bbox_transform=fig3.transFigure,
                          loc='lower center', borderaxespad=0., title='matching modes')
    fig3.tight_layout(rect=[0, 0.2, 1, 1])

    # get average matching time for best individual setups
    fig4, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5 + 5, 5.8))  # sharey=True)
    fig4.subplots_adjust(right=0.4)
    plot_average_matching_time(data, baseline_data, results[0], fig4.axes[0],
                               best_individual_setup_dict=individual_best_setup_dict,
                               show_legend=True)
    fig4.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                        title='matching modes', bbox_transform=fig4.transFigure)
    fig4.tight_layout(rect=[0, 0, 0.6, 1])

    # save plots:
    if SAVE_PLOTS:
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_barplot')
        plotting.save_figure(fig_mt_and_rr, plt_dir, 'average_matching_time_and_recognition_rate_barplot')
        plotting.save_figure(fig_mt_two_instances, plt_dir, 'recognitionrates_for_best_parameter_two_instances_combination_barplot')
        plotting.save_figure(fig_bt, plt_dir, 'average_model_building_time_average_matching_time_barplot')
        plotting.save_figure(fig3, plt_dir, 'recogniton_rates_for_model_individual_best_setting_barplot')
        plotting.save_figure(fig4, plt_dir, 'average_matching_time_for_model_individual_best_setting_barplot')
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
