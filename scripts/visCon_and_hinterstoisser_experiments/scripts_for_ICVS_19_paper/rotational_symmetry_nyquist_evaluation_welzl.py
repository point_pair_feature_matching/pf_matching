#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke, Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist'
KROISCHKES_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry/welzl_bounding_sphere/rotational_symmetry.bin' #kroischke with welzl
EPOS6_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/epos6_bounding_sphere/rotational_symmetry_nyquist.bin'
SAVE_PATH_WO_EXTENSION = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']

            matching_mode = None
            if used_rs and collapsed_points and d_dist==0.05:
                matching_mode = 'RS cluster\\&hash'
            elif used_rs and not collapsed_points and d_dist==0.05:
                matching_mode = 'RS cluster'
            elif not used_rs and d_dist==0.05:
                matching_mode = 'Baseline'
            elif used_rs and collapsed_points and d_dist!=0.05:
                matching_mode = "rs reduced + nyquist-$d_\mathrm{dist}$"
            elif used_rs and not collapsed_points and d_dist!=0.05:
                matching_mode = "rs + nyquist-$d_\mathrm{dist}$"
            elif not used_rs and d_dist!=0.05:
                matching_mode = "original + nyquist-$d_\mathrm{dist}$"

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300', '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['#c62828', '#534bae', 'k', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    #print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
        ax.set_xlim(right=len(group_names)-1 + bars_per_group * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
        ax.set_xlim(right=len(group_names)-1 + bars_per_group * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=5)
    ax.tick_params(axis='x', top=False)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_average_matching_time(data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    average_match_time_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per matching mode')
    for mode, match_time in average_match_time_per_mode:
        print('mode: %s, time: %f ms' % (mode, match_time))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    bar_by_model(average_match_time_per_model, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylim(0, 40000)
    scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in s')

def ICVS_plot_average_matching_time(data, ax, show_legend):
        """

        @type data: ExperimentDataStructure
        @type ax: Axes
        @return: ax-object the function plotted in
        """

        iDict, basic_data = \
            data_extraction.extract_data(data,
                                         [['statistics', '/matcher']],
                                         [['notes', 'matching_mode']])

        filtered_basic_data = []
        for run_id, run_data in enumerate(basic_data):
            if run_data[iDict['matching_mode']] == 'RS cluster\\&hash' or \
                    run_data[iDict['matching_mode']] == 'RS cluster' or \
                    run_data[iDict['matching_mode']] == 'Baseline':
                filtered_basic_data.append(run_data)

        average_match_time_per_mode = \
            data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                       [iDict['matching_mode'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_match_time)
        print('average matching time per matching mode')
        for mode, match_time in average_match_time_per_mode:
            print('mode: %s, time: %f ms' % (mode, match_time))

        average_match_time_per_model = \
            data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                       [iDict['matching_mode'],
                                                        iDict['model'],
                                                        iDict['/matcher']],
                                                       data_extraction.average_match_time)

        bar_by_model(average_match_time_per_model, ax, 'matching modes', show_legend=show_legend)
        ax.set_ylim(0, 60000)
        scaled_labels = [float(x) / 1000 for x in ax.get_yticks()]
        ax.set_yticklabels(scaled_labels)
        ax.set_ylabel('average matching time in s')

def plot_average_number_of_scene_points(data_1, data_2, ax, show_legend):
    """
    comparing the average number of scene points between two Experiments,
    namely the rotational_symmetry_experiment.py of Kroischke and rotational_symmetry_nyquist_experiment.py
    The difference between both experiments lies not only in the experiment-script-code but also in the matcher-code.
    Main difference: Both point clouds, model and scene, will be either downsampled with slightly differing values for
    d_points_scene and d_points_model or with with the exact same values d_points_scene = d_points_model.
    @type data_1: ExperimentDataStructure
    @type data_2: ExperimentDataStructure
    @param data_1: data of experiment No. 1
    @param data_2: data of experiment No. 2
    @note data: both datas have to be retrieved from different .bin-files! That means one of them needs to have its origin
    in another experiment that has already been done.
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict_1, basic_data_1 =\
        data_extraction.extract_data(data_1,
                                     None,
                                     [['statistics', '/scene_preprocessor']])

    iDict_2, basic_data_2 =\
        data_extraction.extract_data(data_2,
                                     None,
                                     [['statistics', '/scene_preprocessor']])

    average_number_of_scene_points_per_model_1 =\
        data_extraction.nest_and_process_by_values(basic_data_1,
                                                   [iDict_1['model'],
                                                    iDict_1['/scene_preprocessor']],
                                                   data_extraction.average_number_of_cloud_points)

    average_number_of_scene_points_per_model_2 = \
        data_extraction.nest_and_process_by_values(basic_data_2,
                                                   [iDict_2['model'],
                                                    iDict_2['/scene_preprocessor']],
                                                   data_extraction.average_number_of_cloud_points)

    #print("average number of scenes per model =",average_number_of_scene_points_per_model_1)
    #print("average number of scenes per model =",average_number_of_scene_points_per_model_2)

    # create: [( 'd_points_scene = d_dist_abs',[(),(),(),...] ), ( 'd_points_scene = d_points_model', [(),(),(),...] )]
    average_number_of_scene_points_per_model = [("$d_\mathrm{points,\,scene} = d_\mathrm{dist,\,abs}$",     average_number_of_scene_points_per_model_2),
                                                ("$d_\mathrm{points,\,scene} = d_\mathrm{points,\,model}$", average_number_of_scene_points_per_model_1)]

    bar_by_model(average_number_of_scene_points_per_model,
                  ax, 'downsampling modes', show_legend=show_legend)
    ax.set_ylim(0, 16000)
    scaled_labels = [float(x) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average number of scene points')

def plot_model_diameter(data_1, data_2, ax, show_legend):
    """
    comparing the model diameters between two Experiments,
    namely the rotational_symmetry_experiment.py of Kroischke and rotational_symmetry_nyquist_experiment.py
    The difference between both experiments lies not only in the experiment-script-code but also in the matcher-code.
    Main difference: Both point clouds, model and scene, will be either downsampled with slightly differing values for
    d_points_scene and d_points_model or with with the exact same values d_points_scene = d_points_model.
    @type data_1: ExperimentDataStructure
    @type data_2: ExperimentDataStructure
    @param data_1: data of experiment No. 1
    @param data_2: data of experiment No. 2
    @note data: both datas have to be retrieved from different .bin-files! That means one of them needs to have its origin
    in another experiment that has already been done.
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict_1, basic_data_1 = \
        data_extraction.extract_data(data_1,
                                     [['statistics', '/model_preprocessor']],
                                     [['notes', 'matching_mode']])

    iDict_2, basic_data_2 = \
        data_extraction.extract_data(data_2,
                                     [['statistics', '/model_preprocessor']],
                                     [['notes', 'matching_mode']])

    model_diameters_1 = \
        data_extraction.nest_and_process_by_values_advanced(basic_data_1,
                                                            [iDict_1['model'],
                                                             iDict_1['/model_preprocessor']],
                                                            data_extraction.average_key_figure,
                                                            'cloud_diameter')

    model_diameters_2 = \
        data_extraction.nest_and_process_by_values_advanced(basic_data_2,
                                                            [iDict_2['model'],
                                                             iDict_2['/model_preprocessor']],
                                                            data_extraction.average_key_figure,
                                                            'cloud_diameter')

    # print("average number of scenes per model =",average_number_of_scene_points_per_model_1)
    # print("average number of scenes per model =",average_number_of_scene_points_per_model_2)

    # create: [( 'd_points_scene = d_dist_abs',[(),(),(),...] ), ( 'd_points_scene = d_points_model', [(),(),(),...] )]
    model_diameters_per_model = [('EPOS6 algorithm', model_diameters_2),
                                 ('Welzl algorithm', model_diameters_1)]

    bar_by_model(model_diameters_per_model,
                 ax, 'bounding sphere algorithms', show_legend=show_legend)
    ax.set_ylim(0, 0.2)
    scaled_labels = [float(x) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('model diameter [m]')


def plot_key_figure_of_voting_ball(data, ax, show_legend, key_figure, y_max, voting_ball_idx, logarithmic_y_axis = False):
    """
    plot pose-key-figures that are stored in a list, containing one value for each voting ball, such as
    - n_raw_poses,
    - n_pose_clusters,
    - raw_poses_max_weight,
    - raw_poses_median_weight
    per model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    @param key_figure: name of the key figure
    @type key_figure: string
    @param y_max: max value for y-axis
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    # plot key figure per model as bar-plot
    key_figures_list =\
        data_extraction.nest_and_process_by_values_advanced(basic_data,
                                                            [iDict['matching_mode'],
                                                             iDict['model'],
                                                             iDict['/matcher']],
                                                            data_extraction.average_key_figure_per_voting_ball,
                                                            key_figure)

    #print('key figures list =', key_figures_list)

    n_voting_balls = len(key_figures_list[0][1][0][1])
    #print('n_voting_balls =',n_voting_balls, 'content =', key_figures_list[0][1][0][1])

    import copy
    key_figures_list_per_voting_ball = [ copy.deepcopy(key_figures_list) for _ in xrange(n_voting_balls) ]
    for voting_ball_idx in range(n_voting_balls):
        for mode_idx in range(len(key_figures_list)):
            for model_idx in range(len(key_figures_list[mode_idx][1])):
                #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])
                key_figure_of_voting_ball = key_figures_list[mode_idx][1][model_idx][1][voting_ball_idx]
                key_figures_list_per_voting_ball[voting_ball_idx][mode_idx][1][model_idx][1] = key_figure_of_voting_ball
                #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])

    #print('key figures list voting_balls =', key_figures_list_per_voting_ball)
    #print('key figures list =', key_figures_list)

    bar_by_model(key_figures_list_per_voting_ball[voting_ball_idx], ax, 'matching modes', show_legend=show_legend, logarithmic_y_axis=logarithmic_y_axis)
    ax.set_ylabel('average '+ key_figure )
    ax.set_ylim(0, y_max)
    if show_legend:
        ax.legend(loc='upper right')


def plot_key_figure(data, ax, show_legend, key_figure, y_max, logarithmic_y_axis = False):
    """
    version for data from Kroischkes original System (--> no voting balls --> no lists of values).
    plot pose-key-figures such as
    - n_raw_poses,
    - n_pose_clusters,
    - raw_poses_max_weight,
    - raw_poses_median_weight
    per model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    @param key_figure: name of the key figure
    @type key_figure: string
    @param y_max: max value for y-axis
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['notes', 'matching_mode']])

    # plot key figure per model as bar-plot
    key_figures_list =\
        data_extraction.nest_and_process_by_values_advanced(basic_data,
                                                            [iDict['matching_mode'],
                                                             iDict['model'],
                                                             iDict['/matcher']],
                                                            data_extraction.average_key_figure,
                                                            key_figure)

    #print('key figures list =', key_figures_list)

    #n_voting_balls = len(key_figures_list[0][1][0][1])
    #print('n_voting_balls =',n_voting_balls, 'content =', key_figures_list[0][1][0][1])

    #import copy
    #key_figures_list_per_voting_ball = [ copy.deepcopy(key_figures_list) for _ in xrange(n_voting_balls) ]
    #for voting_ball_idx in range(n_voting_balls):
    #    for mode_idx in range(len(key_figures_list)):
    #        for model_idx in range(len(key_figures_list[mode_idx][1])):
    #            #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])
    #            key_figure_of_voting_ball = key_figures_list[mode_idx][1][model_idx][1][voting_ball_idx]
    #            key_figures_list_per_voting_ball[voting_ball_idx][mode_idx][1][model_idx][1] = key_figure_of_voting_ball
    #            #print('key_figures_list[mode_idx][1][model_idx][1] =', key_figures_list[mode_idx][1][model_idx][1])

    #print('key figures list voting_balls =', key_figures_list_per_voting_ball)
    #print('key figures list =', key_figures_list)

    bar_by_model(key_figures_list, ax, 'matching modes', show_legend=show_legend, logarithmic_y_axis=logarithmic_y_axis)
    ax.set_ylabel('average '+ key_figure )
    ax.set_ylim(0, y_max)
    if show_legend:
        ax.legend(loc='upper right')


def plot_recog_rates_first_pose(data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # print overall recognition rates
    rr_first_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    for mode, rr in rr_first_poses_per_mode:
        print('mode: %s, recognition rate best pose cluster: %f' % (mode, rr))

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    bar_by_model(rr_first_poses, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylabel('recognition rate (1 instance)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='upper right')

def ICVS_plot_recog_rates_first_pose(data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    filtered_basic_data = []
    for run_id, run_data in enumerate(basic_data):
        if run_data[iDict['matching_mode']] == 'RS cluster\\&hash' or \
                run_data[iDict['matching_mode']] == 'RS cluster' or \
                run_data[iDict['matching_mode']] == 'Baseline':
            filtered_basic_data.append(run_data)

    # print overall recognition rates
    rr_first_poses_per_mode =\
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    for mode, rr in rr_first_poses_per_mode:
        print('mode: %s, recognition rate best pose cluster: %f' % (mode, rr))

    # plot recognition rate per model as bar-plot
    rr_first_poses =\
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    bar_by_model(rr_first_poses, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylabel('recognition rate')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='lower right')



def plot_recog_rates_all_poses(data, ax, show_legend=False):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment tmp_data for plotting
    @type ax: Axes
    @param ax: axis to plot recognition rate for all returned poses to
    """

    # extract data
    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate for all models
    recog_rate_all_poses =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    bar_by_model(recog_rate_all_poses, ax, 'matching modes', show_legend=show_legend)
    ax.set_ylabel('recognition rate (2 instances)')
    ax.set_ylim(0, 1)
    if show_legend:
        ax.legend(loc='upper right')

    # print overall recognition rates
    rr_first_2_poses_per_mode =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    for mode, rr in rr_first_2_poses_per_mode:
        print('mode: %s, recognition rate best 2 pose cluster: %f' % (mode, rr))


def plot_precisions_of_recognized_poses(experiment_data, ax_distance, ax_angle):
    """
    plot the time required for matching over the number of scene points for different ref point
    steps
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax_distance: Axes
    @param ax_distance: axis to plot distance precision to
    @type ax_angle: Axes
    @param ax_angle: axis to plot orientation precision to
    @return: None
    """
    iDict, basic_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['notes', 'matching_mode']])

    # filter out only poses that were recognized and replace poses dict with the
    # single pose
    recognized_data = []
    for entry in basic_data:
        for pose in entry[iDict['poses']].values():
            if pose['correct']:
                entry_list = list(entry)
                entry_list[iDict['poses']] = pose
                recognized_data.append(tuple(entry_list))

    # replace absolute distance errors with relative ones
    diameters_dict = post_processing.get_model_diameters(experiment_data)
    for entry in recognized_data:
        entry[iDict['poses']]['error']['distance'] /= diameters_dict[entry[iDict['model']]]

    # plot distance error
    average_distance_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_distance_error)
    bar_by_model(average_distance_error, ax_distance, 'matching modes')
    ax_distance.set_ylabel('distance error / model diameter')

    # plot angle error
    average_angle_error = \
        data_extraction.nest_and_process_by_values(recognized_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_angle_error)

    bar_by_model(average_angle_error, ax_angle, 'matching modes')
    ax_angle.set_ylabel('angle error in rad')


if __name__ == '__main__':

    data = ExperimentDataStructure()


    # todo: choose if ADI or drost error metric:
    use_adi_error_metric = True

    if not use_adi_error_metric:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors for all runs...')
        from pf_matching.evaluation.data_post_processing import compute_pose_errors
        compute_pose_errors(data)
        post_processing.compute_recognition(data, max_translation_error_relative, max_rotation_error)
    else:
        data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")

    # for comparison reasons:
    kroischkes_data = ExperimentDataStructure()
    kroischkes_data.from_pickle(KROISCHKES_FILE_PATH)
    post_processing.compute_recognition(kroischkes_data, max_translation_error_relative, max_rotation_error)
    epos6_data = ExperimentDataStructure()
    epos6_data.from_pickle(EPOS6_FILE_PATH)
    post_processing.compute_recognition(epos6_data, max_translation_error_relative, max_rotation_error)

    # prepare for plotting
    mpl.rcParams.update(plotting.ICVS_2019_default_settings)

    add_matching_mode(data)
    add_matching_mode(kroischkes_data)
    add_matching_mode(epos6_data)
    #data.to_yaml(SAVE_PATH_WO_EXTENSION + '.yaml')

    if not use_adi_error_metric:
        # plot
        fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5)) #*****legend has no title******
        fig1.subplots_adjust(bottom=0.4)
        plot_recog_rates_first_pose(data, fig1.axes[0], show_legend=False)
        plot_recog_rates_all_poses(data, fig1.axes[1], show_legend=False)
        fig1.axes[0].legend(bbox_to_anchor=(0.5, 0.05), bbox_transform=fig1.transFigure, ncol=3,
                            loc='lower center', borderaxespad=0., title='matching modes')
        fig1.tight_layout(rect=[0, 0.2, 1, 1])

        fig2, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5, 6))
        plot_average_matching_time(data, fig2.axes[0], show_legend=True)

        fig3, axes = plt.subplots(1, 2)#, figsize=plotting.cm2inch(16, 6))
        plot_precisions_of_recognized_poses(data, *fig3.axes)

        fig4, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6)) #*****legend has no title******
        plot_recog_rates_first_pose(data, fig4.axes[1], show_legend=True)
        plot_average_matching_time(data, fig4.axes[0], show_legend=False)

        fig5, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5, 6)) #*****legend has no title******
        plot_recog_rates_all_poses(data, fig5.axes[0])

        fig6, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5 + 5, 5.8))
        fig6.subplots_adjust(right=0.4)
        plot_average_number_of_scene_points(data, kroischkes_data, fig6.axes[0], show_legend=True)
        fig6.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                            title='down-sampling modes', bbox_transform=fig6.transFigure)
        fig6.tight_layout(rect=[0, 0, 0.6, 1])

        fig7, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6)) #*****legend has no title******
        plot_recog_rates_first_pose(kroischkes_data, fig7.axes[0], show_legend=True)
        plot_recog_rates_all_poses(kroischkes_data, fig7.axes[1])

        fig8 , axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5))
        plot_key_figure_of_voting_ball(data, fig8.axes[0], show_legend=True, key_figure='n_raw_poses',
                                       y_max=16000, voting_ball_idx = 0)
        plot_key_figure(kroischkes_data, fig8.axes[1], show_legend=True, key_figure='n_raw_poses',
                                       y_max=16000)

        fig9, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5))
        plot_key_figure_of_voting_ball(data, fig9.axes[0], show_legend=True, key_figure='n_pose_clusters',
                                       y_max=16000, voting_ball_idx=0)
        plot_key_figure(kroischkes_data, fig9.axes[1], show_legend=True, key_figure='n_pose_clusters',
                                       y_max=16000)

        fig10, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5))
        fig10.axes[0].set_yscale('log')
        plot_key_figure_of_voting_ball(data, fig10.axes[0], show_legend=True, key_figure='raw_poses_max_weight',
                                       y_max=10000, voting_ball_idx=0, logarithmic_y_axis = True)
        plot_key_figure(kroischkes_data, fig10.axes[1], show_legend=True, key_figure='raw_poses_max_weight',
                                       y_max=10000, logarithmic_y_axis = True)

        fig11, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
        fig11.axes[0].set_yscale('log')
        plot_key_figure_of_voting_ball(data, fig11.axes[0], show_legend=True, key_figure='raw_poses_median_weight',
                                       y_max=1000, voting_ball_idx=0, logarithmic_y_axis = True)
        plot_key_figure(kroischkes_data, fig11.axes[1], show_legend=True, key_figure='raw_poses_median_weight',
                                       y_max=1000, logarithmic_y_axis = True)

        fig12, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5+5, 5.8))
        fig12.subplots_adjust(right = 0.4)
        plot_model_diameter(data, epos6_data, fig12.axes[0], show_legend=False)
        fig12.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                            title='bounding sphere algorithms', bbox_transform=fig12.transFigure)
        fig12.tight_layout(rect=[0,0,0.6,1])

        # save figures
        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
            plotting.save_figure(fig1, plt_dir, 'rot_sym_recognition_rates_welzl')
            plotting.save_figure(fig2, plt_dir, 'rot_sym_matching_times_welzl')
            plotting.save_figure(fig3, plt_dir, 'rot_sym_precisions_welzl')
            plotting.save_figure(fig4, plt_dir, 'rot_sym_matching_time_recog_rate_welzl')
            plotting.save_figure(fig5, plt_dir, 'rot_sym_recognition_rates_all_poses_welzl')
            plotting.save_figure(fig6, plt_dir, 'rot_sym_average_number_of_scene_points_welzl')
            plotting.save_figure(fig7, plt_dir, 'rot_sym_recognition_rates_kroischke_welzl')
            # plotting.save_figure(fig8, plt_dir, 'rot_sym_number_of_raw_poses_welzl')
            # plotting.save_figure(fig9, plt_dir, 'rot_sym_number_of_pose_clusters_welzl')
            # plotting.save_figure(fig10, plt_dir, 'rot_sym_average_raw_poses_max_weight_welzl')
            # plotting.save_figure(fig11, plt_dir, 'rot_sym_average_raw_poses_median_weight_welzl')
            plotting.save_figure(fig12, plt_dir, 'rot_sym_model_diameters_welzl_vs_epos6')

    # **********************************************************************************************
    # *********************** output modifications for ICVS 2019 paper *****************************
    # **********************************************************************************************
    print("\n*************** MODIFICATION FOR ICVS 2019 PAPER: ***************\n")

    mpl.rcParams.update(plotting.ICVS_2019_default_settings)
    fig_ICVS, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(12.2, 4)) #*****legend has no title******
    ICVS_plot_recog_rates_first_pose(data, fig_ICVS.axes[1], show_legend=True)
    ICVS_plot_average_matching_time(data, fig_ICVS.axes[0], show_legend=False)
    fig_ICVS.tight_layout()

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_ICVS, plt_dir, 'ICVS_rot_sym_matching_time_recog_rate_welzl')

    else:
        print('Plots are not saved.')

    # show figures for checking layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
