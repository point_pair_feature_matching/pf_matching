#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors

from math import sqrt
import numpy as np
import configparser
import os

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/ICVS_2019/Hinterstoisser_wo_FA_and_HintClustering/publish_n_best_poses/publish_n_best_poses'


class PublishNBestPosesExperiment(ExperimentBase):
    """
    experiment to analyse how many poses should get processed by verifier-node
    The verifier is then used for a combination of the separately tuned Visibility Context
    and Hinterstoisser's Flag Array and Hinterstoisser's Clustering
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension):
    
        ExperimentBase.__init__(self, data_storage)
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension

        # try reading spreading info file from home directory
        # it should be available if experiment is started on remote computer with the
        # aws_experiment_interface
        config = configparser.ConfigParser()
        ini_file_path = os.path.expanduser('~/spreading_info.ini')
        self.job_id = None
        try:
            config.read(ini_file_path)
            self.job_id = config['DEFAULT']['job_id']
        except:
            print(
                "ERROR: Could not load ini-file form %s, conducting COMPLETE experimet instead of only a part." % ini_file_path)

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # distance resolution of feature and downsampling
        self.d_dist_rel = 0.05  # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%, [Hodan et al. 2018] use 5%

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': self.d_dist_rel,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       # do not set d_points here!
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,
                            'model_hash_table_type': 2,  # CMPH
                            'publish_pose_cluster_weights': True,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 20,
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,
                            'use_rotational_symmetry': True,  # match with collapsed models
                            'collapse_symmetric_models': True,
                            'publish_equivalent_poses': True,
                            'd_dist': self.d_dist_rel,
                            'd_dist_is_relative': True,

                            # 'HS_save_dir': '~/ROS/experiment_data/hinterstoisser_pose_verifying/publish_n_best_poses/voting_spaces/',

                            # visibility context (NOT USED!):
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions:
                            'use_neighbour_PPFs_for_training': True,
                            'use_neighbour_PPFs_for_matching': False,# not implemented in matcher-nodelet
                            'use_voting_balls': True,
                            'use_hinterstoisser_clustering': False,
                            'use_hypothesis_verification_with_visibility_context': True,# to publish the n best poses of EACH voting ball
                            'vote_for_adjacent_rotation_angles': True,
                            'flag_array_hash_table_type': 0,  # NONE
                            'flag_array_quantization_steps': 30.0,# same resolution as d_alpha_in_pi
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)


    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() * self.dataset.get_number_of_scenes()

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # match all models of the dataset
        print('matching dataset: %s' % (self.dataset.get_name()))
        self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(self.dataset, re)


    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

        print('Computing pose errors for all runs...')
        compute_pose_errors(self.data)
        
        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

if __name__ == '__main__':
    
    data_storage = data_structure.ExperimentDataStructure()
    experiment = PublishNBestPosesExperiment(data_storage,
                                             DATASET_PATH,
                                             SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=False)
