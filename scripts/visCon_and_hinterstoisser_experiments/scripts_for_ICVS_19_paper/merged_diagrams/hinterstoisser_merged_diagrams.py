#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from twisted.protocols.amp import ListOf

input = raw_input
range = xrange

# project
from pf_matching.evaluation import plotting

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
from matplotlib import cm

# ################################ script settings ##############################################
SAVE_PATH = '~/ROS/experiment_data/ICVS_2019/Hinterstoisser_merged_diagrams/'

SAVE_PLOTS = True
SHOW_PLOTS = False


# TODO: Give recognition rates for one instance detection:
# IDEA: Use always the version with verifier

# TODO: 0. baseline with e_ADI error metric
RESULT_BASELINE = ( "Baseline", #"rs reduced",
                    [('cuboid', 0.70), ('cylinder', 0.9), ('elliptic_prism', 0.95),
                     ('hexagonal_frustum', 0.7), ('pyramid', 0.95)])

# TODO: 1. All Hinterstoisser extensions without verifier; with e_ADI error metric
RESULT_HINT_WO_V = ( "Sampling + Noise + Planar Surfaces",
                    [('cuboid', 0.9), ('cylinder', 0.85), ('elliptic_prism', 0.90),
                     ('hexagonal_frustum', 0.60), ('pyramid', 0.95)])

# TODO: 2. All Hinterstoisser extensions with verifier; with e_ADI error metric
RESULT_HINT = ( "Sampling + Noise + Planar Surfaces\n+ Verification",
                    [('cuboid', 0.8), ('cylinder', 0.95), ('elliptic_prism', 1.00),
                     ('hexagonal_frustum', 0.70), ('pyramid', 0.95)])

# TODO: 3. Hinterstoisser extensions without verifier, Flag array and with Choi Clustering; with ADI error metric
RESULT_HINT_WO_FA_CLUST_WO_V = ( "Sampling + Noise",
                    [('cuboid', 0.80), ('cylinder', 0.85), ('elliptic_prism', 0.90),
                     ('hexagonal_frustum', 0.65), ('pyramid', 0.95)])

# TODO: 4. Hinterstoisser extensions with verifier but without Flag array and with Choi Clustering; with e_ADI error metric
RESULT_HINT_WO_FA_CLUST = ( "Sampling + Noise + Verification",
                    [('cuboid', 0.80), ('cylinder', 0.95), ('elliptic_prism', 0.95),
                     ('hexagonal_frustum', 0.70), ('pyramid', 0.95)])

# TODO: 5. Visibility Context common config; with e_ADI error metric
RESULT_VISCON_COMMON_CONFIG = ( "Sampling + $\\mathbf{PPF}_\\mathrm{aug}$\ncommonly tuned", #"Visibility Context with\ncommon configuration",
                    [('cuboid', 0.9), ('cylinder', 0.95), ('elliptic_prism', 1.0),
                     ('hexagonal_frustum', 0.70), ('pyramid', 0.90)])

# TODO: 6. Visibility Context model specific config; with e_ADI error metric
RESULT_VISCON_MODEL_SPECIFIC = ("Sampling + $\\mathbf{PPF}_\\mathrm{aug}$", #"Visibility Context with\nmodel specific configuration",
                    [('cuboid', 0.9), ('cylinder', 1.0), ('elliptic_prism', 1.0),
                     ('hexagonal_frustum', 0.75), ('pyramid', 0.95)])





def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_four_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_four_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['k',                   # black
                       '#ffb300', '#ffe54c',  # yellow
                       '#7b1fa2', '#af52d5',  # purple
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#c62828', '#ff5f52',  # red

                       '#795548', '#a98274',  # brown
                       ]
    colors_four_bars = ['k','#795548', '#33691e', '#1a237e', '#33691e']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_four_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_four_bars, color=colors_four_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_four_bars)
        ax.set_xlim(right=len(group_names)-1 + bars_per_group * bar_width_four_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
        ax.set_xlim(right=len(group_names)-1 + bars_per_group * bar_width_six_bars)

    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=5)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_recognitionRates_one_instance(data_array, ax, show_legend=True):


    # add all plotting data to bar-plot
    recognition_rate_per_model = []
    for data in data_array:
        recognition_rate_per_model.append(data)  # add data

    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'datasets', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')


if __name__ == '__main__':


    # prepare for plotting
    mpl.rcParams.update(plotting.ICVS_2019_default_settings)

    data_all_hinterstoisser_extensions = []
    data_all_hinterstoisser_extensions.append(RESULT_BASELINE)
    #data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_V)
    #data_all_hinterstoisser_extensions.append(RESULT_HINT)
    #data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_FA_CLUST_WO_V)
    #data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_FA_CLUST)

    data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_FA_CLUST_WO_V)
    data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_V)
    data_all_hinterstoisser_extensions.append(RESULT_HINT_WO_FA_CLUST)
    data_all_hinterstoisser_extensions.append(RESULT_HINT)
    data_all_hinterstoisser_extensions.append(RESULT_VISCON_MODEL_SPECIFIC)

    #data_hinterstoisser_extensions_wo_flag_array_and_clustering = []
    #data_hinterstoisser_extensions_wo_flag_array_and_clustering.append(RESULT_BASELINE)
    #data_hinterstoisser_extensions_wo_flag_array_and_clustering.append(RESULT_VISCON_COMMON_CONFIG)
    #data_hinterstoisser_extensions_wo_flag_array_and_clustering.append(RESULT_VISCON_MODEL_SPECIFIC)

    ICVS_fig, axes = plt.subplots(#1, 2,
                                  figsize=plotting.cm2inch(12.2, 5.5)) #6
    ICVS_fig.subplots_adjust(bottom= 4/11 #2/6
                            , right=0.5)
    plot_recognitionRates_one_instance(data_all_hinterstoisser_extensions,
                                       ICVS_fig.axes[0],
                                       show_legend=False)
    #plot_recognitionRates_one_instance(data_hinterstoisser_extensions_wo_flag_array_and_clustering,
    #                                   ICVS_fig.axes[1],
    #                                   show_legend=False)
    legend = ICVS_fig.axes[0].legend(bbox_to_anchor=(0.53, 26/55 #3.1/6
                                                     ), bbox_transform=ICVS_fig.transFigure,
                        loc='lower left', borderaxespad=0., ncol=1)
    #ICVS_fig.axes[1].legend(bbox_to_anchor=(0.615, 0.10), bbox_transform=ICVS_fig.transFigure,
    #                        loc='lower left', borderaxespad=0., ncol=1)
    ICVS_fig.tight_layout(rect=[0, 3/11#2/6
                                , 0.5, 1]) #(left bottom right top)

    # ***** adding a box with extra text to the figure to explain all matching modes
    ICVS_fig.canvas.draw()  # this draws the figure
    # which allows reading final coordinates in pixels
    leg_pxls = legend.get_window_extent()
    ax_pxls = axes.get_window_extent()
    fig_pxls = ICVS_fig.get_window_extent()

    # Converting back to figure normalized coordinates to create new axis:
    pad = 0.025 #unused
    bottom_dist_px = 0
    left_dist_px = 29
    top_dist_px = 33
    ax2 = ICVS_fig.add_axes([(fig_pxls.x0 + left_dist_px) / fig_pxls.width, #left
                             (fig_pxls.y0 + bottom_dist_px) / fig_pxls.height, #bottom
                             (fig_pxls.width - (2*left_dist_px))/ fig_pxls.width,  #width
                             (fig_pxls.y0 + ax_pxls.y0 - top_dist_px) / fig_pxls.height]) #height

    # eliminating all the tick marks:
    ax2.tick_params(axis='both', left='off', top='off', right='off',
                    bottom='off', labelleft='off', labeltop='off',
                    labelright='off', labelbottom='off')

    # adding some text:
    ax2.text(0.01, 0.06, "\\textbf{Baseline}: Drost-PPFM + angle refinement + RS cluster\\&hash.\n\\textbf{Sampling}: Uses voting spheres.\n\\textbf{Noise}: Storing the adjacent PPFs in the model and voting for ajdacent pose rotation angels.\n\\textbf{Planar Surfaces}: Flag array and bottom-up clustering.\n\\textbf{Verification}: Postprocessing pose clusters with verifier.\n$\\mathbf{PPF}_\\mathrm{aug}$: Augmented feature with model-specific configuration.",
             fontsize=6)



    plt_dir = os.path.dirname(SAVE_PATH)
    #plotting.save_figure(ICVS_fig, plt_dir,
    #                    'ICVS_recognition_rates_per_model_for_Hinterstoissers_extensions')
    plotting.save_figure(ICVS_fig, plt_dir,
                         'ICVS_recognition_rates_per_model_for_Hinterstoissers_extensions_with_table')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
