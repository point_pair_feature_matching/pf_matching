#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from twisted.protocols.amp import ListOf

input = raw_input
range = xrange

# project
from math import floor, ceil, pi, sqrt, pow
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.ros_interface import RuntimeEstimator
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import math

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/ICVS_2019/Hinterstoisser_wo_FA_and_HintClustering/searchradius_and_angleDiffThresh/drosts_error_metric/searchradius_and_angleDiffThresh.bin'
BASELINE_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
use_Krones_evaluation = False  # Krone optimizes efficiency, Ziegler optimizes recognition-rate
max_translation_error_relative = 0.1 # Krone's value
max_rotation_error = pi / 15  # rad # Krone's value
max_x = 1.0 # Krone's value
stepSize = 100 # Krone's value
searchradius_values = [0.5, 1.0, 2.0, 3.0, 5.0] # Krone's values
angleDiffThresh_in_degree_values = [3, 6, 12, 18, 45, 180] # Krone's values
precisions_weight = 0.2  # Krone's value
angleDiffThresh_in_pi_values = [a / 180 for a in angleDiffThresh_in_degree_values]
datasetNames = ['geometric_primitives']
datasetSceneCount = {'geometric_primitives': 20}
datasetSynonyms = ['geometric primitives']
datasetDictionary = {dsName: datasetSynonyms[i] for i, dsName in enumerate(datasetNames)}

# TODO: give the recognition rates from publish_n_best_poses_experiment:
RESULT_FROM_PUBLISH_N_BEST_POSES = ("\\textsc{Hinterstoisser}'s extensions without\nFlag Array and Clusteringwithout verifier",
                                    [('cuboid', 0.75), ('cylinder', 0.75), ('elliptic_prism', 0.90),
                                     ('hexagonal_frustum', 0.65), ('pyramid', 0.95)])
RESULT_FROM_PUBLISH_N_BEST_POSES_TWO_INSTANCE_DETECTION = ("\\textsc{Hinterstoisser}'s extensions without\nFlag Array and Clusteringwithout verifier",
                                                            [('cuboid', 0.45),
                                                             ('cylinder', 0.4),
                                                             ('elliptic_prism', 0.7),
                                                             ('hexagonal_frustum', 0.325),
                                                             ('pyramid', 0.475)])

# orthogonal projection for matplotlib 3D plot
# https://github.com/matplotlib/matplotlib/issues/537#issuecomment-2441206
from mpl_toolkits.mplot3d import proj3d
def orthogonal_proj(zfront, zback):
    a = (zfront + zback) / (zfront - zback)
    b = -2 * (zfront * zback) / (zfront - zback)
    return np.array([[1, 0, 0, 0.15], # 0.15 to move the 3D-plot a little to the right (x)
                     [0, 1, 0, 0.0], # for moving it up and down (y)
                     [0, 0, a, b],
                     [0, 0, 0, zback]])

def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if used_rs and collapsed_points and not used_vb and not used_n_PPFs and not used_hc and\
                    not used_fa and not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_vb and used_n_PPFs and not used_hc and not used_fa \
                    and voted_for_adj_rot_angl:
                matching_mode = 'Hinterstoisser extensions without \nFlag Array and Clusteringwith verifier'
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_four_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_four_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_four_bars = ['k','#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 4:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_four_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_four_bars, color=colors_four_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 4:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_four_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def get_TPs(experiment_data):
    """
    get the numbers of true positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: true positives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])

    tps = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_true_positives)
    # print(tps)
    return tps


def get_FPs(experiment_data):
    """
    get the numbers of false positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: false positives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])

    fps = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_false_positives)
    # print(fps)
    return fps


def get_Relevants(experiment_data):
    """
    get the numbers of false negatives + true positives
    (based on the precomputed verification values in experiment_data)
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @return: false negatives for datasets, searchradius and angleDiffThresh
    """
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['verification']],
                                     [["dynamicParameters", "/verifier", "searchradius"],
                                      ["dynamicParameters", "/verifier", "angleDiffThresh_in_pi"],
                                      ['datasetName']])

    relevants = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['verification']],
                                                   data_extraction.verification_relevants)  # the sum of relevant poses (tp + fn) for a list of dictionaries
    # print(relevants)
    return relevants


def compute_precisions_recalls(tp, fp, last_tp, last_fp, relevants):
    if fp > 0:  # fp > 0 and tp >= 0 ==> tp + fp > 0
        precision = tp / (tp + fp)
    else:
        precision = 1  # limes n->0 n/n = 1

    if relevants > 0:  # tp + fn > 0 ==> tp > 0 oder fn > 0
        recall = tp / (relevants)
    else:  # fn = 0 und tp = 0
        recall = 1  # limes fp->0 tp/tp = 1

    # linear interpolation of the missing tp-steps

    listOfInterpolatedPR = []
    if last_tp is not None and last_fp is not None and tp != last_tp:
        x_list = range(1, abs(tp - last_tp))
        negativesPerPositive = (fp - last_fp) / (tp - last_tp)

        for x in x_list:
            interpolated_tp = tp + x
            interpolated_fp = fp + negativesPerPositive * x

            if relevants > 0:
                interpolated_recall = interpolated_tp / relevants
            else:
                interpolated_recall = 1
            if interpolated_fp > 0:  # fp > 0 and tp >= 0 ==> tp + fp > 0
                interpolated_precision = interpolated_tp / (interpolated_tp + interpolated_fp)
            else:
                interpolated_precision = 1  # limes n->0 n/n = 1

            listOfInterpolatedPR.append((interpolated_recall, interpolated_precision))

    listOfPR = listOfInterpolatedPR + [(recall, precision)]

    return precision, recall, listOfPR


def plot_precision_recall_overV_constAngleDiffThresh(experiment_data, dataset_names_list,
        p_to_use=1,
        angleDiffThresh_list=[1],
        show_legend=False):
    # make 1% steps, i.e. 0.01 steps for V:
    v_steps_list = [v_step / stepSize for v_step in range(int(stepSize * max_x) + 1)]

    true_positives_data_dict = {a:
                                    {dSname:
                                         {r:
                                              {v: None for v in v_steps_list}
                                          for r in searchradius_values}
                                     for dSname in dataset_names_list}
                                for a in angleDiffThresh_list}
    false_positives_data_dict = {a:
                                     {dSname:
                                          {r:
                                               {v: None for v in v_steps_list}
                                           for r in searchradius_values}
                                      for dSname in dataset_names_list}
                                 for a in angleDiffThresh_list}

    relevants_data_dict = {a:
                               {dSname:
                                    {r: None for r in searchradius_values}
                                for dSname in dataset_names_list}
                           for a in angleDiffThresh_list}

    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)

    relevants_list = get_Relevants(experiment_data)

    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_values:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_list:
                        relevants_data_dict[a][datasetName][r] = relevants

    print("getting Data from the experiments, v running from 0 to 1")
    display_steps = [i / 10 for i in range(10 + 1)]
    for v in v_steps_list:
        # print(v)
        post_processing.compute_verified_through_mu(data, v, p_to_use)
        post_processing.compute_verification(data)

        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)

        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_values:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_list:
                            # print(tp)
                            true_positives_data_dict[a][datasetName][r][v] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_values:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_list:
                            # print(fp)
                            false_positives_data_dict[a][datasetName][r][v] = fp

        progress = floor(v * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)

    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {a:
                               {dS:
                                    {r: [] for r in searchradius_values}
                                for dS in extendedDatasetNames_list}
                           for a in angleDiffThresh_list}
    recall_data_dict = {a:
                            {dS:
                                 {r: [] for r in searchradius_values}
                             for dS in extendedDatasetNames_list}
                        for a in angleDiffThresh_list}

    precision_recall_data_dict = {a:
                                      {dS:
                                           {r: [] for r in searchradius_values}
                                       for dS in extendedDatasetNames_list}
                                  for a in angleDiffThresh_list}

    e_data_dict = {a:
                       {dS:
                            {r: [] for r in searchradius_values}
                        for dS in extendedDatasetNames_list}
                   for a in angleDiffThresh_list}

    print("calculating precision and recall values")
    for r in searchradius_values:
        for a in angleDiffThresh_list:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for v in v_steps_list:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[a][datasetName][r]
                    tp = true_positives_data_dict[a][datasetName][r][v]
                    fp = false_positives_data_dict[a][datasetName][r][v]

                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum(
                        [x for name, x in datasetSceneCount.items() if name != datasetName])
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants

                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                             last_tp[datasetName],
                                                                             last_fp[datasetName],
                                                                             relevants)

                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight / precision + (
                                    1 - precisions_weight) / recall)
                    else:
                        e = 1
                    e_data_dict[a][datasetName][r].append((v, e))

                    precision_data_dict[a][datasetName][r].append((v, precision))
                    recall_data_dict[a][datasetName][r].append((v, recall))
                    precision_recall_data_dict[a][datasetName][r] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp

                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp / productOfScenes)
                fp = int(sum_fp / productOfScenes)
                relevants = int(sum_relevants / productOfScenes)

                precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                         last_tp[datasetName],
                                                                         last_fp[datasetName],
                                                                         relevants)

                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight / precision + (1 - precisions_weight) / recall)
                else:
                    e = 1
                e_data_dict[a][datasetName][r].append((v, e))

                precision_data_dict[a][datasetName][r].append((v, precision))
                recall_data_dict[a][datasetName][r].append((v, recall))
                precision_recall_data_dict[a][datasetName][r] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {a:
                                     {dS:
                                          {r: 0 for r in searchradius_values}
                                      for dS in extendedDatasetNames_list}
                                 for a in angleDiffThresh_list}

    print("computing area under curves")
    for a, precision_recall_by_angle in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_angle.items():
            for r, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                area = 0
                # print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        # print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[a][datasetName][r] += 0.5 * (
                                    recall - last_recall) * (precision + last_precision)
                        # print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall

    dictOfReturnFiguresA = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in
                            angleDiffThresh_list}
    # dictOfReturnFiguresB = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    print("generating plots...")
    for a, precision_recall_auc_by_angle in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_angle.items():
            dictOfReturnFiguresA[a][datasetName] = plt.figure(figsize=plotting.cm2inch(15, 10))

            precision_list = sorted(precision_data_dict[a][datasetName].items())
            recall_list = sorted(recall_data_dict[a][datasetName].items())
            e_list = sorted(e_data_dict[a][datasetName].items())

            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for r, precision in precision_list:
                if a != 1:
                    ax.plot(*zip(*precision),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_precision.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for r, recall in recall_list:
                if a != 1:
                    ax.plot(*zip(*recall),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_recall.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_e = plotting.PlotStyle(len(e_list),
                                      additional_plot_settings={"markerfacecolor": "None",
                                                                "marker": "None"})
            ax = plt.subplot(223)
            for r, e in e_list:
                if a != 1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$' % r, **ps_e.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$' % precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0.,
                          title='Parameter')

            """
            dictOfReturnFiguresB[a][datasetName], ax = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5,6))
    
            precision_recall_list = sorted(precision_recall_data_dict[a][datasetName].items())
            
            ps_precisionOverRecall = plotting.PlotStyle(len(precision_recall_list),
                                                        additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
            for r, precision_recall in precision_recall_list:
                auc = precision_recall_auc_dict[a][datasetName][r]
                ax.plot(*zip(*sorted(precision_recall, key=lambda x: (x[0], -x[1]))), label='$%.1f$ $($AUC: $%.2f)$'%(r, auc), **ps_precisionOverRecall.next())
                
            ax.set_xlabel('Trefferquote')
            ax.set_ylabel('Genauigkeit')
            
            ax.grid()
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            
            if show_legend:
                if a != 1:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                              title='$r_s$ mit $\\alpha=\\frac{1}{%d}\\pi$'%(1/a))
                else:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=\\pi$')
            
            # just make these graphics if they are needed
            for alpha in prweights:
                dictOfReturnFiguresC[a][datasetName][alpha], ax = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5,6))

                f_list = sorted(f_data_dict[a][datasetName][alpha].items())
                
                ps_f = plotting.PlotStyle(len(f_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
                for r, f in f_list:
                    ax.plot(*zip(*f), label='$%.1f$'%r, **ps_f.next())
                    
                ax.set_xlabel('$\mathcal{V}$')
                ax.set_ylabel('$F_{%.1f}$'%alpha)
                
                ax.grid()
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                
                if show_legend:
                    ax.legend(loc='upper right', title='$r_s$')
            """

    return dictOfReturnFiguresA


def plot_precision_recall_overP_constAngleDiffThresh(experiment_data, dataset_names_list,
        v_to_use=0,
        angleDiffThresh_list=[1],
        show_legend=False):
    p_steps_list = [(p_step - 1) / stepSize for p_step in range(int(stepSize * max_x) + 1)]

    true_positives_data_dict = {a:
                                    {dSname:
                                         {r:
                                              {p: None for p in p_steps_list}
                                          for r in searchradius_values}
                                     for dSname in dataset_names_list}
                                for a in angleDiffThresh_list}
    false_positives_data_dict = {a:
                                     {dSname:
                                          {r:
                                               {p: None for p in p_steps_list}
                                           for r in searchradius_values}
                                      for dSname in dataset_names_list}
                                 for a in angleDiffThresh_list}

    relevants_data_dict = {a:
                               {dSname:
                                    {r: None for r in searchradius_values}
                                for dSname in dataset_names_list}
                           for a in angleDiffThresh_list}

    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)

    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_values:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_list:
                        # print(relevants)
                        relevants_data_dict[a][datasetName][r] = relevants

    print("getting Data from the experiments, p running from 0 to 1")
    display_steps = [i / 10 for i in range(10 + 1)]
    for p in p_steps_list:
        # print(p)
        post_processing.compute_verified_through_mu(data, v_to_use, p)
        post_processing.compute_verification(data)

        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)

        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_values:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_list:
                            # print(tp)
                            true_positives_data_dict[a][datasetName][r][p] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_values:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_list:
                            # print(fp)
                            false_positives_data_dict[a][datasetName][r][p] = fp

        progress = floor((p * stepSize + 1) / (stepSize * max_x + 1) * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)

    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {a:
                               {dS:
                                    {r: [] for r in searchradius_values}
                                for dS in extendedDatasetNames_list}
                           for a in angleDiffThresh_list}
    recall_data_dict = {a:
                            {dS:
                                 {r: [] for r in searchradius_values}
                             for dS in extendedDatasetNames_list}
                        for a in angleDiffThresh_list}

    precision_recall_data_dict = {a:
                                      {dS:
                                           {r: [] for r in searchradius_values}
                                       for dS in extendedDatasetNames_list}
                                  for a in angleDiffThresh_list}

    e_data_dict = {a:
                       {dS:
                            {r: [] for r in searchradius_values}
                        for dS in extendedDatasetNames_list}
                   for a in angleDiffThresh_list}

    print("calculating precision and recall values")
    for r in searchradius_values:
        for a in angleDiffThresh_list:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for p in p_steps_list[::-1]:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[a][datasetName][r]
                    tp = true_positives_data_dict[a][datasetName][r][p]
                    fp = false_positives_data_dict[a][datasetName][r][p]

                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum(
                        [x for name, x in datasetSceneCount.items() if name != datasetName])
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants

                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                             last_tp[datasetName],
                                                                             last_fp[datasetName],
                                                                             relevants)

                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight / precision + (
                                    1 - precisions_weight) / recall)
                    else:
                        e = 1
                    e_data_dict[a][datasetName][r].append((p, e))

                    precision_data_dict[a][datasetName][r].append((p, precision))
                    recall_data_dict[a][datasetName][r].append((p, recall))
                    precision_recall_data_dict[a][datasetName][r] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp

                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp / productOfScenes)
                fp = int(sum_fp / productOfScenes)
                relevants = int(sum_relevants / productOfScenes)

                precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                         last_tp[datasetName],
                                                                         last_fp[datasetName],
                                                                         relevants)

                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight / precision + (1 - precisions_weight) / recall)
                else:
                    e = 1
                e_data_dict[a][datasetName][r].append((p, e))

                precision_data_dict[a][datasetName][r].append((p, precision))
                recall_data_dict[a][datasetName][r].append((p, recall))
                precision_recall_data_dict[a][datasetName][r] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {a:
                                     {dS:
                                          {r: 0 for r in searchradius_values}
                                      for dS in extendedDatasetNames_list}
                                 for a in angleDiffThresh_list}

    print("computing area under curves")
    v = 0
    for a, precision_recall_by_angle in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_angle.items():
            for r, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                # print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        # print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[a][datasetName][r] += 0.5 * (
                                    recall - last_recall) * (precision + last_precision)
                        # print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall

    dictOfReturnFiguresA = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in
                            angleDiffThresh_list}
    # dictOfReturnFiguresB = {a: {dSname: None for dSname in extendedDatasetNames_list} for a in angleDiffThresh_list}
    print("generating plots...")
    for a, precision_recall_auc_by_angle in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_angle.items():
            dictOfReturnFiguresA[a][datasetName] = plt.figure(figsize=plotting.cm2inch(15, 10))

            precision_list = sorted(precision_data_dict[a][datasetName].items())
            recall_list = sorted(recall_data_dict[a][datasetName].items())
            e_list = sorted(e_data_dict[a][datasetName].items())

            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for r, precision in precision_list:
                if a != 1:
                    ax.plot(*zip(*precision),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_precision.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for r, recall in recall_list:
                if a != 1:
                    ax.plot(*zip(*recall),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_recall.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_e = plotting.PlotStyle(len(e_list),
                                      additional_plot_settings={"markerfacecolor": "None",
                                                                "marker": "None"})
            ax = plt.subplot(223)
            for r, e in e_list:
                if a != 1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$' % r, **ps_e.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$' % precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0.,
                          title='Parameter')

            """
            dictOfReturnFiguresB[a][datasetName], ax = plt.subplots(1,1, figsize=plotting.cm2inch(7.5,6))
    
            precision_recall_list = sorted(precision_recall_data_dict[a][datasetName].items())
            
            ps_precisionOverRecall = plotting.PlotStyle(len(precision_recall_list),
                                                        additional_plot_settings={"markerfacecolor": "None",
                                                                                  "marker": "None"})
            for r, precision_recall in precision_recall_list:
                auc = precision_recall_auc_dict[a][datasetName][r]
                ax.plot(*zip(*sorted(precision_recall, key=lambda x: (x[0], -x[1]))), label='$%.1f$ $($AUC: $%.2f)$'%(r, auc), **ps_precisionOverRecall.next())
                
            ax.set_xlabel('Trefferquote')
            ax.set_ylabel('Genauigkeit')
            
            ax.grid()
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
                
            if show_legend:
                if a != 1:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=\\frac{1}{%d}\\pi$'%(1/a))
                else:
                    ax.legend(bbox_to_anchor=(0., -.5, 1., .102), borderaxespad=0., loc="upper center",
                          title='$r_s$ mit $\\alpha=pi$')
            """

    return dictOfReturnFiguresA


def plot_precision_recall_overV_constSearchradius(experiment_data, dataset_names_list,
        p_to_use=1,
        searchradius_list=[1],
        show_legend=False):
    v_steps_list = [v_step / stepSize for v_step in range(int(stepSize * max_x) + 1)]

    true_positives_data_dict = {r:
                                    {dSname:
                                         {a:
                                              {v: None for v in v_steps_list}
                                          for a in angleDiffThresh_in_pi_values}
                                     for dSname in dataset_names_list}
                                for r in searchradius_list}
    false_positives_data_dict = {r:
                                     {dSname:
                                          {a:
                                               {v: None for v in v_steps_list}
                                           for a in angleDiffThresh_in_pi_values}
                                      for dSname in dataset_names_list}
                                 for r in searchradius_list}

    relevants_data_dict = {r:
                               {dSname:
                                    {a: None for a in angleDiffThresh_in_pi_values}
                                for dSname in dataset_names_list}
                           for r in searchradius_list}

    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)

    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_list:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_in_pi_values:
                        relevants_data_dict[r][datasetName][a] = relevants

    print("getting Data from the experiments, v running from 0 to 1")
    display_steps = [i / 10 for i in range(10 + 1)]
    for v in v_steps_list:
        # print(v)
        post_processing.compute_verified_through_mu(data, v, p_to_use)
        post_processing.compute_verification(data)

        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)

        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_list:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            # print(tp)
                            true_positives_data_dict[r][datasetName][a][v] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_list:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            # print(fp)
                            false_positives_data_dict[r][datasetName][a][v] = fp

        progress = floor(v * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)

    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {r:
                               {dS:
                                    {a: [] for a in angleDiffThresh_in_pi_values}
                                for dS in extendedDatasetNames_list}
                           for r in searchradius_list}
    recall_data_dict = {r:
                            {dS:
                                 {a: [] for a in angleDiffThresh_in_pi_values}
                             for dS in extendedDatasetNames_list}
                        for r in searchradius_list}

    precision_recall_data_dict = {r:
                                      {dS:
                                           {a: [] for a in angleDiffThresh_in_pi_values}
                                       for dS in extendedDatasetNames_list}
                                  for r in searchradius_list}

    e_data_dict = {r:
                       {dS:
                            {a: [] for a in angleDiffThresh_in_pi_values}
                        for dS in extendedDatasetNames_list}
                   for r in searchradius_list}

    print("calculating precision and recall values")
    for r in searchradius_list:
        for a in angleDiffThresh_in_pi_values:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for v in v_steps_list:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[r][datasetName][a]
                    tp = true_positives_data_dict[r][datasetName][a][v]
                    fp = false_positives_data_dict[r][datasetName][a][v]

                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum(
                        [x for name, x in datasetSceneCount.items() if name != datasetName])
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants

                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                             last_tp[datasetName],
                                                                             last_fp[datasetName],
                                                                             relevants)

                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight / precision + (
                                    1 - precisions_weight) / recall)
                    else:
                        e = 1
                    e_data_dict[r][datasetName][a].append((v, e))

                    precision_data_dict[r][datasetName][a].append((v, precision))
                    recall_data_dict[r][datasetName][a].append((v, recall))
                    precision_recall_data_dict[r][datasetName][a] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp

                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp / productOfScenes)
                fp = int(sum_fp / productOfScenes)
                relevants = int(sum_relevants / productOfScenes)

                precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                         last_tp[datasetName],
                                                                         last_fp[datasetName],
                                                                         relevants)

                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight / precision + (1 - precisions_weight) / recall)
                else:
                    e = 1
                e_data_dict[r][datasetName][a].append((v, e))

                precision_data_dict[r][datasetName][a].append((v, precision))
                recall_data_dict[r][datasetName][a].append((v, recall))
                precision_recall_data_dict[r][datasetName][a] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {r:
                                     {dS:
                                          {a: 0 for a in angleDiffThresh_in_pi_values}
                                      for dS in extendedDatasetNames_list}
                                 for r in searchradius_list}

    print("computing area under curves")
    for r, precision_recall_by_radius in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_radius.items():
            for a, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                # print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data, key=lambda x: (x[0], -x[1])):
                    if last_precision is not None and last_recall is not None:
                        # print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[r][datasetName][a] += 0.5 * (
                                    recall - last_recall) * (precision + last_precision)
                        # print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall

    dictOfReturnFiguresA = {r: {dSname: None for dSname in extendedDatasetNames_list} for r in
                            searchradius_list}
    print("generating plots...")
    for r, precision_recall_auc_by_radius in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_radius.items():
            dictOfReturnFiguresA[r][datasetName] = plt.figure(figsize=plotting.cm2inch(15, 10))

            precision_list = sorted(precision_data_dict[r][datasetName].items())
            recall_list = sorted(recall_data_dict[r][datasetName].items())
            e_list = sorted(e_data_dict[r][datasetName].items())

            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for a, precision in precision_list:
                if a != 1:
                    ax.plot(*zip(*precision),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_precision.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for a, recall in recall_list:
                if a != 1:
                    ax.plot(*zip(*recall),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_recall.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_e = plotting.PlotStyle(len(e_list),
                                      additional_plot_settings={"markerfacecolor": "None",
                                                                "marker": "None"})
            ax = plt.subplot(223)
            for a, e in e_list:
                if a != 1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$' % r, **ps_e.next())
            ax.set_xlabel('$\mathcal{V}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$' % precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0.,
                          title='Parameter')

    return dictOfReturnFiguresA, e_data_dict


def plot_precision_recall_overP_constSearchradius(experiment_data, dataset_names_list,
        v_to_use=0,
        searchradius_list=[1],
        show_legend=False):
    p_steps_list = [(p_step - 1) / stepSize for p_step in range(int(stepSize * max_x) + 1)]

    true_positives_data_dict = {r:
                                    {dSname:
                                         {a:
                                              {p: None for p in p_steps_list}
                                          for a in angleDiffThresh_in_pi_values}
                                     for dSname in dataset_names_list}
                                for r in searchradius_list}
    false_positives_data_dict = {r:
                                     {dSname:
                                          {a:
                                               {p: None for p in p_steps_list}
                                           for a in angleDiffThresh_in_pi_values}
                                      for dSname in dataset_names_list}
                                 for r in searchradius_list}

    relevants_data_dict = {r:
                               {dSname:
                                    {a: None for a in angleDiffThresh_in_pi_values}
                                for dSname in dataset_names_list}
                           for r in searchradius_list}

    post_processing.compute_verified_through_mu(data, 0, 1)
    post_processing.compute_verification(data)

    relevants_list = get_Relevants(experiment_data)
    for datasetName, relevants_by_radius in relevants_list:
        for r, relevants_by_angle in relevants_by_radius:
            if r in searchradius_list:
                for a, relevants in relevants_by_angle:
                    if a in angleDiffThresh_in_pi_values:
                        # print(relevants)
                        relevants_data_dict[r][datasetName][a] = relevants

    print("getting Data from the experiments, p running from 0 to 1")
    display_steps = [i / 10 for i in range(10 + 1)]
    for p in p_steps_list:
        # print(p)
        post_processing.compute_verified_through_mu(data, v_to_use, p)
        post_processing.compute_verification(data)

        true_positives_list = get_TPs(experiment_data)
        false_positives_list = get_FPs(experiment_data)

        for datasetName, true_positives_by_radius in true_positives_list:
            for r, true_positives_by_angle in true_positives_by_radius:
                if r in searchradius_list:
                    for a, tp in true_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            # print(tp)
                            true_positives_data_dict[r][datasetName][a][p] = tp
        for datasetName, false_positives_by_radius in false_positives_list:
            for r, false_positives_by_angle in false_positives_by_radius:
                if r in searchradius_list:
                    for a, fp in false_positives_by_angle:
                        if a in angleDiffThresh_in_pi_values:
                            # print(fp)
                            false_positives_data_dict[r][datasetName][a][p] = fp

        progress = floor((p * stepSize + 1) / (stepSize * max_x + 1) * 10) / 10
        if progress in display_steps:
            print('%d %% done' % (progress * 100))
            display_steps.remove(progress)

    extendedDatasetNames_list = dataset_names_list + ['all']

    precision_data_dict = {r:
                               {dS:
                                    {a: [] for a in angleDiffThresh_in_pi_values}
                                for dS in extendedDatasetNames_list}
                           for r in searchradius_list}
    recall_data_dict = {r:
                            {dS:
                                 {a: [] for a in angleDiffThresh_in_pi_values}
                             for dS in extendedDatasetNames_list}
                        for r in searchradius_list}

    precision_recall_data_dict = {r:
                                      {dS:
                                           {a: [] for a in angleDiffThresh_in_pi_values}
                                       for dS in extendedDatasetNames_list}
                                  for r in searchradius_list}

    e_data_dict = {r:
                       {dS:
                            {a: [] for a in angleDiffThresh_in_pi_values}
                        for dS in extendedDatasetNames_list}
                   for r in searchradius_list}

    print("calculating precision and recall values")
    for r in searchradius_list:
        for a in angleDiffThresh_in_pi_values:
            last_tp = {ds: None for ds in extendedDatasetNames_list}
            last_fp = {ds: None for ds in extendedDatasetNames_list}
            for p in p_steps_list[::-1]:
                sum_tp = 0
                sum_fp = 0
                sum_relevants = 0
                for datasetName in dataset_names_list:
                    relevants = relevants_data_dict[r][datasetName][a]
                    tp = true_positives_data_dict[r][datasetName][a][p]
                    fp = false_positives_data_dict[r][datasetName][a][p]

                    # all datasets have the same weight
                    # factor is the product of scenes in other datasets
                    factor = sum(
                        [x for name, x in datasetSceneCount.items() if name != datasetName])
                    sum_tp += factor * tp
                    sum_fp += factor * fp
                    sum_relevants += factor * relevants

                    precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                             last_tp[datasetName],
                                                                             last_fp[datasetName],
                                                                             relevants)
                    # in preselection the recall is more important, than the precision
                    if precision != 0 and recall != 0:
                        e = 1 - 1 / (precisions_weight / precision + (
                                    1 - precisions_weight) / recall)
                    else:
                        e = 1
                    e_data_dict[r][datasetName][a].append((p, e))

                    precision_data_dict[r][datasetName][a].append((p, precision))
                    recall_data_dict[r][datasetName][a].append((p, recall))
                    precision_recall_data_dict[r][datasetName][a] += listOfPR

                    last_tp[datasetName] = tp
                    last_fp[datasetName] = fp

                datasetName = 'all'
                productOfScenes = sum([x for _, x in datasetSceneCount.items()])
                tp = int(sum_tp / productOfScenes)
                fp = int(sum_fp / productOfScenes)
                relevants = int(sum_relevants / productOfScenes)

                precision, recall, listOfPR = compute_precisions_recalls(tp, fp,
                                                                         last_tp[datasetName],
                                                                         last_fp[datasetName],
                                                                         relevants)

                if precision != 0 and recall != 0:
                    e = 1 - 1 / (precisions_weight / precision + (1 - precisions_weight) / recall)
                else:
                    e = 1
                e_data_dict[r][datasetName][a].append((p, e))

                precision_data_dict[r][datasetName][a].append((p, precision))
                recall_data_dict[r][datasetName][a].append((p, recall))
                precision_recall_data_dict[r][datasetName][a] += listOfPR

                last_tp[datasetName] = tp
                last_fp[datasetName] = fp

    precision_recall_auc_dict = {r:
                                     {dS:
                                          {a: 0 for a in angleDiffThresh_in_pi_values}
                                      for dS in extendedDatasetNames_list}
                                 for r in searchradius_list}

    print("computing area under curves")
    v = 0
    for r, precision_recall_by_radius in precision_recall_data_dict.items():
        for datasetName, precision_recall_by_dataset in precision_recall_by_radius.items():
            for a, rp_data in precision_recall_by_dataset.items():
                last_precision = None
                last_recall = None
                # print("looking at r=%s"%r)
                for recall, precision in sorted(rp_data):
                    if last_precision is not None and last_recall is not None:
                        # print("recall is %s and precision is %s" %(recall,precision))
                        precision_recall_auc_dict[r][datasetName][a] += 0.5 * (
                                    recall - last_recall) * (precision + last_precision)
                        # print("area is %s"%area)
                    last_precision = precision
                    last_recall = recall

    dictOfReturnFiguresA = {r: {dSname: None for dSname in extendedDatasetNames_list} for r in
                            searchradius_list}
    print("generating plots...")
    for r, precision_recall_auc_by_radius in precision_recall_auc_dict.items():
        for datasetName, _ in precision_recall_auc_by_radius.items():
            dictOfReturnFiguresA[r][datasetName] = plt.figure(figsize=plotting.cm2inch(15, 10))

            precision_list = sorted(precision_data_dict[r][datasetName].items())
            recall_list = sorted(recall_data_dict[r][datasetName].items())
            e_list = sorted(e_data_dict[r][datasetName].items())

            ps_precision = plotting.PlotStyle(len(precision_list),
                                              additional_plot_settings={"markerfacecolor": "None",
                                                                        "marker": "None"})
            ax = plt.subplot(221)
            for a, precision in precision_list:
                if a != 1:
                    ax.plot(*zip(*precision),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_precision.next())
                else:
                    ax.plot(*zip(*precision), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_precision.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Genauigkeit')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_recall = plotting.PlotStyle(len(recall_list),
                                           additional_plot_settings={"markerfacecolor": "None",
                                                                     "marker": "None"})
            ax = plt.subplot(222)
            for a, recall in recall_list:
                if a != 1:
                    ax.plot(*zip(*recall),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_recall.next())
                else:
                    ax.plot(*zip(*recall), label='$r_s=%.1f$, $\\alpha=\\pi$' % r,
                            **ps_recall.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('Trefferquote')
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            ps_e = plotting.PlotStyle(len(e_list),
                                      additional_plot_settings={"markerfacecolor": "None",
                                                                "marker": "None"})
            ax = plt.subplot(223)
            for a, e in e_list:
                if a != 1:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (r, 1 / a),
                            **ps_e.next())
                else:
                    ax.plot(*zip(*e), label='$r_s=%.1f$, $\\alpha=\\pi$' % r, **ps_e.next())
            ax.set_xlabel('$\mathcal{P}$')
            ax.set_ylabel('$E$ mit $\\beta=%.1f$' % precisions_weight)
            ax.grid()
            ax.set_xlim(0, max_x)
            ax.set_ylim(0, 1)

            if show_legend:
                ax.legend(bbox_to_anchor=(1.5, 1), loc='upper left', borderaxespad=0.,
                          title='Parameter')

    return dictOfReturnFiguresA, e_data_dict


def plot_relevant_model_points_over_searchradius(experiment_data, ax, datasets, show_legend=False):
    """
    plot the number of relevant model points over the searchradius
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier',
                                                       'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]
    average_model_points_time_per_angleDiffThresh_per_searchradius = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['searchradius'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_model_points)

    ps = plotting.PlotStyle(len(average_model_points_time_per_angleDiffThresh_per_searchradius),
                            additional_plot_settings={"markerfacecolor": "None"})
    ax.plot(*zip(*average_model_points_time_per_angleDiffThresh_per_searchradius),
            label='Anzahl der Modellpunkte', **ps.next())

    ax.set_xlabel('$r_s$')
    ax.set_ylabel('Anzahl der\nrelevanten Modellpunkte')
    ax.grid(which='both')

    if show_legend:
        ax.legend(loc='lower right')

    ax.set_xlim(0, max(searchradius_values))
    ax.set_ylim(0, 500)

    return ax


def plot_mu_time_over_angleDiffThresh(experiment_data, ax, datasets):
    """
    plot the time required for calculating mu time over the angleDiffThresh
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier',
                                                       'angleDiffThresh_in_pi'],
                                                      ['dynamicParameters', '/verifier',
                                                       'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]

    average_muvalue_time_per_searchradius_per_angleDiffThresh = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)

    ps = plotting.PlotStyle(len(average_muvalue_time_per_searchradius_per_angleDiffThresh),
                            additional_plot_settings={"markerfacecolor": "None"})
    for r, plot_data in average_muvalue_time_per_searchradius_per_angleDiffThresh:
        ax.plot(*zip(*plot_data), label='$%.1f$' % (r), **ps.next())

    ax.set_xlabel('$\\alpha$')
    ax.set_ylabel('mittlere Berechnungszeit\nf\\"ur $\\mu$ in ms')
    ax.grid(which='both')
    ax.legend(loc='upper right', title='$r_s$')
    ax.set_xlim(0, max(angleDiffThresh_in_pi_values))
    # ax.set_ylim(1, 1800)

    return ax


def plot_mu_time_over_searchradius(experiment_data, ax, datasets):
    """
    plot the time required for calculating mu time over the searchradius
    @type experiment_data: ExperimentDataStrucure
    @param experiment_data: experiment_data
    @type ax: Axes
    @param ax: axis to plot to
    @return: ax-objet the function plotted in
    """
    iDict, basic_data = data_extraction.extract_data(experiment_data,
                                                     [['statistics', '/verifier']],
                                                     [['datasetName'],
                                                      ['dynamicParameters', '/verifier',
                                                       'angleDiffThresh_in_pi'],
                                                      ['dynamicParameters', '/verifier',
                                                       'searchradius']])
    relevant_data = [entry for entry in basic_data
                     if entry[iDict['datasetName']] in datasets]

    average_muvalue_time_per_angleDiffThresh_per_searchradius = \
        data_extraction.nest_and_process_by_values(relevant_data,
                                                   [iDict['angleDiffThresh_in_pi'],
                                                    iDict['searchradius'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)

    ps = plotting.PlotStyle(len(average_muvalue_time_per_angleDiffThresh_per_searchradius),
                            additional_plot_settings={"markerfacecolor": "None"})
    for a, plot_data in average_muvalue_time_per_angleDiffThresh_per_searchradius:
        if a != 1:
            ax.plot(*zip(*plot_data), label='$\\frac{1}{%d}\\pi$' % (1 / a), **ps.next())
        else:
            ax.plot(*zip(*plot_data), label='$\\pi$', **ps.next())

    ax.set_xlabel('$r_s$')
    ax.set_ylabel('mittlere Berechnungszeit\nf\\"ur $\\mu$ in ms')
    ax.grid(which='both')
    ax.legend(loc='upper left', title='$\\alpha$')
    ax.set_xlim(0, max(searchradius_values))
    # ax.set_ylim(1, 1800)

    return ax


def plot_recognitionRate_two_instances(data, baseline_data, best_setup, ax,
                                       additional_plot_data=None, show_legend=True):

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi'],
                                      ['datasetName']])
    # print(basic_data)
    # print(iDict)

    # loop over searchradii and angle thresholds to filter the run-data according to its search radius and angle threshold
    # empty filtered data list
    filtered_data = []
    # find runs that have the currently evaluated searchradius and angle threshold
    for run in basic_data:
            if run[iDict['searchradius']] == best_setup['searchradius'] and \
                    run[iDict['angleDiffThresh_in_pi']] == best_setup['angleDiffThresh_in_pi']:
                # add data of this run to filtered data
                filtered_data.append(run)

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    # print('\n**************** per model ***************\n',recognition_rate_per_model_with_verifier_info)
    # print('\n**************** all models ***************\n',recognition_rate_with_verifier_info)

    # extend dict for storing the best setups
    best_setup['recognition_rate_two_instance_detection'] = -1

    # filling the best_setup_dict
    for sr, searchradius_data in recognition_rate_with_verifier_info:
        for adt, angleDiffThresh_data in searchradius_data:
            if best_setup['angleDiffThresh_in_pi'] == adt \
                    and best_setup['searchradius'] == sr:
                best_setup['recognition_rate_two_instance_detection'] = angleDiffThresh_data
    if best_setup['recognition_rate_two_instance_detection'] == -1:
        print("Data does not have runs for best searchradius or best angleDiffThresh. Something went wrong...")
        return None

    print('\nTwo-instance-detection: The best setup yields the recognition-rate: ', best_setup)

    # get the model-specific recognition rates, and
    # put the model-specific recognition rates into a format, that bar_by_model() can handle
    for sr, searchradius_data in recognition_rate_per_model_with_verifier_info:
        if sr == best_setup['searchradius']:
            for adt, angleDiffThresh_data in searchradius_data:
                if adt == best_setup['angleDiffThresh_in_pi']:
                    plot_data=angleDiffThresh_data

    #print('Two-instance-detection: Best recognition rates for each model =', plot_data)

    # ******************************************************************************************************************
    # do plotting --- barplot
    # ******************************************************************************************************************

    # add baseline-data
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])  # add "baseline"-mode
    if additional_plot_data is None:
        # case that optimal V and P are not known yet:
        recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions without Flag Array and\nClustering with $r_\mathrm{s}=%s$ and "
                                           "$\\alpha=%s\pi$,\n$\mathcal{V}=0.0, \mathcal{P}=1.0$, without conflict-detection"
                                           % (best_setup["searchradius"], round(best_setup['angleDiffThresh_in_pi'],3)),
                                          plot_data))
    else:
        # best V and P are known:
        recognition_rate_per_model.append(RESULT_FROM_PUBLISH_N_BEST_POSES_TWO_INSTANCE_DETECTION)
        recognition_rate_per_model.append(additional_plot_data)
        recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions without Flag Array and\nClustering with $r_\mathrm{s}=%s$ and "
                                           "$\\alpha=%s\pi$,\n$\mathcal{V}=%s, \mathcal{P}=%s$, without conflict-detection"
                                           % (best_setup["searchradius"], round(best_setup['angleDiffThresh_in_pi'],3),
                                              best_setup["supportthreshold"], best_setup["penaltythreshold"]),
                                          plot_data))

    print('\nTwo-instance-detection: Best recognition rates for each model are now:')
    for x in recognition_rate_per_model:
        print(str(x))

    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'datasets', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (two instances)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')

    return {'best_setup_dict': best_setup, 'plot_data': recognition_rate_per_model[1]}


def plot_max_recognitionRate_for_Krones_parameter_values(data, searchradius,
        angleDiffThreshold_in_pi, ax, bestV, bestP, show_legend):
    # add the 'weight' in the 'verified_poses' dict as 'verificationWeight' to the 'poses' dict.
    post_processing.add_verifiedWeight_to_poses(data)

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi'],
                                      ['datasetName']])
    # print(basic_data)
    # print(iDict)

    # loop over searchradii and angle thresholds to filter the run-data according to its search radius and angle threshold
    # empty filtered data list
    filtered_data = []
    # find runs that have the currently evaluated searchradius and angle threshold
    for run in basic_data:
        if run[iDict['searchradius']] == searchradius and \
                run[iDict['angleDiffThresh_in_pi']] == angleDiffThreshold_in_pi:
            # add data of this run to filtered data
            filtered_data.append(run)

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['datasetName'],
                                                    iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    # print('\n**************** per model ***************\n',recognition_rate_per_model_with_verifier_info)
    # print('\n**************** all models ***************\n',recognition_rate_with_verifier_info)

    best_setup_dict = {dataset[0]:
                           {'searchradius': -1, 'angleDiffThresh_in_pi': -1, 'recognition_rate': -1}
                       for dataset in recognition_rate_with_verifier_info
                       }
    for dataset_data in recognition_rate_with_verifier_info:
        dataset = dataset_data[0]
        for searchradius_data in dataset_data[1]:
            sr = searchradius_data[0]
            for angleDiffThresh_data in searchradius_data[1]:
                adt = angleDiffThresh_data[0]
                if angleDiffThresh_data[1] > best_setup_dict[dataset]['recognition_rate']:
                    best_setup_dict[dataset]['recognition_rate'] = angleDiffThresh_data[1]
                    best_setup_dict[dataset]['searchradius'] = sr
                    best_setup_dict[dataset]['angleDiffThresh_in_pi'] = adt

    print('Krone, single-instance-detection: The best setup yields the recognition-rate: ',
          best_setup_dict)

    plot_data = []
    for dataset_data in recognition_rate_per_model_with_verifier_info:
        dataset = dataset_data[0]
        for searchradius_data in dataset_data[1]:
            sr = searchradius_data[0]
            if sr == best_setup_dict[dataset]['searchradius']:
                for angleDiffThresh_data in searchradius_data[1]:
                    adt = angleDiffThresh_data[0]
                    if adt == best_setup_dict[dataset]['angleDiffThresh_in_pi']:
                        model_data = angleDiffThresh_data[1]
                        # print(model_data)
                        plot_data.append(("\\textsc{Krone}'s tuning\n"
                                          "$r_\\mathrm{s}=%s, \\alpha=%s\\pi$\n"
                                          "$\\mathcal{V}=%s, \\mathcal{P}=%s$"
                                          %(sr, adt, bestV, bestP), tuple(model_data)))
    plot_data = tuple(plot_data)

    # print('plot data =', plot_data)

    # plot best recognition rate for each model
    bar_by_model(plot_data, ax, 'datasets', show_legend=show_legend)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='lower right')


def find_searchRadius_angleDiffThresh_combination_with_max_recognitionRate(data, baseline_data,
                                                                           ax, ax_3D, show_legend):
    # **********************************************************************************************
    # baseline
    # **********************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the
    # matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # **********************************************************************************************
    # extensions - with verifier
    # **********************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi']])
    # print(basic_data)
    # print(iDict)

    #filter cylinder from data:
    filtered_basic_data = []
    for run_id, run_data in enumerate(basic_data):
        # filter 'cylinder' model
        model_name = run_data[iDict['model']]
        if model_name != 'cylinder':
            filtered_basic_data.append(run_data)

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_verifier_info = \
        data_extraction.nest_and_process_by_values(filtered_basic_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)

    # print('\n**************** per model ***************\n',recognition_rate_per_model_with_verifier_info)
    # print('\n**************** all models ***************\n',recognition_rate_with_verifier_info)

    # dict for storing the best setups
    best_setup_dict = {'searchradius': -1, 'angleDiffThresh_in_pi': -1, 'recognition_rate': -1}

    #  get the best combination of searchradius and angleDiffThresh
    for sr, searchradius_data in recognition_rate_with_verifier_info:
        for adt, angleDiffThresh_data in searchradius_data:
            if angleDiffThresh_data > best_setup_dict['recognition_rate']:
                best_setup_dict['recognition_rate'] = angleDiffThresh_data
                best_setup_dict['searchradius'] = sr
                best_setup_dict['angleDiffThresh_in_pi'] = adt

    print('The best setup yields the recognition-rate: ',
          best_setup_dict)

    # get the recognition numbers for best setup
    for sr, searchradius_data in recognition_rate_per_model_with_verifier_info:
        if sr == best_setup_dict['searchradius']:
            for adt, angleDiffThresh_data in searchradius_data:
                if adt == best_setup_dict['angleDiffThresh_in_pi']:
                    plot_data = angleDiffThresh_data

    print('Best recognition rates for each model =', plot_data)

    # ******************************************************************************************************************
    # do plotting - barplot
    # ******************************************************************************************************************

    # merge plot_data and baseline-recognition numbers
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rate_per_model.append(
        ("\\textsc{Hinterstoisser}'s extensions without Flag Array and Clustering\nwith $r_\mathrm{s}=%s$ and "
         "$\\alpha=%s\pi$,\n$\mathcal{V}=0.0, \mathcal{P}=1.0$, without\nconflict-detection"
         % (best_setup_dict["searchradius"], round(best_setup_dict['angleDiffThresh_in_pi'],3)),
         plot_data))

    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'datasets', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')

    # ******************************************************************************************************************
    # do plotting - 3D plot
    # ******************************************************************************************************************

    if ax_3D is not None:

        # ignoreFactors and gapSizeFactors for plotting
        xi = np.array(searchradius_values, dtype=np.float)
        yi = np.array(angleDiffThresh_in_pi_values, dtype=np.float)
        # generate 2D-meshgrids for the plot
        x_grid, y_grid = np.meshgrid(xi, yi)
        x_max = []
        y_max = []
        rr_max = []
        for sr, rr_by_sr in recognition_rate_with_verifier_info:  # loop over search radii
            for adt, rr_by_adt in rr_by_sr:  # loop over angle difference thresholds
                x_max.append(sr)
                y_max.append(adt)
                rr_max.append(rr_by_adt)

        # interpolate average matching time (mti) for the
        # ignoreFactors (xi) and gapSizeFactors (yi) from the real data (x,y,mt)
        # from scipy.interpolate import griddata
        # rri = griddata((x_max, y_max), mt_max, (x_grid, y_grid), method='cubic')
        rri = (np.array(rr_max).reshape((xi.size, yi.size))).T

        # fourth dimension - colormap --> not used. because the generated colors are irritating
        # create colormap according to rr-value
        color_dimension = rri  # change to desired fourth dimension
        minn, maxx = color_dimension.min(), color_dimension.max()
        norm = mpl.colors.Normalize(minn, maxx)
        m = plt.cm.ScalarMappable(norm=norm, cmap=cm.coolwarm)
        m.set_array([])
        fcolors = m.to_rgba(color_dimension)

        ax_3D.plot_surface(X=x_grid, Y=np.log10(y_grid), Z=rri,
                           rstride=1, cstride=1,
                           cmap=cm.coolwarm, linewidth=0.5,
                           # facecolors=fcolors,
                           antialiased=True,
                           vmin=minn, vmax=maxx, shade=False,
                           edgecolor='k', alpha=1)
        # set camera view-point
        ax_3D.azim = 57.0
        ax_3D.elev = 40.0
        ax_3D.dist = 10.5

        ax_3D.set_xlabel('searchradius')
        ax_3D.set_ylabel('angle difference\nthreshold in $\pi$')
        ax_3D.set_zlabel('recognition rate\n(one instance)')
        ax_3D.set_xticks([x_ for x_ in xi])
        ax_3D.set_xticklabels(ax_3D.get_xticks(), rotation=-39, va='baseline', ha='left',
                              rotation_mode='anchor')
        ax_3D.set_yticks([y_ for y_ in np.log10(yi)])
        scaled_labels = [round(math.pow(10, float(t)), 2) for t in ax_3D.get_yticks()]
        ax_3D.set_yticklabels(scaled_labels, rotation=19, va='center', ha='right', rotation_mode=None)
        ax_3D.set_zlim(0, 1)
        ax_3D.set_zticklabels(ax_3D.get_zticks(), va='center', ha='right')
        ax_3D.xaxis._axinfo['label']['space_factor'] = 2.7
        ax_3D.yaxis._axinfo['label']['space_factor'] = 3.7
        ax_3D.zaxis._axinfo['label']['space_factor'] = 3.3
        # make the panes transparent (r,g,b,alpha)
        ax_3D.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_3D.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax_3D.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines black (r,g,b,alpha)
        ax_3D.xaxis._axinfo["grid"]['color'] = (189 / 255, 189 / 255, 189 / 255, 1)
        ax_3D.yaxis._axinfo["grid"]['color'] = (189 / 255, 189 / 255, 189 / 255, 1)
        ax_3D.zaxis._axinfo["grid"]['color'] = (189 / 255, 189 / 255, 189 / 255, 1)

        # ax_3D.yaxis.set_scale('log')

    return {'best_setup_dict': best_setup_dict, 'plot_data': recognition_rate_per_model}


def find_mu_v_and_mu_p_combination_with_max_recognition_rate(data, best_setup_dict, ax,
                                                             additional_plot_data, show_legend):

    # TODO: choose step sizes in %:
    v_steps_stepsize = 1
    p_steps_stepsize = 3

    # TODO: choose range for threshold variation (you can choose it according tho the histogram output)
    v_steps_list = [x / 100.0 for x in range(0, 6 + v_steps_stepsize, v_steps_stepsize)]
    p_steps_list = [x / 100.0 for x in range(14, 50 + p_steps_stepsize, p_steps_stepsize)] #10 to 40

    print('v_steps :', v_steps_list)
    print('p_steps :', p_steps_list)

    recognition_rate_per_model_with_verifier_info = []
    recognition_rate_with_verifier_info = []

    # for progress calculations
    n_runs = len(v_steps_list) * len(p_steps_list)
    re = RuntimeEstimator(n_runs)

    # wait = input('\nThis optimization will probably take %s min (~10s per run)!...\n'
    #             'HIT <ENTER> TO CONTINUE...' % (n_runs * 10 / 60))

    for v_step in v_steps_list:
        for p_step in p_steps_list:

            re.run_start()

            # mark poses as verified
            post_processing.compute_verified_through_mu(data, v_step, p_step)
            # add the 'weight' in the 'verified_poses' dict as 'verificationWeight' to the 'poses' dict.
            post_processing.add_verifiedWeight_to_poses(data)

            # extract data
            iDict, basic_data = \
                data_extraction.extract_data(data,
                                             [['poses']],
                                             [['dynamicParameters', '/verifier',
                                               'searchradius'],
                                              ['dynamicParameters', '/verifier',
                                               'angleDiffThresh_in_pi'],
                                              ['dynamicParameters', '/verifier',
                                               'supportthreshold'],
                                              ['dynamicParameters', '/verifier',
                                               'penaltythreshold'],
                                              ['datasetName']])
            # print(basic_data)
            # print(iDict)

            # loop over searchradii and angle thresholds to filter the run-data according to its search radius and angle threshold
            # empty filtered data list
            filtered_data = []
            # find the runs that have the best searchradius and angle threshold
            for run in basic_data:
                if run[iDict['searchradius']] == best_setup_dict['searchradius'] and \
                        run[iDict['angleDiffThresh_in_pi']] == best_setup_dict[
                    'angleDiffThresh_in_pi']:

                    # additionally filter all cylinder models:
                    if run[iDict['model']] != 'cylinder':
                        # add data of this run to filtered data
                        # print('run =', run)
                        filtered_data.append(run)
                        # print('filtered_data =', filtered_data)
                        # wait = input("hit enter to continue!")

            # recognition rate for each model of the datasets:
            list_of_tuples = data_extraction.nest_and_process_by_values(filtered_data,
                                                                        [iDict['datasetName'],
                                                                         iDict['model'],
                                                                         iDict['poses']],
                                                                        data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)
            # convert to list, add v_step and p_step info, and convert back to tuple
            list_of_tuples = list(list_of_tuples[0])
            list_of_tuples.append(v_step)
            list_of_tuples.append(p_step)
            tuple_of_tuples = tuple(list_of_tuples)
            # print(tuple_of_tuples)
            recognition_rate_per_model_with_verifier_info.append(tuple_of_tuples)

            # recognition rate for complete dataset, not for each model of the datasets:
            list_of_tuples = data_extraction.nest_and_process_by_values(filtered_data,
                                                                        [iDict['datasetName'],
                                                                         iDict['poses']],
                                                                        data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)
            list_of_tuples = list(list_of_tuples[0])
            list_of_tuples.append(v_step)
            list_of_tuples.append(p_step)
            tuple_of_tuples = tuple(list_of_tuples)
            recognition_rate_with_verifier_info.append(tuple_of_tuples)
            # print('v_step =', v_step, 'p_step =', p_step)
            # print('\n**************** per model ***************\n',recognition_rate_per_model_with_verifier_info)
            # print('\n**************** all models ***************\n',recognition_rate_with_verifier_info)
            # wait = input("hit enter to continue!")

            re.run_end()

    best_setup_dict[
        'supportthreshold'] = 0.0  # TODO: change this if data was not verified with those values!
    best_setup_dict[
        'penaltythreshold'] = 1.0  # TODO: change this if data was not verified with those values!

    print('\nThe best old setups were:', best_setup_dict)
    # print('new recognition rates:\n',recognition_rate_with_verifier_info)
    # wait = input("hit enter to continue!")
    # print('new recognition rates per model:\n',recognition_rate_per_model_with_verifier_info)
    # wait = input("hit enter to continue!")

    # get combination of supportthreshold and penatlythreshold with max recognition rate
    for setup in recognition_rate_with_verifier_info:  # loop over [('dataset_name', recog-rate, v_step, p_step), ... ]
        if setup[1] >= best_setup_dict['recognition_rate'] and \
                setup[2] >= best_setup_dict['supportthreshold'] and \
                setup[3] <= best_setup_dict['penaltythreshold']:
            best_setup_dict['recognition_rate'] = setup[1]
            best_setup_dict['supportthreshold'] = setup[2]
            best_setup_dict['penaltythreshold'] = setup[3]

    print('The best new setups are: ', best_setup_dict)

    plot_data = [additional_plot_data[0]]
    plot_data.append(RESULT_FROM_PUBLISH_N_BEST_POSES)
    plot_data.append(additional_plot_data[1])
    for setup in recognition_rate_per_model_with_verifier_info:  # loop over [('dataset_name', recog-rate, v_step, p_step), ... ]
        if setup[2] == best_setup_dict['supportthreshold'] and \
                setup[3] == best_setup_dict['penaltythreshold']:
            plot_data.append(
                ("\\textsc{Hinterstoisser}'s extensions with $r_\mathrm{s}=$%s and "
                 "$\\alpha=%s\pi$,\n$\mathcal{V}=$%s, $\mathcal{P}=$%s, without conflict-detection"
                 % (best_setup_dict["searchradius"], round(best_setup_dict['angleDiffThresh_in_pi'],3),
                    best_setup_dict['supportthreshold'], best_setup_dict['penaltythreshold']),
                 setup[1]))  # recog-rate
    plot_data = tuple(plot_data)

    print('\nBest recognition rates (one instance detection) for each model are now:')
    for x in plot_data:
        print(str(x))

    bar_by_model(plot_data, ax, 'datasets', show_legend=show_legend)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')

    return best_setup_dict


def histogram_of_mu_v_and_mu_p_for_voting_ball(experiment_data, dataset_name, best_setup,
        votingball_idx, ax, title,
        key_figure='mu_v', range_=[0, 1], show_legend=True, only_correct_poses=True):
    # define the dataset to use
    if dataset_name not in best_setup.keys():
        print("unknown dataset '%s'" % (dataset_name))
        return

    print(
        "Creating µ_v and µ_p histograms for %s-Dataset for the best searchradius %s and angleDiffThresh_in_pi %s" % (
            dataset_name, best_setup[dataset_name]['searchradius'],
            best_setup[dataset_name]['angleDiffThresh_in_pi']))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi'],
                                      ['datasetName']])

    relevant_data = [entry for entry in base_data if entry[iDict['datasetName']] == dataset_name and
                     entry[iDict['searchradius']] == best_setup[dataset_name]['searchradius'] and
                     entry[iDict['angleDiffThresh_in_pi']] == best_setup[dataset_name][
                         'angleDiffThresh_in_pi']
                     ]
    # print (relevant_data)
    # wait = input('HIT ENTER TO CONTINUE...')

    # counter array to count how many times a pose at the i'th rank is correct
    histogram_resolution = 100
    histogram_bars_height = [0] * histogram_resolution

    # TODO: delete after debugging:
    # correct_counter = 0
    # incorrect_counter = 0

    # do the counting
    for histogram_bar_idx in xrange(histogram_resolution):
        for entry in relevant_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for pose in poses_of_votingball:
                if pose['mu'][key_figure] >= range_[0] + (
                        histogram_bar_idx * (range_[1] - range_[0]) / histogram_resolution) and \
                        pose['mu'][key_figure] < range_[0] + (
                        (histogram_bar_idx + 1) * (range_[1] - range_[0]) / histogram_resolution):
                    # print('pose that fulfills %s-requirements: >= %s and < %s:' %(key_figure,
                    #                                                               range_[0] + (histogram_bar_idx * (range_[1]-range_[0])/histogram_resolution),
                    #                                                               range_[0] + ( (histogram_bar_idx+1) * (range_[1]-range_[0]) / histogram_resolution)),
                    #                                                             pose)
                    # wait = input('HIT ENTER TO CONTINUE...')
                    if only_correct_poses:  # count only for correct poses
                        if pose['correct']:
                            # print("pose->correct: %s, %s : %s" % (True, key_figure, pose['mu'][key_figure]))
                            histogram_bars_height[histogram_bar_idx] += 1
                            # correct_counter += 1
                    else:  # count only for incorrect poses
                        if not pose['correct']:
                            histogram_bars_height[histogram_bar_idx] += 1
                            # incorrect_counter += 1

    ax.grid()
    # print (histogram_bars_height)
    # print (correct_counter)
    # print (incorrect_counter)    # correct and incorrect counters have to sum up to the total number of published poses

    for i, number in enumerate(histogram_bars_height):
        if only_correct_poses:
            ax.bar(i, number, color='#33691e')  # / len(relevant_data))
        else:
            ax.bar(i, number, color='#c62828')  # / len(relevant_data))
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$\%s$ in percent" % (key_figure))
    ax.set_ylabel("number of occurances")
    ax.set_xlim(range_[0], range_[1] * histogram_resolution)
    ax.set_ylim(0, 30)
    ax.set_xlim(0, 60)
    ax.set_title(title)

    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def histogram_of_mu_v_and_mu_p(experiment_data, best_setup, ax, title, key_figure='mu_v',
                               range_=[0, 1], only_correct_poses=True,
                               max_count=0):
    """
    plot a histogram
    choose between plotting the penalty- or support-threshold
    and the correct or false poses
    :param experiment_data: ExperimentDataStructure()-object
    :param best_setup: dictionary containing best searchradius and best angleDiffThresh_in_pi
    :param ax: axis of figure
    :param title: title of the histogram, set empty "" if no title is wanted
    :param key_figure: choose between string 'mu_v' and 'mu_p'
    :param range_: range to plot the key figure, defaults to [0,1]
    :type range_: list with upper and lower bounds
    :param only_correct_poses:
    :type only_correct_poses: bool, defaults to True
    :param max_count: highest bar of a previous histogram. used to realize the same y-axis-limits
    :return: max_count
    """

    print(
        "Creating µ_v and µ_p histograms for the best searchradius %s and angleDiffThresh_in_pi %s" % (
            best_setup['searchradius'],
            best_setup['angleDiffThresh_in_pi']))

    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi']])

    relevant_data = [entry for entry in base_data if
                     entry[iDict['searchradius']] == best_setup['searchradius'] and
                     entry[iDict['angleDiffThresh_in_pi']] == best_setup['angleDiffThresh_in_pi']]


    # TODO: set histogram-resolution ( = number of bars )
    histogram_resolution = 100

    histogram_dict = {}
    for bar_idx in range(histogram_resolution):
        histogram_dict[bar_idx] = {'bar_range':[0,0], 'count':0}
        lower_range_bounding = range_[0] + bar_idx * (range_[1] - range_[0]) / histogram_resolution
        upper_range_bounding = range_[0] + (bar_idx + 1) * (range_[1] - range_[0]) / histogram_resolution
        histogram_dict[bar_idx]['bar_range'] = [lower_range_bounding, upper_range_bounding]
    #print("empty histogram:\n",histogram_dict)

    # do the counting
    for bar_idx, histogram_bar in histogram_dict.items():
        # loop over runs
        for entry in relevant_data:
            # extract the poses and count
            for i in entry[iDict['poses']]:
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['mu'][key_figure] >= histogram_bar['bar_range'][0] and \
                        pose['mu'][key_figure] < histogram_bar['bar_range'][1]:
                    if only_correct_poses:  # count only for correct poses
                        if pose['correct']:
                            # print("pose->correct: %s, %s : %s" % (True, key_figure, pose['mu'][key_figure]))
                            histogram_bar['count'] += 1
                    else:  # count only for incorrect poses
                        if not pose['correct']:
                            histogram_bar['count'] += 1

    #print("filled histogram:\n",histogram_dict)

    # check if max count is higher than the max count of the partner-histogram
    for histogram_bar in histogram_dict.values():
        if histogram_bar['count'] > max_count:
            max_count = histogram_bar['count']

    # plot histogram
    for bar_idx, histogram_bar in histogram_dict.items():
        if only_correct_poses:
            ax.bar(histogram_bar['bar_range'][0]*100, histogram_bar['count'], color='#33691e',
                   width=(histogram_bar['bar_range'][1]-histogram_bar['bar_range'][0])*100)
        else:
            ax.bar(histogram_bar['bar_range'][0]*100, histogram_bar['count'], color='#c62828',
                   width=(histogram_bar['bar_range'][1] - histogram_bar['bar_range'][0]) * 100)
    # do some plotting stuff
    if key_figure == 'mu_v':
        ax.set_xlabel("$\mu_\mathcal{V}$ in percent")
    if key_figure == 'mu_p':
        ax.set_xlabel("$\mu_\mathcal{P}$ in percent")
    ax.set_ylabel("number of occurances")
    ax.set_ylim(0, int(math.ceil(max_count / 100.0)) * 100) #set y-max to max_count and round up to the next 100
    ax.set_xlim(range_[0]*100, range_[1]*100)
    ax.set_title(title)
    ax.grid()

    return max_count


def scatter_plot(data, best_setup, ax, show_legend = False):
    """
    make a scatter plot (x,y,color) of the poses with the highest verificationWeight.
    x = mu_v
    y = mu_p
    color of data-point in diagram = correct (green) or false (red)

    Note: Gives a better idea than the histogram_histogram_of_mu_v_and_mu_p plots does

    :param data: ExperimentDataStructure()-object
    :param best_setup: dict that contains best searchradius and best angleDiffThresh_in_pi
    :param ax: axis of figure (subplot)
    :return: None
    """
    print(
        "Creating µ_v and µ_p scatter plot for the best searchradius %s and angleDiffThresh_in_pi %s" % (
            best_setup['searchradius'],
            best_setup['angleDiffThresh_in_pi']))

    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['poses']],
                                     [['dynamicParameters', '/verifier', 'searchradius'],
                                      ['dynamicParameters', '/verifier', 'angleDiffThresh_in_pi']])

    relevant_data = [entry for entry in base_data if
                     entry[iDict['searchradius']] == best_setup['searchradius'] and
                     entry[iDict['angleDiffThresh_in_pi']] == best_setup['angleDiffThresh_in_pi']]

    x_correct = []
    x_false = []
    y_correct = []
    y_false = []
    for run in relevant_data:
        max_idx = -1
        max_weight = -1 # because not-verified poses have verifictationWeight = 0
        for i in run[iDict['poses']]:
            pose = run[iDict['poses']][i]  # get i'th pose
            # print('pose',pose_idx, '=',pose)
            if pose['verificationWeight'] > max_weight:
                max_idx = i
                max_weight = pose['verificationWeight']
                # print('max pose =',max_idx,'=', poses_dict[max_idx])
        if max_idx > -1 and max_weight > 0 and run[iDict['poses']][max_idx]['correct']:
            x_correct.append(run[iDict['poses']][max_idx]['mu']['mu_v'] * 100)
            y_correct.append(run[iDict['poses']][max_idx]['mu']['mu_p'] * 100)
        elif max_idx > -1 and max_weight > 0 and not run[iDict['poses']][max_idx]['correct']:
            x_false.append(run[iDict['poses']][max_idx]['mu']['mu_v'] * 100)
            y_false.append(run[iDict['poses']][max_idx]['mu']['mu_p'] * 100)

    #print("mu_v:\n", x)
    #print("mu_p:\n", y)
    #print("correct:\n", correct)

    ax.scatter(x=x_correct, y=y_correct, c='#33691e', s=2, marker=',', linewidths=0, label='correct')
    ax.scatter(x=x_false, y=y_false, c='#c62828', s=2, marker=',', linewidths=0, label='false')
    ax.set_xlabel("$\mu_\mathcal{V}$ in percent")
    ax.set_ylabel("$\mu_\mathcal{P}$ in percent")
    ax.set_xlim(0,40) #20
    ax.set_ylim(0,60) #70
    ax.grid()
    if show_legend:
        ax.legend(loc='upper right')


if __name__ == '__main__':

    # load data and do post-processing
    print('Loading Experiment-data...')
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_correctness_from_verifier(data, max_translation_error_relative,
                                                      max_rotation_error)
    # post_processing.compute_verified(data)

    # for comparison reasons:
    print('Loading Baseline-data...')
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)
    post_processing.compute_pose_errors(baseline_data)
    post_processing.compute_recognition(baseline_data, max_translation_error_relative,
                                        max_rotation_error)

    # mark which run belongs to which matching mode
    add_matching_mode(baseline_data)

    # prepare for plotting
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    # change 3D-perspective
    proj3d.persp_transformation = orthogonal_proj

    print("this may take a while...")

    if use_Krones_evaluation:
        print("generating one graphic per angleDiffThresh")
        figA = plot_precision_recall_overV_constAngleDiffThresh(data, datasetNames,
                                                                angleDiffThresh_list=[1],
                                                                show_legend=True)
        for key, entry in figA.items():
            for fig in entry.values():
                fig.tight_layout()
        if SAVE_PLOTS:
            print("saving plotted figures...")
            plt_dir = os.path.dirname(FILE_PATH)
            for a, fig_by_ds in figA.items():
                for dataset, figure in fig_by_ds.items():
                    plotting.save_figure(figure,
                                         plt_dir,
                                         '%s_pUr_V_angle_%s' % (dataset, int(a * 180)))

                    plt.close(figure)

        figA = plot_precision_recall_overP_constAngleDiffThresh(data, datasetNames,
                                                                angleDiffThresh_list=[1],
                                                                show_legend=True)
        for key, entry in figA.items():
            for fig in entry.values():
                fig.tight_layout()
        if SAVE_PLOTS:
            print("saving plotted figures...")
            plt_dir = os.path.dirname(FILE_PATH)
            for a, fig_by_ds in figA.items():
                for dataset, figure in fig_by_ds.items():
                    plotting.save_figure(figure,
                                         plt_dir,
                                         '%s_pUr_P_angle_%s' % (dataset, int(a * 180)))

                    plt.close(figure)

        selected_searchradius = {datasetNames[0]: 1.0,
                                 # datasetNames[1]: 0.5,
                                 # datasetNames[2]: 1.0,
                                 'all': 1.0}  # selections made because of constant anglediffthresh

        print("generating one graphic per searchradius")
        figA, e_dictV = plot_precision_recall_overV_constSearchradius(data, datasetNames,
                                                                      searchradius_list=[0.5, 1],
                                                                      show_legend=True)
        for key, entry in figA.items():
            for fig in entry.values():
                fig.tight_layout()
        if SAVE_PLOTS:
            print("saving plotted figures...")
            plt_dir = os.path.dirname(FILE_PATH)

            for r, fig_by_ds in figA.items():
                for dataset, figure in fig_by_ds.items():
                    if selected_searchradius[dataset] == r:
                        r_as_string = '%.1f' % r
                        r_for_saving = r_as_string.replace('.', '-')
                        plotting.save_figure(figure,
                                             plt_dir,
                                             '%s_pUr_V_radius_%s' % (dataset, r_for_saving))

                    plt.close(figure)

        figA, e_dictP = plot_precision_recall_overP_constSearchradius(data, datasetNames,
                                                                      searchradius_list=[0.5, 1],
                                                                      show_legend=True)
        for key, entry in figA.items():
            for fig in entry.values():
                fig.tight_layout()
        if SAVE_PLOTS:
            print("saving plotted figures...")
            plt_dir = os.path.dirname(FILE_PATH)

            for r, fig_by_ds in figA.items():
                for dataset, figure in fig_by_ds.items():
                    if selected_searchradius[dataset] == r:
                        r_as_string = '%.1f' % r
                        r_for_saving = r_as_string.replace('.', '-')
                        plotting.save_figure(figure,
                                             plt_dir,
                                             '%s_pUr_P_radius_%s' % (dataset, r_for_saving))

                    plt.close(figure)

        selected_angleDiffThresh = {datasetNames[0]: 1 / 4,
                                    # datasetNames[1]: 1/10,
                                    # datasetNames[2]: 1/10,
                                    'all': 1 / 15}  # selections made because of constant searchradius

        extendeddataset_dict = {name: [name] for name in datasetNames}
        extendeddataset_dict['all'] = datasetNames

        for dsname, dslist in extendeddataset_dict.items():
            listOfVEs = e_dictV[selected_searchradius[dsname]][dsname][
                selected_angleDiffThresh[dsname]]
            sortedVEs = sorted(listOfVEs, key=lambda x: (x[1], x[0]))
            bestV, bestVsE = sortedVEs[0]
            listOfPEs = e_dictP[selected_searchradius[dsname]][dsname][
                selected_angleDiffThresh[dsname]]
            sortedPEs = sorted(listOfPEs, key=lambda x: (x[1], -x[0]))
            bestP, bestPsE = sortedPEs[0]

            print('For the selected searchradius %.1f and angleDiffThresh 1/%d of Dataset \'%s\'\n' \
                  'the best e for v is %.2f (v=%.2f) and the best e for p is %.2f (p=%.2f)' \
                  % (selected_searchradius[dsname], 1 / selected_angleDiffThresh[dsname], dsname,
                     bestVsE, bestV, bestPsE, bestP))
            #
            # for dsname, dslist in extendeddataset_dict.items():
            #
            #     fig, axes = plt.subplots(1,2, figsize=plotting.cm2inch(15, 7))
            #     plot_relevant_model_points_over_searchradius(data, fig.axes[0], dslist)
            #     plot_mu_time_over_searchradius(data, fig.axes[1], dslist)
            #
            #     if SAVE_PLOTS:
            #         plt_dir = os.path.dirname(FILE_PATH)
            #         plotting.save_figure(fig, plt_dir, 'mu_times_%s' %dsname)

            if (dsname != 'all'):
                print(
                    "\n************** Best recognition rate (one instance with highest verificationWeight) with Efficiency as optimization-criterion ( = Krone's approach) **************")
                # TODO: plotting recognition rate for models of dataset, using the identified best parameters of Krone:
                # mark poses as verified: TODO: attention! using the same parameters for all datasets:
                print(
                    'Used best parameters are: searchradius = %s, angleDiffTresh = %s, supportthreshold = %s, penaltythreshold = %s' % (
                    selected_searchradius[dsname], selected_angleDiffThresh[dsname], bestV, bestP))

                # calculate the recognition rate of Krone's best r_s and \alpha values:
                fig0, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
                plot_max_recognitionRate_for_Krones_parameter_values(data,
                                                                     selected_searchradius[dsname],
                                                                     selected_angleDiffThresh[
                                                                         dsname], fig0.axes[0],
                                                                     bestV=0.0, bestP=1.0,
                                                                     show_legend=True)

                # now apply the best V and P values:
                post_processing.compute_verified_through_mu(data, bestV, bestP)
                # this here (true positive, etc. rates) is not really necessary, we could skip it:
                post_processing.compute_verification(data)

                # calculate the recognition rate of Krone's best configuration (r_s, \alpha, V, P)
                plot_max_recognitionRate_for_Krones_parameter_values(data,
                                                                     selected_searchradius[dsname],
                                                                     selected_angleDiffThresh[
                                                                         dsname], fig0.axes[1],
                                                                     bestV, bestP,
                                                                     show_legend=True)
                fig0.tight_layout()
                # save plot
                plt_dir = os.path.dirname(FILE_PATH)
                plotting.save_figure(fig0, plt_dir,
                                     '%s_Krones_best_recognition_rate_per_model' % (dsname))

    print(
        "\n************** Best recognition rate (for one instance with highest verificationWeight) with Precision as optimization-criterion ( = Ziegler's approach) **************")
    print(
        "--> Finding best values for searchradius and angleDiffThresh_in_pi, such that recognition-rate is at its maximum",
        "\n--> Verfier does not sort out pose-hypotheses, it just weights them!")

    # mark every pose as verified, i.e. set min_mu_v = 0  and max_mu_p = 1:
    post_processing.compute_verified_through_mu(data, 0.0, 1.0)
    # add the 'weight' in the 'verified_poses' dict as 'verificationWeight' to the 'poses' dict.
    post_processing.add_verifiedWeight_to_poses(data)

    # 1.a) get best searchRadius and best angleDiffTresh_in_pi for one-instance-detection:
    fig1, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(13.5, 5.8))
    fig1.subplots_adjust(right=0.4)
    # for figure containing subplots 3D recognition rate ( 1.a) ) and scatter plot ( 2.b) )
    fig_scatter_and_3D_rr = plt.figure(figsize=plotting.cm2inch(13.5, 6))
    ax_3D = fig_scatter_and_3D_rr.add_subplot(1, 2, 2, projection='3d')
    results = find_searchRadius_angleDiffThresh_combination_with_max_recognitionRate(data,
                                                                                     baseline_data,
                                                                                     fig1.axes[0],
                                                                                     ax_3D,
                                                                                     show_legend=True)
    fig1.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                        title='matching modes', bbox_transform=fig1.transFigure)
    fig1.tight_layout(rect=[0, 0, 0.6, 1])

    # 1.b) get recognition rate of two-instance detection too,
    #      and plot it together with one-instance-detection:
    fig5, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5))
    fig5.subplots_adjust(bottom=0.2)
    find_searchRadius_angleDiffThresh_combination_with_max_recognitionRate(data, baseline_data,
                                                                           fig5.axes[0],
                                                                           ax_3D = None,
                                                                           show_legend=False)
    results_two_instances = plot_recognitionRate_two_instances(data, baseline_data, results['best_setup_dict'],
                                                               fig5.axes[1], show_legend=False)
    fig5.axes[1].legend(bbox_to_anchor=(0.5, 0.00), bbox_transform=fig5.transFigure,
                        loc='lower center', borderaxespad=0., title='matching modes')
    fig5.tight_layout(rect=[0, 0.2, 1, 1])

    plt_dir = os.path.dirname(FILE_PATH)
    plotting.save_figure(fig1, plt_dir,
                         '%s_Zieglers_best_recognition_rate_per_model_no_support_or_penalty_threshold' % (
                             'geometric primitives'))
    plotting.save_figure(fig5, plt_dir,
                         '%s_Zieglers_best_recognition_rate_two_instances_per_model_no_support_or_penalty_threshold'
                         % ('geometric primitives'))

    # 2.a) plot Histogram of mu_v and mu_p for defining their range and stepsize for 3.
    print(
        "\n--> Finding best values for supporthreshold and penaltythreshold, such that recognition-rate is at its maximum")
    print("--> Plotting histograms of µ_v and µ_p.")
    fig2, axes = plt.subplots(2, 2, figsize=plotting.cm2inch(15, 12), sharey=False)
    max_count = histogram_of_mu_v_and_mu_p(data, results_two_instances['best_setup_dict'], fig2.axes[0],
                               "histogram of $\mu_\mathcal{V}$ for correct poses\nboth voting balls",
                               key_figure='mu_v', only_correct_poses=True, range_=[0,1])
    histogram_of_mu_v_and_mu_p(data, results_two_instances['best_setup_dict'], fig2.axes[1],
                               "histogram of $\mu_\mathcal{P}$ for correct poses\nboth voting balls",
                               max_count=max_count,
                               key_figure='mu_p', only_correct_poses=True, range_=[0,1])
    max_count = histogram_of_mu_v_and_mu_p(data, results_two_instances['best_setup_dict'], fig2.axes[2],
                               "histogram of $\mu_\mathcal{V}$ for incorrect poses\nboth voting balls",
                               key_figure='mu_v', only_correct_poses=False, range_=[0,1])
    histogram_of_mu_v_and_mu_p(data, results_two_instances['best_setup_dict'], fig2.axes[3],
                               "histogram of $\mu_\mathcal{P}$ for incorrect poses\nboth voting balls",
                               max_count=max_count,
                               key_figure='mu_p', only_correct_poses=False, range_=[0,1])
    fig2.tight_layout()

    plt_dir = os.path.dirname(FILE_PATH)
    plotting.save_figure(fig2, plt_dir,
                         '%s_histogram_of_mu_v_and_mu_p_for_combined_voting_balls' % (
                             'geometric primitives'))

    # 2.b) make a scatter plot to see how correct and false poses mix with respect
    #      to their mu_v and mu_p values
    #      Also suitable for defining their range and stepsize for 3.
    print("--> Plotting scatter plot with µ_v and µ_p of poses with highest verifiacationWeight.")
    fig4, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5, 6))
    scatter_plot(data, results_two_instances['best_setup_dict'], fig4.axes[0], show_legend=True)
    fig4.tight_layout()

    plotting.save_figure(fig4, plt_dir,'scatter_plot_of_mu_v_and_mu_p_for_combined_voting_balls')

    # for figure containing subplots 3D recograte (1.) and scatter plot ( 2.b) )
    ax_2D = fig_scatter_and_3D_rr.add_subplot(1, 2, 1)
    scatter_plot(data, results['best_setup_dict'], ax_2D, show_legend=True)
    fig_scatter_and_3D_rr.tight_layout()
    plt_dir = os.path.dirname(FILE_PATH)
    plotting.save_figure(fig_scatter_and_3D_rr, plt_dir,'scatter_and_recognition_rate_dependency_from_parameters_3D_plot')

    print(
        "\n--> TODO: Choose the range in which the supporththreshold and the penalytthreshold shall be varied, to find max precision.")

    # 3. get best mu_v and best mu_p
    #    you can use the results from the preceding experiment to choose the v_steps and p_steps
    #    you'd have to adjust those v_steps and p_steps in the find_mu_v_and_mu_p_combination_with_max_recognition_rate()
    print(
       "\n--> Verfier DOES sort out pose-hypotheses now! Varying supportthreshold and penaltythreshold "
       "in the following steps:")
    fig3, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.75))
    fig3.subplots_adjust(bottom=7/31)
    find_mu_v_and_mu_p_combination_with_max_recognition_rate(data, results_two_instances['best_setup_dict'],
                                                             fig3.axes[0],
                                                             additional_plot_data=results['plot_data'],
                                                             show_legend=False)
    plot_recognitionRate_two_instances(data, baseline_data, results_two_instances['best_setup_dict'],
                                       fig3.axes[1],
                                       additional_plot_data = results_two_instances['plot_data'],
                                       show_legend=False)
    fig3.axes[1].legend(bbox_to_anchor=(0.5, 0.00), bbox_transform=fig3.transFigure,
                        loc='lower center', borderaxespad=0., title='matching modes', ncol=2)
    fig3.tight_layout(rect=[0, 7/31, 1, 1])

    plt_dir = os.path.dirname(FILE_PATH)
    plotting.save_figure(fig3, plt_dir,
                        '%s_Zieglers_best_recognition_rate_per_model_with_optimited_support_and_penalty_thresholds' % ('geometric primitives'))

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
