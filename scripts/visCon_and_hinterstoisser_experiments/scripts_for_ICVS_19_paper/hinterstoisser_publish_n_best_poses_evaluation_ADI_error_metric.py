#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICVS_2019/Hinterstoisser_methods/hinterstoisser_publish_n_best_poses/hinterstoisser_publish_n_best_poses'
BASELINE_FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist'
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_PLOTS = True
SHOW_PLOTS = False
max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if used_rs and collapsed_points and not used_vb and not used_n_PPFs and not used_hc and\
                    not used_fa and not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_vb and used_n_PPFs and used_hc and used_fa \
                    and voted_for_adj_rot_angl:
                matching_mode = "\\textsc{Hinterstoisser}'s extensions without verifier"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def plot_correct_pose_barchart(experiment_data, datasetName, best_n_range, instances_per_scene, ax,
        votingball_idx, title, show_legend):
    """
    at which position of the 'n_best_poses' are the correct poses. plot the number of poses over position
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" % (
        datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    relevant_data = \
        [entry for entry in base_data if entry[iDict['datasetName']] == datasetName]
    # print (relevant_data)
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        for entry in relevant_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball):
                if idx == posenumber:
                    # pose = poses_of_votingball[idx]
                    if pose['correct']:
                        # print("idx: %s; pose->correct: %s" % (idx, True))
                        list[idx] += 1

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(relevant_data), color='#1a237e')
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def plot_at_least_one_correct_pose_barchart(experiment_data, datasetName, best_n_range,
        instances_per_scene, ax, votingball_idx,
        title, show_legend):
    """
    plots the probability that there is at least one correct pose within the the first n poses
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" % (
            datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    relevant_data = \
        [entry for entry in base_data if entry[iDict['datasetName']] == datasetName]
    # print (relevant_data)
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        # if posenumber != 0:
        #    list[posenumber] = list[posenumber-1]
        for entry in relevant_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball, start=0):
                if idx <= posenumber and pose['correct']:
                    list[posenumber] += 1
                    break

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(relevant_data), color='#33691e') #green
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def plot_at_least_one_false_pose_barchart(experiment_data, datasetName, best_n_range,
        instances_per_scene, ax, votingball_idx,
        title, show_legend):
    """
    plots the probability that there is at least one false pose within the the first n poses
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print(
        "Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" % (
            datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(experiment_data,
                                     [['poses']],
                                     [['datasetName']])

    relevant_data = \
        [entry for entry in base_data if entry[iDict['datasetName']] == datasetName]
    # print (relevant_data)
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range

    # do the counting
    for posenumber in range(best_n_range):
        # if posenumber != 0:
        #    list[posenumber] = list[posenumber-1]
        for entry in relevant_data:  # loop over runs
            # print("entry-->poses: %s" % (len(entry)))

            # extract all the poses that belong to the voting ball we are interested in:
            poses_of_votingball = []
            for i in entry[iDict['poses']]:  # loop over poses of this run
                pose = entry[iDict['poses']][i]  # get i'th pose
                if pose['votingBall'] == votingball_idx:  # check votingball-index of pose
                    poses_of_votingball.append(
                        pose)  # store, if pose belongs to correct voting ball

            # now, we can count
            for idx, pose in enumerate(poses_of_votingball, start=0):
                if idx <= posenumber and not pose['correct']:
                    list[posenumber] += 1
                    break

    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number / len(relevant_data), color='#c62828') #red
        # print("i: %s, number: %s" % (i, number))
    ax.set_xlabel("$i$'th pose-hypothesis")
    # ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0, 1)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc="upper right", title="Pose")


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['k','#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_recog_rates_first_pose(data, baseline_data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    print("\n(baseline performance) Recognition rate (one instance):", recognition_rate_baseline)
    print("(baseline performance) Recognition rates for each model (one instance):", recognition_rate_per_model_baseline)

    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate per model as bar-plot
    recognition_rate_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    recognition_rate_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    print("\nRecognition rate (one instance):", recognition_rate_extensions)
    print("Recognition rate for each model (one instance):", recognition_rate_per_model_extensions)

    # add baseline-data to extension-data
    recognition_rates_per_model = []
    recognition_rates_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rates_per_model.append(recognition_rate_per_model_extensions[0])

    bar_by_model(recognition_rates_per_model, ax, 'matching mode', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')


def plot_recog_rates_two_best_poses(data, baseline_data, ax, show_legend):
    """
    plot the recognition rates per matching mode and model
    @type data: ExperimentDataStructure
    @param data: experiment data for plotting
    @type ax: Axes or None
    @param ax: axis to plot recognition rate for first pose to
    """

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)

    recognition_rate_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    print("\n(baseline performance) Recognition rate (two instances):", recognition_rate_baseline)
    print("(baseline performance) Recognition rates for each model (two instances):", recognition_rate_per_model_baseline)


    # ******************************************************************************************************************
    # extensions - without verifier
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # plot recognition rate per model as bar-plot
    recognition_rate_per_model_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)

    recognition_rate_extensions = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)
    print("\nRecognition rate (two instances):", recognition_rate_extensions)
    print("Recognition rate for each model (two instances):", recognition_rate_per_model_extensions)

    # add baseline-data to extension-data
    recognition_rates_per_model = []
    recognition_rates_per_model.append(recognition_rate_per_model_baseline[0])
    recognition_rates_per_model.append(recognition_rate_per_model_extensions[0])

    bar_by_model(recognition_rates_per_model, ax, 'matching mode', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (two instances)')
    ax.set_ylim(0, 1.0)
    if show_legend:
        ax.legend(loc='upper right')


if __name__ == '__main__':

    # load data and do post-processing
    print("Loading Experiment-Data and Baseline-Experiment-Data")
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)
    baseline_data = ExperimentDataStructure()

    compute_pose_errors_necessary = True
    baseline_compute_pose_errors_necessary = False

    if compute_pose_errors_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors and recognition-values for all runs...')
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_ADI(data, dataset)

        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(data,
                                                                             max_translation_error_relative)
        print('Saving Experiment-data with computed pose-errors...')
        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
    else:
        try:
            data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
        except:
            input = raw_input("You specified that the pose errors were already calculated.\n"
                              "But there is no file named '%s_with_computed_errors_hodan.bin'.\n"
                              "Therefore it is assumed that the file '%s.bin' contains those errors.\n"
                              "Hit <ENTER> to continue..." % (
                                  FILE_PATH_WO_EXTENSION, FILE_PATH_WO_EXTENSION))
            data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

    if baseline_compute_pose_errors_necessary:
        baseline_data.from_pickle(BASELINE_FILE_PATH_WO_EXTENSION + ".bin")
        print('Computing pose errors and recognition-values for all runs of baseline data...')
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_ADI(baseline_data,
                                                                                     dataset)

        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(baseline_data,
                                                                             max_translation_error_relative)
        print('Saving Experiment-data with computed pose-errors of baseline data...')
        baseline_data.to_pickle(BASELINE_FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
    else:
        try:
            baseline_data.from_pickle(
                BASELINE_FILE_PATH_WO_EXTENSION + "_with_computed_errors_ADI.bin")
        except:
            input = raw_input(
                "You specified that the pose errors were already calculated for baseline data.\n"
                "But there is no file named '%s_with_computed_errors_hodan.bin'.\n"
                "Therefore it is assumed that the file '%s.bin' contains those errors.\n"
                "Hit <ENTER> to continue..." % (
                    BASELINE_FILE_PATH_WO_EXTENSION, BASELINE_FILE_PATH_WO_EXTENSION))
            baseline_data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

    # mark which run belongs to which matching mode
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    # prepare for plotting
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    print("plotting bar chart of correct poses")
    fig1, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig1.axes[0].set_ylabel("rate of correct\nhypotheses at $i$'th pose")

    plot_correct_pose_barchart(data, 'geometric_primitives', 20, 2, fig1.axes[0], show_legend=False,
                               title='small voting ball', votingball_idx=1)
    plot_correct_pose_barchart(data, 'geometric_primitives', 20, 2, fig1.axes[1], show_legend=False,
                               title='large voting ball', votingball_idx=2)
    fig1.tight_layout()

    print("plotting bar chart probability of at least one correct pose")
    fig2, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig2.axes[0].set_ylabel("probability of having at\nleast one correct hypothesis\nwithin first $i$ poses")

    plot_at_least_one_correct_pose_barchart(data, 'geometric_primitives', 20, 2, fig2.axes[0],
                                            show_legend=False,
                                            title='small voting ball', votingball_idx=1)
    plot_at_least_one_correct_pose_barchart(data, 'geometric_primitives', 20, 2, fig2.axes[1],
                                            show_legend=False,
                                            title='large voting ball', votingball_idx=2)
    fig2.tight_layout()

    print("plotting bar chart probability of at least one false pose")
    fig4, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    fig4.axes[0].set_ylabel("probability of having at\nleast one false hypothesis\nwithin first $i$ poses")

    plot_at_least_one_false_pose_barchart(data, 'geometric_primitives', 20, 2, fig4.axes[0],
                                          show_legend=False,
                                          title='small voting ball', votingball_idx=1)
    plot_at_least_one_false_pose_barchart(data, 'geometric_primitives', 20, 2, fig4.axes[1],
                                          show_legend=False,
                                          title='large voting ball', votingball_idx=2)
    fig4.tight_layout()

    # plot recognition rates
    fig3, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.25))
    fig3.subplots_adjust(bottom=5/29)
    plot_recog_rates_first_pose(data, baseline_data, fig3.axes[0], show_legend=False)
    plot_recog_rates_two_best_poses(data, baseline_data, fig3.axes[1], show_legend=False)
    fig3.axes[0].legend(bbox_to_anchor=(0.5, 0.00), bbox_transform=fig3.transFigure,
                        loc='lower center', borderaxespad=0., title='matching modes')
    fig3.tight_layout(rect=[0, 5/29, 1, 1])
    # plot_recog_rates_all_poses(data, fig1.axes[1])

    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig1, plt_dir, 'barcharts_correct_poses')
        plotting.save_figure(fig2, plt_dir, 'barcharts_at_least_one_correct_pose')
        plotting.save_figure(fig4, plt_dir, 'barcharts_at_least_one_false_pose')
        plotting.save_figure(fig3, plt_dir,
                             '%s_recognition_rate_per_model' % ('geometric_primitives'))

    # show figures for checking Layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
