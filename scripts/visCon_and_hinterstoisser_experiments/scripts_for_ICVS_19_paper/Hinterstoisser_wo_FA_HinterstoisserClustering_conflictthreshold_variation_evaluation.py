#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone, Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/ICVS_2019/Hinterstoisser_wo_FA_and_HintClustering/conflictthreshold/conflictthreshold.bin'
BASELINE_FILE_PATH = '~/ROS/experiment_data/rotational_symmetry_nyquist/welzl_bounding_sphere/rotational_symmetry_nyquist.bin'
SAVE_PLOTS = True
SHOW_PLOTS = False
precisions_weight = 0.5
use_Krones_evaluation = True

max_translation_error_relative = 0.1
max_rotation_error = pi / 15  # rad

datasetNames = [  # 'MBO', 'household_objects_multi_instance',
    'geometric_primitives']
datasetSynonyms = [  # 'MBO', 'Haushaltsgegenst\\"ande',
    'geometric primitives']
datasetDictionary = {dsName: datasetSynonyms[i] for i, dsName in enumerate(datasetNames)}


# TODO: give the recognition rates from results of searchradius_angleDiffThresh_variation_evaluation:
RESULT_ONE_INSTANCE_DETECTION = [("\\textsc{Hinterstoisser}'s extensions without Flag Array and Clustering without verifier", [('cuboid', 0.75), ('cylinder', 0.75), ('elliptic_prism', 0.9), ('hexagonal_frustum', 0.65), ('pyramid', 0.95)]),
                                 ("with $r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, $\\mathcal{V}=0.0, \\mathcal{P}=1.0$, without conflict-detection",[('cuboid', 0.65), ('cylinder', 0.0), ('elliptic_prism', 0.95),('hexagonal_frustum', 0.70), ('pyramid', 0.95)]),
                                 ("with $r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, $\\mathcal{V}=0.02$, $\\mathcal{P}=0.2$, without conflict-detection",[('cuboid', 0.75), ('cylinder', 0.0), ('elliptic_prism', 1.0),('hexagonal_frustum', 0.60), ('pyramid', 0.90)])]

RESULT_TWO_INSTANCE_DETECTION = [("\\textsc{Hinterstoisser}'s extensions without Flag Array and Clustering without verifier", [('cuboid', 0.45), ('cylinder', 0.4), ('elliptic_prism', 0.7), ('hexagonal_frustum', 0.325), ('pyramid', 0.475)]),
                                 ("with $r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, $\\mathcal{V}=0.0, \\mathcal{P}=1.0$, without conflict-detection", [('cuboid', 0.15), ('cylinder', 0.15), ('elliptic_prism', 0.2), ('hexagonal_frustum', 0.0), ('pyramid', 0.0)]),
                                 ("with $r_\\mathrm{s}=0.5$ and $\\alpha=0.1\\pi$, $\\mathcal{V}=0.02, \\mathcal{P}=0.2$, without conflict-detection", [('cuboid', 0.15), ('cylinder', 0.15), ('elliptic_prism', 0.2), ('hexagonal_frustum', 0.0), ('pyramid', 0.0)])]


def add_matching_mode(data):
    """
    add a note with a name for the matching mode to the experiment data
    @type data: ExperimentDataStructure
    @param data: data to check / modify
    """
    for run_data in data['runData'].values():

        try:
            used_rs = run_data['dynamicParameters']['/matcher']['use_rotational_symmetry']
            collapsed_points = run_data['dynamicParameters']['/matcher']['collapse_symmetric_models']
            d_dist = run_data['dynamicParameters']['/matcher']['d_dist'] # to distinguish between nyquist and non-nyquist experiments in baseline
            used_vb = run_data['dynamicParameters']['/matcher']['use_voting_balls']
            used_n_PPFs = run_data['dynamicParameters']['/matcher']['use_neighbour_PPFs_for_training']
            used_hc = run_data['dynamicParameters']['/matcher']['use_hinterstoisser_clustering']
            used_fa = run_data['dynamicParameters']['/matcher']['flag_array_hash_table_type']
            voted_for_adj_rot_angl = run_data['dynamicParameters']['/matcher']['vote_for_adjacent_rotation_angles']

            matching_mode = None
            if used_rs and collapsed_points and not used_vb and not used_n_PPFs and not used_hc and\
                    not used_fa and not voted_for_adj_rot_angl and d_dist == 0.05:
                matching_mode = 'baseline'
            elif used_rs and collapsed_points and used_vb and used_n_PPFs and used_hc and used_fa \
                    and voted_for_adj_rot_angl:
                matching_mode = "\\textsc{Hinterstoisser}'s extensions\nwithout verifier"
            else:
                matching_mode = 'other_modes'

            try:
                run_data['notes']['matching_mode'] = matching_mode
            except KeyError:
                run_data['notes'] = {'matching_mode': matching_mode}

        except KeyError:
            # matcher parameters not given in this run -> will later be copied from previous
            pass


def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    #colors_six_bars = ['#c62828', '#ff5f52',  # red
    #                   '#33691e', '#629749',  # green
    #                   '#1a237e', '#534bae',  # blue
    #                   '#ffb300', '#ffe54c',  # yellow
    #                   ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']
    colors_six_bars = colors_three_bars # the six'th color is not used here, hence it's non-definition

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def find_best_conflict_threshold_by_maximizing_recognition_rate(data, baseline_data, ax_dataset_one,
                                                                ax_dataset_two, ax_models,
                                                                show_legend=True):

    iDict, base_data = data_extraction.extract_data(data,
                                                    [['poses']],
                                                    [["dynamicParameters", "/verifier",
                                                      "conflictthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "supportthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "penaltythreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "angleDiffThresh_in_pi"],
                                                     ["dynamicParameters", "/verifier",
                                                      "searchradius"]])

    # calculate recognition rate for each model of the datasets:
    # highest verifierWeight-ed pose and the second highest verifierWeight-ed pose need to be correct and verifierd.
    # furthermore they may not belong to the same ground-thruth-pose.
    recognition_rate_per_model_with_verifier_info = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_verifier_info = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_two_best_poses_that_are_correct_and_verified)

    # NOTE: for the following rates it is always required, that the highest verifierWeight-ed pose is correct and verified!

    # false positives part 1: counts how often the highest verifierWeight-ed pose and the second highest share the same ground truth
    # this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    false_positives_rate_part_one = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_of_false_positive_second_best_verified_pose_part_one)

    # false positives part 2: counts how often the second highest verifierWeight-ed pose is false but got verified
    false_positives_rate_part_two = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_of_false_positive_second_best_verified_pose_part_two)

    # false-positives part 1+2:
    # counts how often the second highest verifierWeight-ed pose is falsely verified. This includes the times
    # that the second highest verifierWeight-ed pose belongs to the same gt-pose as the highest verifierWeight-ed pose
    # = total false positives
    false_positives_rate = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_false_positive_second_best_verified_pose)

    # counts how often the second highest verifierWeight-ed pose is falsely not verified
    false_negatives_rate = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_false_negative_second_best_verified_pose)

    # true negatives part 1:
    # counts how often the correct second highest verifierWeight-ed pose is not verified, because it has the same gt as the
    # first highest verifierWeight-ed pose
    true_negatives_rate_part_one = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_true_negative_second_best_verified_pose_part_one)

    # true negatives part 2:
    # the 2'nd highest verifierWeight-ed pose is incorrect and did not get verified
    true_negatives_rate_part_two = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_true_negative_second_best_verified_pose_part_two)

    # true negatives part 1+2
    # = total true negatives
    true_negatives_rate = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['searchradius'],
                                                    iDict['angleDiffThresh_in_pi'],
                                                    iDict['supportthreshold'],
                                                    iDict['penaltythreshold'],
                                                    iDict['conflictthreshold'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_true_negative_second_best_verified_pose)

    # plot settings
    mpl.rcParams.update(plotting.krone_thesis_default_settings)
    colors_rr = ['#c62828','#33691e','#ffb300','#1a237e']
    #['#d50000', '#1b5e20', '#1a237e', '#ffb300']  # ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']
    colors_rr_details = ['#c62828','#33691e','#629749','#1a237e', '#534bae']
    #['#d50000', '#1b5e20', '#548658', '#ffb300']  # ['#1f77b4', '#ff7f0e','#ff7f0e', '#2ca02c']
    markers_rr = ['x', '+', 'v', '^']
    markers_rr_details = ['x', '+', 'v', '^','o']
    # ps_rr = plotting.PlotStyle(4, additional_plot_settings={"markerfacecolor": "None"})
    # ps_rr_details = plotting.PlotStyle(4, additional_plot_settings={"markerfacecolor": "None"})


    rr_total = []
    fp_total = []
    fn_total = []
    tn_total = []
    # plot recognition-rate over conflict-threshold for each dataset
    print('\n(Two instance detection) Recognition rates for each conflictthreshold:\n', recognition_rate_with_verifier_info)
    for sr, rr_by_sr in recognition_rate_with_verifier_info:
        for a, rr_by_a in rr_by_sr:
            for sup, rr_by_sup in rr_by_a:
                for pen, rr_by_pen in rr_by_sup:
                    # print('tp_by_a', rr_by_a)
                    # print('(%s, %s)' % (sr, a))
                    rr_list = []
                    for conflict_th, rrs in rr_by_pen:
                        rr_list.append((conflict_th, rrs))
                        rr_total.append((conflict_th, rrs))
                    ax_dataset_one.plot(*zip(*rr_list), label='recognition rate (two instances)',
                                        color=colors_rr[0], marker=markers_rr[0],
                                        markerfacecolor="None", markeredgecolor=colors_rr[0])
    for sr, fp_by_sr in false_positives_rate:
        for a, fp_by_a in fp_by_sr:
            for sup, fp_by_sup in fp_by_a:
                for pen, fp_by_pen in fp_by_sup:
                    fp_list = []
                    for conflict_th, fps in fp_by_pen:
                        fp_list.append((conflict_th, fps))
                        fp_total.append((conflict_th, fps))
                    ax_dataset_one.plot(*zip(*fp_list), label="false positives rate",
                                        color=colors_rr[1], marker=markers_rr[1],
                                        markerfacecolor="None", markeredgecolor=colors_rr[1])
    for sr, fn_by_sr in false_negatives_rate:
        for a, fn_by_a in fn_by_sr:
            for sup, fn_by_sup in fn_by_a:
                for pen, fn_by_pen in fn_by_sup:
                    fn_list = []
                    for conflict_th, fns in fn_by_pen:
                        fn_list.append((conflict_th, fns))
                        fn_total.append((conflict_th, fns))
                    ax_dataset_one.plot(*zip(*fn_list), label="false negatives rate",
                                        color=colors_rr[2], marker=markers_rr[2],
                                        markerfacecolor="None", markeredgecolor=colors_rr[2])
    for sr, tn_by_sr in true_negatives_rate:
        for a, tn_by_a in tn_by_sr:
            for sup, tn_by_sup in tn_by_a:
                for pen, tn_by_pen in tn_by_sup:
                    tn_list = []
                    for conflict_th, tns in tn_by_pen:
                        tn_list.append((conflict_th, tns))
                        tn_total.append((conflict_th, tns))
                    ax_dataset_one.plot(*zip(*tn_list), label="true negatives rate", color=colors_rr[3],
                                        marker=markers_rr[3], markerfacecolor="None",
                                        markeredgecolor=colors_rr[3])
    ax_dataset_one.set_ylim(0, 1)
    ax_dataset_one.set_ylabel(r'rates with correct and verified $1\textsuperscript{st}$ pose')
    ax_dataset_one.set_xlabel('conflict threshold')
    ax_dataset_one.grid()
    ax_dataset_one.legend(loc='upper right')

    # TODO: Attention: The sum_total has to be the same as the RR for one-instance detection!
    sum_total = [[0,0] for x in range(len(rr_total))]
    for keynumber in [rr_total, fp_total, fn_total, tn_total]:
        for idx, data in enumerate(keynumber):
            sum_total[idx][0] = data[0]
            sum_total[idx][1] += data[1]
    print("\nCheck: Does the sum equal the Recognition rate for one-instance detection? For all conflict-thresholds?")
    print("Summed up, RR + fp + fn + tn rates equals to: [conflict-threshold, sum] ", sum_total)

    # plot additional graphs for closer inspection of false positives and true negatives
    for sr, rr_by_sr in recognition_rate_with_verifier_info:
        for a, rr_by_a in rr_by_sr:
            for sup, rr_by_sup in rr_by_a:
                for pen, rr_by_pen in rr_by_sup:
                    # print('tp_by_a', rr_by_a)
                    # print('(%s, %s)' % (sr, a))
                    rr_list = []
                    for conflict_th, rrs in rr_by_pen:
                        rr_list.append((conflict_th, rrs))
                    ax_dataset_two.plot(*zip(*rr_list), label='recognition rate (two instances)',
                                        color=colors_rr_details[0], marker=markers_rr_details[0],
                                        markerfacecolor="None", markeredgecolor=colors_rr_details[0])
    for sr, fp_by_sr in false_positives_rate_part_one:
        for a, fp_by_a in fp_by_sr:
            for sup, fp_by_sup in fp_by_a:
                for pen, fp_by_pen in fp_by_sup:
                    fp_list = []
                    for conflict_th, fps in fp_by_pen:
                        fp_list.append((conflict_th, fps))
                    ax_dataset_two.plot(*zip(*fp_list), label="false positives rate (part one)",
                                        color=colors_rr_details[1], marker=markers_rr_details[1],
                                        markerfacecolor="None", markeredgecolor=colors_rr_details[1])
    for sr, fp_by_sr in false_positives_rate_part_two:
        for a, fp_by_a in fp_by_sr:
            for sup, fp_by_sup in fp_by_a:
                for pen, fp_by_pen in fp_by_sup:
                    fp_list = []
                    for conflict_th, fps in fp_by_pen:
                        fp_list.append((conflict_th, fps))
                    ax_dataset_two.plot(*zip(*fp_list), label="false positives rate (part two)",
                                        color=colors_rr_details[2], marker=markers_rr_details[2],
                                        markerfacecolor="None", markeredgecolor=colors_rr_details[2])
    for sr, tn_by_sr in true_negatives_rate_part_one:
        for a, tn_by_a in tn_by_sr:
            for sup, tn_by_sup in tn_by_a:
                for pen, tn_by_pen in tn_by_sup:
                    tn_list = []
                    for conflict_th, tns in tn_by_pen:
                        tn_list.append((conflict_th, tns))
                    ax_dataset_two.plot(*zip(*tn_list), label="true negatives rate (part one)",
                                        color=colors_rr_details[3], marker=markers_rr_details[3],
                                        markerfacecolor="None", markeredgecolor=colors_rr_details[3])
    for sr, tn_by_sr in true_negatives_rate_part_two:
        for a, tn_by_a in tn_by_sr:
            for sup, tn_by_sup in tn_by_a:
                for pen, tn_by_pen in tn_by_sup:
                    tn_list = []
                    for conflict_th, tns in tn_by_pen:
                        tn_list.append((conflict_th, tns))
                    ax_dataset_two.plot(*zip(*tn_list), label="true negatives rate (part two)",
                                        color=colors_rr_details[4], marker=markers_rr_details[4],
                                        markerfacecolor="None", markeredgecolor=colors_rr_details[4])

    ax_dataset_two.set_ylim(0, 1)
    #ax_dataset_two.set_ylabel(r"rates with correct and verified $1\textsuperscript{st}$ pose")
    ax_dataset_two.set_xlabel('conflict threshold')
    ax_dataset_two.grid()
    ax_dataset_two.legend(loc='upper right')

    # dict for storing the best setups
    best_setup_dict = {'searchradius': -1, 'angleDiffThresh_in_pi': -1, 'recognition_rate': -1,
                       'supportthreshold': -1, 'penaltythreshold': -1, 'conflictthreshold': -1}

    # get conflictthreshold with max recognition rate
    for searchradius, rr_of_searchradius in recognition_rate_with_verifier_info:
        for angleDiffThresh, rr_of_anglediffThresh in rr_of_searchradius:
            for supportthreshold, rr_by_supportthresh in rr_of_anglediffThresh:
                for penaltythreshold, rr_by_penaltythresh in rr_by_supportthresh:
                    for conflictthreshold, rr_of_conflictthreshold in rr_by_penaltythresh:
                        if rr_of_conflictthreshold >= best_setup_dict['recognition_rate']:
                            best_setup_dict['recognition_rate'] = rr_of_conflictthreshold
                            best_setup_dict['angleDiffThresh_in_pi'] = angleDiffThresh
                            best_setup_dict['searchradius'] = searchradius
                            best_setup_dict['conflictthreshold'] = conflictthreshold
                            best_setup_dict['supportthreshold'] = supportthreshold
                            best_setup_dict['penaltythreshold'] = penaltythreshold

    print('\n(Two instance detection) The best new setups are: ', best_setup_dict)

    # format data to plot
    plot_data = []
    # loop over [('searchradius', [(angleDiff , [(supportthresh), [(penaltythresh), [(conflictthresh, [(cuboid, rr ), (cylinder, rr) , ...] )] )] )] )] )]
    for searchradius, rr_of_searchradius in recognition_rate_per_model_with_verifier_info:
            for angleDiffThresh, rr_of_anglediffThresh in rr_of_searchradius:
                for supportthreshold, rr_by_supportthresh in rr_of_anglediffThresh:
                    for penaltythreshold, rr_by_penaltythresh in rr_by_supportthresh:
                        for conflictthreshold, rr_of_conflictthreshold in rr_by_penaltythresh:
                            if conflictthreshold == best_setup_dict['conflictthreshold']\
                                    and searchradius == best_setup_dict['searchradius']\
                                    and angleDiffThresh == best_setup_dict['angleDiffThresh_in_pi']\
                                    and supportthreshold == best_setup_dict['supportthreshold']\
                                    and penaltythreshold == best_setup_dict['penaltythreshold']:
                                plot_data = rr_of_conflictthreshold

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recog_rate_total)

    # merge baseline-data and experiment-data and data from searchradius_angleDiffThresh_evaluation
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0])  # add "baseline"-mode
    for data in RESULT_TWO_INSTANCE_DETECTION:
        recognition_rate_per_model.append(data)
    recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions with "
                                       "$r_\\mathrm{s}=%s$ and $\\alpha=%s\\pi$,\n"
                                       "$\\mathcal{V}=%s$, $\\mathcal{P}=%s$, with "
                                       "conflict threshold = %s"
                                       %(best_setup_dict['searchradius'],
                                         round(best_setup_dict['angleDiffThresh_in_pi'],3),
                                         best_setup_dict['supportthreshold'],
                                         best_setup_dict['penaltythreshold'],
                                         best_setup_dict['conflictthreshold']),
                                       plot_data))

    print('\n(Two instance detection) Best recognition-rates of the models:')
    for x in recognition_rate_per_model:
        print(x)

    # plot and plot-settings
    bar_by_model(recognition_rate_per_model, ax_models, legend_title='matching modes',
                 show_legend=False, show_top_ticks=False)
    ax_models.set_ylabel('recognition rate (two instances)')
    ax_models.set_ylim(0, 1.0)

    return best_setup_dict


def plot_how_often_best_poses_have_highest_verifictionWeight(data, best_setup_dict, ax_1, ax_2,
        show_legend=True):
    # extrct data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['poses']],
                                                    [["dynamicParameters", "/verifier",
                                                      "conflictthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "angleDiffThresh_in_pi"],
                                                     ["dynamicParameters", "/verifier",
                                                      "searchradius"],
                                                     ['datasetName']])

    # filter data
    filtered_data = []
    for run in base_data:
        for dataset_name, best_setup in best_setup_dict.items():
            if run[iDict['datasetName']] == dataset_name:
                if run[iDict['conflictthreshold']] == 0.08 and \
                        run[iDict['searchradius']] == best_setup['searchradius'] and \
                        run[iDict['angleDiffThresh_in_pi']] == best_setup['angleDiffThresh_in_pi']:
                    filtered_data.append(run)

    # average_rate_of_second_best_pose_having_second_highest_verificationWeight
    average_rate_per_model_1 = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_second_best_pose_having_second_highest_verificationWeight)

    print('\nAverage rate per model that second best pose has second highest verificationWeight =',
          average_rate_per_model_1)
    # plot and plot-settings
    bar_by_model(average_rate_per_model_1, ax_1, 'datasets', show_legend=False, show_top_ticks=False)
    ax_1.set_ylabel(
        'rate that second best\npose (of Kroischke) has\nsecond highest\nverificationWeight (Krone)')
    ax_1.set_ylim(0, 1.2)

    # average_rate_of_best_pose_having_highest_verificationWeight
    average_rate_per_model_2 = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_pose_having_highest_verificationWeight)

    print('Average rate per model that best pose has highest verificationWeight =',
          average_rate_per_model_2)
    # plot and plot-settings
    bar_by_model(average_rate_per_model_2, ax_2, 'datasets', show_legend=False, show_top_ticks=False)
    ax_2.set_ylabel(
        'rate that best pose\n(of Kroischke) has\nhighest verificationWeight\n(Krone)')
    ax_2.set_ylim(0, 1.2)


def plot_recognition_rate_for_best_conflict_threshold_one_instance(data, baseline_data,
        best_setup_dict, ax, show_legend=False):

    # ******************************************************************************************************************
    # baseline
    # ******************************************************************************************************************

    # print('baseline data =', baseline_data)
    # print('data =', data)
    # extract data
    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['recognition']],
                                     [['notes', 'matching_mode']])

    # get recognition rate of baseline algorithm using the pose that got the highest weight from the matcher.
    # for each model
    recognition_rate_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['matching_mode'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # ******************************************************************************************************************
    # extensions - with verifier
    # ******************************************************************************************************************

    # extrct data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['poses']],
                                                    [["dynamicParameters", "/verifier",
                                                      "conflictthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "supportthreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "penaltythreshold"],
                                                     ["dynamicParameters", "/verifier",
                                                      "angleDiffThresh_in_pi"],
                                                     ["dynamicParameters", "/verifier",
                                                      "searchradius"]])

    # filter data
    filtered_data = []
    for run in base_data:
        if run[iDict['conflictthreshold']] == best_setup_dict['conflictthreshold'] \
                and run[iDict['searchradius']] == best_setup_dict['searchradius'] \
                and run[iDict['angleDiffThresh_in_pi']] == best_setup_dict['angleDiffThresh_in_pi'] \
                and run[iDict['supportthreshold']] == best_setup_dict['supportthreshold'] \
                and run[iDict['penaltythreshold']] == best_setup_dict['penaltythreshold']:
            filtered_data.append(run)

    # calculate recognition rate for each model of the datasets:
    recognition_rate_per_model_with_verifier_info_one_instance = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                   [iDict['model'],
                                                    iDict['poses']],
                                                   data_extraction.average_rate_of_best_poses_that_are_correct_and_verified)


    # merge baseline-data and experiment data and data of searchradius_angleDiffThresh_evaluation
    recognition_rate_per_model = []
    recognition_rate_per_model.append(recognition_rate_per_model_baseline[0]) # add "baseline"-mode
    for data in RESULT_ONE_INSTANCE_DETECTION:
        recognition_rate_per_model.append(data)
    recognition_rate_per_model.append(("\\textsc{Hinterstoisser}'s extensions with "\
                                       "$r_\\mathrm{s}=%s$ and $\\alpha=%s\\pi$, "\
                                       "$\\mathcal{V}=%s$, $\\mathcal{P}=%s$, "\
                                       "conflict threshold = %s"\
                                       %(best_setup_dict['searchradius'],
                                         round(best_setup_dict['angleDiffThresh_in_pi'],3),
                                         best_setup_dict['supportthreshold'],
                                         best_setup_dict['penaltythreshold'],
                                         best_setup_dict['conflictthreshold']),
                                        recognition_rate_per_model_with_verifier_info_one_instance))

    print('\n(One instance detection) The recognition rates for each model are:')
    for x in recognition_rate_per_model:
        print(x)

    # plot and plot-settings
    bar_by_model(recognition_rate_per_model, ax, 'matching mode',
                 show_legend=show_legend, show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    if (show_legend):
        ax.legend(loc='upper right')


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    post_processing.compute_correctness_from_verifier(data, max_translation_error_relative,
                                                      max_rotation_error)
    post_processing.compute_verified(
        data)  # adds verificationWeight and verified-field to each pose
    post_processing.compute_verification(data)  # computes tp's, fp's, fn's

    if use_Krones_evaluation:

        mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

        iDict, base_data = \
            data_extraction.extract_data(data,
                                         [['verification']],
                                         [["dynamicParameters", "/verifier", "conflictthreshold"],
                                          ["dynamicParameters", "/verifier",
                                           "angleDiffThresh_in_pi"],
                                          ["dynamicParameters", "/verifier", "searchradius"],
                                          ['datasetName']])

        precisions = \
            data_extraction.nest_and_process_by_values(base_data,
                                                       [iDict['datasetName'],
                                                        iDict['searchradius'],
                                                        iDict['angleDiffThresh_in_pi'],
                                                        iDict['conflictthreshold'],
                                                        iDict['verification']],
                                                       data_extraction.average_precision)

        recalls = \
            data_extraction.nest_and_process_by_values(base_data,
                                                       [iDict['datasetName'],
                                                        iDict['searchradius'],
                                                        iDict['angleDiffThresh_in_pi'],
                                                        iDict['conflictthreshold'],
                                                        iDict['verification']],
                                                       data_extraction.average_recall)

        figures = plt.figure(figsize=plotting.cm2inch(15, 10))
        ps_precision = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
        ps_recall = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
        ps_e = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})
        ps_rr = plotting.PlotStyle(6, additional_plot_settings={"markerfacecolor": "None"})

        precision_dict = {ds:
                              {sr:
                                   {a:
                                        {cth: value for cth, value in precision_by_a}
                                    for a, precision_by_a in precision_by_sr}
                               for sr, precision_by_sr in precision_by_ds}
                          for ds, precision_by_ds in precisions}

        recall_dict = {ds:
                           {sr:
                                {a:
                                     {cth: value for cth, value in recall_by_a}
                                 for a, recall_by_a in recall_by_sr}
                            for sr, recall_by_sr in recall_by_ds}
                       for ds, recall_by_ds in recalls}

        e_dict = {ds:
                      {sr:
                           {a: [] for a, recall_by_a in recall_by_sr}
                       for sr, recall_by_sr in recall_by_ds}
                  for ds, recall_by_ds in recalls}

        print("precision")
        ax = plt.subplot(221)
        for prec_dataset, precisions_by_dataset in precisions:
            for searchradius, precisions_by_searchradius in precisions_by_dataset:
                for angleDiffThresh, precisions_by_angleDiffThresh in precisions_by_searchradius:
                    print("%s (%s, %s)" % (prec_dataset, searchradius, angleDiffThresh))
                    for conflictthreshold, precision in precisions_by_angleDiffThresh:
                        precision_dict[prec_dataset][searchradius][angleDiffThresh][
                            conflictthreshold] = precision
                    ax.plot(*zip(*precisions_by_angleDiffThresh),
                            label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (
                            searchradius, 1 / angleDiffThresh), **ps_precision.next())

        ax.set_xlim(0, 1)
        ax.set_ylim(0, 1)
        ax.grid()
        ax.set_xlabel('conflict threshold')
        ax.set_ylabel('precision')

        print("recall")
        ax_rec = plt.subplot(222)
        ax_e = plt.subplot(223)
        for rec_dataset, recalls_by_dataset in recalls:
            for searchradius, recalls_by_searchradius in recalls_by_dataset:
                for angleDiffThresh, recalls_by_angleDiffThresh in recalls_by_searchradius:
                    print("%s (%s, %s)" % (rec_dataset, searchradius, angleDiffThresh))
                    e_list = []
                    for conflictthreshold, recall in recalls_by_angleDiffThresh:
                        precision = precision_dict[rec_dataset][searchradius][angleDiffThresh][
                            conflictthreshold]
                        recall_dict[rec_dataset][searchradius][angleDiffThresh][
                            conflictthreshold] = recall
                        if precision != 0 and recall != 0:
                            e = 1 - 1 / (precisions_weight / precision + (
                                        1 - precisions_weight) / recall)
                        else:
                            e = 1
                        e_list.append((conflictthreshold, e))
                    sorted_e_list = sorted(e_list, key=lambda x: (x[1], x[0]))
                    print(
                        'the best effectivness is achieved with a conflictthreshold of %.2f (e: %.2f with precision=%.2f and recall=%.2f).' \
                        % (sorted_e_list[0][0], sorted_e_list[0][1],
                           precision_dict[rec_dataset][searchradius][angleDiffThresh][
                               sorted_e_list[0][0]],
                           recall_dict[rec_dataset][searchradius][angleDiffThresh][
                               sorted_e_list[0][0]]))
                    ax_rec.plot(*zip(*recalls_by_angleDiffThresh),
                                label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$' % (
                                searchradius, 1 / angleDiffThresh), **ps_recall.next())
                    ax_e.plot(*zip(*e_list), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$, %s' \
                                                   % (searchradius, 1 / angleDiffThresh,
                                                      datasetDictionary[rec_dataset]),
                              **ps_e.next())

        ax_rec.set_xlim(0, 1)
        ax_rec.set_ylim(0, 1)
        ax_rec.grid()
        ax_rec.set_xlabel('conflict threshold')
        ax_rec.set_ylabel('recall')

        ax_e.set_xlim(0, 1)
        ax_e.set_ylim(0, 0.5)
        ax_e.grid()
        ax_e.set_xlabel('conflict threshold')
        ax_e.set_ylabel('$E$ with $\\beta=%.1f$' % precisions_weight)

        ax_e.legend(bbox_to_anchor=(1.15, 1), loc='upper left', borderaxespad=0., title='Parameter')

        truepositives = \
            data_extraction.nest_and_process_by_values(base_data,
                                                       [iDict['datasetName'],
                                                        iDict['searchradius'],
                                                        iDict['angleDiffThresh_in_pi'],
                                                        iDict['conflictthreshold'],
                                                        iDict['verification']],
                                                       data_extraction.verification_true_positives)

        instances_per_model_per_scene = {  # datasetNames[0]: 1,
            # datasetNames[1]: 2,
            datasetNames[0]: 2}
        scenes_per_dataset = {  # datasetNames[0]: 50,
            # datasetNames[1]: 15,
            datasetNames[0]: 20}
        models_per_scene = {  # datasetNames[0]: 4,
            # datasetNames[1]: 5,
            datasetNames[0]: 5}

        fig_rr, ax = plt.subplots(1, 1, figsize=plotting.cm2inch(15, 10))

        print('true_positives =', truepositives)
        for dataset, tp_by_dataset in truepositives:
            print('dataset =', dataset)
            print('true_positives_by_dataset =', tp_by_dataset)
            recognizable = scenes_per_dataset[dataset] * models_per_scene[dataset] * \
                           instances_per_model_per_scene[dataset]
            print('recognizable =', recognizable)
            for sr, tp_by_sr in tp_by_dataset:
                for a, tp_by_a in tp_by_sr:
                    print('tp_by_a', tp_by_a)
                    print('(%s, %s)' % (sr, a))
                    rr_list = []
                    for conflict_th, tps in tp_by_a:
                        recognition_rate = tps / recognizable
                        rr_list.append((conflict_th, recognition_rate))
                    ax.plot(*zip(*rr_list), label='$r_s=%.1f$, $\\alpha=\\frac{1}{%d} \\pi$, %s' \
                                                  % (sr, 1 / a, datasetDictionary[dataset]),
                            **ps_rr.next())
        ax.set_ylim(0, 10)
        ax.set_ylabel('recognition rate')
        ax.set_xlabel('conflict threshold')
        ax.grid()
        ax.legend(loc='lower right', title='Parameter')

        if SAVE_PLOTS:
            plt_dir = os.path.dirname(FILE_PATH)
            plotting.save_figure(figures, plt_dir, 'pUr_conflictthreshold')
            plotting.save_figure(fig_rr, plt_dir, 'recognitionrates')

    # ********************************************************************************************************************
    # * Krone allows a verified pose to be a true positive, even if it belongs to a ground thruth pose, that has already *
    # * been found by another true positive. In Ziegler's approach of verifying only one model in a scene at a time this *
    # * leads to recognition-rates above 100% !!!! Therefore Krones way of calculating true positives can not be used! ***
    # ********************************************************************************************************************

    # ********************************************************************************************************************
    # * Krone calculates the recognition-rate as #true_positives / #recognizable_poses. This differs from Kroischke's ****
    # * definition of the recognition rate! Because Kroischke allows a correct pose to be a true positive only, if all ***
    # * its predecessor poses with higher weights are true positives as well. Krone is not that strict. As a result, *****
    # * Krone's recognition rates can't be compared to Kroischke's, as Krone's values have a tendency to be higher! ******
    # ********************************************************************************************************************

    # for comparison reasons:
    print('Loading Baseline-data...')
    baseline_data = ExperimentDataStructure()
    baseline_data.from_pickle(BASELINE_FILE_PATH)
    post_processing.compute_pose_errors(baseline_data)
    post_processing.compute_recognition(baseline_data, max_translation_error_relative,
                                        max_rotation_error)

    # mark which run belongs to which matching mode
    add_matching_mode(data)
    add_matching_mode(baseline_data)

    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # find best conflict threshold. it maximizes the recognition rate for two-instance-detection
    fig_rr_models, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 8.5))
    fig_rr_models.subplots_adjust(bottom=5/17)
    fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 9), sharey=True)
    best_setup_dict = find_best_conflict_threshold_by_maximizing_recognition_rate(data, baseline_data,
                                                                                  fig_rr.axes[0],
                                                                                  fig_rr.axes[1],
                                                                                  fig_rr_models.axes[1],
                                                                                  show_legend=False)
    # plot recognition rate for one-instace detection:
    plot_recognition_rate_for_best_conflict_threshold_one_instance(data, baseline_data,
                                                                   best_setup_dict,
                                                                   fig_rr_models.axes[0],
                                                                   show_legend=False)
    fig_rr.tight_layout()
    fig_rr_models.axes[0].legend(bbox_to_anchor=(0.5, 0.00), bbox_transform=fig_rr_models.transFigure,
                        loc='lower center', borderaxespad=0., title='matching modes', ncol=1)
    fig_rr_models.tight_layout(rect=[0, 5/17, 1, 1])


    # TODO: debugging: check rate of how often the second best pose of Kroischke is the second best verified pose of Krone:
    fig_debugging_1, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5, 6))
    fig_debugging_2, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5, 6))
    plot_how_often_best_poses_have_highest_verifictionWeight(data, best_setup_dict,
                                                             fig_debugging_1.axes[0],
                                                             fig_debugging_2.axes[0],
                                                             show_legend=True)

    fig_debugging_1.tight_layout()
    fig_debugging_2.tight_layout()

    # TODO: delete after debugging
    # data.to_yaml('~/ROS/experiment_data/hinterstoisser_pose_verifying/hinterstoisser_conflictthreshold/hinterstoisser_conflictthreshold_after_evaluation.yaml')

    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrate_over_conflict_threshold')
        plotting.save_figure(fig_rr_models, plt_dir,
                             'recognitionrate_of_models_for_best_conflict_threshold')
        plotting.save_figure(fig_debugging_1, plt_dir,
                             'average_rate_of_second_best_pose_having_second_highest_verificationWeight')
        plotting.save_figure(fig_debugging_2, plt_dir,
                             'average_rate_of_best_pose_having_highest_verificationWeight')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
