# -*- coding: utf-8 -*-

"""
Copyright (c) 2019
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# import from project
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching import ros_interface


# script parameters
# TODO: Change the path to the dataset to match your directory setup below:
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_PATH = '/home/markus/ROS/experiment_data/ICVS_2019/Kroischkes_system/snapshots/snapshots'


class PrimitivesDemoExperiment(ExperimentBase):
    """
    simple demo to show matching algorithm in action including rotational symmetry and hinterstoisser's extensions
    Used to generate snapshots of the matching results. Reason: Manually checking the error-metrics,
    because Kroischke's error metric and Hinterstoisser's ADI error metric have bugs.
    """

    def __init__(self, dataset_path,save_path_wo_extension):
        """
        @param dataset_path: file path of the dataset's class script
        @param d_dist_abs: absolute value for d_dist to use for all models
        """

        data_storage = ExperimentDataStructure()
        ExperimentBase.__init__(self, data_storage)

        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension



    def setup(self):

        # start up all launch files and connect the experiment node
        self.launch_and_connect_with_verifier()

        # distance resolution of feature and downsampling
        self.d_dist_rel = 0.05  # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%, [Hodan et al. 2018] use 5%

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': self.d_dist_rel,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       # do not set d_points here!
                                       }

        matcher_settings = {'show_results': True,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,
                            'model_hash_table_type': 2,  # CMPH
                            'publish_pose_cluster_weights': False,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 16, # 16 per voting ball
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,
                            'use_rotational_symmetry': True,  # match with collapsed models
                            'collapse_symmetric_models': True,
                            'publish_equivalent_poses': True,
                            'd_dist': self.d_dist_rel,
                            'd_dist_is_relative': True,

                            # 'HS_save_dir': '~/ROS/experiment_data/hinterstoisser_pose_verifying/publish_n_best_poses/voting_spaces/',
                            'results_screenshot_dir': self.save_path_wo_extension + "/wo_verifier/",

                            # visibility context (NOT USED!):
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions:
                            'use_neighbour_PPFs_for_training': False,
                            'use_neighbour_PPFs_for_matching': False, # not implemented in matcher-nodelet
                            'use_voting_balls': False,
                            'use_hinterstoisser_clustering': False,
                            'use_hypothesis_verification_with_visibility_context': False, # to publish the n best poses of EACH voting ball
                            'vote_for_adjacent_rotation_angles': False,
                            'flag_array_hash_table_type': 0,  # NONE
                            'flag_array_quantization_steps': 30.0, # same resolution as d_alpha_in_pi
                            }

        verifier_settings = {'show_results': False,
                             'results_screenshot_dir': self.save_path_wo_extension,
                             'publish_statistics': True,
                             'publish_mu_values': True,
                             'filter_selfocclusion': False,
                             'use_verification_of_normals': True,
                             'use_conflict_analysis': True,
                             'searchradius': 0.5,
                             'searchradius_is_relative': True,
                             'scenezbufferingthreshold': 0.5,
                             'zbufferingthreshold_is_relative': True,
                             'supportthreshold': 0.02,
                             'penaltythreshold': 0.2,
                             'angleDiffThresh_in_pi': 1/10,
                             'zbufferingresolution': 75,  # Krone's favoured value
                             'conflictthreshold': 0.02
                             }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_scenes() * self.dataset.get_number_of_models()

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        self.loop_models_and_scenes_with_same_d_points_for_model_and_scene_hinterstoisser(self.dataset,
                                                                                          re,
                                                                                          model_indices=[0]
                                                                                          #scene_indices=[3,19]
                                                                                          )

        # # model training
        # print ('-' * 40 + '\n' + 'Adding model...')
        # for model_index in range(self.dataset.get_number_of_models()):
        #
        #     d_points_abs_model = self.add_model_and_get_d_points_abs_hinterstoisser(self.dataset,
        #                                                                             model_index,
        #                                                                             clear_matcher=True,
        #                                                                             clear_verifier=True)
        #     self.set_scene_preprocessor_d_points(d_points_abs_model, is_relative=False)
        #
        #     print('-' * 40 + '\n' + 'Matching to scenes...')
        #     # matching in scenes
        #     for scene_index in range(self.dataset.get_number_of_scenes()):
        #
        #         self.interface.new_run()
        #         self.add_scene_for_verification(self.dataset, scene_index)

    def end(self):
        self.wait_for_enter('-' * 40 + '\n' +
                            'Matching done. Press enter and the demo will clean up...')


if __name__ == '__main__':

    demo = PrimitivesDemoExperiment(DATASET_PATH, SAVE_PATH)
    demo.run(wait_before_looping=False)
