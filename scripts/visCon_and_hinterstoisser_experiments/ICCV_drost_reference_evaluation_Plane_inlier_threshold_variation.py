#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference_Plane_inlier_threshold_variation_extended_range'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too
max_rotation_error = pi / 15  # rad


def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def find_best_conflict_threshold_by_maximizing_recognition_rate(data, ax_rr_over_threshold,
        ax_rr_best_threshold, show_legend=True):

    # extract data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['recognition']],
                                                    [["dynamicParameters", "/scene_preprocessor",
                                                      "plane_inlier_threshold"]])

    # calculate recognition rate for each model of the datasets:
    recognition_rate_per_model = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['plane_inlier_threshold'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_per_threshold = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['plane_inlier_threshold'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    print('recognition rates =', recognition_rate_per_threshold)

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['#c62828','#33691e','#1a237e','#ffb300']
    #['#d50000', '#1b5e20', '#1a237e', '#ffb300']  # ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']
    colors_rr_details = ['#c62828','#33691e','#629749','#ffb300']
    #['#d50000', '#1b5e20', '#548658', '#ffb300']  # ['#1f77b4', '#ff7f0e','#ff7f0e', '#2ca02c']
    markers_rr = ['x', '+', 'v', '^']
    markers_rr_details = ['x', '+', 'v', '^']
    # ps_rr = plotting.PlotStyle(4, additional_plot_settings={"markerfacecolor": "None"})
    # ps_rr_details = plotting.PlotStyle(4, additional_plot_settings={"markerfacecolor": "None"})

    # plot recognition-rate over plane-inlier-threshold
    rr_list = []
    for inlthr, rr_by_inlthr in recognition_rate_per_threshold:
        rr_list.append((inlthr*100, rr_by_inlthr))
        ax_rr_over_threshold.plot(*zip(*rr_list), label='recognition rate',
                                    color=colors_rr[0], marker=markers_rr[0],
                                    markerfacecolor="None", markeredgecolor=colors_rr[0])

    ax_rr_over_threshold.set_ylim(0, 1)
    ax_rr_over_threshold.set_ylabel('recognition rate')
    ax_rr_over_threshold.set_xlabel('plane inlier threshold in cm')
    ax_rr_over_threshold.grid()

    # dict for storing the best setups
    best_setup = {'recognition_rate': -1, 'plane_inlier_threshold': -1}

    # get conflictthreshold with max recognition rate
    for inlthr, rr_by_inlthr in recognition_rate_per_threshold:
        if rr_by_inlthr >= best_setup['recognition_rate']:
            best_setup['recognition_rate'] = rr_by_inlthr
            best_setup['plane_inlier_threshold'] = inlthr

    print('the best inlier threshold is: ', best_setup)

    # format data to make a bar plot for best inlier threshold
    plot_data = []
    for inlthr, rr_by_inlthr in recognition_rate_per_model:
        if inlthr == best_setup['plane_inlier_threshold']:
            plot_data.append(('inlier threshold = %s' %(inlthr*100), rr_by_inlthr))
        if inlthr == 0.022:
            plot_data.append(('inlier threshold = %s' %(inlthr*100), rr_by_inlthr))
    #plot_data = tuple(plot_data)
    print('Best recognition-rates of the models =', plot_data)

    # plot and plot-settings
    bar_by_model(plot_data, ax_rr_best_threshold, legend_title='',
                 show_legend=True, show_top_ticks=False)
    ax_rr_best_threshold.set_ylabel('recognition rate')
    ax_rr_best_threshold.set_ylim(0, 1.0)
    ax_rr_best_threshold.legend(loc='upper left')
    ax_rr_best_threshold.legend(loc='upper left')

    return best_setup


def plot_average_matching_time(data, ax_mt_over_threshold, ax_mt_best_threshold, show_legend, best_rr_setup):
    """

    @type data: ExperimentDataStructure
    @parameter data: experiment data
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [["dynamicParameters", "/scene_preprocessor",
                                                      "plane_inlier_threshold"]])

    average_match_time_per_threshold =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['plane_inlier_threshold'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('average matching time per plane_inlier_threshold')
    for threshold, match_time in average_match_time_per_threshold:
        print('mode: %s, time: %f ms' % (threshold, match_time))

    average_match_time_per_threshold_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['plane_inlier_threshold'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per plane_inlier_threshold per model = ',
          average_match_time_per_threshold_per_model)

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['#c62828', '#33691e', '#1a237e', '#ffb300']
    markers_rr = ['x', '+', 'v', '^']
    markers_rr_details = ['x', '+', 'v', '^']

    # plot recognition-rate over plane-inlier-threshold
    mt_list = []
    for inlthr, mt_by_inlthr in average_match_time_per_threshold:
        mt_list.append((inlthr * 100, mt_by_inlthr))
        ax_mt_over_threshold.plot(*zip(*mt_list), label='average matching time',
                                  color=colors_rr[0], marker=markers_rr[0],
                                  markerfacecolor="None", markeredgecolor=colors_rr[0])

    ax_mt_over_threshold.set_ylim(0, 480000) # max = 8 min
    ax_mt_over_threshold.set_yticks([y for y in range(0,480001,1000*60)])
    scaled_labels = [round(float(x) / 1000 / 60,1) for x in ax_mt_over_threshold.get_yticks()]
    ax_mt_over_threshold.set_yticklabels(scaled_labels)
    ax_mt_over_threshold.set_ylabel('average matching time in min')
    ax_mt_over_threshold.set_xlabel('plane inlier threshold in cm')
    ax_mt_over_threshold.grid()

    # dict for storing the best setups
    best_setup = {'matching_time': float('inf'), 'plane_inlier_threshold': -1}

    # get conflictthreshold with max recognition rate
    for inlthr, rr_by_inlthr in average_match_time_per_threshold:
        if rr_by_inlthr < best_setup['matching_time']:
            best_setup['matching_time'] = rr_by_inlthr
            best_setup['plane_inlier_threshold'] = inlthr

    print('the best inlier threshold is: ', best_setup)

    # format data to make a bar plot for best inlier threshold
    plot_data = []
    for inlthr, mt_by_inlthr in average_match_time_per_threshold_per_model:
        if inlthr == best_setup['plane_inlier_threshold']:
            plot_data.append(('inlier threshold = %s' % (inlthr * 100), mt_by_inlthr))
        if inlthr == best_rr_setup['plane_inlier_threshold']:
            plot_data.append(('inlier threshold = %s' % (inlthr * 100), mt_by_inlthr))
    # plot_data = tuple(plot_data)
    print('Best matching_times of the models =', plot_data)

    # plot and plot-settings
    bar_by_model(plot_data, ax_mt_best_threshold, legend_title='',
                 show_legend=True, show_top_ticks=False)
    ax_mt_best_threshold.set_ylim(0, 780000)
    ax_mt_best_threshold.set_yticks([y for y in range(0,780001,1000*60)])
    scaled_labels = [round(float(x) / 1000 / 60,1) for x in ax_mt_best_threshold.get_yticks()]
    ax_mt_best_threshold.set_yticklabels(scaled_labels)
    ax_mt_best_threshold.set_ylabel('average matching time in min')
    ax_mt_best_threshold.legend(loc='upper left')


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_is_necessary = False

    if compute_pose_errors_is_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        # compute pose errors with Hinterstoisser's error metric
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative)

        # TODO: delete after debugging
        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        data.to_yaml(FILE_PATH_WO_EXTENSION + '_with_computed_errors.yaml')
    else:
        data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")


    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # find best plane-inlier threshold. it maximizes the recognition rate for one-instance-detection
    fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    best_setup_dict = find_best_conflict_threshold_by_maximizing_recognition_rate(data,
                                                                                  fig_rr.axes[0],
                                                                                  fig_rr.axes[1],
                                                                                  show_legend=False)

    fig_rr.tight_layout()

    fig_mt, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    plot_average_matching_time(data,
                              fig_mt.axes[0],
                              fig_mt.axes[1],
                              show_legend=False,
                              best_rr_setup=best_setup_dict)

    fig_mt.tight_layout()


    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrate_over_plane_inlier_threshold_and_best_recgnition_rate_barplot_')
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_over_plane_inlier_threshold_and_best_average_matchnig_time_barplot_')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
