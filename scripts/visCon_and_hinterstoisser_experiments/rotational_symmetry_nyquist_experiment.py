#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke, Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
import time
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/rotational_symmetry_nyquist/rotational_symmetry_nyquist'

MATCHER_SETTINGS =  [# match without considering the Nyquist-Criterion
                     {'use_rotational_symmetry': True,      # match without collapsing models
                      'collapse_symmetric_models': False,
                      'publish_equivalent_poses': True,
                      'd_dist': 0.05,
                      'd_dist_is_relative': True},
                     
                     {'use_rotational_symmetry': True,      # match with collapsed models
                      'collapse_symmetric_models': True,
                      'publish_equivalent_poses': True,
                      'd_dist': 0.05,
                      'd_dist_is_relative': True},
                     
                     {'use_rotational_symmetry': False,     # match the traditional way
                      'd_dist': 0.05,
                      'd_dist_is_relative': True},    
                     
                     # match with Nyquist-Criterion for d_dist
                     {'d_dist': 1/((2/0.05)+1),             # Nyquist-Criterion
                      'd_dist_is_relative': True,
                      'use_rotational_symmetry': True,      # match without collapsing models
                      'collapse_symmetric_models': False,
                      'publish_equivalent_poses': True},
                     
                     {'d_dist': 1/((2/0.05)+1),             # Nyquist-Criterion
                      'd_dist_is_relative': True,
                      'use_rotational_symmetry': True,      # match with collapsed models
                      'collapse_symmetric_models': True,
                      'publish_equivalent_poses': True},
                     
                     {'d_dist': 1/((2/0.05)+1),             # Nyquist-Criterion
                      'd_dist_is_relative': True,
                      'use_rotational_symmetry': False},    # match the traditional way
                     ]


class RotationalSymmetryModelsExperiment(ExperimentBase):
    """
    experiment matching rotationally symmetric objects in scenes exploiting / not exploiting the
    rotational symmetry of the models
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension, matcher_settings):

        ExperimentBase.__init__(self, data_storage)
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension
        self.matcher_settings = matcher_settings

    def setup(self):

        # sets up scene and model pipeline identically but introduces noisification for
        # processed clouds
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': 0.05,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,     
                            'model_hash_table_type': 1, # STL
                            'publish_pose_cluster_weights': True,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 2,
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,

			                #'HS_save_dir': '~/ROS/experiment_data/rotational_symmetry_nyquist/voting_spaces/',
                            
                            # visibility context:
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,
            
                            # hinterstoisser extensions:
                            'use_neighbour_PPFs_for_training': False,
                            'use_neighbour_PPFs_for_matching': False,
                            'use_voting_balls': False,
                            'use_hinterstoisser_clustering': False,
                            'use_hypothesis_verification_with_visibility_context': False,
                            'vote_for_adjacent_rotation_angles': False,
                            'flag_array_hash_table_type': 0, # None
                            'flag_array_quantization_steps': 30.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() *\
                 self.dataset.get_number_of_scenes() *\
                 len(self.matcher_settings)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over matcher setup
        for matcher_settings_dict in self.matcher_settings:

            # set up use of rotation symmetry etc.
            self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings_dict)

            # iterate over models and scenes
            self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(self.dataset, re)

            print('Saving temporary data for this and all previous setups in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')


if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = RotationalSymmetryModelsExperiment(data_storage,
                                                    DATASET_PATH,
                                                    SAVE_FILE_WO_EXTENSION,
                                                    matcher_settings=MATCHER_SETTINGS)
    experiment.run(wait_before_looping=False)
