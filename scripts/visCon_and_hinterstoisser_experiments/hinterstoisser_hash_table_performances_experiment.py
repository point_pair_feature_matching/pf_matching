#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors

from operator import itemgetter

# other script settings
TIME_OUT_S = 30
DATASET_DICTS = [{'path': '~/ROS/datasets/geometric_primitives/blensor_dataset.py',
                  'model_preprocessor_settings' : {'d_points': 0.05,
                                                   'd_points_is_relative': True,
                                                   },
                  'matcher_settings' : { # match with collapsed models:
                                        'use_rotational_symmetry': True,
                                        'collapse_symmetric_models': True,
                                        'publish_equivalent_poses': True,
                                        'd_dist': 0.05,
                                        'd_dist_is_relative': True,
                                        'publish_n_best_poses': 2,
                                        # hinterstoisser extensions:
                                        'flag_array_quantization_steps': 30.0,  # same resolution as d_alpha_in_pi
                                        },
                  'verifier_settings':{'use_conflict_analysis': True}, # for better two-instance recognition rates
                  'changing_matcher_settings' : { #'stl_stl':
                                                  #{'model_hash_table_type': 1,  # STL
                                                  # 'flag_array_hash_table_type': 1,  # STL
                                                  #},
                                                  'stl_stl_prebuilt':
                                                  {'model_hash_table_type': 1,  # STL
                                                   'flag_array_hash_table_type': 2,  # STL prebuilt
                                                   },
                                                   # 'stl_cmph' won't work, because CMPH flag array needs CMPH model hash table
                                                  #'stl_oph':
                                                  #{'model_hash_table_type': 1,  # STL
                                                  # 'flag_array_hash_table_type': 4,  # Own perfect hash function
                                                  #},
                                                  #'cmph_stl':
                                                  #{'model_hash_table_type': 2,  # CMPH
                                                  # 'flag_array_hash_table_type': 1,  # STL
                                                  #},
                                                  #'cmph_stl_prebuilt':
                                                  #{'model_hash_table_type': 2,  # CMPH
                                                  # 'flag_array_hash_table_type': 2,  # STL prebuilt
                                                  #},
                                                  #'cmph_cmph':
                                                  #{'model_hash_table_type': 2,  # CMPH
                                                  # 'flag_array_hash_table_type': 3,  # CMPH
                                                  #},
                                                  #'cmph_oph':
                                                  #{'model_hash_table_type': 2,  # CMPH
                                                  # 'flag_array_hash_table_type': 4,  # Own perfect hash function
                                                  #},
                                                }
                  }
                 ]
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/hinterstoisser_hash_table_performances/hinterstoisser_hash_table_performances'

class HinterstoisserHashTablePerformancesExperiment(ExperimentBase):
    """
    experiment to analyse the mu_v and mu_p values relating to true/false pose
    """
    def __init__(self, data_storage, dataset_dicts, save_path_wo_extension):
    
        ExperimentBase.__init__(self, data_storage)

        self.dataset_dicts = dataset_dicts
        for dataset_dict in self.dataset_dicts:
            dataset_dict['dataset_object'] = self.load_dataset(dataset_dict['path'])
        self.save_path_wo_extension = save_path_wo_extension

    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=True,
                                         dyn_reconfigure_callbacks=True)
        self.launch_and_connect_hinterstoisser()
        
        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       # do not set d_points here!
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,
                            'publish_pose_cluster_weights': True,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,

                            # 'HS_save_dir': '~/ROS/experiment_data/hinterstoisser_pose_verifying/publish_n_best_poses/voting_spaces/',

                            # visibility context (NOT USED!):
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions
                            'use_neighbour_PPFs_for_matching': False,  # not implemented in matcher-nodelet
                            'use_neighbour_PPFs_for_training': True,
                            'use_voting_balls': True,
                            'use_hinterstoisser_clustering': True,
                            'use_hypothesis_verification_with_visibility_context': True, # to publish the n best poses of EACH voting ball
                            'vote_for_adjacent_rotation_angles': True,
                            }

        verifier_settings = {'show_results': False,
                             'publish_statistics': True,
                             'publish_mu_values': True,
                             'use_verification_of_normals': True,
                             'filter_selfocclusion': False,
                             'searchradius': 0.5,
                             'searchradius_is_relative': True,
                             'scenezbufferingthreshold': 0.5,
                             'zbufferingthreshold_is_relative': True,
                             'angleDiffThresh_in_pi': 1/30,
                             'supportthreshold': 0.0,
                             'penaltythreshold': 0.35,
                             'conflictthreshold': 0.08,
                             'zbufferingresolution':75,
                             }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
    def looping(self):
        
        # calculate number of runs to perform
        n_runs = 0
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            n_runs += dataset.get_number_of_scenes() * len(dataset_dict['changing_matcher_settings']) * dataset.get_number_of_models()

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()
        
        # iterate over
        for dataset_dict in self.dataset_dicts:
            
            # extract and setup dataset
            dataset = dataset_dict['dataset_object']
            self.setup_dataset_properties(dataset)
            
            self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                          dataset_dict['model_preprocessor_settings'])
            self.interface.set_dyn_reconfigure_parameters('verifier',
                                                          dataset_dict['verifier_settings'])
            self.interface.set_dyn_reconfigure_parameters('matcher',
                                                          dataset_dict['matcher_settings'])
            
            print('dataset: %s' %(dataset.get_name()))
            
            matcher_settings_for_dataset_dict = dataset_dict['changing_matcher_settings']
            for extension_package, matcher_settings in sorted(matcher_settings_for_dataset_dict.items(), key=itemgetter(0)):
                print("matching %s"%(extension_package))
                self.interface.set_dyn_reconfigure_parameters('matcher',
                                                              matcher_settings)

                # Matching all models of the dataset and verifying them afterwards
                # Model-specific down-sampled scenes are being used in Matcher and Verifier
                if extension_package == "stl_stl":
                    self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(dataset, re, [1], range(7,20))
                    self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(dataset, re, [2,3,4])
                else:
                    self.loop_models_and_scenes_with_same_d_points_for_model_and_scene_hinterstoisser(dataset, re)

                print('Saving temporary data for this setup and all previous setups in case something goes wrong...')
                self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        
        for dataset_dict in self.dataset_dicts:
            dataset = dataset_dict['dataset_object']
            if dataset.get_error_metric() == 'Hinterstoisser_Lepetit_2013':
                print('Computing pose errors with error-metric from [Hinterstoisser, Lepetit et al. 2013] for all runs...')
                compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(self.data, dataset)
            elif dataset.get_error_metric() == 'Kroischke_2016':
                print('Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
                compute_pose_errors(self.data)
            else:
                print('Unknown error-metric, not calculating any errors...')

        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        #self.data.to_yaml(self.save_path_wo_extension + '.yaml')
        
if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = HinterstoisserHashTablePerformancesExperiment(data_storage,
                                                                DATASET_DICTS,
                                                                SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=False)
