#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_find_verifier_settings/ICCV_hinterstoisser_find_verifier_settings_the_resultsfile'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too

model_diameters = { 'ape'    : 0.1032,
                    'can' 	 : 0.2027,
                    'cat' 	 : 0.1519,
                    'driller': 0.2604,
                    'duck'	 : 0.1106,
                    'average': 0.16576}

def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def find_best_zBufferingResolution_by_maximizing_verificationWeight(data, ax, show_legend=True):

    post_processing.compute_verified(data)

    # extract data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['statistics','/verifier'],['recognition'],
                                                     ['poses']],
                                                    [['dynamicParameters', '/verifier',
                                                      'zbufferingresolution'],
                                                     ['dynamicParameters', '/verifier',
                                                      'searchradius'],
                                                     ['dynamicParameters', '/verifier',
                                                      'angleDiffThresh_in_pi']])

    # filter data by searchradius and angleDiffThresh_in_pi:
    filtered_data=[]
    for run in base_data:
        if run[iDict['searchradius']] == 0.5 and run[iDict['angleDiffThresh_in_pi']] == 1.0 and \
                run[iDict['recognition']]['truePositives']>0:
            filtered_data.append(run)
    #print(filtered_data)

    # get average verificationWeight for each model:
    averageVerificationWeight_per_model = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                           [iDict['zbufferingresolution'],
                                                            iDict['model'],
                                                            iDict['poses']],
                                                           data_extraction.average_verificationWeight_of_best_poses_that_are_correct_and_verified)

    # get average verificationWeight averaged for all models:
    averageVerificationWeight = \
        data_extraction.nest_and_process_by_values(filtered_data,
                                                           [iDict['zbufferingresolution'],
                                                            iDict['poses']],
                                                           data_extraction.average_verificationWeight_of_best_poses_that_are_correct_and_verified)

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['k', '#c62828','#33691e','#1a237e','#ffb300', 'gray']
    markers_rr = ['x', '+', 'v', '^', '*','o']

    #print(averageVerificationWeight_per_model)

    # plot averageModelPoints over zbufferingresolution
    plot_dict ={}
    for zbr, amp_by_zbr in averageVerificationWeight_per_model:
        for model, amp_by_model in amp_by_zbr:
            if model not in plot_dict:
                plot_dict[model] =[]

    #print(plot_dict)
    for model in plot_dict.keys():
        for zbr, avw_by_zbr in averageVerificationWeight_per_model:
            for model_, avw_by_model in avw_by_zbr:
                if model == model_:
                    plot_dict[model].append((zbr, avw_by_model))

    plot_dict['average'] = averageVerificationWeight
    #print(plot_dict)

    color_counter = -1
    for model, avw_by_zbr in sorted(plot_dict.items()):
        color_counter += 1
        ax.plot(*zip(*avw_by_zbr), label=model,
                color=colors_rr[color_counter], marker=markers_rr[color_counter],
                markerfacecolor="None", markeredgecolor=colors_rr[color_counter])


    ax.set_ylim(0, 160)
    ax.set_ylabel('average verificationWeight\nof correct poses')
    ax.set_xlabel('z-buffering resolution')
    ax.grid()
    if show_legend:
        ax.legend(loc='upper left', ncol=2)

    return plot_dict

def plot_best_zBufferingResolution_over_model_diameter(model_diameters_dict, plot_dict, ax):

    # get best zbufferig resolution for each model
    plot_data = []
    for model, model_data in plot_dict.items():
        if model in model_diameters_dict.keys() and model != 'average':
            index = np.argmax([avg_vw_per_res for res, avg_vw_per_res in model_data])
            plot_data.append((model_diameters_dict[model], model_data[index][0]))
    plot_data.sort(key=lambda x: x[0])

    from sklearn import linear_model
    # Create linear regression object
    regr = linear_model.LinearRegression()
    # Train the model using the training sets
    x_is = np.array([x for x, y in plot_data])
    y_is = np.array([y for x, y in plot_data])
    x_is.resize((5,1))
    regr.fit(x_is, y_is)
    # Make predictions using the testing set
    x_plot = np.resize(np.array([0,0.3]),(2,1))
    y_lin = regr.predict(x_plot)

    colors_rr = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300', 'gray']
    markers_rr = ['x', '+', 'o', '^', '*', 'o']

    ax.plot(model_diameters_dict['average'], 160,
            label="best average\nz-buffering resolution",
            color=colors_rr[2], marker=markers_rr[2], markersize = 4,
            markerfacecolor=colors_rr[2], markeredgecolor=colors_rr[2],
            linestyle='None',)
    ax.plot(*zip(*plot_data), label="best model-specific\nz-buffering resolution",
            color=colors_rr[0], marker=markers_rr[0],
            markerfacecolor="None", markeredgecolor=colors_rr[0])
    ax.plot(x_plot, y_lin, color=colors_rr[1], label="linear model")
    ax.grid()
    ax.set_xlabel("model diameter [m]")
    ax.set_ylabel("best z-buffering resolution")
    ax.set_xlim(0, 0.3)
    ax.set_ylim(0, 250)
    import matplotlib.lines as mlines
    #black_line = mlines.Line2D([], [], label="best model-specific\nz-buffering resolution",
    #                           color=colors_rr[0], marker=markers_rr[0],
    #                           markerfacecolor="None", markeredgecolor=colors_rr[0])
    #green_star = mlines.Line2D( [], [], label="best average\nz-buffering resolution",
    #                            color=colors_rr[2], marker=markers_rr[2],
    #                            markerfacecolor="None", markeredgecolor=colors_rr[2])
    #red_line = mlines.Line2D([], [], color = colors_rr[1], label="linear model")
    ax.legend(loc="lower left",numpoints=1)#, handles=[black_line, green_star, red_line])


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_is_necessary = False

    print("Loading Experiment-data for [Hinterstoisser et al. 2014]-error-metric")
    if compute_pose_errors_is_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

        print("Computing pose errors with [Hinterstoisser et al. 2014]-error-metric")
        # compute pose errors with Hinterstoisser's error metric
        post_processing.compute_pose_errors_from_verifier_by_hinterstoisser_lepetit_et_al_2013(data,
                                                                                               dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_from_verifier_with_hinterstoisser_lepetit_2013(data,
                                                                                           max_translation_error_relative)

        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        # data_hinterstoisser_error_metric.to_yaml(
        #    FILE_PATH_WO_EXTENSION + '_with_computed_errors.yaml')
    else:
        data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")

    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # find best zbuffering resolution. it maximizes the average "verificationWeight" value
    fig_rr, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(8.5 + 5, 5.8))
    fig_rr.subplots_adjust(right=0.4)
    plot_data_dict = find_best_zBufferingResolution_by_maximizing_verificationWeight( data,
                                                                                      fig_rr.axes[0],
                                                                                      show_legend=True)

    fig_rr.axes[0].legend(bbox_to_anchor=(0.6, 0.55), loc='center left', borderaxespad=0.,
                          title='models', ncol=2, bbox_transform=fig_rr.transFigure)
    fig_rr.tight_layout(rect=[0, 0, 0.6, 1])


    # plot the best model-specific z-buffering resolution over the model diameter
    fig_lin_model, axes = plt.subplots(1,1, figsize=plotting.cm2inch(7.5, 5.8))
    plot_best_zBufferingResolution_over_model_diameter(model_diameters, plot_data_dict,
                                                       fig_lin_model.axes[0])
    fig_lin_model.tight_layout()

    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'averageVerificationWeight_over_zbufferingresolution_plot')
        plotting.save_figure(fig_lin_model, plt_dir, 'best_zbufferingresolution_over_model_diameter_plot')


    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
