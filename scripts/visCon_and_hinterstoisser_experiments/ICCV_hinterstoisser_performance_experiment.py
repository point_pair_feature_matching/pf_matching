#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors

import configparser
import os

# other script settings
TIME_OUT_S = 30
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_performance/ICCV_hinterstoisser_performance'
MODEL_INDICES = [0,1,2,3,4] # match only the "unambiguous" models
SCENE_INDICES = [[0,201],[202,403],[404,605],[606,807],[808,1009],[1010,1213]] # for spreading the scenes across 6 AWS-instances

class HinterstoisserHashTablePerformancesExperiment(ExperimentBase):
    """
    experiment to analyse the mu_v and mu_p values relating to true/false pose
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension):
    
        ExperimentBase.__init__(self, data_storage)

        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension

        # try reading spreading info file from home directory
        # it should be available if experiment was started on remote computer with the
        # aws_experiment_interface
        config = configparser.ConfigParser()
        ini_file_path = os.path.expanduser('~/spreading_info.ini')
        self.job_id = None
        try:
            config.read(ini_file_path)
            self.job_id = int(config['DEFAULT']['job_id'])
        except:
            print(
                "ERROR: Could not load ini-file form %s, conducting COMPLETE experimet instead of only a part." % ini_file_path)

    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=True,
                                         dyn_reconfigure_callbacks=True)
        self.launch_and_connect_hinterstoisser()

        # distance resolution of feature and downsampling
        d_dist_rel = 0.05  # [Hodan et al. 2018] use 5%, [Hinterstoisser et al. 2016] use 2.5%

        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        model_preprocessor_settings = {'pre_downsample_method': 0,   # NONE
                                       'post_downsample_method': 3,  # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'normal_estimation_method': 0,# NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': d_dist_rel,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,# 5 MLS_POLY2
                                       'pre_downsample_method': 3,   # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'post_downsample_method': 3,  # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.02, # TODO: choose best value from inlier_variation_experiment
                                       'plane_inlier_threshold_is_relative': False,
                                       'pre_downsample_dist_ratio': 0.5,
                                       # do not set d_points here!
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,
                            'publish_pose_cluster_weights': True,
                            'refPointStep': 5, #  for good performance. btw: [Hodan et al. 2018] use 5 too.
                            'maxThresh': 1.0,
                            'd_dist': d_dist_rel,
                            'd_dist_is_relative': True,
                            'use_rotational_symmetry': False,
                            'collapse_symmetric_models': False,
                            'publish_n_best_poses': 7, # TODO: choose number per voting ball! 2 was best for "geometric primitives"
                            'model_hash_table_type': 2,  # CMPH, because it's faster in this scenario
                            'd_alpha_in_pi': 1 / 16, # [Hinterstoisser et al. 2016] use 32steps/360°? or 32steps/180°
                            'd_angle_in_pi': 1 / 22, # [Drost et al. 2010] use 12° steps =1/15 [Hinterstoisser et al. 2016] use 22steps/180°

                            # 'HS_save_dir': '~/ROS/experiment_data/hinterstoisser_pose_verifying/publish_n_best_poses/voting_spaces/',

                            # visibility context (NOT USED!):
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions (everything activated!)
                            'use_neighbour_PPFs_for_matching': False,  # not implemented in matcher-nodelet
                            'use_neighbour_PPFs_for_training': True,
                            'use_voting_balls': True,
                            'use_hinterstoisser_clustering': True,
                            'use_hypothesis_verification_with_visibility_context': True, # to publish the n best poses of EACH voting ball
                            'vote_for_adjacent_rotation_angles': True,
                            'flag_array_quantization_steps': 32.0,     # same resolution as d_alpha_in_pi
                            'flag_array_hash_table_type': 3,  # CMPH, because it's faster in this scenario
                            }

        verifier_settings = {'show_results': False,
                             'publish_statistics': True,
                             'publish_mu_values': True,
                             'use_verification_of_normals': True,
                             'filter_selfocclusion': False,
                             'searchradius': 0.5,               # TODO: use best value from searchradius-variation experiment
                             'searchradius_is_relative': True,
                             'scenezbufferingthreshold': 0.5,   # TODO: use same value as for searchradius
                             'zbufferingthreshold_is_relative': True,
                             'angleDiffThresh_in_pi': 1/10,     # TODO: use best value from searchradius-variation experiment
                             'supportthreshold': 0.0,           # TODO: use best value from searchradius-variation experiment
                             'penaltythreshold': 1.0,           # TODO: use best value from searchradius-variation experiment
                             'conflictthreshold': 0.08, # moot, since conflict analysis is not used
                             'zbufferingresolution': 160,       # TODO: use best value from find_verifier_settings experiment
                             'use_conflict_analysis': False,  # not useful for one-instance single model detection
                             }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.wait_for_enter('-' * 40 + '\n'
                            'Pipeline started and configured.\n'
                            'View / change parameters manually through "rosrun rqt_reconfigure rqt_reconfigure".\n'
                            'Show pipeline setup via "rqt_graph".\n'
                            'Press enter to start matching...')
        
    def looping(self):
        
        # calculate number of runs to perform
        n_runs = 0
        # check if experiment shall be splitted and distributed over several machines:
        if self.job_id is None:
            # do it all on this machine
            n_runs += len(MODEL_INDICES) * self.dataset.get_number_of_scenes()
        else:
            # do only the i'th part of the whole experiment on this machine. do the other parts
            # on other machines.
            n_runs += len(MODEL_INDICES) * (SCENE_INDICES[self.job_id][1]+1 - SCENE_INDICES[self.job_id][0])

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # setup dataset
        self.setup_dataset_properties(self.dataset)

        # Matching all models of the dataset and verifying them afterwards
        # Model-specific down-sampled scenes are being used in Matcher and Verifier

        # check if experiment shall be splitted and distributed over several machines:
        if self.job_id is None:
            # do it all on this machine
            self.loop_models_and_scenes_with_same_d_points_for_model_and_scene_hinterstoisser(self.dataset,
                                                                                              re,
                                                                                              model_indices=MODEL_INDICES)
        else:
            # do only the i'th part of the whole experiment on this machine. do the other parts
            # on other machines.
            scene_indices = range(SCENE_INDICES[self.job_id][0], SCENE_INDICES[self.job_id][1]+1, 1)

            print("Got information to perform only a part of the whole experiment. Experiment-Splitting activated.\n"
                  "Start looping through scene-indices from %s to %s:"
                  %(min(scene_indices), max(scene_indices)))

            self.loop_models_and_scenes_with_same_d_points_for_model_and_scene_hinterstoisser(
                self.dataset,
                re,
                model_indices=MODEL_INDICES,
                scene_indices=scene_indices)


        print('Saving temporary data for this setup and all previous setups in case something goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        
        print('Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
        compute_pose_errors(self.data)

        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        #self.data.to_yaml(self.save_path_wo_extension + '.yaml')


if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = HinterstoisserHashTablePerformancesExperiment(data_storage,
                                                               DATASET_PATH,
                                                               SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=False)
