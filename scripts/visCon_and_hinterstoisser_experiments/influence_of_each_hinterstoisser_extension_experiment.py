#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors


# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'

SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/influence_of_each_Hinterstoisser_extension/influence_of_each_Hinterstoisser_extension_geometric_primitives'

class influenceOfEachHinterstoisserExtensionExperiment(ExperimentBase):
    """
    experiment to analyse the effects of each [Hinterstoisser et al. 2016] extension separartely
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension):
        
        ExperimentBase.__init__(self, data_storage)
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension

        
    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False) # noting happens inside this function !?
        self.launch_and_connect()
        
        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
	
	d_dist = 0.07 #0.05
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'normal_estimation_method': 0,  # 0 NONE
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': d_dist,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       }

        matcher_settings = {'show_results': False,
                            'd_dist': d_dist,
                            'd_dist_is_relative': True,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,                            
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'use_rotational_symmetry': False,
                            'collapse_symmetric_models': False,
                            'publish_clustered_poses': True, # this is essential here, otherwise the clusters won't get saved
                            'publish_n_best_poses': 2.0,
			                'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,
                            
                            'model_hash_table_type': 1, # STL
                            
                            # visibility context:
                            'd_VisCon': d_dist,
                            'd_VisCon_is_relative': True,
                            'voxelSize_intersectDetect': d_dist,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions: 
                            'flag_array_quantization_steps': 30.0, #to match with d_alpha
                            'use_hypothesis_verification_with_visibility_context': False
                            # the rest gets changed in looping()
                            }
        

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
        self.setup_dataset_properties(self.dataset)
        
    def looping(self):
        # match "using rs without collapsing models" too! the equivalent poses that are being calculated there, 
        # are necessary for calculating meaningful recognition rates. without them, the recognitionrates drop
	# a lot, because the ground truth poses for the rotational symmetric models cover only one of the 
	# many rotational symmetric "true poses"
        matcher_settings = [{'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 0,
                             'use_rotational_symmetry': True,
                             'collapse_symmetric_models': False},  # match using rs without collapsing models
			                {'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 0,
			                 'use_rotational_symmetry': False,
                             'collapse_symmetric_models': False},    #traditional matching
                            {'use_neighbour_PPFs_for_training': True,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 0},
                            {'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': True,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 0},
                            {'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': True,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 0},
                            {'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': True,
                             'flag_array_hash_table_type': 0},
                            {'use_neighbour_PPFs_for_training': False,
                             'use_voting_balls': False,
                             'use_hinterstoisser_clustering': False,
                             'vote_for_adjacent_rotation_angles': False,
                             'flag_array_hash_table_type': 2}] # STL prebuilt Flag Array
        
        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_models() *\
                 self.dataset.get_number_of_scenes() *\
                 len(matcher_settings) # number of configurations to compare to each other
        
        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()
        
        # iterate over matcher setup
        for matcher_setting in matcher_settings:
                     
            # activate a [Hinterstoisser et al. 2016] extension separately
            self.interface.set_dyn_reconfigure_parameters('matcher', matcher_setting)
            
            #print('activated extension: %s' %(matcher_setting))
            
             # iterate over models and scenes
            self.loop_models_and_scenes(self.dataset, re)
            
                        
    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

        print('Computing pose errors for all runs...')
        compute_pose_errors(self.data)
        
        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        #self.data.to_yaml(self.save_path_wo_extension + '.yaml') # don't do this, file might get too big for RAM
        
if __name__ == '__main__':
    
    data_storage = data_structure.ExperimentDataStructure()
    experiment = influenceOfEachHinterstoisserExtensionExperiment(data_storage,
                                                                  DATASET_PATH,
                                                                  SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=False)
