#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_parameter_variations/ICCV_hinterstoisser_parameter_variations'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too
max_rotation_error = pi / 15  # rad


def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    #colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']
    # special case: we know that we need first a red and then a blue bar
    colors_three_bars = ['#c62828', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def plot_recognition_rates(data, ax_rr_over_refpointstep, ax_rr_barplot, show_legend=True):

    # extract data
    iDict, base_data = data_extraction.extract_data(data,
                                                    [['recognition']],
                                                    [["dynamicParameters", "/matcher",
                                                      "d_dist"],
                                                     ["dynamicParameters", "/matcher",
                                                      "refPointStep"]])

    # calculate recognition rate for each model of the datasets:
    recognition_rate_per_model = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['model'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_per_threshold = \
        data_extraction.nest_and_process_by_values(base_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['recognition']],
                                                   data_extraction.recognition_rate_best_poses)
    print('recognition rates =', recognition_rate_per_threshold)

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['#c62828','#33691e','#1a237e','#ffb300']
    markers_rr = ['x', '+', 'v', '^']

    # plot recognition-rate over refpointstep
    # get all d_dist values
    plot_dict = {}
    for d_dist, rr_by_d_dist in recognition_rate_per_threshold:
        if d_dist not in plot_dict:
            plot_dict[d_dist] = []

    # sort all (refpointstep, recograte) pairs into plot_dict
    for d_dist in plot_dict.keys():
        for d_dist_, rr_by_d_dist in recognition_rate_per_threshold:
            for rps, rr_by_rps in rr_by_d_dist:
                if d_dist_ == d_dist:
                    plot_dict[d_dist].append((rps, rr_by_rps))

    # do plotting
    color_counter = -1
    for d_dist, rr_by_d_dist in sorted(plot_dict.items()):
        color_counter += 1
        ax_rr_over_refpointstep.plot(*zip(*rr_by_d_dist), label=d_dist,
                color=colors_rr[color_counter], marker=markers_rr[color_counter],
                markerfacecolor="None", markeredgecolor=colors_rr[color_counter])

    ax_rr_over_refpointstep.set_ylim(0, 1)
    ax_rr_over_refpointstep.set_ylabel('recognition rate')
    ax_rr_over_refpointstep.set_xlabel('refPointStep')
    ax_rr_over_refpointstep.grid()
    if show_legend:
        ax_rr_over_refpointstep.legend(loc='upper right', title="$d_\mathrm{dist}$")

    # dict for storing the best setups
    best_setup = {'recognition_rate': -1, 'd_dist': -1, 'refPointStep':-1}

    # get conflictthreshold with max recognition rate
    for d_dist, rr_by_d_dist in recognition_rate_per_threshold:
        for refPointStep, rr_by_refPointStep in rr_by_d_dist:
            if rr_by_refPointStep >= best_setup['recognition_rate']:
                best_setup['recognition_rate'] = rr_by_refPointStep
                best_setup['d_dist'] = d_dist
                best_setup['refPointStep'] = refPointStep

    print('the best parameter-setup regarding recognition rate is: ', best_setup)

    # format data to make a bar plot for best inlier threshold
    plot_data = []
    for d_dist, rr_by_d_dist in recognition_rate_per_model:
        for refPointStep, rr_by_refPointStep in rr_by_d_dist:
            if d_dist == best_setup['d_dist'] and refPointStep == best_setup['refPointStep']:
                plot_data.append(('$d_\mathrm{dist}$ = %s, refPointStep = %s' %(d_dist, refPointStep), rr_by_refPointStep))

    #plot_data = tuple(plot_data)
    print('Best recognition-rates of the models =', plot_data)

    # plot and plot-settings
    bar_by_model(plot_data, ax_rr_barplot, legend_title='',
                 show_legend=True, show_top_ticks=False)
    ax_rr_barplot.set_ylabel('recognition rate')
    ax_rr_barplot.set_ylim(0, 1.0)
    if show_legend:
        ax_rr_barplot.legend(loc='upper left')

    return best_setup


def plot_average_matching_time(data, ax_mt_over_refpointstep, ax_mt_barplot, show_legend, best_rr_setup):
    """

    @type data: ExperimentDataStructure
    @parameter data: experiment data
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [["dynamicParameters", "/matcher",
                                       "d_dist"],
                                      ["dynamicParameters", "/matcher",
                                       "refPointStep"]])

    average_match_time_per_threshold =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('average matching time per d_dist and refPointStep:')
    for d_dist, mt_by_d_dist in average_match_time_per_threshold:
        for refPointStep, mt_by_refPointStep in mt_by_d_dist:
            print('d_dist: %s refPointStep = %s, time: %f ms' % (d_dist, refPointStep, mt_by_refPointStep))

    average_match_time_per_threshold_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    #print('average matching time per plane_inlier_threshold per model = ',
    #      average_match_time_per_threshold_per_model)

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['#c62828', '#33691e', '#1a237e', '#ffb300']
    markers_rr = ['x', '+', 'v', '^']
    markers_rr_details = ['x', '+', 'v', '^']

    # plot average matching time over refpointstep
    # get all d_dist values
    plot_dict = {}
    for d_dist, mt_by_d_dist in average_match_time_per_threshold:
        if d_dist not in plot_dict:
            plot_dict[d_dist] = []

    # sort all (refpointstep, matchingtime) pairs into plot_dict
    for d_dist in plot_dict.keys():
        for d_dist_, mt_by_d_dist in average_match_time_per_threshold:
            for rps, mt_by_rps in mt_by_d_dist:
                if d_dist_ == d_dist:
                    plot_dict[d_dist].append((rps, mt_by_rps))

    # do plotting
    color_counter = -1
    for d_dist, mt_by_d_dist in sorted(plot_dict.items()):
        color_counter += 1
        ax_mt_over_refpointstep.plot(*zip(*mt_by_d_dist), label=d_dist,
                color=colors_rr[color_counter], marker=markers_rr[color_counter],
                markerfacecolor="None", markeredgecolor=colors_rr[color_counter])

    ax_mt_over_refpointstep.set_ylim(0, 130000) # max = 1.1 min
    ax_mt_over_refpointstep.set_yticks([y for y in range(0,130001,1000*30)])
    scaled_labels = [round(float(x) / 1000 / 60,1) for x in ax_mt_over_refpointstep.get_yticks()]
    ax_mt_over_refpointstep.set_yticklabels(scaled_labels)
    ax_mt_over_refpointstep.set_ylabel('average matching time in min')
    ax_mt_over_refpointstep.set_xlabel('refPointStep')
    ax_mt_over_refpointstep.grid()
    if show_legend:
        ax_mt_over_refpointstep.legend(loc='upper right', title="$d_\mathrm{dist}$")

    # dict for storing the best setups
    best_setup = {'matching_time': float('inf'), 'd_dist': -1, 'refPointStep':-1}

    # get parameter-setup with min matching time
    for d_dist, mt_by_d_dist in average_match_time_per_threshold:
        for refPointStep, mt_by_refPointStep in mt_by_d_dist:
            if mt_by_refPointStep < best_setup['matching_time']:
                best_setup['matching_time'] = mt_by_refPointStep
                best_setup['d_dist'] = d_dist
                best_setup['refPointStep'] = refPointStep

    print('the best parameter-setup regarding avg. matching time is: ', best_setup)
    print('the best parameter-setup regarding recognition rate is : ', best_rr_setup)

    # format data to make a bar plot for best inlier threshold
    plot_data = []
    for d_dist, mt_by_d_dist in average_match_time_per_threshold_per_model:
        for refPointStep, mt_by_refPointStep in mt_by_d_dist:
            if d_dist == best_setup['d_dist'] and refPointStep == best_setup['refPointStep']:
                plot_data.append(('$d_\mathrm{dist}$ = %s, refPointStep = %s' % (d_dist, refPointStep), mt_by_refPointStep))
            if d_dist == best_rr_setup['d_dist'] and refPointStep == best_rr_setup['refPointStep']:
                plot_data.append(('$d_\mathrm{dist}$ = %s, refPointStep = %s' % (d_dist, refPointStep), mt_by_refPointStep))
    # plot_data = tuple(plot_data)
    print('Best matching_times of the models =', plot_data)

    # plot and plot-settings
    bar_by_model(plot_data, ax_mt_barplot, legend_title='',
                 show_legend=True, show_top_ticks=False)
    ax_mt_barplot.set_ylim(0,130000)
    ax_mt_barplot.set_yticks([y for y in range(0,130001,1000*30)])
    scaled_labels = [round(float(x) / 1000 / 60,1) for x in ax_mt_barplot.get_yticks()]
    ax_mt_barplot.set_yticklabels(scaled_labels)
    ax_mt_barplot.set_ylabel('average matching time in min')
    if show_legend:
        ax_mt_barplot.legend(loc='upper left')


if __name__ == '__main__':

    # load data and do post-processing
    data = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_is_necessary = False

    if compute_pose_errors_is_necessary:
        data.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
        # compute pose errors with Hinterstoisser's error metric
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative)

        data.to_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        data.to_yaml(FILE_PATH_WO_EXTENSION + '_with_computed_errors.yaml')
    else:
        data.from_pickle(FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")


    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # find best plane-inlier threshold. it maximizes the recognition rate for one-instance-detection
    fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    best_setup_dict = plot_recognition_rates(data,fig_rr.axes[0],fig_rr.axes[1],show_legend=True)

    fig_rr.tight_layout()

    fig_mt, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6))
    plot_average_matching_time(data,
                              fig_mt.axes[0],
                              fig_mt.axes[1],
                              show_legend=True,
                              best_rr_setup=best_setup_dict)

    fig_mt.tight_layout()


    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrate_over_refPointStep_and_d_dist_and_best_recgnition_rate_barplot')
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_refPointStep_and_d_dist_and_best_average_matchnig_time_barplot')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
