#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors, get_model_diameters
import pf_matching.pose_tools as pt

# project
from math import floor, ceil, pi, sqrt, pow
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np
import tf.transformations as tft


# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
#FILE_PATH = '~/ROS/experiment_data/ICCV_drost_reference_Plane_inlier_threshold_variation/ICCV_drost_reference_Plane_inlier_threshold_variation_part_2.bin' #'~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference.bin'
FILE_PATH = '~/ROS/experiment_data/ICCV_hinterstoisser_pose_verifying/ICCV_hinterstoisser_publish_n_best_poses/ICCV_hinterstoisser_publish_n_best_poses.bin'

VISUALIZE_I_TH_POSES = 1


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized(data, dataset):

    diameters_dict = get_model_diameters(data)

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names= data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        # both data-elements need to have the same model name, because i don't check it here
        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                             copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform = tft.identity_matrix()

            # assuming that each data-element has the same equivalent poses
            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                             copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                print('got no model equivalent poses')
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # get model load path and model scale factor
            model_absolute_load_path = ""
            model_scale_factor = dataset.get_model_scale_factor()
            for model_idx in range(dataset.get_number_of_models()):
                if dataset.get_model_name(model_idx) == model_name:
                    model_absolute_load_path = dataset.get_model_path(model_idx)
                    break
            if not model_absolute_load_path:
                return {'average distance': None}

            # use pcl-python bindings
            import pcl
            import time

            total_start = time.time()

            # load the gt-model as a pcl point cloud
            gt_model = pcl.load_XYZI(model_absolute_load_path)
            # load the model as a pcl point cloud
            pose_model = pcl.load_XYZI(model_absolute_load_path)

            # store the model points in np array of tuples(?) [(x1,y1,z1), (x2, y2,z2), ...]
            gt_model_points = gt_model.to_array()
            pose_model_points = pose_model.to_array()

            start = time.time()
            # # scale model
            pose_model_points = pose_model_points * model_scale_factor
            pose_model_points = pose_model_points + np.array((0.0,0.0,0.0,1.0), dtype='f')
            # scale gt-model
            gt_model_points = gt_model_points * model_scale_factor
            gt_model_points = gt_model_points + np.array((0.0,0.0,0.0,1.0), dtype='f')
            end = time.time()
            print ("scaling time using vectors =", end-start)

            i_th_pose = 0
            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                i_th_pose += 1
                model_pose = data.get_nested(poses_path + [pose_index])
                #print('model pose:\n', pt.pose_dict2matrix(model_pose))

                # go through all ground truth poses
                gts_path = ["runData", run_id, "groundTruth", model_name]
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])

                    #print('ground truth pose %i:' %gt_index, gt_pose,'\n', pt.pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pt.pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)

                    gt_pose_transformed = pt.transform_gt(gt_pose, model_transform, scene_transform)
                    #print('*transformed ground truth pose %i:\n' %gt_index, gt_pose_transformed)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = pt.concatenate_transforms(model_pose, equivalent_pose)

                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)

                        display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized(model_test_pose,
                                                                                             gt_pose_transformed,
                                                                                             pt.pose_dict2matrix(model_transform),
                                                                                             dataset,
                                                                                             scene_name,
                                                                                             i_th_pose,
                                                                                             pose_model,
                                                                                             pose_model_points,
                                                                                             gt_model,
                                                                                             gt_model_points,
                                                                                             total_start)


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops(data, dataset):

    diameters_dict = get_model_diameters(data)

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names= data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        # both data-elements need to have the same model name, because i don't check it here
        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                             copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform = tft.identity_matrix()

            # assuming that each data-element has the same equivalent poses
            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                             copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                print('got no model equivalent poses')
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # get model load path and model scale factor
            model_absolute_load_path = ""
            model_scale_factor = dataset.get_model_scale_factor()
            for model_idx in range(dataset.get_number_of_models()):
                if dataset.get_model_name(model_idx) == model_name:
                    model_absolute_load_path = dataset.get_model_path(model_idx)
                    break
            if not model_absolute_load_path:
                return {'average distance': None}

            # use pcl-python bindings
            import pcl
            import time

            total_start = time.time()

            # load the gt-model as a pcl point cloud
            gt_model = pcl.load_XYZI(model_absolute_load_path)
            # load the model as a pcl point cloud
            pose_model = pcl.load_XYZI(model_absolute_load_path)

            # store the model points in np array of tuples(?) [(x1,y1,z1), (x2, y2,z2), ...]
            gt_model_points = gt_model.to_array()
            pose_model_points = pose_model.to_array()

            start = time.time()
            # scale model
            for i in range(0, pose_model.size):
                pose_model_points[i] *= model_scale_factor
                pose_model_points[i][3] = 1
                # print('pose point after scaling:', pose_model_points[i])
            # scale gt-model
            for i in range(0, gt_model.size):
                gt_model_points[i] *= model_scale_factor
                gt_model_points[i][3] = 1
                # print('gt point after scaling:', gt_model_points[i])
            end = time.time()
            print("scaling time using for loop =", end - start)

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])
                #print('model pose:\n', pt.pose_dict2matrix(model_pose))

                # go through all ground truth poses
                gts_path = ["runData", run_id, "groundTruth", model_name]
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])

                    #print('ground truth pose %i:' %gt_index, gt_pose,'\n', pt.pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pt.pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)

                    gt_pose_transformed = pt.transform_gt(gt_pose, model_transform, scene_transform)
                    #print('*transformed ground truth pose %i:\n' %gt_index, gt_pose_transformed)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = pt.concatenate_transforms(model_pose, equivalent_pose)

                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)

                        display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops(model_test_pose,
                                                                                             gt_pose_transformed,
                                                                                             pt.pose_dict2matrix(model_transform),
                                                                                             dataset,
                                                                                             scene_name,
                                                                                             pose_model,
                                                                                             pose_model_points,
                                                                                             gt_model,
                                                                                             gt_model_points,
                                                                                             total_start)


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized_all_in_display_function(data, dataset):

    diameters_dict = get_model_diameters(data)

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names= data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        # both data-elements need to have the same model name, because i don't check it here
        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                             copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform = tft.identity_matrix()

            # assuming that each data-element has the same equivalent poses
            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                             copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                print('got no model equivalent poses')
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])
                #print('model pose:\n', pt.pose_dict2matrix(model_pose))

                # go through all ground truth poses
                gts_path = ["runData", run_id, "groundTruth", model_name]
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])

                    #print('ground truth pose %i:' %gt_index, gt_pose,'\n', pt.pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pt.pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)

                    gt_pose_transformed = pt.transform_gt(gt_pose, model_transform, scene_transform)
                    #print('*transformed ground truth pose %i:\n' %gt_index, gt_pose_transformed)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = pt.concatenate_transforms(model_pose, equivalent_pose)

                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)

                        display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized_all_in_here(model_test_pose,
                                                                                             gt_pose_transformed,
                                                                                             model_transform,
                                                                                             dataset, model_name,
                                                                                             scene_name)


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops_all_in_display_function(data, dataset):

    diameters_dict = get_model_diameters(data)

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names= data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        # both data-elements need to have the same model name, because i don't check it here
        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                             copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                print('got no model load transform')
                model_transform = tft.identity_matrix()

            # assuming that each data-element has the same equivalent poses
            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                             copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                print('got no model equivalent poses')
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # get model load path and model scale factor
            model_absolute_load_path = ""
            model_scale_factor = dataset.get_model_scale_factor()
            for model_idx in range(dataset.get_number_of_models()):
                if dataset.get_model_name(model_idx) == model_name:
                    model_absolute_load_path = dataset.get_model_path(model_idx)
                    break
            if not model_absolute_load_path:
                return {'average distance': None}

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])
                #print('model pose:\n', pt.pose_dict2matrix(model_pose))

                # go through all ground truth poses
                gts_path = ["runData", run_id, "groundTruth", model_name]
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])

                    #print('ground truth pose %i:' %gt_index, gt_pose,'\n', pt.pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pt.pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)

                    gt_pose_transformed = pt.transform_gt(gt_pose, model_transform, scene_transform)
                    #print('*transformed ground truth pose %i:\n' %gt_index, gt_pose_transformed)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = pt.concatenate_transforms(model_pose, equivalent_pose)

                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)

                        display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops_all_in_here(model_test_pose,
                                                                                             gt_pose_transformed,
                                                                                             pt.pose_dict2matrix(model_transform),
                                                                                             dataset,
                                                                                             scene_name,
                                                                                             model_absolute_load_path,
                                                                                             model_scale_factor)


def display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops(pose, gt,
                                                                         model_load_transform, dataset,
                                                                         scene_name, i_th_pose, pose_model,
                                                                         pose_model_points, gt_model,
                                                                         gt_model_points, total_start):


    # check if any of the inputs is None
    if None in [pose, gt]:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose_recentered = pt.pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pt.pose_dict2matrix(gt)

    # all values exist, calculate errors

    # get scene load path
    scene_absolute_load_path = ""
    for scene_idx in range(dataset.get_number_of_scenes()):
        if dataset.get_scene_name(scene_idx) == scene_name:
            scene_absolute_load_path = dataset.get_scene_path(scene_idx)
            break
    if not scene_absolute_load_path:
        return {'average distance': None}

    import pcl
    import time

    # load the scene as a pcl point cloud
    scene = pcl.load_XYZI(scene_absolute_load_path)

    # transform gt-model into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    start = time.time()
    for i in range(0, gt_model.size):
        # 1. recenter model, because we loaded the original model above
        # 2. apply gt-pose of the recentered model to the recentered model
        # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
        # was determined assuming the cameras view-direction was the negative z-axis.
        gt_transformed = pt.concatenate_transforms(R, gt, pt.inverse(model_load_transform))
        gt_model_points[i] = gt_transformed.dot(gt_model_points[i])
    end = time.time()
    print("transforming gt-model time using for loop =", end - start)

    # transform model into pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    start = time.time()
    for i in range(0, pose_model.size):
        # 1. recenter model, because we loaded the original model above
        # 2. apply pose-hypothesis of the recentered model to the recentered model
        pose_transformed = pt.concatenate_transforms(pose, pt.inverse(model_load_transform))
        pose_model_points[i] = pose_transformed.dot(pose_model_points[i])
    end = time.time()
    print("transforming pose-model time using for loop =", end - start)

    # save transformed poses in the original point clouds
    gt_model.from_array(np.array(gt_model_points, dtype='f'))
    pose_model.from_array(np.array(pose_model_points, dtype='f'))

    # calculate distance between model points of gt and pose of recentered models
    start = time.time()
    sum_distance = 0
    for i in range(0, pose_model.size):
        sum_distance += np.linalg.norm(gt_model_points[i] - pose_model_points[i])
    average_distance = sum_distance / pose_model.size
    end = time.time()
    print("averaging time using for loop =", end - start)
    print('average distance =', average_distance)

    total_end = time.time()
    print("time for one error-computation =", total_end - total_start)
    # if (i_th_pose == VISUALIZE_I_TH_POSES):
    #     # visulalize the model poses:
    #     import pcl.pcl_visualization
    #     visual = pcl.pcl_visualization.CloudViewing()
    #     visual.ShowGrayCloud(gt_model, b'gt')
    #     visual.ShowGrayCloud(pose_model, b'pose')
    #     visual.ShowGrayCloud(scene, b'scene')
    #
    #     flag = True
    #     while flag:
    #         flag != visual.WasStopped()
    #     end


def display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized(pose, gt,
                                                                        model_load_transform, dataset,
                                                                        scene_name, i_th_pose, pose_model,
                                                                        pose_model_points, gt_model,
                                                                        gt_model_points, total_start):


    # check if any of the inputs is None
    if None in [pose, gt]:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose_recentered = pt.pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pt.pose_dict2matrix(gt)

    # all values exist, calculate errors

    # get scene load path
    scene_absolute_load_path = ""
    for scene_idx in range(dataset.get_number_of_scenes()):
        if dataset.get_scene_name(scene_idx) == scene_name:
            scene_absolute_load_path = dataset.get_scene_path(scene_idx)
            break
    if not scene_absolute_load_path:
        return {'average distance': None}

    import pcl
    import time

    # load the scene as a pcl point cloud
    scene = pcl.load_XYZI(scene_absolute_load_path)

    # transform gt-model into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    start = time.time()
    gt_transformed = pt.concatenate_transforms(R, gt, pt.inverse(model_load_transform))
    gt_model_points = np.dot(gt_transformed, gt_model_points.T).T
    end = time.time()
    print("transforming gt-model time using vectors =", end - start)

    # transform model into pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    start = time.time()
    pose_transformed = pt.concatenate_transforms(pose, pt.inverse(model_load_transform))
    pose_model_points = np.dot(pose_transformed, pose_model_points.T).T
    end = time.time()
    print("transforming pose-model time using vectors =", end - start)

    # save transformed poses in the original point clouds
    gt_model.from_array(np.array(gt_model_points, dtype='f'))
    pose_model.from_array(np.array(pose_model_points, dtype='f'))

    # calculate distance between model points of gt and pose of recentered models
    start = time.time()
    distance = np.linalg.norm(gt_model_points - pose_model_points, axis=1)
    average_distance = np.mean(distance)
    end = time.time()
    print("averaging time using vectors =", end - start)
    print('average distance =', average_distance)

    total_end = time.time()
    print ("time for one error-computation =", total_end - total_start)
    # if (i_th_pose == VISUALIZE_I_TH_POSES):
    #     # visulalize the model poses:
    #     import pcl.pcl_visualization
    #     visual = pcl.pcl_visualization.CloudViewing()
    #     visual.ShowGrayCloud(gt_model, b'gt')
    #     visual.ShowGrayCloud(pose_model, b'pose')
    #     visual.ShowGrayCloud(scene, b'scene')
    #
    #     flag = True
    #     while flag:
    #         flag != visual.WasStopped()
    #     end


def display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops_all_in_here(pose, gt,
                                                                         model_load_transform, dataset,
                                                                         scene_name, model_absolute_load_path,
                                                                         model_scale_factor):


    # check if any of the inputs is None
    if None in [pose, gt]:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose_recentered = pt.pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pt.pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pt.pose_dict2matrix(model_load_transform)

    # all values exist, calculate errors

    # # get scene load path  !!!!!!! THIS IS NOT NECESSARY FOR ERROR CALCULATION.
    #                        !!!!!!! SCENE ONLY FOR DISPLAYING RESASONS.
    # scene_absolute_load_path = ""
    # for scene_idx in range(dataset.get_number_of_scenes()):
    #     if dataset.get_scene_name(scene_idx) == scene_name:
    #         scene_absolute_load_path = dataset.get_scene_path(scene_idx)
    #         break
    # if not scene_absolute_load_path:
    #     return {'average distance': None}
    #
    # # load the scene as a pcl point cloud
    # scene = pcl.load_XYZI(scene_absolute_load_path)

    # use pcl-python bindings
    import pcl
    import time

    total_start = time.time()

    # load the gt-model as a pcl point cloud
    gt_model = pcl.load_XYZI(model_absolute_load_path)
    # load the model as a pcl point cloud
    pose_model = pcl.load_XYZI(model_absolute_load_path)

    # store the model points in np array of tuples(?) [(x1,y1,z1), (x2, y2,z2), ...]
    gt_model_points = gt_model.to_array()
    pose_model_points = pose_model.to_array()

    # scale gt-model and transform gt-model into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    start = time.time()
    for i in range(0, gt_model.size):
        gt_model_points[i] *= model_scale_factor
        gt_model_points[i][3] = 1
        # 1. recenter model, because we loaded the original model above
        # 2. apply gt-pose of the recentered model to the recentered model
        # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
        # was determined assuming the cameras view-direction was the negative z-axis.
        gt_transformed = pt.concatenate_transforms(R, gt, pt.inverse(model_load_transform))
        gt_model_points[i] = gt_transformed.dot(gt_model_points[i])
    end = time.time()
    print("transforming gt-model time using for loop =", end - start)

    # scale pose-model and transform pose-model into pose-hypothesis
    start = time.time()
    for i in range(0, pose_model.size):
        pose_model_points[i] *= model_scale_factor
        pose_model_points[i][3] = 1
        # 1. recenter model, because we loaded the original model above
        # 2. apply pose-hypothesis of the recentered model to the recentered model
        pose_transformed = pt.concatenate_transforms(pose, pt.inverse(model_load_transform))
        pose_model_points[i] = pose_transformed.dot(pose_model_points[i])
    end = time.time()
    print("transforming pose-model time using for loop =", end - start)

    # save transformed poses in the original point clouds
    gt_model.from_array(gt_model_points)
    pose_model.from_array(pose_model_points)

    # calculate distance between model points of gt and pose of recentered models
    start = time.time()
    sum_distance = 0
    for i in range(0, pose_model.size):
        sum_distance += np.linalg.norm(gt_model_points[i] - pose_model_points[i])
    average_distance = sum_distance / pose_model.size
    end = time.time()
    print("averaging time using for loop =", end - start)
    print('average distance =', average_distance)

    total_end = time.time()
    print("time for one error-computation =", total_end - total_start)
    # if (i_th_pose == VISUALIZE_I_TH_POSES):
    #     # visulalize the model poses:
    #     import pcl.pcl_visualization
    #     visual = pcl.pcl_visualization.CloudViewing()
    #     visual.ShowGrayCloud(gt_model, b'gt')
    #     visual.ShowGrayCloud(pose_model, b'pose')
    #     visual.ShowGrayCloud(scene, b'scene')
    #
    #     flag = True
    #     while flag:
    #         flag != visual.WasStopped()
    #     end


def display_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized_all_in_here(pose, gt,
                                                                        model_load_transform, dataset,
                                                                        model_name, scene_name):


    # check if any of the inputs is None
    if None in [pose, gt]:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose_recentered = pt.pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pt.pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pt.pose_dict2matrix(model_load_transform)


    # all values exist, calculate errors

    # # get scene load path  !!!!!!! THIS IS NOT NECESSARY FOR ERROR CALCULATION.
    #                        !!!!!!! SCENE ONLY FOR DISPLAYING RESASONS.
    #                        !!!!!!! makes stuff really slow
    # scene_absolute_load_path = ""
    # for scene_idx in range(dataset.get_number_of_scenes()):
    #     if dataset.get_scene_name(scene_idx) == scene_name:
    #         scene_absolute_load_path = dataset.get_scene_path(scene_idx)
    #         break
    # if not scene_absolute_load_path:
    #     return {'average distance': None}
    #
    # # load the scene as a pcl point cloud
    # scene = pcl.load_XYZI(scene_absolute_load_path)

    # use pcl-python bindings
    import pcl
    import time

    total_start = time.time()

    # get model load path and model scale factor
    model_absolute_load_path = ""
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_name(model_idx) == model_name:
            model_absolute_load_path = dataset.get_model_path(model_idx)
            break
    if not model_absolute_load_path:
        return {'average distance': None}

    # load the gt-model as a pcl point cloud
    gt_model = pcl.load_XYZI(model_absolute_load_path)
    # load the model as a pcl point cloud
    pose_model = pcl.load_XYZI(model_absolute_load_path)

    # store the model points in np array
    gt_model_points = gt_model.to_array()
    pose_model_points = pose_model.to_array()

    start = time.time()
    # scale model
    pose_model_points = pose_model_points * model_scale_factor
    pose_model_points = pose_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f')
    # scale gt-model
    gt_model_points = gt_model_points * model_scale_factor
    gt_model_points = gt_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f')
    end = time.time()
    print("scaling time using vectors =", end - start)

    # transform gt-model into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    start = time.time()
    gt_transformed = pt.concatenate_transforms(R, gt, pt.inverse(model_load_transform))
    gt_model_points = np.dot(gt_transformed, gt_model_points.T).T
    end = time.time()
    print("transforming gt-model time using vectors =", end - start)

    # transform model into pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    start = time.time()
    pose_transformed = pt.concatenate_transforms(pose, pt.inverse(model_load_transform))
    pose_model_points = np.dot(pose_transformed, pose_model_points.T).T
    end = time.time()
    print("transforming pose-model time using vectors =", end - start)

    # # !!!!!!! THIS IS NOT NECESSARY FOR ERROR CALCULATION.
    #   !!!!!!! ONLY FOR DISPLAYING RESASONS.
    # start = time.time()
    # # save transformed poses in the original point clouds
    # gt_model.from_array(np.array(gt_model_points, dtype='f'))
    # pose_model.from_array(np.array(pose_model_points, dtype='f'))
    # end = time.time()
    # print("copying time for arrays (gt model and pose model) =", end-start)

    # calculate distance between model points of gt and pose of recentered models
    start = time.time()
    distance = np.linalg.norm(gt_model_points - pose_model_points, axis=1)
    average_distance = np.mean(distance)
    end = time.time()
    print("averaging time using vectors =", end - start)
    print('average distance =', average_distance)

    total_end = time.time()
    print ("time for one error-computation =", total_end - total_start)
    #     # visulalize the model poses:
    #     import pcl.pcl_visualization
    #     visual = pcl.pcl_visualization.CloudViewing()
    #     visual.ShowGrayCloud(gt_model, b'gt')
    #     visual.ShowGrayCloud(pose_model, b'pose')
    #     visual.ShowGrayCloud(scene, b'scene')
    #
    #     flag = True
    #     while flag:
    #         flag != visual.WasStopped()
    #     end


if __name__ == '__main__':
    # load data and do post-processing
    print('Loading Experiment-data...')
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    print('Computing pose errors with error-metric from [Hinterstoisser, Lepetit et al. 2013] for all runs...')
    #compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops(data, dataset)
    #compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized(data, dataset)
    compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_vectorized_all_in_display_function(data, dataset)
    #compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_for_loops_all_in_display_function(data, dataset)