#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ####################### script settings ############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_viscon/ICCV_viscon'
BASELINE_FILE_PATH = '~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too
THETA = 0.3 # max error score for error metric of [Hodan et al. 2018]
VISIBILITY_THRESHOLD = 0.1 # occlusion rate of model may not exceed 90%

# first twenty scene indices of ICCV 2015 subset by [Hodan et al. 2018]
SCENE_INDICES = [3, 8, 17, 27, 36, 38, 39, 41, 47, 58, 61, 62, 64, 65, 69, 72, 79, 89, 96, 97]
MODEL_INDICES = [0,1,2,3,4] # only the "unambiguous" models


def bar_by_model(plot_data, ax, legend_title, show_legend=True, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    colors_six_bars = ['k', # black
                       '#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       '#7b1fa2', '#af52d5',  # purple
                       '#795548', '#a98274',  # brown
                       ]
    # use custom colours to match them with colors of 'ICCV_drost_reference_evaluation'
    colors_six_bars = ['#c62828',   # red
                       '#33691e',   # green
                       '#ff5f52',   # lighter red
                       '#629749',   # lighter green
                       ]
    colors_three_bars = ['#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    #print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i], label=bar_names, log=logarithmic_y_axis)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i], label=bar_names, log=logarithmic_y_axis)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def recognition_rate_for_hodans_subset(#data_hinterstoisser,
                                       data_hodan,
                                       #baseline_data_hinterstoisser,
                                       baseline_data_hodan,
                                       ax, show_legend=False, title=""):

    # **********************************************************************************************
    # baseline --- get all data of model-scene combinations where the model is more then 10% visible
    # **********************************************************************************************

    # extract data for Hodan's error metric:
    iDict_baseline_hodan, base_data_baseline_hodan = \
        data_extraction.extract_data(baseline_data_hodan,
                                     [['recognition'], ['poses']],
                                     [['datasetName']])

    # filter runs for which the model-visibility is too low
    filtered_base_data_baseline_hodan = []
    run_ids = []
    number_of_scnenes_where_model_visible = {dataset.get_model_name(i): 0 for i in MODEL_INDICES}
    for run_id, run_data in enumerate(base_data_baseline_hodan):
        # get error dict of first pose of model in this run
        # because visib.frac. is the same for all poses
        error_dict = run_data[iDict_baseline_hodan['poses']][0]['error']
        # get scene index from scene name
        scene_name = run_data[iDict_baseline_hodan['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if error_dict['visibility fraction of GT'] >= VISIBILITY_THRESHOLD and scene_idx in SCENE_INDICES:
            filtered_base_data_baseline_hodan.append(run_data)
            run_ids.append(run_id)
            number_of_scnenes_where_model_visible[run_data[iDict_baseline_hodan['model']]] += 1

    print("\n(baseline performance) (Hodan's subset) number of scenes in which the models are visible:",
          number_of_scnenes_where_model_visible)

    # # extract data for Hinterstoisser's error metric:
    # iDict_baseline_hinterstoisser, base_data_baseline_hinterstoisser = data_extraction.extract_data(
    #                                                                 baseline_data_hinterstoisser,
    #                                                                 [['recognition']],
    #                                                                 [['datasetName']])
    #
    # # filter runs for which the model-visibility is too low
    # filtered_base_data_baseline_hinterstoisser = []
    # for run_id, run_data in enumerate(base_data_baseline_hinterstoisser):
    #     # since visibility fract info is not available in hinterstoisser-error-dict,
    #     # we simply select the same runs that were chosen for filtered_base_data_hodan
    #     if run_id in run_ids:
    #         filtered_base_data_baseline_hinterstoisser.append(run_data)

    # **********************************************************************************************
    # VisCon --- get all data of model-scene combinations where the model is more then 10% visible
    # **********************************************************************************************

    # extract data for Hodan's error metric:
    iDict_hodan, base_data_hodan = data_extraction.extract_data(data_hodan,
                                                                [['recognition'],['poses']],
                                                                [['datasetName']])

    # filter runs for which the model-visibility is too low
    filtered_base_data_hodan = []
    run_ids =[]
    number_of_scnenes_where_model_visible = {dataset.get_model_name(i):0 for i in MODEL_INDICES}
    for run_id, run_data in enumerate(base_data_hodan):
        # get error dict of first pose of model in this run
        # because visib.frac. is the same for all poses
        error_dict = run_data[iDict_hodan['poses']][0]['error']
        # get scene index from scene name
        scene_name = run_data[iDict_hodan['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if error_dict['visibility fraction of GT'] >= VISIBILITY_THRESHOLD and scene_idx in SCENE_INDICES:
            filtered_base_data_hodan.append(run_data)
            run_ids.append(run_id)
            number_of_scnenes_where_model_visible[run_data[iDict_hodan['model']]] += 1

    print("(Hodan's subset) number of scenes in which the models are visible:", number_of_scnenes_where_model_visible)

    # # extract data for Hinterstoisser's error metric:
    # iDict_hinterstoisser, base_data_hinterstoisser = data_extraction.extract_data(data_hinterstoisser,
    #                                                                               [['recognition'],
    #                                                                                ['poses']],
    #                                                                               [['datasetName']])
    #
    # # filter runs for which the model-visibility is too low
    # filtered_base_data_hinterstoisser = []
    # for run_id, run_data in enumerate(base_data_hinterstoisser):
    #     # since visibility fract info is not available in hinterstoisser-error-dict,
    #     # we simply select the same runs that were chosen for filtered_base_data_hodan
    #     if run_id in run_ids:
    #         filtered_base_data_hinterstoisser.append(run_data)

    # **********************************************************************************************
    # baseline --- calculate recognition rates
    # **********************************************************************************************

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_baseline_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_baseline_hodan,
                                                   [iDict_baseline_hodan['datasetName'],
                                                    iDict_baseline_hodan['model'],
                                                    iDict_baseline_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_baseline_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_baseline_hodan,
                                                   [iDict_baseline_hodan['datasetName'],
                                                    iDict_baseline_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # # recognition rate for each model of the datasets:
    # recognition_rate_per_model_with_baseline_hinterstoisser_error = \
    #     data_extraction.nest_and_process_by_values(filtered_base_data_baseline_hinterstoisser,
    #                                                [iDict_baseline_hinterstoisser['datasetName'],
    #                                                 iDict_baseline_hinterstoisser['model'],
    #                                                 iDict_baseline_hinterstoisser['recognition']],
    #                                                data_extraction.recognition_rate_best_poses)
    #
    # # recognition rate for complete dataset, not for each model of the datasets:
    # recognition_rate_with_with_baseline_hinterstoisser_error = \
    #     data_extraction.nest_and_process_by_values(filtered_base_data_baseline_hinterstoisser,
    #                                                [iDict_baseline_hinterstoisser['datasetName'],
    #                                                 iDict_baseline_hinterstoisser['recognition']],
    #                                                data_extraction.recognition_rate_best_poses)

    print("(baseline performance) Recognition rates for first twenty scenes of subset of [Hodan et al. 2018] (scenes where model is almost invisible are sorted out):")
    #print("(baseline performance) Hinterstoisser's error metric:",recognition_rate_with_with_baseline_hinterstoisser_error)
    print("(baseline performance) Hodan's error metric:", recognition_rate_with_with_baseline_hodan_error,"\n")

    # **********************************************************************************************
    # VisCon --- calculate recognition rates
    # **********************************************************************************************

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['model'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # # recognition rate for each model of the datasets:
    # recognition_rate_per_model_with_hinterstoisser_error = \
    #     data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
    #                                                [iDict_hinterstoisser['datasetName'],
    #                                                 iDict_hinterstoisser['model'],
    #                                                 iDict_hinterstoisser['poses']],
    #                                                data_extraction.recognition_rate_best_poses)
    #
    # # recognition rate for complete dataset, not for each model of the datasets:
    # recognition_rate_with_with_hinterstoisser_error = \
    #     data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
    #                                                [iDict_hinterstoisser['datasetName'],
    #                                                 iDict_hinterstoisser['poses']],
    #                                                data_extraction.recognition_rate_best_poses)

    print("(VisCon performance) Recognition rates for first twenty scenes of subset of [Hodan et al. 2018] (scenes where model is almost invisible are sorted out):")
    #print("Hinterstoisser's error metric:",recognition_rate_with_with_hinterstoisser_error)
    print("(VisCon performance) Hodan's error metric:", recognition_rate_with_with_hodan_error,"\n")

    # **********************************************************************************************
    # do plotting
    # **********************************************************************************************

    #print(recognition_rate_per_model_with_hodan_error)
    recognition_rate_per_model = [#["baseline - \\textsc{Hinterstoisser}'s error metric",
                                  # recognition_rate_per_model_with_baseline_hinterstoisser_error[0][1]],
                                  ["\\textsc{Kroischke}'s S2S - \\textsc{Hodan}'s error metric",
                                   recognition_rate_per_model_with_baseline_hodan_error[0][1]],
                                  #["\\textsc{Hinterstoisser}'s error metric",
                                  # recognition_rate_per_model_with_hinterstoisser_error[0][1]],
                                  ["\\textsc{Ziegler}'s S2SVisCon - \\textsc{Hodan}'s error metric",
                                   recognition_rate_per_model_with_hodan_error[0][1]]]
    print("Model-specific recognition rates:")
    for x in recognition_rate_per_model:
        print(str(x))
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'matching modes', show_legend=show_legend,
                 show_top_ticks=False)
    #ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc='upper center')


def plot_average_matching_time(baseline_data, data, ax, show_legend):
    """
    Plot the average matching time and compare Hinterstoisser's extensions with Kroischkes system

    Note: The time that the verifier needs to verify all poses is neglected here.
          It should not exceed a few seconds, Krone reports a few hundred ms. Therefore
          it is really small compared to the matching times.
    :param baseline_data: for comparison
    :param data:
    :param ax: ax of figure
    :param show_legend: boolean to toggle legend
    :return:
    """

    # ******************************************************************************************************************
    # baseline --- get average matching times
    # ******************************************************************************************************************

    iDict, basic_data = \
        data_extraction.extract_data(baseline_data,
                                     [['statistics', '/matcher']],
                                     [['datasetName']])

    average_match_time_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('\n(baseline performance) average matching time for complete dataset:')
    for dataset, mt_by_dataset in average_match_time_baseline:
        print('Dataset: %s, time: %f ms' % (dataset, mt_by_dataset))

    average_match_time_per_model_baseline = \
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average_match_time_per_model_baseline = ', average_match_time_per_model_baseline)

    # filter runs for the first twenty scenes of Hodan's subset
    filtered_basic_data_hodan = []
    run_ids = []
    for run_id, run_data in enumerate(basic_data):
        # get scene index from scene name
        scene_name = run_data[iDict['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if scene_idx in SCENE_INDICES:
            filtered_basic_data_hodan.append(run_data)
            run_ids.append(run_id)

    average_match_time_baseline_hodan = \
        data_extraction.nest_and_process_by_values(filtered_basic_data_hodan,
                                                   [iDict['datasetName'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('\n(baseline performance) average matching time for first twenty scenes of Hodans subset:')
    for dataset, mt_by_dataset in average_match_time_baseline_hodan:
        print('Dataset: %s, time: %f ms' % (dataset, mt_by_dataset))

    average_match_time_per_model_baseline_hodan = \
        data_extraction.nest_and_process_by_values(filtered_basic_data_hodan,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average_match_time_per_model_baseline = ', average_match_time_per_model_baseline_hodan)

    # ******************************************************************************************************************
    # VisCon --- get average matching times
    # ******************************************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [['datasetName']]) # dataset is only extracted, to enable use of 'nest_and_process_by_values'

    average_match_time =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('(VisCon performance) average matching time for first twenty scenes of Hodans subset:')
    for dataset, mt_by_dataset in average_match_time:
        print('Dataset: %s, time: %f ms' % (dataset, mt_by_dataset))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per model: ',average_match_time_per_model)

    # **********************************************************************************************
    # do plotting
    # **********************************************************************************************

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    # format data for plotting
    plot_data =[[ "\\textsc{Kroischke}'s S2S",
                  average_match_time_per_model_baseline[0][1] ],
                [ "\\textsc{Ziegler}'s S2SVisCon",
                  average_match_time_per_model[0][1] ]]
    #print("plot_data =", plot_data)

    # plot and plot-settings
    bar_by_model(plot_data, ax, legend_title='matching modes', show_legend=show_legend,
                 show_top_ticks=False, logarithmic_y_axis=False)
    ax.set_ylim(1,200000)
    ax.set_yticks([y for y in range(0,200001,1000*30)])
    scaled_labels = [round(float(x) / 1000 / 60,2) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in min')
    if show_legend:
        ax.legend(loc='upper left')


def print_average_verification_time(data):
    """
    calculate average verification time
    not plotting it, because it's really small difference compared to the matching time.

    :param baseline_data: for comparison
    :param data:
    :param ax: ax of figure
    :param show_legend: boolean to toggle legend
    :return:
    """

    # ******************************************************************************************************************
    # with Hinterstoisser extensions and tuned verifier --- get average matching + verification times
    # ******************************************************************************************************************

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/verifier']],
                                     [['datasetName']]) # dataset is only extracted, to enable use of 'nest_and_process_by_values'

    average_match_time =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)

    print('average verification time for complete dataset:')
    for dataset, mt_by_dataset in average_match_time:
        print('Dataset: %s, time: %f ms' % (dataset, mt_by_dataset))

    average_match_time_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['datasetName'],
                                                    iDict['model'],
                                                    iDict['/verifier']],
                                                   data_extraction.average_mu_time)
    print('average verification time per model: ',average_match_time_per_model)


if __name__ == '__main__':

    # load data and do post-processing
    #data_hinterstoisser_error_metric = ExperimentDataStructure()
    data_hodan_error_metric = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    #compute_pose_errors_is_necessary_hinterstoisser = False
    compute_pose_errors_is_necessary_hodan = False

    # print("Loading Experiment-data for [Hinterstoisser et al. 2014]-error-metric")
    # if compute_pose_errors_is_necessary_hinterstoisser:
    #     data_hinterstoisser_error_metric.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")
    #
    #     print("Computing pose errors with [Hinterstoisser et al. 2014]-error-metric")
    #     # compute pose errors with Hinterstoisser's error metric
    #     post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(
    #         data_hinterstoisser_error_metric, dataset)
    #     # check pose-error. add tp's, fp's, unrecognized gt's
    #     # ATTENTION! does not consider verifier-node results!
    #     post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(
    #         data_hinterstoisser_error_metric, max_translation_error_relative)
    #
    #     data_hinterstoisser_error_metric.to_pickle(
    #         FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
    #     #data_hinterstoisser_error_metric.to_yaml(
    #     #    FILE_PATH_WO_EXTENSION + '_with_computed_errors.yaml')
    # else:
    #     data_hinterstoisser_error_metric.from_pickle(
    #         FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")

    print("Loading Experiment-data for [Hodan et al. 2018]-error-metric")
    if compute_pose_errors_is_necessary_hodan:
        data_hodan_error_metric.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

        print("Computing pose errors with [Hodan et al. 2018]-error-metric")
        # compute pose errors with Hodan's error metric
        post_processing.compute_pose_errors_by_hodan_et_al_2018(
            data_hodan_error_metric, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        # ATTENTION! does not consider verifier-node results!
        post_processing.compute_recognition_with_hodan_et_al_2018(
            data_hodan_error_metric, THETA)

        data_hodan_error_metric.to_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors_hodan.bin")
        #data_hodan_error_metric.to_yaml(
        #    FILE_PATH_WO_EXTENSION + '_with_computed_errors_hodan.yaml')
    else:
        data_hodan_error_metric.from_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors_hodan.bin")

    # load baseline-data for comparison
    # note: errors and recognition numbers have to be already calculated
    #baseline_data_hinterstoisser_error_metric = ExperimentDataStructure()
    baseline_data_hodan_error_metric = ExperimentDataStructure()
    # print("Loading Baseline-data for [Hinterstoisser et al. 2014]-error-metric")
    # baseline_data_hinterstoisser_error_metric.from_pickle(
    #     BASELINE_FILE_PATH + "_with_computed_errors.bin")
    print("Loading Baseline-data for [Hodan et al. 2018]-error-metric")
    baseline_data_hodan_error_metric.from_pickle(
        BASELINE_FILE_PATH + "_with_computed_errors_hodan.bin")

    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)


    print("Plot recognition rates with the error-metrics:\n"
          " - [Hodan et al. 2018]\n"
          #" - [Hinterstoisser et al. 2014]\n"
          "for the datasets:\n"
          #" - complete ICCV 2015 dataset\n"
          " - first twenty scenes of ICCV 2015 subset by [Hodan et al 2018]\n")
    # plot recognition rates for different subsets of the ICCV 2015 dataset, to
    # compare the [Hinterstoisser et al. 2014] with the [Hodan et al. 2018] error metric

    # using only verificationWeight to determine best pose.
    # i.e. not using mu_v and mu_p values produced by verifier
    #post_processing.compute_verified(data_hodan_error_metric)
    #post_processing.compute_verified(data_hinterstoisser_error_metric)
    post_processing.compute_recognition_with_hodan_et_al_2018(data_hodan_error_metric, THETA)

    #fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5), sharey=True)
    #fig_rr.subplots_adjust(bottom=0.2)
    # recognition_rate_for_complete_dataset(data_hinterstoisser_error_metric,
    #                                       data_hodan_error_metric,
    #                                       baseline_data_hinterstoisser_error_metric,
    #                                       baseline_data_hodan_error_metric,
    #                                       fig_rr.axes[0],
    #                                       show_legend=False,
    #                                       title="Complete ICCV 2015 dataset")
    fig_rr, axes = plt.subplots(1,1, figsize = plotting.cm2inch(8.5 + 5, 5.8))
    fig_rr.subplots_adjust(right=0.4)
    recognition_rate_for_hodans_subset(#data_hinterstoisser_error_metric,
                                       data_hodan_error_metric,
                                       #baseline_data_hinterstoisser_error_metric,
                                       baseline_data_hodan_error_metric,
                                       fig_rr.axes[0],
                                       show_legend=False,
                                       title="First twenty scenes of ICCV 2015 subset by [Hodan et al. 2018]")
    # fig_rr.axes[0].legend(bbox_to_anchor=(0.5, 0.00),
    #                       bbox_transform=fig_rr.transFigure,
    #                       loc='lower center', borderaxespad=0., title='matching modes',
    #                       ncol=2)
    # fig_rr.tight_layout(rect=[0, 0.2, 1, 1])
    fig_rr.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                          title='matching modes', bbox_transform=fig_rr.transFigure)
    fig_rr.tight_layout(rect=[0, 0, 0.6, 1])


    print("Plot average matching time per model:")
    # plot average matching time:
    fig_mt, axes = plt.subplots(1, 1, figsize = plotting.cm2inch(8.5 + 5, 5.8))
    fig_mt.subplots_adjust(right=0.4)
    plot_average_matching_time(baseline_data_hodan_error_metric,
                               data_hodan_error_metric,
                               fig_mt.axes[0],
                               show_legend=True)
    fig_mt.axes[0].legend(bbox_to_anchor=(0.6, 0.5), loc='center left', borderaxespad=0.,
                        title='matching modes', bbox_transform=fig_mt.transFigure)
    fig_mt.tight_layout(rect=[0, 0, 0.6, 1])

    # print("\n( Applying V and P thresholds now! ) Plot recognition rates with the error-metrics:\n"
    #       " - [Hodan et al. 2018]\n"
    #       " - [Hinterstoisser et al. 2014]\n"
    #       "for the datasets:\n"
    #       " - complete ICCV 2015 dataset\n"
    #       " - ICCV 2015 subset by [Hodan et al 2018]\n")
    # # plot recognition rates for different subsets of the ICCV 2015 dataset, to
    # # compare the [Hinterstoisser et al. 2014] with the [Hodan et al. 2018] error metric
    #
    # # using verificationWeight to determine best pose AND
    # # applying also V and P thresholds to sort out false pose-hypotheses
    # post_processing.assign_mu_to_poses(data_hodan_error_metric)
    # post_processing.compute_verified_through_mu(data_hodan_error_metric, 0.01, 0.2)
    #
    # post_processing.assign_mu_to_poses(data_hinterstoisser_error_metric)
    # post_processing.compute_verified_through_mu(data_hinterstoisser_error_metric, 0.01, 0.2)
    #
    # fig_rr_verified, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 7.5), sharey=True)
    # fig_rr_verified.subplots_adjust(bottom=0.2)
    # recognition_rate_for_complete_dataset(data_hinterstoisser_error_metric,
    #                                       data_hodan_error_metric,
    #                                       baseline_data_hinterstoisser_error_metric,
    #                                       baseline_data_hodan_error_metric,
    #                                       fig_rr_verified.axes[0],
    #                                       show_legend=False,
    #                                       title="Complete ICCV 2015 dataset")
    # recognition_rate_for_hodans_subset(data_hinterstoisser_error_metric,
    #                                    data_hodan_error_metric,
    #                                    baseline_data_hinterstoisser_error_metric,
    #                                    baseline_data_hodan_error_metric,
    #                                    fig_rr_verified.axes[1],
    #                                    show_legend=False,
    #                                    title="Subset by [Hodan et al. 2018]")
    # fig_rr_verified.axes[0].legend(bbox_to_anchor=(0.5, 0.00),
    #                                bbox_transform=fig_rr_verified.transFigure,
    #                                loc='lower center', borderaxespad=0., title='matching modes',
    #                                ncol=2)
    # fig_rr_verified.tight_layout(rect=[0, 0.2, 1, 1])
    #
    # # print the average matching+verification time in the console. not plotting it, because
    # # there is no visually recognizable difference to the sole matching time.
    # print_average_verification_time(data_hinterstoisser_error_metric)

    # save plots:
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates_barplot')
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_barplot')
        #plotting.save_figure(fig_rr_verified, plt_dir, 'recognitionrates_using_verification_by_V_and_P_for_different_metrics_and_subsets_barplot')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
