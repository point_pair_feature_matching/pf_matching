#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *
from re import search
from bzrlib.conflicts import Conflict

input = raw_input
range = xrange

# project
from math import floor, ceil, pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting
from pf_matching.experiments.base_experiments import ExperimentBase
import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH_WO_EXTENSION = '~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference'
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_PLOTS = True
SHOW_PLOTS = False

max_translation_error_relative = 0.1 # 10% of the model diameter is used by [Krull et al 2015] too
THETA = 0.3 # max error score for error metric of [Hodan et al. 2018]

# scene indices of ICCV 2015 subset by [Hodan et al. 2018]
SCENE_INDICES = [3, 8, 17, 27, 36, 38, 39, 41, 47, 58, 61, 62, 64, 65, 69, 72, 79, 89, 96, 97, 102,
                 107, 110, 115, 119, 124, 126, 136, 153, 156, 162, 166, 175, 176, 178, 203, 207,
                 217, 219, 221, 224, 243, 248, 249, 254, 258, 263, 266, 268, 277, 283, 307, 310,
                 322, 326, 338, 342, 356, 362, 365, 368, 387, 389, 402, 415, 417, 425, 428, 434,
                 435, 438, 442, 446, 453, 473, 474, 476, 477, 480, 491, 494, 499, 501, 503, 521,
                 527, 529, 532, 535, 540, 543, 549, 560, 563, 571, 575, 589, 603, 607, 611, 615,
                 625, 642, 648, 649, 650, 652, 667, 669, 679, 691, 695, 703, 708, 711, 727, 736,
                 737, 739, 740, 750, 754, 756, 757, 758, 761, 762, 764, 768, 769, 770, 773, 775,
                 785, 788, 791, 794, 801, 803, 804, 808, 809, 819, 821, 828, 837, 840, 844, 850,
                 856, 867, 871, 877, 883, 886, 894, 902, 903, 904, 907, 909, 918, 925, 934, 942,
                 952, 956, 961, 968, 969, 972, 982, 984, 991, 1001, 1012, 1038, 1050, 1057, 1061,
                 1069, 1071, 1087, 1098, 1099, 1103, 1107, 1117, 1123, 1131, 1144, 1148, 1151, 1157,
                 1168, 1169, 1176, 1180, 1199, 1212]
MODEL_INDICES = [0,1,2,3,4] # only the "unambiguous" models

def bar_by_model(plot_data, ax, legend_title, show_legend=False, group_name_replacements=('_', '\n'),
        logarithmic_y_axis=False, show_top_ticks=True):
    """
    make a bar plot
    @param plot_data: nested data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    """

    # hard-coded settings
    bar_width_six_bars = 0.1
    bar_width_three_bars = 0.2
    #colors_six_bars = ['#d50000', '#e04040', '#1b5e20', '#548658', '#1a237e', '#373f8e', '#ffb300',
    #                   '#ffc640']
    #colors_three_bars = ['#d50000', '#1b5e20', '#1a237e', '#ffb300']
    colors_six_bars = ['#c62828', '#ff5f52',  # red
                       '#33691e', '#629749',  # green
                       '#1a237e', '#534bae',  # blue
                       '#ffb300', '#ffe54c',  # yellow
                       ]
    colors_three_bars = ['k', '#c62828', '#33691e', '#1a237e', '#ffb300']

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # print("plot_data =", plot_data)

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # get number of bars per model, to enable switching between colors-modes and bar-widths
    bars_per_group = len(plot_data)
    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):

        # replace group name with plot location
        if bars_per_group <= 3:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_three_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_three_bars, color=colors_three_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)
        else:
            plt_tuples = [(group_dict[entry[0]] + i * bar_width_six_bars, entry[1]) for entry in
                          group_data]
            ax.bar(*zip(*plt_tuples), width=bar_width_six_bars, color=colors_six_bars[i],
                   label=bar_names, log=logarithmic_y_axis, linewidth=0.5)

    # set group names under bars
    ax.set_xlim(0)
    if bars_per_group <= 3:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_three_bars)
    else:
        ax.set_xticks(group_loc + n_bars / 2 * bar_width_six_bars)
    group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=7)
    ax.tick_params(axis='x', top=show_top_ticks)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')


def recognition_rate_for_complete_dataset(data_hinterstoisser, data_hodan, ax, show_legend=False,
                                          title=""):

    # **********************************************************************************************
    # get all data of model-scene combinations where the model is more then 10% visible
    # **********************************************************************************************

    # extract data for Hodan's error metric:
    iDict_hodan, base_data_hodan = data_extraction.extract_data(data_hodan,
                                                                [['recognition'],['poses']],
                                                                [['datasetName']])

    # filter runs for which the model-visibility is too low
    filtered_base_data_hodan = []
    run_ids =[]
    number_of_scnenes_where_model_visible = {dataset.get_model_name(i):0 for i in MODEL_INDICES}
    for run_id, run_data in enumerate(base_data_hodan):
        # get error dict of first pose of model in this run
        # because visib.frac. is the same for all poses
        error_dict = run_data[iDict_hodan['poses']][0]['error']
        if error_dict['visibility fraction of GT'] >= 0.1:
            filtered_base_data_hodan.append(run_data)
            run_ids.append(run_id)
            number_of_scnenes_where_model_visible[run_data[iDict_hodan['model']]] += 1

    print("number of scenes in which the models are visible:", number_of_scnenes_where_model_visible)

    # extract data for Hinterstoisser's error metric:
    iDict_hinterstoisser, base_data_hinterstoisser = data_extraction.extract_data(data_hinterstoisser,
                                                                                  [['recognition']],
                                                                                  [['datasetName']])

    # filter runs for which the model-visibility is too low
    filtered_base_data_hinterstoisser = []
    for run_id, run_data in enumerate(base_data_hinterstoisser):
        # since visibility fract info is not available in hinterstoisser-error-dict,
        # we simply select the same runs that were chosen for filtered_base_data_hodan
        if run_id in run_ids:
            filtered_base_data_hinterstoisser.append(run_data)

    # **********************************************************************************************
    # calculate recognition rates
    # **********************************************************************************************

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['model'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['model'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print("Recognition rates for complete dataset (scenes where model is invisible are sorted out):")
    print("Hinterstoisser's error metric:",recognition_rate_with_with_hinterstoisser_error)
    print("Hodan's error metric:", recognition_rate_with_with_hodan_error,"\n")

    # recognition rate for each model of the datasets:
    unfiltered_recognition_rate_per_model_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['model'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    unfiltered_recognition_rate_with_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print("Recognition rates for complete dataset (all scenes):")
    print("Hinterstoisser's error metric:", unfiltered_recognition_rate_with_with_hinterstoisser_error,"\n")

    # **********************************************************************************************
    # do plotting
    # **********************************************************************************************

    #print(recognition_rate_per_model_with_hodan_error)
    recognition_rate_per_model = [["Hinterstoisser's error metric\nwithout occlusion filter",
                                   unfiltered_recognition_rate_per_model_with_hinterstoisser_error[0][1]],
                                  ["Hinterstoisser's error metric",
                                   recognition_rate_per_model_with_hinterstoisser_error[0][1]],
                                  ["Hodan's error metric",
                                   recognition_rate_per_model_with_hodan_error[0][1]]]
    #print(recognition_rate_per_model)
    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'error metrics', show_legend=show_legend,
                 show_top_ticks=False)
    ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc='upper right')


def recognition_rate_for_hodans_subset(data_hinterstoisser, data_hodan, ax, show_legend=False,
                                       title=""):

    # **********************************************************************************************
    # get all data of model-scene combinations where the model is more then 10% visible
    # **********************************************************************************************

    # extract data for Hodan's error metric:
    iDict_hodan, base_data_hodan = data_extraction.extract_data(data_hodan,
                                                                [['recognition'], ['poses']],
                                                                [['datasetName']])

    # filter runs for which the model-visibility is too low
    # get only the scenes of Hodan's subset
    filtered_base_data_hodan = []
    run_ids = []
    for run_id, run_data in enumerate(base_data_hodan):
        # get error dict of first pose of model in this run
        # because visib.frac. is the same for all poses
        error_dict = run_data[iDict_hodan['poses']][0]['error']
        # get scene index from scene name
        scene_name = run_data[iDict_hodan['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if error_dict['visibility fraction of GT'] >= 0.1 and scene_idx in SCENE_INDICES:
            filtered_base_data_hodan.append(run_data)
            run_ids.append(run_id)

    # extract data for Hinterstoisser's error metric:
    iDict_hinterstoisser, base_data_hinterstoisser = data_extraction.extract_data(data_hinterstoisser,
                                                                                  [['recognition']],
                                                                                  [['datasetName']])

    # filter runs for which the model-visibility is too low
    # get only the scenes of Hodan's subset
    filtered_base_data_hinterstoisser = []
    for run_id, run_data in enumerate(base_data_hinterstoisser):
        # since visibility fract info is not available in hinterstoisser-error-dict,
        # we simply select the same runs that were chosen for filtered_base_data_hodan
        if run_id in run_ids:
            filtered_base_data_hinterstoisser.append(run_data)

    # get only the scenes of Hodan's subset, do not filter by model-visibility
    filtered_base_data_hinterstoisser_wo_occlusion_filter = []
    for run_id, run_data in enumerate(base_data_hinterstoisser):
        # get scene index from scene name
        scene_name = run_data[iDict_hinterstoisser['scene']]
        scene_idx = int(scene_name.lstrip('depth_'))
        if scene_idx in SCENE_INDICES:
            filtered_base_data_hinterstoisser_wo_occlusion_filter.append(run_data)

    # **********************************************************************************************
    # calculate recognition rates
    # **********************************************************************************************

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['model'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hodan_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hodan,
                                                   [iDict_hodan['datasetName'],
                                                    iDict_hodan['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['model'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hinterstoisser_error = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print("Recognition rates for subset of [Hodan et al. 2018] (scenes where model is invisible are sorted out):")
    print("Hinterstoisser's error metric:", recognition_rate_with_with_hinterstoisser_error)
    print("Hodan's error metric:", recognition_rate_with_with_hodan_error,"\n")

    # recognition rate for each model of the datasets:
    recognition_rate_per_model_with_hinterstoisser_error_wo_occlusion_filter = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser_wo_occlusion_filter,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['model'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    # recognition rate for complete dataset, not for each model of the datasets:
    recognition_rate_with_with_hinterstoisser_error_wo_occlusion_filter = \
        data_extraction.nest_and_process_by_values(filtered_base_data_hinterstoisser_wo_occlusion_filter,
                                                   [iDict_hinterstoisser['datasetName'],
                                                    iDict_hinterstoisser['recognition']],
                                                   data_extraction.recognition_rate_best_poses)

    print("Recognition rates for subset of [Hodan et al. 2018] (scenes where model is invisible kept too!):")
    print("Hinterstoisser's error metric:", recognition_rate_with_with_hinterstoisser_error_wo_occlusion_filter)

    # **********************************************************************************************
    # do plotting
    # **********************************************************************************************

    recognition_rate_per_model = [["\\textsc{Hinterstoisser}'s error metric\n without occlusion filter",
                                   recognition_rate_per_model_with_hinterstoisser_error_wo_occlusion_filter[0][1]],
                                  ["\\textsc{Hinterstoisser}'s error metric",
                                   recognition_rate_per_model_with_hinterstoisser_error[0][1]],
                                  ["\\textsc{Hodan}'s error metric",
                                   recognition_rate_per_model_with_hodan_error[0][1]]]

    print("\n(hodan's subset) model-specific recognition rates:")
    for entry in recognition_rate_per_model:
        print(entry)

    # plot best recognition rate for each model:
    bar_by_model(recognition_rate_per_model, ax, 'error metrics', show_legend=show_legend,
                 show_top_ticks=False)
    #ax.set_ylabel('recognition rate (one instance)')
    ax.set_ylim(0, 1.0)
    ax.set_title(title)
    if show_legend:
        ax.legend(loc='upper right')


def plot_average_matching_time(data, ax, show_legend):
    """

    @type data: ExperimentDataStructure
    @parameter data: experiment data
    @type ax: Axes
    @return: ax-object the function plotted in
    """

    iDict, basic_data =\
        data_extraction.extract_data(data,
                                     [['statistics', '/matcher']],
                                     [["dynamicParameters", "/matcher",
                                       "d_dist"],
                                      ["dynamicParameters", "/matcher",
                                       "refPointStep"]])

    average_match_time_per_threshold =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)

    print('average matching time:')
    for d_dist, mt_by_d_dist in average_match_time_per_threshold:
        for refPointStep, mt_by_refPointStep in mt_by_d_dist:
            print('d_dist: %s refPointStep = %s, time: %f ms' % (d_dist, refPointStep, mt_by_refPointStep))

    average_match_time_per_threshold_per_model =\
        data_extraction.nest_and_process_by_values(basic_data,
                                                   [iDict['d_dist'],
                                                    iDict['refPointStep'],
                                                    iDict['model'],
                                                    iDict['/matcher']],
                                                   data_extraction.average_match_time)
    print('average matching time per model: ',
          average_match_time_per_threshold_per_model[0][1][0][1])

    # plot settings
    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)
    colors_rr = ['#c62828', '#33691e', '#1a237e', '#ffb300']
    markers_rr = ['x', '+', 'v', '^']
    markers_rr_details = ['x', '+', 'v', '^']

    # format data for plotting
    plot_data =[[ "\\textsc{Kroischke}'s S2S-Matching ", average_match_time_per_threshold_per_model[0][1][0][1] ]]

    # plot and plot-settings
    bar_by_model(plot_data, ax, legend_title='',
                 show_legend=True, show_top_ticks=False)
    ax.set_ylim(0,60000)
    ax.set_yticks([y for y in range(0,60001,1000*10)])
    scaled_labels = [round(float(x) / 1000 / 60,2) for x in ax.get_yticks()]
    ax.set_yticklabels(scaled_labels)
    ax.set_ylabel('average matching time in min')
    ax.legend(loc='upper left')


if __name__ == '__main__':

    # load data and do post-processing
    data_hinterstoisser_error_metric = ExperimentDataStructure()
    data_hodan_error_metric = ExperimentDataStructure()
    dataset = ExperimentBase.load_dataset(DATASET_PATH)

    compute_pose_errors_is_necessary_hinterstoisser = False
    compute_pose_errors_is_necessary_hodan = False

    print("Loading Experiment-data for [Hinterstoisser et al. 2014]-error-metric")
    if compute_pose_errors_is_necessary_hinterstoisser:
        data_hinterstoisser_error_metric.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

        print("Computing pose errors with [Hinterstoisser et al. 2014]-error-metric")
        # compute pose errors with Hinterstoisser's error metric
        post_processing.compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(
            data_hinterstoisser_error_metric, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hinterstoisser_lepetit_2013(
            data_hinterstoisser_error_metric, max_translation_error_relative)

        data_hinterstoisser_error_metric.to_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")
        #data_hinterstoisser_error_metric.to_yaml(
        #    FILE_PATH_WO_EXTENSION + '_with_computed_errors.yaml')
    else:
        data_hinterstoisser_error_metric.from_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors.bin")

    print("Loading Experiment-data for [Hodan et al. 2018]-error-metric")
    if compute_pose_errors_is_necessary_hodan:
        data_hodan_error_metric.from_pickle(FILE_PATH_WO_EXTENSION + ".bin")

        print("Computing pose errors with [Hodan et al. 2018]-error-metric")
        # compute pose errors with Hodan's error metric
        post_processing.compute_pose_errors_by_hodan_et_al_2018(
            data_hodan_error_metric, dataset)
        # check if pose-error. add tp's, fp's, unrecognized gt's
        post_processing.compute_recognition_with_hodan_et_al_2018(
            data_hodan_error_metric, THETA)

        data_hodan_error_metric.to_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors_hodan.bin")
        #data_hodan_error_metric.to_yaml(
        #    FILE_PATH_WO_EXTENSION + '_with_computed_errors_hodan.yaml')
    else:
        data_hodan_error_metric.from_pickle(
            FILE_PATH_WO_EXTENSION + "_with_computed_errors_hodan.bin")

    mpl.rcParams.update(plotting.ziegler_thesis_default_settings)

    print("Plot recognition rates with the error-metrics:\n"
          " - [Hodan et al. 2018]\n"
          " - [Hinterstoisser et al. 2014]\n"
          "for the datasets:\n"
          " - complete ICCV 2015 dataset\n"
          " - ICCV 2015 subset by [Hodan et al 2018]\n")
    # plot recognition rates for different subsets of the ICCV 2015 dataset, to
    # compare the [Hinterstoisser et al. 2014] with the [Hodan et al. 2018] error metric
    fig_rr, axes = plt.subplots(1, 2, figsize=plotting.cm2inch(15, 6), sharey=True)
    recognition_rate_for_complete_dataset(data_hinterstoisser_error_metric,
                                          data_hodan_error_metric,
                                          fig_rr.axes[0],
                                          show_legend=False,
                                          title="Complete ICCV 2015 dataset")
    recognition_rate_for_hodans_subset(data_hinterstoisser_error_metric,
                                       data_hodan_error_metric,
                                       fig_rr.axes[1],
                                       show_legend=True,
                                       title="Subset by [Hodan et al. 2018]")
    fig_rr.tight_layout()

    print("Plot average matching time per model:")
    # plot average matching time:
    fig_mt, axes = plt.subplots(1, 1, figsize=plotting.cm2inch(7.5, 6))
    plot_average_matching_time(data_hinterstoisser_error_metric,
                               fig_mt.axes[0],
                               show_legend=False)
    fig_mt.tight_layout()

    # save plots:
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH_WO_EXTENSION)
        plotting.save_figure(fig_rr, plt_dir, 'recognitionrates_for_different_metrics_and_subsets_barplot')
        plotting.save_figure(fig_mt, plt_dir, 'average_matching_time_barplot')

    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
