#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors

from math import sqrt
import numpy as np
import configparser
import os

# other script settings
TIME_OUT_S = 30
DATASET_PATH = '~/ROS/datasets/geometric_primitives/blensor_dataset.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/viscon_tuning/viscon_tuning_debugging_without_tuning'

class VisConTuningExperiment(ExperimentBase):
    """
    experiment to tune VisCon parameters
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension):
    
        ExperimentBase.__init__(self, data_storage)

        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension

        # try reading spreading info file from home directory
        # it should be available if experiment is started on remote computer with the
        # aws_experiment_interface
        config = configparser.ConfigParser()
        ini_file_path = os.path.expanduser('~/spreading_info.ini')
        self.job_id = None
        try:
            config.read(ini_file_path)
            self.job_id = config['DEFAULT']['job_id']
        except:
            print("ERROR: Could not load ini-file form %s, conducting COMPLETE experimet instead of only a part." %ini_file_path)


    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=True,
                                         dyn_reconfigure_callbacks=True)
        self.launch_and_connect()

        # distance resolution of feature and downsampling
        self.d_dist_rel = 0.12  # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%, [Hodan et al. 2018] use 5%

        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 1,  # VG
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': self.d_dist_rel,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 1,  # VG
                                       'post_downsample_method': 1,  # VG
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.006,
                                       'plane_inlier_threshold_is_relative': False
                                       # do not set d_points here!
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': False,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': True,
                            'publish_pose_cluster_weights': True,
                            'refPointStep': 2,
                            'maxThresh': 1.0,
                            'rs_maxThresh': 0.4,
                            'rs_pose_weight_thresh': 0.3,
                            'use_rotational_symmetry': True,
                            'collapse_symmetric_models': True, # match with collapsed models for speedup
                            'publish_equivalent_poses': True,
                            'd_dist': self.d_dist_rel,
                            'd_dist_is_relative': True,
                            'publish_n_best_poses': 2,
                            'model_hash_table_type': 2,  # CMPH
                            #'d_angle_in_pi': 1 / 20, # [Drost et al. 2010] use 12° steps =1/15 [Hinterstoisser et al. 2016] use 22steps/180°
                            #'d_alpha_in_pi': 1 / 15, # [Hinterstoisser et al. 2016] use 32steps/360°? or 32steps/180°

                            # 'HS_save_dir': '~/ROS/experiment_data/hinterstoisser_pose_verifying/publish_n_best_poses/voting_spaces/',

                            # visibility context
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0, # moot if gapsize_surfaceCase==0
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0, # moot without 'advancedIntersectionClassification'
                            'visualizeVisibilityContextFeature': False,

                            # hinterstoisser extensions ( all off! )
                            'use_neighbour_PPFs_for_matching': False,  # not implemented in matcher-nodelet
                            'use_neighbour_PPFs_for_training': False,
                            'use_voting_balls': True,
                            'use_hinterstoisser_clustering': False,
                            'use_hypothesis_verification_with_visibility_context': False,
                            'vote_for_adjacent_rotation_angles': False,
                            'flag_array_quantization_steps': 30.0,
                            'flag_array_hash_table_type': 0, # NONE
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
    def looping(self):

        # ******************************************************************************************
        # TODO: choose settings
        # ******************************************************************************************

        # choose number of different voxelsizes to test
        # ATTENTION! You HAVE TO use the same number of steps as in viscon_parameter_tuning_experiment:
        #            Otherwise you will not get the same voxel-sizes as then.
        voxelsize_nsteps = 5

        # provide the length of the shortest bounding box edge relative to the models diameter
        # get the bounding box dimensions from meshlab --> d_min
        # get the model diameters from .yaml files of previous experiments --> diam
        # --> d_min_rel=d_min/diam
        d_min_rel = {'cuboid': 0.29,  # with diam=173mm and d_min=50mm
                     'cylinder': 0.57,  # with diam=122mm and d_min=70mm
                     'elliptic_prism': 0.29,  # with diam=175mm and d_min=50mm
                     'hexagonal_frustum': 0.58,  # with diam=104mm and d_min=60mm
                     'pyramid': 0.64}  # with diam=156mm and d_min=100mm

        # define ignore_factor values to test
        # ignore_factor is defined as multiples of the voxeldiameter
        ignore_factors = [0.5]
        print("ignore_factor:", ignore_factors)

        # define gapsize_surfaceCase values to test:
        # ignore_factor is defined as multiples of the voxeldiameter
        gapsizes = [0.0]
        print("gapsize_surfaceCase factor:", gapsizes)

        # define voxelsizes to test
        # here you can now define your voxel-indixes you actually want to use
        voxelsize_indices = [1]
        print("voxelsize_index:", voxelsize_indices)

        # ******************************************************************************************
        # Do experiments
        # ******************************************************************************************

        # extract and setup dataset
        self.setup_dataset_properties(self.dataset)

        # define model indices
        if self.job_id is not None:
            model_indices = [int(self.job_id)]  # if experiment is spreaded, let each AMI
            # do only one model --> minimizing model-learning time per AMI
            # TODO: Needs exactly five AMI's! One for each model.
            print("Got information to perform only a part of the whole experiment. "
                  "Experiment-Splitting activated.\nStart looping through model-indices "
                  "from %s to %s:" % (min(model_indices), max(model_indices)))
        else:
            model_indices = [0]  # range(self.dataset.get_number_of_models())

        # calculate number of runs to perform
        n_runs = self.dataset.get_number_of_scenes() * len(model_indices) \
                 * len(voxelsize_indices) * len(ignore_factors) * len(gapsizes)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        for voxelsize_idx in voxelsize_indices:

            # loop over ignore factors
            for ignore_factor in ignore_factors:

                # loop over gapsize factors
                for gapsize in gapsizes:

                    # set matcher-settings
                    matcher_settings = {  # visibility context
                        'ignoreFactor_intersectClassific': ignore_factor,
                        'gapSizeFactor_allCases_intersectClassific': 1.0,
                        'gapSizeFactor_surfaceCase_intersectClassific': gapsize}
                    self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)

                    # loop over models
                    for model_index in model_indices:
                        # get model name
                        model_name = self.dataset.get_model_name(model_index)
                        print('model %d: %s' % (model_index, model_name))

                        # now we can define the voxelsizes
                        # voxelsize = voxel-edge-length
                        voxelsizes = np.linspace(self.d_dist_rel / (sqrt(3)),
                                                 # min: voxeldiameter=d_points
                                                 0.5 * d_min_rel[model_name],
                                                 # max: 0.5 * d_min_rel
                                                 voxelsize_nsteps)
                        voxelsize = voxelsizes[voxelsize_idx]

                        print("using voxelsize=%s of %s" % (voxelsize, voxelsizes))

                        # set matcher-settings
                        matcher_settings = {  # visibility context
                            'd_VisCon': sqrt(3 * pow(voxelsize, 2)),  # d_VisCon=voxeldiameter
                            'd_VisCon_is_relative': True,
                            'voxelSize_intersectDetect': voxelsize}
                        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)

                        # Matching all models of the dataset and verifying them afterwards
                        # Model-specific down-sampled scenes are being used in Matcher and Verifier
                        model_d_points_abs = self.add_model_and_get_d_points_abs(
                            self.dataset,
                            model_index,
                            clear_matcher=True)
                        self.set_scene_preprocessor_d_points(model_d_points_abs, is_relative=False)

                        self.loop_scenes(self.dataset, re,
                                         model_indices=[model_index],
                                         scene_indices=[1])

                        print(
                            'Saving temporary data for this and all previous models in case something goes wrong...')
                        self.data.to_pickle(self.save_path_wo_extension + '.bin')

                    print(
                        'Saving temporary data for this setup and all previous setups in case something goes wrong...')
                    self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def end(self):
        
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        
        print('Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
        compute_pose_errors(self.data)

        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        #self.data.to_yaml(self.save_path_wo_extension + '.yaml')
        
if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = VisConTuningExperiment(data_storage,
                                        DATASET_PATH,
                                        SAVE_FILE_WO_EXTENSION)
    
    experiment.run(wait_before_looping=False)
