#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like detectLinuxBrokenPipeBehavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# project
from math import pi
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation import plotting

import pf_matching.evaluation.data_post_processing as post_processing

# others
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import os.path
import numpy as np

# ################################ script settings ##############################################
FILE_PATH = '~/ROS/experiment_data/hinterstoisser_clustering/cluster_weights_per_voting_ball.bin'
SAVE_PATH_WO_EXTENSION = '~/ROS/experiment_data/hinterstoisser_clustering/cluster_weights_per_voting_ball'
SAVE_PLOTS = True
SHOW_PLOTS = True
max_translation_error_relative = 0.1
max_rotation_error = pi / 15 # rad
epsilonband_values = [ 0.02]
datasetNames = ['geometric_primitives']
datasetSynonyms = ['primitive Gegenstande']

datasetDictionary = {dsName: datasetSynonyms[i] for i, dsName in enumerate(datasetNames)}

def plot_correct_pose_barchart_for_votingball(experiment_data, datasetName, best_n_range, instances_per_scene, votingball_idx, ax, show_legend):
    """
    at which position of the 'n_best_poses' are the correct poses. plot the number of poses over position
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print("Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" %(datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']],
                                     [['datasetName']])
    
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range
    
    # do the counting
    for posenumber in range(best_n_range):
        for entry in base_data:
            #print("entry-->poses: %s" % (len(entry)))
            for i in entry[iDict['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']]:
                if i == posenumber:
                    pose = entry[iDict['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']][i]
                    if pose['correct']:
                        #print("i: %s; pose->correct: %s" % (i, True))
                        list[i] += 1
                        
    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number/len(base_data))
        #print("i: %s, number: %s" % (i, number))
    ax.set_xlabel('$i$-te Lagehypothese')
    #ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0,0.3)
    datasetNameForChart = datasetDictionary[datasetName]+' votingball '+str(votingball_idx)
    ax.set_title(datasetNameForChart)
    if show_legend:
        ax.legend(loc="upper right", title ="Pose")

def plot_average_clusterweights_for_votingball(experiment_data, datasetName, best_n_range, instances_per_scene, votingball_idx, ax, show_legend):
    """
    at which position of the 'n_best_poses' are the correct poses. plot the number of poses over position
    @type experiment_data: ExperimentDataStructure
    @param experiment_data: experiment data
    @type ax: Axes
    @param ax: axis to plot mu
    @type show_legend: boolean
    @param show_legend: show the legend in plot
    """
    print("Creating correct pose barchart for %s-Dataset with the best %s poses and %s instances per scene" %(datasetName, best_n_range, instances_per_scene))
    iDict, base_data = \
        data_extraction.extract_data(data,
                                     [['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']],
                                     [['datasetName']])
    
    # counter array to count how many times a pose at the i'th rank is correct
    list = [0] * best_n_range
    
    # do the counting
    for posenumber in range(best_n_range):
        for entry in base_data:
            #print("entry-->poses: %s" % (len(entry)))
            for i in entry[iDict['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']]:
                if i == posenumber:
                    pose = entry[iDict['Clustered_Poses_of_Voting_Ball_'+str(votingball_idx)+'_Poses']][i]
                    if pose['correct']:
                        #print("i: %s; pose->correct: %s" % (i, True))
                        list[i] += pose['weight']
                        
    ax.grid()
    for i, number in enumerate(list):
        ax.bar(i, number/len(base_data))
        #print("i: %s, number: %s" % (i, number))
    ax.set_xlabel('$i$-te Lagehypothese')
    #ax.set_ylabel("Anteil richtige Posen")
    ax.set_xlim(0, best_n_range)
    ax.set_ylim(0,400)
    datasetNameForChart = datasetDictionary[datasetName]+' votingball '+str(votingball_idx)
    ax.set_title(datasetNameForChart)
    if show_legend:
        ax.legend(loc="upper right", title ="Pose")


if __name__ == '__main__':
    
    # load data and do post-processing
    data = ExperimentDataStructure()
    data.from_pickle(FILE_PATH)
    #post_processing.compute_pose_errors_of_intermediate_poses(data)
    #print('Saving complete data...')
    #data.to_pickle(SAVE_PATH_WO_EXTENSION + '.bin')
    #data.to_yaml(SAVE_PATH_WO_EXTENSION + '.yaml')
    
    #post_processing.compute_correctness_of_intermediate_poses(data, max_translation_error_relative, max_rotation_error)
    #print('Saving complete data...')
    #data.to_pickle(SAVE_PATH_WO_EXTENSION + '.bin')
    #data.to_yaml(SAVE_PATH_WO_EXTENSION + '.yaml')
    
    # prepare
    mpl.rcParams.update(plotting.krone_thesis_default_settings)
      
    print("plotting bar chart of correct poses")
    bars, axes = plt.subplots(1,2, figsize=plotting.cm2inch(16,7), sharey=True)
    bars.axes[0].set_ylabel("Anteil korrekter Hypothesen")
   
    plot_correct_pose_barchart_for_votingball(data, "geometric_primitives", 200, 2, 0, bars.axes[0], show_legend=True)
    plot_correct_pose_barchart_for_votingball(data, "geometric_primitives", 200, 2, 1, bars.axes[1], show_legend=True)
    
    
    print("plotting bar chart of correct poses")
    bars_1, axes = plt.subplots(1,2, figsize=plotting.cm2inch(16,7), sharey=True)
    bars_1.axes[0].set_ylabel("Gewichte der PoseCluster")
    plot_average_clusterweights_for_votingball(data, "geometric_primitives", 200, 2, 0, bars_1.axes[0], show_legend=True)
    plot_average_clusterweights_for_votingball(data, "geometric_primitives", 200, 2, 1, bars_1.axes[1], show_legend=True)
    
    # save figures
    if SAVE_PLOTS:
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(bars, plt_dir, 'barcharts_correct_poses')
        plt_dir = os.path.dirname(FILE_PATH)
        plotting.save_figure(bars_1, plt_dir, 'barcharts_cluster_weights')
        
    # show figures for checking Layout
    if SHOW_PLOTS:
        plt.show()
    else:
        print('Plots are not shown on screen.')
    
