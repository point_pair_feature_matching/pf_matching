#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/ICCV_drost_reference/ICCV_drost_reference'

MODEL_INDICES = [0,1,2,3,4] # match only the "unambiguous" models

class DrostReferenceExperiment(ExperimentBase):
    """
    Reference experiment. Matching ICCV 2015 dataset with Drost/Kroischke system-setup.
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension):

        ExperimentBase.__init__(self, data_storage)
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension

    def setup(self):

        # sets up scene and model pipeline
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # distance resolution of feature and downsampling
        d_dist_rel = 0.05 # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': d_dist_rel, # [Drost et al. 2010] use 5%
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'pre_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'post_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.02, # TODO: choose best value from inlier_variation_experiment
                                       'plane_inlier_threshold_is_relative': False,
                                       'pre_downsample_dist_ratio': 0.5,
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,     
                            'model_hash_table_type': 1, # STL, because it's at least two times faster in this scenario
                            'publish_pose_cluster_weights': False,
                            'refPointStep': 5, # [Drost et al. 2010] "often use 5" says [Hinterstoisser et al. 2016]
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 2,
                            'd_angle_in_pi': 1/22, # [Drost et al. 2010] use 12° steps =1/15 [Hinterstoisser et al. 2016] use 22steps/180°
                            'use_rotational_symmetry': False,
                            'collapse_symmetric_models': False,
                            'd_dist': d_dist_rel, # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%
                            'd_dist_is_relative': True,
                            'd_alpha_in_pi': 1/16, # [Hinterstoisser et al. 2016] use 32steps/360°? or 32steps/180°

                            # visibility context: (not used!)
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,
            
                            # hinterstoisser extensions: (everything deactivated!)
                            'use_neighbour_PPFs_for_training': False,
                            'use_neighbour_PPFs_for_matching': False,
                            'use_voting_balls': False,
                            'use_hinterstoisser_clustering': False,
                            'use_hypothesis_verification_with_visibility_context': False,
                            'vote_for_adjacent_rotation_angles': False,
                            'flag_array_hash_table_type': 0, # None
                            'flag_array_quantization_steps': 32.0,
                            }

        model_loader_init_settings = {'recenter_cloud': False}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)


    def looping(self):

        # calculate number of runs to perform
        n_runs = len(MODEL_INDICES)* self.dataset.get_number_of_scenes()

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over models and scenes
        self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(self.dataset, re,
                                                                           model_indices=MODEL_INDICES)

        print('Saving temporary data for this and all previous setups in case something goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')


    # def end(self):
    #
    #     print('Saving data to make sure we got it if post-processing goes wrong...')
    #     self.data.to_pickle(self.save_path_wo_extension + '.bin')
    #
    #     if self.dataset.get_error_metric() == 'Hinterstoisser_Lepetit_2013':
    #         print(
    #             'Computing pose errors with error-metric from [Hinterstoisser, Lepetit et al. 2013] for all runs...')
    #         compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(self.data, self.dataset)
    #     elif self.dataset.get_error_metric() == 'Kroischke_2016':
    #         print(
    #             'Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
    #         compute_pose_errors(self.data)
    #     else:
    #         print('Unknown error-metric, not calculating any errors...')
    #
    #     print('Saving complete data...')
    #     self.data.to_pickle(self.save_path_wo_extension + '.bin')
    #     # self.data.to_yaml(self.save_path_wo_extension + '.yaml')

if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = DrostReferenceExperiment(data_storage,
                                          DATASET_PATH,
                                          SAVE_FILE_WO_EXTENSION)

    experiment.run(wait_before_looping=False)