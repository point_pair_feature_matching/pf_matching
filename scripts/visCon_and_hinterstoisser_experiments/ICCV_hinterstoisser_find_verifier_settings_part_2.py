#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2018
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

input = raw_input
range = xrange

# import from project
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_from_verifier_by_hinterstoisser_lepetit_et_al_2013, assign_mu_to_poses


# other script settings
TIME_OUT_S = 30
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_find_verifier_settings/ICCV_hinterstoisser_find_verifier_settings_the_resultsfile'
FILE_PATH = '~/ROS/experiment_data/ICCV_hinterstoisser_find_verifier_settings/ICCV_hinterstoisser_find_verifier_settings.bin'
SEARCHRADIUS_RELATIVES = [0.5] # chose the smallest of [0.5, 1.0, 2.0, 3.0, 5.0]
ANGLEDIFFTHRESHS_IN_DEGREE = [180] # chose the biggest of [3, 6, 12, 18, 45, 180]
           
ZBUFFERINGRESOLUTION = range(20,300,10)
# result: choose 160, it returns the highest number of "averageModelPoints"

SCENE_INDICES = [3, 8, 17, 27, 36, 38, 39, 41, 47, 58, 61, 62, 64, 65, 69, 72, 79, 89, 96, 97]#, 102,
                 # 107, 110, 115, 119, 124, 126, 136, 153, 156, 162, 166, 175, 176, 178, 203, 207,
                 # 217, 219, 221, 224, 243, 248, 249, 254, 258, 263, 266, 268, 277, 283, 307, 310,
                 # 322, 326, 338, 342, 356, 362, 365, 368, 387, 389, 402, 415, 417, 425, 428, 434,
                 # 435, 438, 442, 446, 453, 473, 474, 476, 477, 480, 491, 494, 499, 501, 503, 521,
                 # 527, 529, 532, 535, 540, 543, 549, 560, 563, 571, 575, 589, 603, 607, 611, 615,
                 # 625, 642, 648, 649, 650, 652, 667, 669, 679, 691, 695, 703, 708, 711, 727, 736,
                 # 737, 739, 740, 750, 754, 756, 757, 758, 761, 762, 764, 768, 769, 770, 773, 775,
                 # 785, 788, 791, 794, 801, 803, 804, 808, 809, 819, 821, 828, 837, 840, 844, 850,
                 # 856, 867, 871, 877, 883, 886, 894, 902, 903, 904, 907, 909, 918, 925, 934, 942,
                 # 952, 956, 961, 968, 969, 972, 982, 984, 991, 1001, 1012, 1038, 1050, 1057, 1061,
                 # 1069, 1071, 1087, 1098, 1099, 1103, 1107, 1117, 1123, 1131, 1144, 1148, 1151, 1157,
                 # 1168, 1169, 1176, 1180, 1199, 1212]
MODEL_INDICES = [0,1,2,3,4] # only the "can" model


class ZBufferingResolutionVariationExperiment(ExperimentBase):
    """
    experiment to manually find the verifier settings for zbufferingresolution
    """
    def __init__(self, input_data, data_storage, dataset_path, save_path_wo_extension, searchradius_relatives, angleDiffThreshs_in_degree):
    
        ExperimentBase.__init__(self, data_storage)

        self.input_data = input_data
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension
        self.searchradius_relatives = searchradius_relatives
        self.angleDiffThreshs_in_pi = [angle_in_degree/180 for angle_in_degree in angleDiffThreshs_in_degree]
        
    def setup(self):
        
        self.interface.set_output_levels(statistics_callbacks=True,
                                         dyn_reconfigure_callbacks=True)
        self.launch_and_connect_with_verifier_without_matcher()

        # distance resolution of feature and downsampling
        d_dist_rel = 0.05  # [Drost et al. 2010] use 5%, [Hinterstoisser et al. 2016] use 2.5%, [Hodan et al. 2018] use 5%

        # initial setup of dynamic reconfigure parameters
        # default settings for pre-processing and model matching
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 3,  # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'd_points': d_dist_rel,
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # 5 MLS_POLY2
                                       'pre_downsample_method': 3,  # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'post_downsample_method': 3,  # 1=VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       'plane_inlier_threshold': 0.02, # TODO: choose best value from inlier_variation_experiment
                                       'plane_inlier_threshold_is_relative': False,
                                       'pre_downsample_dist_ratio': 0.5,
                                       'd_points_is_relative': False,
                                       # 'd_points': is determined by each model separately
                                       # --> do not set d_points here!
                                       }
        
        verifier_settings = {'show_results': False, #TODO switch to 'True' to visualize results
                             'show_filtered_models_in_visualization': True, #TODO
                             'publish_statistics': True,
                             'supportthreshold': 0,
                             'penaltythreshold': 1,
                             'publish_mu_values': True,
                             'use_conflict_analysis': False,
                             'use_verification_of_normals': True,
                             'filter_selfocclusion': False,
                             }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)
        
    def looping(self):
        
        # calculate number of runs to perform
        n_runs = 0
        n_runs += len(SCENE_INDICES) * len(MODEL_INDICES) * len(ZBUFFERINGRESOLUTION)
        n_runs = n_runs * len(self.searchradius_relatives) * len(self.angleDiffThreshs_in_pi)
        
        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # extract and setup dataset
        self.setup_dataset_properties(self.dataset)

        # loop over angle difference thresholds and searchradii:
        for angleDiffThresh_in_pi in self.angleDiffThreshs_in_pi:
            print("angleDiffThresh_in_pi: %s" %(angleDiffThresh_in_pi))
            verifier_settings = {'angleDiffThresh_in_pi': angleDiffThresh_in_pi}
            self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)
            for sr in self.searchradius_relatives:
                print("searchradius is %s percent of the model_d_dist" % (sr*100))
                verifier_settings = {'searchradius': sr,
                                     'searchradius_is_relative': True,
                                     'scenezbufferingthreshold': sr,
                                     'zbufferingthreshold_is_relative': True}
                self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)

                # loop over all zbufferingresolutions:
                for zbufres in ZBUFFERINGRESOLUTION:
                    verifier_settings = {'zbufferingresolution': zbufres}
                    self.interface.set_dyn_reconfigure_parameters('verifier', verifier_settings)

                    # simulate matching all models of the dataset and verifying them afterwards
                    # for verification model-specific down-sampled scenes are being used ( difference to Krone )
                    self.loop_scenes_with_one_model_for_verification_without_matcher(self.dataset, re,
                                                                                     self.input_data,
                                                                                     use_equivalent_poses=False,
                                                                                     n_best_poses=7, # TODO set accoring to 'experiment_part_1.py'
                                                                                     scene_indices=SCENE_INDICES,
                                                                                     model_indices=MODEL_INDICES)

                    print('Saving temporary data for this setup and all previous setups in case something goes wrong...')
                    self.data.to_pickle(self.save_path_wo_extension + '.bin')
            
    def end(self):
        
        input = raw_input("Hit <ENTER> to clean up") #TODO

        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
        
        
if __name__ == '__main__':
    
    input_data = data_structure.ExperimentDataStructure()
    input_data.from_pickle(FILE_PATH)

    data_storage = data_structure.ExperimentDataStructure()
    experiment = ZBufferingResolutionVariationExperiment(input_data,
                                                         data_storage,
                                                         DATASET_PATH,
                                                         SAVE_FILE_WO_EXTENSION,
                                                         SEARCHRADIUS_RELATIVES,
                                                         ANGLEDIFFTHRESHS_IN_DEGREE)
    
    experiment.run(wait_before_looping=False)
