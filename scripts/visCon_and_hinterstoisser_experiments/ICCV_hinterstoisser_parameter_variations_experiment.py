#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import imp
import time
from pf_matching import ros_interface
from pf_matching import data_structure
from pf_matching.experiments.base_experiments import ExperimentBase
from pf_matching.evaluation.data_post_processing import compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013, compute_pose_errors

# other script settings
TIME_OUT_S = 5 * 60
DATASET_PATH = '~/ROS/datasets/OcclusionChallengeICCV2015/dataset_adapted_for_ICCV.py'
SAVE_FILE_WO_EXTENSION = '~/ROS/experiment_data/ICCV_hinterstoisser_parameter_variations/ICCV_hinterstoisser_parameter_variations'

PLANE_INLIER_THRESHOLDS = [0.020]
#TODO: define the d_dist_rel and refPointStep pairs you want to examine:
D_DIST_REL =      [#0.025, 0.025,
                   0.050, 0.050, 0.050, 0.075, 0.075, 0.075, 0.100, 0.100, 0.100]
REF_POINT_STEPS = [#1,     5,
                   5,     10,    20,    5,     10,    20,    5,     10,    20   ]
SCENE_INDICES = [121, 28, 333, 987, 654, 321, 132, 456, 789, 1001] # 10 random scenes
MODEL_INDICES = [0,1,2,3,4] # only the "unambigeous" models

class HinterstoisserParameterVariationsExperiment(ExperimentBase):
    """
    Experiment for finding the number of poses that should be evaluated with verifier.
    For ICCV dataset!
    Use wise plane inlier threshold for ICCV dataset!
    """
    def __init__(self, data_storage, dataset_path, save_path_wo_extension, plane_inlier_thresholds_,
                 d_dist_rel_, ref_point_steps_):

        ExperimentBase.__init__(self, data_storage)
        self.dataset = self.load_dataset(dataset_path)
        self.save_path_wo_extension = save_path_wo_extension
        self.plane_inlier_thresholds = plane_inlier_thresholds_
        self.d_dist_rel = d_dist_rel_[::-1] # reverse to get the more interesting results first ;-)
        self.ref_point_steps = ref_point_steps_[::-1]

    def setup(self):

        # sets up scene and model pipeline
        self.interface.set_output_levels(statistics_callbacks=False,
                                         dyn_reconfigure_callbacks=False)
        self.launch_and_connect()

        # initial setup of dynamic reconfigure parameters
        model_preprocessor_settings = {'pre_downsample_method': 0,  # NONE
                                       'post_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'normal_estimation_method': 0,  # NONE
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       #'d_points': gets varied
                                       'd_points_is_relative': True,
                                       }

        scene_preprocessor_settings = {'normal_estimation_method': 5,  # MLS_POLY2
                                       'pre_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'post_downsample_method': 3,  # 1= VoxelGrid 2=Uniform Sampling 3=VoxelGrid for large scenes
                                       'remove_NaNs': True,
                                       'flip_normals': False,
                                       'remove_largest_plane': True,
                                       'remove_plane_first': True,
                                       #'plane_inlier_threshold': gets variied
                                       'plane_inlier_threshold_is_relative': False,
                                       'pre_downsample_dist_ratio': 0.5,
                                       }

        matcher_settings = {'show_results': False,
                            'match_S2S': True,
                            'match_B2B': False,
                            'match_S2B': False,
                            'match_B2S': False,
                            'match_S2SVisCon': False,
                            'model_hash_table_type': 2, # CMPH, because it's faster in this scenario
                            'publish_pose_cluster_weights': True,
                            #'refPointStep': gets varied
                            'maxThresh': 1.0,
                            'publish_n_best_poses': 2, # per voting ball! 2 was best for "geometric primitives"
                            'd_angle_in_pi': 1/22, # [Drost et al. 2010] use 12° steps =1/15 [Hinterstoisser et al. 2016] use 22steps/180°
                            'use_rotational_symmetry': False,
                            'collapse_symmetric_models': False,
                            #'d_dist': gets varied
                            'd_dist_is_relative': True,
                            'd_alpha_in_pi': 1/16, # [Hinterstoisser et al. 2016] use 32steps/360°? or 32steps/180°

                            #'HS_save_dir': '~/ROS/experiment_data/rotational_symmetry_nyquist/voting_spaces/',
                            
                            # visibility context: (not used!)
                            'd_VisCon': 0.05,
                            'd_VisCon_is_relative': False,
                            'voxelSize_intersectDetect': 0.008,
                            'ignoreFactor_intersectClassific': 0.9,
                            'gapSizeFactor_allCases_intersectClassific': 1.0,
                            'gapSizeFactor_surfaceCase_intersectClassific': 0.0,
                            'advancedIntersectionClassification': False,
                            'alongSurfaceThreshold_intersectClassific': 10.0,
                            'othorgonalToSurfaceThreshold_intersectClassific': 15.0,
                            'visualizeVisibilityContextFeature': False,
            
                            # hinterstoisser extensions: (everything activated!)
                            'use_neighbour_PPFs_for_training': True,
                            'use_neighbour_PPFs_for_matching': False, # not implemented
                            'use_voting_balls': True,
                            'use_hinterstoisser_clustering': True,
                            'use_hypothesis_verification_with_visibility_context': True,
                            'vote_for_adjacent_rotation_angles': True,
                            'flag_array_hash_table_type': 3, # CMPH, because it's faster in this scenario
                            'flag_array_quantization_steps': 32.0,
                            }

        model_loader_init_settings = {'recenter_cloud': True}
        scene_loader_init_settings = {'recenter_cloud': False}

        self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_preprocessor',
                                                      model_preprocessor_settings)
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_init_settings)
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_init_settings)

        self.setup_dataset_properties(self.dataset)

    def looping(self):

        # calculate number of runs to perform
        n_runs = len(SCENE_INDICES) * len(MODEL_INDICES) * len(self.plane_inlier_thresholds) * \
                 len(self.ref_point_steps)

        # initialize
        re = ros_interface.RuntimeEstimator(n_runs)
        self.interface.new_run()

        # iterate over scene_preprocessor setup
        for inlier_threshold in self.plane_inlier_thresholds:
            print('Performing downsampling with plane-removal inlier threshold:', inlier_threshold)

            for value_pair_idx in range(0, len(self.d_dist_rel)):
                print('Performing matching with d_dist_rel = d_points_rel =',
                      self.d_dist_rel[value_pair_idx], "and refPointStep =",
                      self.ref_point_steps[value_pair_idx])

                # edit inlier_threshold
                scene_preprocessor_settings = {'plane_inlier_threshold': inlier_threshold}
                self.interface.set_dyn_reconfigure_parameters('scene_preprocessor', scene_preprocessor_settings)
                # edit d_points_rel and d_dist_rel
                model_preprocessor_settings = {'d_points': self.d_dist_rel[value_pair_idx]}
                self.interface.set_dyn_reconfigure_parameters('model_preprocessor', model_preprocessor_settings)
                matcher_settings = {'refPointStep': self.ref_point_steps[value_pair_idx],
                                    'd_dist': self.d_dist_rel[value_pair_idx]}
                self.interface.set_dyn_reconfigure_parameters('matcher', matcher_settings)

                # iterate over models and scenes
                self.loop_models_and_scenes_with_same_d_points_for_model_and_scene(self.dataset, re,
                                                                                   scene_indices=SCENE_INDICES,
                                                                                   model_indices=MODEL_INDICES)

                print('Saving temporary data for this and all previous setups in case something goes wrong...')
                self.data.to_pickle(self.save_path_wo_extension + '.bin')

    # def end(self):
    #
    #     print('Saving data to make sure we got it if post-processing goes wrong...')
    #     self.data.to_pickle(self.save_path_wo_extension + '.bin')
    #
    #     if self.dataset.get_error_metric() == 'Hinterstoisser_Lepetit_2013':
    #         print(
    #             'Computing pose errors with error-metric from [Hinterstoisser, Lepetit et al. 2013] for all runs...')
    #         compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(self.data, self.dataset)
    #     elif self.dataset.get_error_metric() == 'Kroischke_2016':
    #         print(
    #             'Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
    #         compute_pose_errors(self.data)
    #     else:
    #         print('Unknown error-metric, not calculating any errors...')
    #
    #     print('Saving complete data...')
    #     self.data.to_pickle(self.save_path_wo_extension + '.bin')
    #     # self.data.to_yaml(self.save_path_wo_extension + '.yaml')

if __name__ == '__main__':

    data_storage = data_structure.ExperimentDataStructure()
    experiment = HinterstoisserParameterVariationsExperiment(data_storage,
                                                             DATASET_PATH,
                                                             SAVE_FILE_WO_EXTENSION,
                                                             PLANE_INLIER_THRESHOLDS,
                                                             D_DIST_REL,
                                                             REF_POINT_STEPS)

    experiment.run(wait_before_looping=False)