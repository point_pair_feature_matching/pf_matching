# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

# ROS related
import tf.transformations as tft


# ROS messages
# noinspection PyProtectedMember
#from cffi import model #seems to be not needed
from pf_matching_core.msg._WeightedPose import WeightedPose
import numpy as np

# ####################### data conversion ################################################


def weighted_pose2pose_dict(weighted_pose_msg):
    """
    convert a message of type WeightedPose to a dictionary containing the xyzw-"quaternion"
    representing the pose's orientation, the xyz-"translation" and the "weight"
    @type weighted_pose_msg: WeightedPose
    @param weighted_pose_msg: message with the weighted pose
    @return:
    """
    return {"quaternion": [weighted_pose_msg.orientation.x,
                           weighted_pose_msg.orientation.y,
                           weighted_pose_msg.orientation.z,
                           weighted_pose_msg.orientation.w],
            "translation": [weighted_pose_msg.position.x,
                            weighted_pose_msg.position.y,
                            weighted_pose_msg.position.z],
            "weight": weighted_pose_msg.weight,
	    "votingBall": weighted_pose_msg.votingBall}


def pose_dict2weighted_pose(pose_dict):
    if pose_dict['translation'] is None or pose_dict['quaternion'] is None:
        return None
    else:
        wp_msg = WeightedPose()
        wp_msg.orientation.x = pose_dict['quaternion'][0]
        wp_msg.orientation.y = pose_dict['quaternion'][1]
        wp_msg.orientation.z = pose_dict['quaternion'][2]
        wp_msg.orientation.w = pose_dict['quaternion'][3]
        wp_msg.position.x = pose_dict['translation'][0]
        wp_msg.position.y = pose_dict['translation'][1]
        wp_msg.position.z = pose_dict['translation'][2]
        wp_msg.weight = pose_dict['weight']
	wp_msg.votingBall = pose_dict['votingBall']
        return wp_msg

def pose_dict2matrix(pose_dict):
    """
    convert a dictionary representing a pose to a 4x4 homogeneous transformation test_matrix
    @param pose_dict: of the form {"quaternion": xyzw-list, "translation": xyz-list}
    @return: 4x4 transformation test_matrix as numpy array or None if one of the dict entries
             was None
    """
    if pose_dict['translation'] is None or pose_dict['quaternion'] is None:
        return None
    else:
        translation_matrix = tft.translation_matrix(pose_dict['translation'])
        rotation_matrix = tft.quaternion_matrix(pose_dict['quaternion'])
        return translation_matrix.dot(rotation_matrix)


def to_matrix(transform):
    """
    convert from pose dict to matrix if required
    @param transform: pose dict or transform matrix
    @return: transform matrix identical to transform or equivalent to transform if transform is
             @permission a dict
    """
    if type(transform) is dict:
        return pose_dict2matrix(transform)
    else:
        return transform


def matrix2pose_dict(matrix):
    """
    convert a 4x4 homogeneous transformation test_matrix to a dictionary representation
    @param matrix: 4x4 transformation test_matrix without scaling
    @return: dict of the form {"quaternion": xyzw-list, "translation": xyz-list}, entries are None
             if the input was None
    """
    if matrix is None:
        return {'translation': None,
                'quaternion': None}
    else:
        return {'translation': tft.translation_from_matrix(matrix).tolist(),
                'quaternion': tft.quaternion_from_matrix(matrix).tolist()}


# ############### calculating differences between poses #################################


def pose_error(pose, gt):
    """
    calculate the error of a posse compared to a ground truth for the pose
    The error is calculated separately for the quaternion representation or the orientation and
    for translations:
    angle = $arccos(|q_1 \cdot q_2|)$ in [0, pi]
    distance = euclidean distance
    @param pose: the pose to check, 4x4 homogeneous transformation or of the form
                 {"quaternion": xyzw-list, "translation": xyz-list}
    @param gt: the pose to check, 4x4 homogeneous transformation or of the form
               {"quaternion": xyzw-list, "translation": xyz-list}
    @return: dictionary of form {'distance': euclidean_distance, 'angle': quaternion_angle} or
             {'distance': None, 'angle': None} if any input was None
    """

    # check if any of the inputs is None
    #if None in [pose, gt]:
    if pose is None or gt is None:
        return {'distance': None, 'angle': None}

    # extract pose values
    if type(pose) is not dict:
        pose = matrix2pose_dict(pose)
    q_pose = pose['quaternion']
    t_pose = pose['translation']

    # extract ground truth values
    if type(gt) is not dict:
        gt = matrix2pose_dict(gt)
    q_gt = gt['quaternion']
    t_gt = gt['translation']

    if None in [q_pose, t_pose, q_gt, t_gt]:
        # one of the entries is None, can't calculate a proper error
        return {'distance': None, 'angle': None}
    else:
        # all values exist, calculate errors
        quaternion_dot_product = np.abs(np.dot(np.array(q_pose), np.array(q_gt)))
        if quaternion_dot_product > 1:
            quaternion_dot_product = 1

        quaternion_angle = 2 * np.arccos(quaternion_dot_product)
        euclidean_distance = np.linalg.norm(np.array(t_pose) - np.array(t_gt))
        #print('distance', float(euclidean_distance), ' angle', float(quaternion_angle))
        return {'distance': float(euclidean_distance), 'angle': float(quaternion_angle)}

def pose_error_by_hinterstoisser_lepetit_et_al_2013(pose, gt, model_load_transform, dataset, model_name):
    """
    calculate the error of a pose compared to a ground truth for the pose
    The error is calculated as the average distance between the model points transformed into the pose-hypothesis
    and the model points transformed into the ground-truth-pose.
    This error-metric is used only for the ICCV 2015 dataset by [Krull et al. 2015]

    @param pose: the pose to check, 4x4 homogeneous transformation or of the form
                 {"quaternion": xyzw-list, "translation": xyz-list}
    @param gt: the pose to check, 4x4 homogeneous transformation or of the form
               {"quaternion": xyzw-list, "translation": xyz-list}
    @param model_load_transform: the recentering transformation
    @type model_load_transform: 4x4 homogeneous transformation or dictionary of the form
                                {"quaternion": xyzw-list, "translation": xyz-list}
    @return: dictionary of form {'average distance': average_euclidean_distance} or
             {'average distance': None} if any input was None
    """
    import pcl

    # check if any of the inputs is None
    #if None in [pose, gt, model_load_transform]:
    if pose is None or gt is None or model_load_transform is None:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose = pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pose_dict2matrix(model_load_transform)

    # all values exist, calculate errors

    # get model load path and model scale factor
    model_absolute_load_path = ""
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_name(model_idx) == model_name:
            model_absolute_load_path = dataset.get_model_path(model_idx)
            break
    if not model_absolute_load_path:
        return {'average distance': None}

    # load the model as a pcl point cloud
    gt_model = pcl.load_XYZI(model_absolute_load_path, "ply")
    pose_model = pcl.load_XYZI(model_absolute_load_path, "ply")
    #scene = pcl.load_XYZI(dataset.get_scene_path(0))

    gt_model_points = gt_model.to_array()
    pose_model_points = pose_model.to_array()

    # scale model and transform into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)))
    for i in range(0, gt_model.size):
        gt_model_points[i] *= model_scale_factor
        gt_model_points[i][3] = 1
        # 1. recenter model, because we loaded the original model above
        # 2. apply gt-pose of the recentered model to the recentered model
        # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
        # was determined assuming the cameras view-direction was the negative z-axis.
        gt_transformed = concatenate_transforms(R, gt, inverse(model_load_transform))
        gt_model_points[i] = gt_transformed.dot(gt_model_points[i])

    # scale model and transform into pose-hypothesis
    for i in range(0, pose_model.size):
        pose_model_points[i] *= model_scale_factor
        pose_model_points[i][3] = 1 # need the four'th coordinate for applying homogeneous transforms
        # 1. recenter model, because we loaded the original model above
        # 2. apply pose-hypothesis of the recentered model to the recentered model
        pose_transformed = concatenate_transforms(pose, inverse(model_load_transform))
        pose_model_points[i] = pose_transformed.dot(pose_model_points[i])

    # visulalize the model poses:
    # save transformed poses in the original point clouds
    #gt_model.from_array(gt_model_points)
    #pose_model.from_array(pose_model_points)
    #import pcl.pcl_visualization
    #visual = pcl.pcl_visualization.CloudViewing()
    #visual.ShowGrayCloud(gt_model, b'gt')
    #visual.ShowGrayCloud(pose_model, b'pose')
    #visual.ShowGrayCloud(scene, b'scene')
    #flag = True
    #while flag:
    #    flag != visual.WasStopped()
    #end

    #print('pose_model.size:',pose_model.size)
    #print('pose_model:', pose_model)
    #print('pose_model_points.size', pose_model_points.size)
    #print('len(pose_model_points):', len(pose_model_points))

    # calculate distance between model points
    sum_distance = 0
    for i in range(0, len(pose_model_points)):
        sum_distance += np.linalg.norm(gt_model_points[i] - pose_model_points[i])
    average_distance = sum_distance / len(pose_model_points)
    #print('average distance =', average_distance)
    return {'average distance': float(average_distance)}


def pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized(pose, gt, model_load_transform, dataset, model_name):
    """
    much faster version using numpy vectorization. 1s vs. 10ms  --> 100 times faster
    calculate the error of a pose compared to a ground truth for the pose
    The error is calculated as the average distance between the model points transformed into the pose-hypothesis
    and the model points transformed into the ground-truth-pose.
    This error-metric is used only for the ICCV 2015 dataset by [Krull et al. 2015]

    @param pose: the pose to check, 4x4 homogeneous transformation or of the form
                 {"quaternion": xyzw-list, "translation": xyz-list}
    @param gt: the pose to check, 4x4 homogeneous transformation or of the form
               {"quaternion": xyzw-list, "translation": xyz-list}
    @param model_load_transform: the recentering transformation
    @type model_load_transform: 4x4 homogeneous transformation or dictionary of the form
                                {"quaternion": xyzw-list, "translation": xyz-list}
    @param dataset
    @type ExperimentBase
    @param model_name: name of the model
    @type model_name: string
    @return: dictionary of form {'average distance': average_euclidean_distance} or
             {'average distance': None} if any input was None
    """
    #import pcl #old

    # check if any of the inputs is None
    #if None in [pose, gt, model_load_transform]:
    if pose is None or gt is None or model_load_transform is None:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose = pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pose_dict2matrix(model_load_transform)

    # all values exist, calculate errors

    # get model load path and model scale factor
    model_absolute_load_path = ""
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_name(model_idx) == model_name:
            model_absolute_load_path = dataset.get_model_path(model_idx)
            break
    if not model_absolute_load_path:
        return {'average distance': None}

    # *** OLD: using pythonPCL wrapper requires full PCL installation, ROS PCL does not suffice ****
    # load the gt-model as a pcl point cloud
    #gt_model = pcl.load_XYZI(model_absolute_load_path)
    # load the pose-model as a pcl point cloud
    #pose_model = pcl.load_XYZI(model_absolute_load_path)

    # store the model points in numpy-arrays
    #gt_model_points = gt_model.to_array()
    #pose_model_points = pose_model.to_array()

    # ****** NEW: using plyfile package instead for loading PLY point clouds *******
    from plyfile import PlyData, PlyElement
    # load the gt-model as a PlyData element
    gt_model = PlyData.read(model_absolute_load_path)
    # convert PlyElement to numpy array
    gt_model_points = np.asarray(gt_model['vertex'].data[['x', 'y', 'z']])
    gt_model_points = np.array(gt_model_points.tolist())

    # scale gt-model
    gt_model_points = gt_model_points * model_scale_factor
    #gt_model_points = gt_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f')
    temp = np.ones((gt_model_points.shape[0], 4))
    temp[:, :-1] = gt_model_points
    gt_model_points = temp
    # scale pose-model
    #pose_model_points = pose_model_points * model_scale_factor
    #pose_model_points = pose_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f')
    pose_model_points = gt_model_points.copy()

    # transform gt-model into gt-pose
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    gt_transformed = concatenate_transforms(R, gt, inverse(model_load_transform))
    gt_model_points = np.dot(gt_transformed, gt_model_points.T).T

    # transform pose-model into pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    pose_transformed = concatenate_transforms(pose, inverse(model_load_transform))
    pose_model_points = np.dot(pose_transformed, pose_model_points.T).T

    # reduce the dimensions from 4 to three, to save computing time
    gt_model_points = gt_model_points[:, :3]
    pose_model_points = pose_model_points[:, :3]

    # calculate distance between model points of gt and pose
    distance = np.linalg.norm(gt_model_points - pose_model_points, axis=1)
    average_distance = np.mean(distance)

    return {'average distance': float(average_distance)}


def pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized_ADI(pose, gt, model_load_transform, dataset, model_name):
    """
    much faster version using numpy vectorization. 1s vs. 10ms  --> 100 times faster
    calculate the error of a pose compared to a ground truth for the pose
    The error is calculated as the average distance between the model points transformed into the pose-hypothesis
    and the model points transformed into the ground-truth-pose.
    This ADI error-metric is only for use with "geometric primitives" dataset

    @param pose: the pose to check, 4x4 homogeneous transformation or of the form
                 {"quaternion": xyzw-list, "translation": xyz-list}
    @param gt: the pose to check, 4x4 homogeneous transformation or of the form
               {"quaternion": xyzw-list, "translation": xyz-list}
    @param model_load_transform: the recentering transformation
    @type model_load_transform: 4x4 homogeneous transformation or dictionary of the form
                                {"quaternion": xyzw-list, "translation": xyz-list}
    @param dataset
    @type ExperimentBase
    @param model_name: name of the model
    @type model_name: string
    @return: dictionary of form {'average distance': average_euclidean_distance} or
             {'average distance': None} if any input was None
    """

    # check if any of the inputs is None
    #if None in [pose, gt, model_load_transform]:
    if pose is None or gt is None or model_load_transform is None:
        return {'average distance': None}

    # extract pose values
    if type(pose) is dict:
        pose = pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pose_dict2matrix(model_load_transform)

    # all values exist, calculate errors

    # get model load path and model scale factor
    model_absolute_load_path = ""
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_name(model_idx) == model_name:
            model_absolute_load_path = dataset.get_model_path(model_idx)
            break
    if not model_absolute_load_path:
        return {'average distance': None}

    # ****** using pythonPCL wrapper requires full PCL installation, ROS PCL does not suffice ******
    #import pcl
    # load the gt-model as a pcl point cloud
    #gt_model = pcl.load_XYZI(model_absolute_load_path)
    # load the pose-model as a pcl point cloud
    #pose_model = pcl.load_XYZI(model_absolute_load_path)
    # store the model points in numpy-arrays
    #gt_model_points = gt_model.to_array()
    #pose_model_points = pose_model.to_array()

    # ****** using open3D package instead for loading PLY point clouds PROBLEM: not working on Ubuntu 14.04 *******
    #import open3d as op
    # load the gt-model as a Open3D point cloud
    #gt_model = op.read_point_cloud(model_absolute_load_path, format='xyz')
    # load the pose-model as a Open3D point cloud
    #pose_model = op.read_point_cloud(model_absolute_load_path, format='xyz')
    # convert Open3D.PointClouds to numpy array
    #gt_model_points = np.asarray(gt_model.points)
    #pose_model_points = np.asarray(pose_model.points)

    # ****** using plyfile package instead for loading PLY point clouds *******
    from plyfile import PlyData, PlyElement
    from scipy.spatial import cKDTree
    # load the gt-model as a PlyData element
    gt_model = PlyData.read(model_absolute_load_path)
    #pose_model = PlyData.read(model_absolute_load_path)
    # convert PlyElement to numpy array
    gt_model_points = np.asarray(gt_model['vertex'].data[['x','y','z']])
    gt_model_points = np.array(gt_model_points.tolist())
    #pose_model_points = np.asarray(pose_model['vertex'].data[['x','y','z']])
    #pose_model_points = np.array(pose_model_points.tolist())


    # scale gt-model
    gt_model_points = gt_model_points * model_scale_factor
    #gt_model_points = gt_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f') #does not work with plyfile package
    temp = np.ones((gt_model_points.shape[0],4))
    temp[:,:-1] = gt_model_points
    gt_model_points = temp
    # scale pose-model
    #pose_model_points = pose_model_points * model_scale_factor
    #pose_model_points = pose_model_points + np.array((0.0, 0.0, 0.0, 1.0), dtype='f')
    #temp = np.ones((pose_model_points.shape[0], 4))
    #temp[:, :-1] = pose_model_points
    #pose_model_points = temp
    pose_model_points = gt_model_points.copy()

    # transform gt-model into gt-pose
    #theta = np.radians(180)
    #c, s = np.cos(theta), np.sin(theta)
    #R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    inverse_model_load_transform = inverse(model_load_transform)
    #gt_transformed = concatenate_transforms(R, gt, inverse_model_load_transform)
    gt_transformed = concatenate_transforms(gt, inverse_model_load_transform)
    gt_model_points = np.dot(gt_transformed, gt_model_points.T).T

    # transform pose-model into pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    pose_transformed = concatenate_transforms(pose, inverse_model_load_transform)
    pose_model_points = np.dot(pose_transformed, pose_model_points.T).T

    # reduce the dimensions from 4 to three, to save computing time
    gt_model_points = gt_model_points[:, :3]
    pose_model_points = pose_model_points[:, :3]

    # instanciate the kdtree:
    gt_model_points_KDtree = cKDTree(gt_model_points)

    # calculate distance between model points of gt and pose
    #min_distance_array = np.full(pose_model_points.shape[0], fill_value=np.nan)
    # iterating array using c_index flag
    #point_it = np.nditer(pose_model_points, flags=['external_loop'], order='C')
    #index = 0
    #while not point_it.finished:
    #for point_index in range(0,pose_model_points.shape[0],1):
        #distance = np.linalg.norm(gt_model_points - point_it[0], axis=1)
        #distance = np.linalg.norm(gt_model_points - pose_model_points[point_index], axis=1)
        #min_distance_array[point_index] = np.amin(distance)
        #index +=1
        #point_it.iternext()

    # alternative: filter points: find the nearest neighbour with the KDtree
    min_distance_array, index = gt_model_points_KDtree.query(pose_model_points, k=1, p=2, n_jobs=-1)
    #print(min_distance_array)
    average_min_distance = np.mean(min_distance_array)

    return {'average distance': float(average_min_distance)}



def pose_error_by_hodan_et_al_2018(pose, gt, model_load_transform, dataset, model_name, scene_name):
    """
    calculate the error of a pose compared to a ground truth for the pose
    This error-metric is used only for the ICCV 2015 dataset by [Krull et al. 2015] in the context
    of the SIXD challenge 2017.

    @param pose: the pose to check, 4x4 homogeneous transformation or of the form
                 {"quaternion": xyzw-list, "translation": xyz-list}
    @param gt: the pose to check, 4x4 homogeneous transformation or of the form
               {"quaternion": xyzw-list, "translation": xyz-list}
    @param model_load_transform: the recentering transformation
    @type model_load_transform: 4x4 homogeneous transformation or dictionary of the form
                                {"quaternion": xyzw-list, "translation": xyz-list}
    @param dataset
    @type ExperimentBase
    @param model_name: name of the model
    @type model_name: string
    @return: dictionary of form {'average distance': average_euclidean_distance} or
             {'average distance': None} if any input was None
    """

    from pf_matching.evaluation.pysixd import pose_error as PE
    from pf_matching.evaluation.pysixd import inout

    # check if any of the inputs is None
    #if None in [pose, gt, model_load_transform]:
    if pose is None or gt is None or model_load_transform is None:
        return {'error score': None}

    # extract pose values
    if type(pose) is dict:
        pose = pose_dict2matrix(pose)

    # extract ground truth values
    if type(gt) is dict:
        gt = pose_dict2matrix(gt)

    # extract model load transform
    if type(model_load_transform) is dict:
        model_load_transform = pose_dict2matrix(model_load_transform)

    # all values exist, calculate errors

    # get model load path and model scale factor
    model_with_faces_absolute_load_path = "" # for Hodan's error metric
    model_scale_factor = dataset.get_model_scale_factor()
    for model_idx in range(dataset.get_number_of_models()):
        if dataset.get_model_w_faces_name(model_idx) == model_name:
            model_with_faces_absolute_load_path = dataset.get_model_w_faces_path(model_idx)
            break
    if not model_with_faces_absolute_load_path:
        return {'error score': None}

    # get scene depth IMAGE load path
    scene_absolute_load_path = ""
    for scene_idx in range(dataset.get_number_of_scenes()):
        if dataset.get_scene_name(scene_idx) == scene_name:
            scene_absolute_load_path = dataset.get_scene_depth_img_path(scene_idx)
            break
    if not scene_absolute_load_path:
        return {'error score': None}

    # load the model with faces
    model = inout.load_ply(model_with_faces_absolute_load_path)
    #print("model\n", model)

    # scale model
    model["pts"] = model["pts"] * model_scale_factor
    #print("model_points after scaling\n", model)

    # transform gt
    theta = np.radians(180)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0, 0), (0, c, -s, 0), (0, s, c, 0), (0, 0, 0, 1)), dtype='f')
    # 1. recenter model, because we loaded the original model above
    # 2. apply gt-pose of the recentered model to the recentered model
    # 3. rotate gt-pose by 180° about the camera's x-axis, because originally the gt-pose
    # was determined assuming the cameras view-direction was the negative z-axis.
    gt_transformed = concatenate_transforms(R, gt, inverse(model_load_transform))

    # transform pose-hypothesis
    # 1. recenter model, because we loaded the original model above
    # 2. apply pose-hypothesis of the recentered model to the recentered model
    pose_transformed = concatenate_transforms(pose, inverse(model_load_transform))

    #print("gt_transformed\n", gt_transformed)
    #print("pose_transformed\n", pose_transformed)

    # truncating homogeneous matrices to get R and t parts again
    R_est = pose_transformed[0:-1, 0:-1]
    t_est = pose_transformed[0:-1, 3:] * 1000 # pose from matching is in m, we need mm!

    R_gt = gt_transformed[0:-1, 0:-1]
    t_gt = gt_transformed[0:-1, 3:] * 1000 # pose from gt-file is in m, we need mm!

    #print("R_est =\n", R_est)
    #print("t_est =\n", t_est)
    #print("R_gt =\n", R_gt)
    #print("t_gt =\n", t_gt)

    T = np.array((( 0,-1, 0),
                  ( 0, 0, 1),
                  (-1, 0, 0)))

    #R_gt_hodan = np.array((( 0.95056306,  0.30593701, -0.05501342),
    #                       ( 0.25487580, -0.86845921, -0.42548639),
    #                       (-0.17791941,  0.39038865, -0.90340488)), dtype='f')
    #t_gt_hodan = np.array((163.38414646, 147.18650225, 940.12661319), dtype='f')

    # transform with T to get Hodan's ground truth coordinate system
    R_est = R_est.dot(T)
    R_gt = R_gt.dot(T)
    #print("scene:", scene_name, " model:", model_name)
    #print("after manipulation:")
    #print("R_est =\n", R_est)
    #print("t_est =\n", t_est)
    #print("R_gt =\n", R_gt)
    #print("t_gt =\n", t_gt)
    #print("hodan's ground truth is:\n",R_gt_hodan,"\n", t_gt_hodan)

    # load intrinsic camera parameters from file
    K = inout.load_cam_params(dataset.get_camera_parameters_path())['K']
    #print("K:\n", K)

    # load depth-image (.png) of scene
    scene_depth_image = inout.load_depth(scene_absolute_load_path)

    # delta and tau are thresholds, which are defined in [Hodan, Michel et al. 2018]
    delta = 15 # 15mm
    tau = 20 # 20mm

    # calculate error
    #error_score = PE.vsd(R_est, t_est, R_gt, t_gt, model, scene_depth_image, K,
    #                     delta, tau, cost_type='step')

    #user_input = raw_input("Press 'y' to show vis-mask, or 'n' to skip visualization...")
    # calculate error and visibility-degree
    error_and_visibility_degree = PE.vsd_and_visDeg(R_est, t_est, R_gt, t_gt, model, scene_depth_image, K,
                                                    delta, tau, cost_type='step',
                                                    user_input='n')

    #print("error and visibility degree:", error_and_visibility_degree)

    # for comparison / debugging:
    #translational_error = PE.te(t_est, t_gt)
    #rotational_error = PE.re(R_est, R_gt)
    #print("Hodans Rotational Error = %s, Translational Error = %s"
    #      %(rotational_error, translational_error))
    #xavers_pose_error = pose_error(pose, gt)
    #print("Xavers Rotational Error = %s, Translational Error = %s"
    #      %(xavers_pose_error['angle'], xavers_pose_error['distance']))

    #programPause = raw_input("Press <ENTER> to continue...")
    return error_and_visibility_degree



# noinspection PyShadowingNames
def pose_error_ok(error, distance_thresh, angle_thresh):
    """
    check if the pose error is within the threshold values
    @param error: pre-calculated error, dict of form {'distance': float, 'angle': float}
    @param distance_thresh: max. allowed distance error
    @param angle_thresh: max. allowed angle error
    @return: True, False or None if the pose error contained a None-value
    """
    angle_error = error['angle']
    dist_error = error['distance']

    if None in [angle_error, dist_error]:
        return None
    else:
        return angle_error <= angle_thresh and dist_error <= distance_thresh


# ################### transformation of poses ###################################################
# noinspection PyShadowingNames
def concatenate_transforms(*transforms):
    """
    concatenate transformations and calculate the final transformation
    @param transforms: transformations, can be represented by 4x4 homogeneous matrices or dict
                       of form {"quaternion": xyzw-list, "translation": xyz-list}
    @return: 4x4 matrix representing final transformation
    """
    if len(transforms) < 1:
        raise ValueError('Need at least 1 transform to concatenate, got %d.' % len(transforms))

    total_transform = tft.identity_matrix()
    for t in transforms:
        total_transform = total_transform.dot(to_matrix(t))
    return total_transform


def inverse(pose):
    """
    calculate the inverse of the given pose / transform
    @param pose: pose as dict or 4x4 homogeneous transformation matrix
    @return: 4x4 homogeneous transformation matrix that is the inverse of pose
    """
    return tft.inverse_matrix(to_matrix(pose))


# ############ convenience functions for pose transforms ##################################

def transform_gt(ground_truth, model_load_transform, scene_load_transform):
    """
    transform a ground truth so that it fits for a model and scene that were transformed upon
    loading (after the ground truth was determined)
    @param ground_truth: the original ground truth, pose dict or 4x4 matrix
    @param model_load_transform: additional transform of the model, pose dict or 4x4 matrix
    @param scene_load_transform: additional transform of the scene, pose dict or 4x4 matrix
    @return: 4x4 matrix representing new ground truth
    """
    return concatenate_transforms(inverse(scene_load_transform),
                                  ground_truth,
                                  model_load_transform)


def transform_model_pose(model_pose, equivalent_pose_transform):
    """
    transform a model pose with an equivalent pose transformation (because the model exhibited
    rotational symmetry)
    @param model_pose: original model pose, pose dict or 4x4 matrix
    @param equivalent_pose_transform: pose dict or 4x4 matrix
    @return: new model pose as 4x4 matrix
    """
    return concatenate_transforms(model_pose, equivalent_pose_transform)

if __name__ == '__main__':
    # ################################ dict <-> matrix conversion ###############################

    # test error calculation of Hinterstoisser's error-metric
    from pf_matching.experiments.base_experiments import ExperimentBase

    dataset = ExperimentBase.load_dataset('~/ROS/datasets/geometric_primitives/blensor_dataset.py')
    model_name = 'cuboid'
    pose = {'quaternion': [  0.0860227569937706,
                            -0.8472607731819153,
                            -0.14622388780117035,
                             0.503356397151947 ],
            'translation': [ 0.059938158839941025,
                            -0.06636304408311844,
                            -0.7864593863487244  ] }
    gt = {'quaternion': [ 0.08679633943388332,
                         -0.8448862577065529,
                         -0.14550248490556286,
                          0.5074077827429054 ],
            'translation': [  0.060044970363378525,
                             -0.06445939838886261,
                             -0.7866469621658325   ] }
    pose_error_by_hinterstoisser_lepetit_et_al_2013(pose, gt, dataset, model_name)
    input('HIT ENTER TO CONTINUE ...')


    # test converting back and forth
    for i in range(10):
        test_matrix = tft.translation_matrix(tft.random_vector(3)).dot(tft.random_rotation_matrix())
        assert tft.is_same_transform(test_matrix, pose_dict2matrix(matrix2pose_dict(test_matrix)))

    # test right order of multiplications
    test_matrix = tft.identity_matrix()
    test_matrix[:3, 3] = [1, 2, 3]
    assert tft.is_same_transform(test_matrix, pose_dict2matrix({'translation': [1, 2, 3],
                                                                'quaternion': [0, 0, 0, 1]}))

    # test None inputs
    none_dict = {'translation': None, 'quaternion': None}
    assert pose_dict2matrix(none_dict) is None
    assert matrix2pose_dict(None) == none_dict

    # ############################ differences between poses ####################################
    dist_thresh = angle_thresh = 10**-6

    # test numerical stability if quaternion dot product is > 1
    test_dict = {'translation': [0, 0, 0],
                 'quaternion': [-0.18588782, 0.13853682, 0.96088771, 0.15148623]}
    assert pose_error_ok(pose_error(test_dict, test_dict), dist_thresh, angle_thresh)
    test_dict = {'translation': [0, 0, 0],
                 'quaternion': [-0.14505772, 0.92636827, -0.18142588, -0.2964536]}
    assert pose_error_ok(pose_error(test_dict, test_dict), dist_thresh, angle_thresh)

    # test equality with matrices
    for i in range(10):
        test_matrix = tft.translation_matrix(tft.random_vector(3)).dot(tft.random_rotation_matrix())
        assert pose_error_ok(pose_error(test_matrix, test_matrix), dist_thresh, angle_thresh)

    # ################ pose conversion ################################################
    t = [1, 2, 3]
    q = [-0.14505772, 0.92636827, -0.18142588, -0.2964536]
    t3 = {'translation': t,
          'quaternion': [0, 0, 0, 1]}
    t2 = tft.translation_matrix(t)
    t1 = {'translation': [0, 0, 0],
          'quaternion': q}
    t_result = tft.quaternion_matrix(q)
    t_result[:3, 3] = [2 * i for i in t]
    assert np.allclose(t_result, concatenate_transforms(t3, t2, t1))

    # #######################################################################
    print ('All checks passed.')
