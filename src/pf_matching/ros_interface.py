# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

# ROS related
import rospy
import roslaunch
import rospkg
import dynamic_reconfigure.client

# ROS messages
import std_msgs.msg
import geometry_msgs.msg
import pf_matching_core.msg

# ROS services
import std_srvs.srv

# project related
from pf_matching import data_structure
from pf_matching.pose_tools import *

# other
import numpy as np
import functools
import threading
import time


# noinspection PyPep8Naming
class ExperimentInterface(object):
    """
    provide the interface to ROS for experiments with pf-matching

    @warning  This class should be used inside a with-statement. Ff you instantiate it manually,
              make sure that you call the _cleanup_ROS()-method or you will end up with
              zombie-threads from launching launch-files.
    """

    def __init__(self, data_storage):
        """
        constructor
        @type data_storage: data_structure.Experiment_data_structure
        @param data_storage: all collected data get's stored into this
        """

        # data storage
        if not isinstance(data_storage, data_structure.ExperimentDataStructure):
            raise TypeError("Data storage has not the expected type.")
        self.data = data_storage
        self._data_lock = threading.Lock()  # callbacks might be executed in parallel

        # output
        self._show_statistics_callback = False
        self._show_dyn_reconfigure_callback = False

        # internal state
        self._current_run = -2  # starts at -1 for initial configuration after incrementation
        self.new_run()
        self._n_models = 0  # number of models currently in the matcher

        # ROS-related
        self._ROS_launch_parent = None      # storage for sub-processes for launch files
        self._dyn_reconfigure_clients = {}  # store as server node name : client object
        rospy.init_node("experiment_interface")

        # ROS subscribers
        self._sub_poses = None
        self._sub_intermediate_poses = None
        self._sub_statistics = None
        self._sub_statistics_arrays = None
        self._sub_load_transform = None

        # ROS publishers
        self._pub_model_path = None
        self._pub_scene_path = None
        self._pub_poses = None

        # initialize statistics subscribers
        # TODO: should be done by user, keep for now for backwards compatibility
        self.setup_intermediate_pose_subscriber()
        self.setup_statistics_subscriber()
        self.setup_statistics_array_subscriber()

    def __enter__(self):
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_value, traceback):
        self._cleanup_ROS()

    def __del__(self):
        self._cleanup_ROS()

    # noinspection PyPep8Naming
    def _cleanup_ROS(self):
        """
        shut down all launch files and the interface node
        @return: None
        """
        # close down launcher
        if self._ROS_launch_parent is not None:
            self._ROS_launch_parent.shutdown()

        # close down the node
        rospy.signal_shutdown("Interface closed by clean-up method.")

    def set_output_levels(self, statistics_callbacks=False, dyn_reconfigure_callbacks=False):
        """
        set which messages should be printed to console
        @param statistics_callbacks: if True, shown a message when receiving a statistics parameter
        @param dyn_reconfigure_callbacks: if True, shown a message when receiving an updated
                                          configuration of a node
        @return:
        """

    # ############### starting up pipeline ##################################################

    def run_launch_files(self, pkg_launch_file_tuples):
        """
        run ROS launch files for the experiment setup
        @param pkg_launch_file_tuples: list of tuples containing the package name and the launch
                                       file name, e.g. [('pkg1', 'setup_xyz.launch'), (...), ...]
                                       It is assumed that all launch files reside in the "/launch"
                                       subdirectory of a package.
        @return: None
        """
        # get an instance of RosPack with the default search paths and resolve launch file locations
        rospack = rospkg.RosPack()
        launch_file_paths = [rospack.get_path(tupl[0]) +
                             "/launch/" +
                             tupl[1] for tupl in pkg_launch_file_tuples]

        # set up logging
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(uuid)

        # make the launcher and start it
        self._ROS_launch_parent = roslaunch.parent.ROSLaunchParent(uuid, launch_file_paths)
        self._ROS_launch_parent.start()

        # save the started launch files for later reference
        with self._data_lock:
            self.data["launchFiles"].append(pkg_launch_file_tuples)

    # ############### various setup functions to connect to ros ###############################

    # TODO: for setup of model and scene publisher: also set up node name  and add dynamic
    # reconfigure server -> automatically set the frame_id via dynamic reconfigure before
    # publishing model / scene
    def setup_model_path_publisher(self, topic_name):
        """
        set up the publisher for model point cloud file paths to the pcLoader node
        @param topic_name: name of the topic for the file path
        @return: None
        """
        self._pub_model_path = rospy.Publisher(topic_name, std_msgs.msg.String, queue_size=10)

    def setup_scene_path_publisher(self, topic_name):
        """
        set up the publisher for scene point cloud file paths to the pcLoader node
        @param topic_name: name of the topic for the file path
        """
        self._pub_scene_path = rospy.Publisher(topic_name, std_msgs.msg.String, queue_size=10)

    def setup_poses_publisher(self, topic_name):
        """
        set up the publisher for poses
        !!! Just use this, if you are working without a matcher and you want to publish messages that normaly come from the matcher !!!
        @param topic_name: name of topic for poses
        """
        self._pub_poses = rospy.Publisher(topic_name, pf_matching_core.msg.WeightedPoseArray, queue_size=10)
        
    
    def setup_load_transform_subscriber(self, topic_name):
        """
        set up the subscriber for load transforms from the pcLoader node
        @param topic_name: name of the topic of the transformations
        """
        self._sub_load_transform = rospy.Subscriber(topic_name,
                                                    geometry_msgs.msg.TransformStamped,
                                                    self._load_transform_callback)

    def setup_statistics_subscriber(self, topic_name="/pf_statistics"):
        """
        set up the subscriber for scalar statistics
        @param topic_name:  name of the topic for scalar statistics
        """
        self._sub_statistics = rospy.Subscriber(topic_name,
                                                pf_matching_core.msg.Statistics,
                                                self._statistics_callback)

    def setup_statistics_array_subscriber(self, topic_name="/pf_statistics_arrays"):
        """
        set up the subscriber for statistics arrays
        @param topic_name:  name of the topic for statistics arrays
        """
        self._sub_statistics_arrays = rospy.Subscriber(topic_name,
                                                       pf_matching_core.msg.StatisticsArray,
                                                       self._statistics_array_callback)

    def setup_poses_subscriber(self, topic_name="/poses"):
        """
        set up the subscriber for the object poses found through matching
        @param topic_name: name of the ROS topic
        """
        self._sub_poses = rospy.Subscriber(topic_name, pf_matching_core.msg.WeightedPoseArray,
                                           self._poses_callback)

    def setup_verifiedposes_subscriber(self, topic_name="/verifiedposes"):
        """
        set up the subscriber for the object poses verified through verier
        @param topic_name: name of the ROS topic
        """
        self._sub_poses = rospy.Subscriber(topic_name, pf_matching_core.msg.WeightedPoseArray,
                                           self._verified_poses_callback)

    def setup_intermediate_pose_subscriber(self, topic_name="/intermediate_poses"):
        """
        set up the subscriber for intermediate poses (raw poses and clustered poses)
        @param topic_name: name of the ROS topic
        """
        self._sub_intermediate_poses = rospy.Subscriber(topic_name,
                                                        pf_matching_core.msg.WeightedPoseArray,
                                                        self._intermediate_poses_callback)

    def setup_dyn_reconfigure_clients(self, node_names):
        """
        @param node_names: list with names of nodes that provide dynamic reconfigure
                           functionality, leading '/' will be added if missing
        """

        for node_name in node_names:

            # add leading '/' for absolute node-name of required
            if node_name[0] is not "/":
                node_name = "/" + node_name

            # set up reconfigure client for the node
            callback = functools.partial(self._dyn_reconfigure_params_callback, node_name)
            self._dyn_reconfigure_clients[node_name] = dynamic_reconfigure.client.Client(
                name=node_name, timeout=20, config_callback=callback)

    def publish_model(self, name, path):
        """
        publish a message with a model file path via self._pub_model_path and save the model name in
        the internal data structure
        @param name: name of the model
        @param path: absolute file path to the model point cloud
        """
        if self._pub_model_path:
            self._pub_model_path.publish(path)
            with self._data_lock:
                self.data.set_nested(["runData", self._current_run, "models", self._n_models], name)
            self._n_models += 1
        else:
            raise ValueError("Publisher for model path was not set up.")

    def publish_scene(self, name, path):
        """
        publish a message with a scene file path via self._pub_scene_path and save the scene name in
        the internal data structure
        @param name: name of the model
        @param path: absolute file path to the model point cloud
        """
        if self._pub_scene_path:
            self._pub_scene_path.publish(path)
            with self._data_lock:
                self.data.set_nested(["runData", self._current_run, "scene"], name)
                self.data.set_nested(["runData", self._current_run, "start_time"],
                                     rospy.Time.now().to_sec())
        else:
            raise ValueError("Publisher for scene path was not set up.")

    def publish_n_best_poses(self, poses_array, n, scene_name, model_name):
        """
        publish a message with the first n values of pose_array via self._pub_poses
        @param pose_array: array of poses_dicts to publish
        @param n: number of poses to publish
        @param scene_name: name of scene
        @param model_name: name of model
        """
        if self._pub_poses:
            msg_out = pf_matching_core.msg.WeightedPoseArray()
            msg_out.model = model_name
            msg_out.sceneHeader.frame_id = scene_name
            msg_out.poseType = 'final'
            poses = []
            
            for i in range(n):
                pose = poses_array[i]
                weightedpose = pose_dict2weighted_pose(pose)
                #print(pose)
                poses.append(weightedpose)
            
            msg_out.poses = poses
            #print(poses)
            self._pub_poses.publish(msg_out)
                
        else:
            raise ValueError("Publisher for poses was not set up.")
            
    
    def clear_models(self, node_name):
        """
        erase all stored models from internal storage and the matcher node
        @param node_name: absolute name of matcher or verifier to clear models from
        @return: None
        """

        try:
            # erase from node
            service = rospy.ServiceProxy(node_name + "/clear_models", std_srvs.srv.Empty)
            service()

            # erase from data structure
            with self._data_lock:
                if self.data.exists(["runData", self._current_run, "models"]):
                    self.data.del_nested(["runData", self._current_run, "models"])
                self._n_models = 0

        except rospy.ServiceException as e:
            print("Service call to clear models failed: %s" % e)

    def set_dyn_reconfigure_parameters(self, node_name, parameters):
        """
        @param node_name: name of dynamically reconfigurable node that was set up by
                        setup_dyn_reconfigure_clients, leading '/' will be added if missing
        @param parameters: dict of the form {"parameter_name" : value}
        """

        # add leading '/' for absolute node-name of required
        if node_name[0] is not "/":
            node_name = "/" + node_name

        while True:
            try:
                result = self._dyn_reconfigure_clients[node_name].update_configuration(parameters)
                return result
            except dynamic_reconfigure.DynamicReconfigureCallbackException:
                print ('service call failed: update of dynamic configuration of ', node_name, ". Trying again ...")
                time.sleep(1)  # wait a moment


    # ############### callbacks ###############################################

    def _dyn_reconfigure_params_callback(self, node_name, config):

        # add leading '/' for absolute node-name of required
        if node_name[0] is not "/":
            node_name = "/" + node_name

        path = ["runData", self._current_run, "dynamicParameters", node_name]

        with self._data_lock:

            # save the configuration under the current run number and node name
            self.data.set_nested(path, dict(config))

            # delete the "groups" entry, data is not needed and partly redundant
            if "groups" in self.data.get_nested(path):
                self.data.del_nested(path + ["groups"])

        if self._show_dyn_reconfigure_callback:
            print("got new dynamic reconfigure parameter set for node " + node_name)

    # noinspection PyProtectedMember
    def _intermediate_poses_callback(self, msg_in):

        poses = {i: weighted_pose2pose_dict(pose) for i, pose in enumerate(msg_in.poses)}
        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, msg_in.poseType + 'Poses',
                                  msg_in.model],
                                 poses)

    def _poses_callback(self, msg_in):

        # noinspection PyShadowingNames
        poses = {i: weighted_pose2pose_dict(pose) for i, pose in enumerate(msg_in.poses)}

        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, "poses", msg_in.model], poses)

    def _verified_poses_callback(self, msg_in):
        
        verified_poses = {i: weighted_pose2pose_dict(pose) for i, pose in enumerate(msg_in.poses)}
        
        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, "verified_poses", msg_in.model], verified_poses)

    # noinspection PyProtectedMember
    def _nesting_path_from_statistics_message(self, msg_in):
        """
        resolve the path for nesting the data from a statistics message; Takes care of empty
        specifiers
        @type msg_in: pf_matching_core.msg.Statistics, pf_matching_core.msg.StatisticsArray
        @param msg_in: statistics message
        @return: list with nesting path
        """
        nesting_path = ["runData", self._current_run, "statistics",
                        msg_in._connection_header['callerid']]

        if not msg_in.specifier:
            nesting_path += [msg_in.parameter_name]
        else:
            nesting_path += [msg_in.specifier, msg_in.parameter_name]

        return nesting_path

    # noinspection PyProtectedMember
    def _statistics_callback(self, msg_in):
        """
        save scalar statistics value
        @type msg_in: pf_matching_core.msg.Statistics
        @param msg_in: the statistics message
        @return: None
        """
        if(self._show_statistics_callback):
            print("got statistics value %f from node %s for parameter %s: %s" %
                  (msg_in.value, msg_in._connection_header['callerid'], msg_in.specifier,
                   msg_in.parameter_name))

        nesting_path = self._nesting_path_from_statistics_message(msg_in)

        with self._data_lock:
            self.data.set_nested(nesting_path, msg_in.value)

    # noinspection PyProtectedMember
    def _statistics_array_callback(self, msg_in):
        """
        save array statistics values
        @type msg_in: pf_matching_core.msg.StatisticsArray
        @param msg_in: the statistics message
        @return: None
        """
        if self._show_statistics_callback:
            print("got statistics array from node %s for parameter %s: %s" %
                  (msg_in._connection_header['callerid'], msg_in.specifier, msg_in.parameter_name))

        nesting_path = self._nesting_path_from_statistics_message(msg_in)

        with self._data_lock:
            self.data.set_nested(nesting_path, list(msg_in.values))

    def _load_transform_callback(self, msg_in):

        translation = msg_in.transform.translation
        quaternion = msg_in.transform.rotation
        frame = msg_in.header.frame_id  # usually model name or scene name

        # assemble the pose
        pose = {"translation": [translation.x, translation.y, translation.z],
                "quaternion": [quaternion.x, quaternion.y, quaternion.z, quaternion.w]}

        self.data.set_nested(["runData", self._current_run, "loadTransforms", frame], pose)

    # ###################### managing runs ###########################################

    def new_run(self):
        """
        initialize a new algorithm run
        @return: None
        """
        with self._data_lock:
            self._current_run += 1
        # self.tfsReceived = 0 # TODO: unused

        return self._current_run

    def wait_for_statistics(self, node, parameter, specifier='', wait_time=0.1, time_out_time=None):
        """
        block until a specified parameter is available in the data structure (and was
               therefore received as a message) for this run or until we time out

        Raises an exception if we time out.

        @param node absolute ROS name of the node, the leading '/' can be omitted
        @param parameter name of the statistics parameter to wait for
        @param specifier additional nesting specifier of message
        @param wait_time time in seconds to wait between checks
        @param time_out_time time in seconds before raising an exception because of timing out,
                           None waits indefinitely
        @return: value of the parameter or None if the waiting timed out
        """
        # add leading '/' for absolute node-name if required
        if node[0] is not "/":
            node = "/" + node

        # nice info message
        print("waiting for statistics parameter '%s' of node '%s' in run %d..." %
              (parameter, node, self._current_run))

        # full path to the parameter
        if specifier:
            path = ["runData", self._current_run, "statistics", node, specifier, parameter]
        else:
            path = ["runData", self._current_run, "statistics", node, parameter]

        # block until parameter exists or we time out
        t_start = time.time()
        while not self.data.exists(path):

            # block
            time.sleep(wait_time)

            # check timeout if set
            if time_out_time is not None:
                if time.time() - t_start > time_out_time:

                    # print message and save info that we timed out in the data structure
                    print("Waiting for statistics timed out after %f seconds" % time_out_time)
                    if self.data.exists(["runData", self._current_run, "timeOutErrors"]):
                        self.data.set_nested(["runData", self._current_run, "timeOutErrors"],
                                             self.data.get_nested(["runData", self._current_run,
                                                                   "timeOutErrors"]) + 1
                                             )
                    else:
                        self.data.set_nested(["runData", self._current_run, "timeOutErrors"], 1)

                    raise Exception("timed out")

        # return parameter
        return self.data.get_nested(path)

    # ####################### acquiring of external data ###############################

    def add_gt_to_current_run(self, gt_matrices, model_name):
        """
        add a model's ground truth pose to the stored data; The data is stored as
               translation and quaternion (stored as xyzw)
        @param gt_matrices 4x4 homogeneous transformations as numpy array or None if no
                                 data is available
        @param model_name name of the model
        """
        if gt_matrices is None:
            gts = {}
        else:
            # noinspection PyShadowingNames
            gts = {i: matrix2pose_dict(gt_matrix) for i, gt_matrix in enumerate(gt_matrices)}

        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, "groundTruth", model_name], gts)

    def add_occlusion_to_current_run(self, degrees_of_occlusion, model_name):
        """
        add a model's degree of occlusion to the stored data
        @param degrees_of_occlusion: list of degrees of occlusion in percent [0,100] or None if
                                     no data is available
        @param model_name name of the model
        """
        if degrees_of_occlusion is None:
            doos = {}
        else:
            # noinspection PyShadowingNames
            doos = {i: doo for i, doo in enumerate(degrees_of_occlusion)}

        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, "degreeOfOcclusion", model_name],
                                 doos)

    def add_dataset_info_to_current_run(self, dataset_name, dataset_units):
        """
        add information about the dataset for the current run
        @param dataset_name: name of the dataset
        @param dataset_units: distance units
        @return: None
        """
        with self._data_lock:
            self.data.set_nested(['runData', self._current_run, 'datasetName'], dataset_name)
            self.data.set_nested(['runData', self._current_run, 'datasetUnits'], dataset_units)

    def add_note_to_current_run(self, what, note):
        """
        add a freely definable note to the current run;
        The note will be stored in the run data in the form {'notes': {what: note}}
        @param what: short nesting hint / description of what the note is about
        @param note: the note, usually a string but can be any serializable python object
        @return: None
        """
        with self._data_lock:
            self.data.set_nested(['runData', self._current_run, 'notes', what], note)
            
    def add_equivalent_poses_to_current_run(self, equivPoses, model_name):
        """
        add informations about equivalentPoses to the current run:
        this is needed for the verification without matcher
        @param equivPoses: dict of equivalent poses for the model
        @param model_name: name of the model
        @return: None
        """
        with self._data_lock:
            self.data.set_nested(["runData", self._current_run, 'equivalentPoses',
                                  model_name],
                                 equivPoses)


class RuntimeEstimator(object):
    """
    keeps track of how long individual runs take, what run it is right now and how much time is left
    """

    def __init__(self, n_runs):
        """
        @param n_runs: total number of runs that will be performed
        """
        self.start_time = -1  # in seconds
        self.n_runs = n_runs
        self.current_run = 0
        self.total_runtime = 0

        print("%d runs are to be performed." % self.n_runs)

    def run_start(self):
        """
        begin measuring a new run
        @return: None
        """
        self.current_run += 1
        self.start_time = time.time()

    def run_end(self):
        """
        end the measuring of a run
        @return: None
        """
        # make sure run_start was called
        if self.start_time < 0:
            raise RuntimeError("run_start() was not called before run_end().")

        # calculate time difference and ETA (= estimated time of arrival)
        time_diff = time.time() - self.start_time
        self.total_runtime += time_diff
        eta = self.total_runtime / self.current_run * (self.n_runs - self.current_run)

        # output
        print("Run %d of %d complete (%.1f %%). Took %.1f s, remaining time: %.1f min" %
              (self.current_run, self.n_runs, self.current_run / self.n_runs * 100, time_diff,
               eta / 60)
              )

        # reset start time
        self.start_time = time.time()


if __name__ == "__main__":
    d = data_structure.ExperimentDataStructure()
    interface = ExperimentInterface(d)
