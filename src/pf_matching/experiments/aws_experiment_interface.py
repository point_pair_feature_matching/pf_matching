# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
import time
import os
import configparser

# import from project
from pf_matching import aws_interface
from pf_matching.data_structure import ExperimentDataStructure

from docutils.nodes import status


class AWSExperimentInterface:
    """
    Class that provides an User-Interface to start, track, kill, etc. a specified experiment-script on an 
    existing AWS AMI instance. It works with an instance of the "aws_interface"-class, but it is more user-friendly. 
    Furthermore, it provides the watchdog-functionality, to stop the AMI as soon as the experiment is done, 
    or as soon as the result-files are transferred, respectively (tracking-mode).
    """
    def __init__(self, path_experiment_script, instance_id ,ssh_private_key_file, 
                 username, save_path_remote, save_path_local, save_file_name,
                 watchdog_filename, watchdog_path_remote, watchdog_path_local):
        
        self.experiment_script = path_experiment_script         # here we can use ~ to specify path, because we resolve that later.
        self.instance_id = instance_id                          # instance ID of the AMI
        self.ssh_key = os.path.expanduser(ssh_private_key_file)
        self.save_path_remote = save_path_remote                # always give absolute path, no ~
        self.save_path_local = save_path_local                  # here we can use ~ to specify path, because we resolve that later.
        self.save_file_name = save_file_name
        self.username = username                                # depends on the AMI you are using. e.g. 'ubuntu' if an ubuntu AMI is being used 
        self.watchdog_path_remote = watchdog_path_remote        # always give absolute path, no ~
        self.watchdog_path_local = watchdog_path_local          # here we can use ~ to specify path, because we resolve that later.
        self.watchdog_filename = watchdog_filename
        self.watchdog_script = watchdog_path_remote+watchdog_filename
    
    def startExperiment(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)
        
        # check if experiment has already been started
        already_running = self.aws.checkIfExperimentIsStillRunning(self.experiment_script) 
        if not already_running:
            # check if roscore has already been started
            while self.aws.checkIfExperimentIsStillRunning("roscore") != 1:
                self.aws.startROScore()
                time.sleep(2)
            # start the experiment
            self.aws.runExperimentOnRemote(self.experiment_script, True)
              
        self.aws.putFileViaSSH(self.watchdog_filename, self.watchdog_path_local, self.watchdog_path_remote)
        
        # check if experiment was started
        status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script)
        if status == 1:
            self.startRemoteWatchdog()
            self.aws.closeSSHconnection()
            print("Experiment successfully started.")
                        
        else:
            print("Something went wrong.")
            self.aws.closeSSHconnection()
        
        
    def checkoutExperiment(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key) 
        reinstanciate_watchdog = self.killRemoteWatchdog()
        status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script, True)    
        # automatically switch to tracking, to prevent unnecessary costs
        if self.confirm('Check finished. Do you want to switch to tracking mode now?'):
            self.aws.closeSSHconnection()
            self.trackExperiment()
        elif reinstanciate_watchdog:
            self.startRemoteWatchdog()
            self.aws.closeSSHconnection()
            
    
    def trackExperiment(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key) 
        self.killRemoteWatchdog()
        status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script, False)
        # check status til experiment is done.
        while status != 0:
            time.sleep(60) # wait x seconds
            status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script, False)
        # experiment done. download the results-file to local machine
        self.aws.getFileViaSSH(self.save_file_name, self.save_path_remote, self.save_path_local)
        self.aws.closeSSHconnection()
        # stop the instance, given that it costs money each second it is running
        self.aws.stopAMI()
    
    
    def stopAMI(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key)
        reinstanciate_watchdog = self.killRemoteWatchdog() # if there is one.
        status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script)
        if status == 1:
            if self.confirm('Experiment is not done yet, stop AMI anyway?'):
                self.aws.stopAMI()
            else:
                if reinstanciate_watchdog:
                    self.startRemoteWatchdog()
        elif status == -1:
            if self.confirm('Status of experiment is not known, stop AMI anyway?'):
                self.aws.stopAMI()
            else:
                if reinstanciate_watchdog:
                    self.startRemoteWatchdog()
        else:
            if self.confirm('Experiment is done. Do you want to save the experiment-data locally?'):
                # experiment done. download the results-file to local machine
                if self.aws.getFileViaSSH(self.save_file_name, self.save_path_remote, self.save_path_local):
                    print('Stopping AMI.')
                    self.aws.stopAMI()
                    return
                else:
                    print('Download went wrong. Not stopping AMI.')
                    if reinstanciate_watchdog:
                        self.startRemoteWatchdog()
                    return
            print('Stopping AMI.')
            self.aws.stopAMI()
            
    
    def killExperiment(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key) 
        reinstanciate_watchdog = self.killRemoteWatchdog()
        if self.confirm('You are about to kill the experiment. Do you really want to continue?'):
            self.aws.terminateExperimentOnRemote(self.experiment_script)
            self.aws.terminateExperimentOnRemote("roscore")
            self.aws.killExperimentOnRemote(self.experiment_script) # usually the process continues running after SIGTERM
        else:
            print('Kill was cancelled.')
        if reinstanciate_watchdog:
            self.startRemoteWatchdog()
        self.aws.closeSSHconnection
    
    
    def downloadExperimentData(self):
        
        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key) 
        reinstanciate_watchdog = self.killRemoteWatchdog() # if there is one.
        if not self.aws.getFileViaSSH(self.save_file_name, self.save_path_remote, self.save_path_local):
            print('Download went wrong.')
        if reinstanciate_watchdog: # set it up again if there was one before download
            self.startRemoteWatchdog()
        self.aws.closeSSHconnection()
    
    
    def confirm(self, message):
        """
        Ask user to enter Y or N (case-insensitive).
        :return: True if the answer is Y.
        :rtype: bool
        """
        answer = ""
        while answer not in ["y", "n"]:
            answer = raw_input(message+" [y/n] ").lower()
        if answer == 'y':
            return True
        else:
            return False
        
        
    def startRemoteWatchdog(self, called_manually = False):
        """
        the only case, in which we don't want the watchdog, is whilst 'tracking' the experiment
        neverthelesll we deactivate it temporarily during performing other checks.
        """
        if called_manually: # only for the case that user called this function manually
            self.aws = aws_interface.AWSinterface(self.instance_id)
            self.aws.establishSSHconnection(self.username, self.ssh_key) 
            if self.aws.checkIfExperimentIsStillRunning(self.experiment_script) == 1: 
                if not self.aws.checkIfExperimentIsStillRunning(self.watchdog_script): 
                    self.aws.runShellCommandOnRemote(self.watchdog_script + " " + self.experiment_script, enable_logout=True)
                    print("Watchdog on remote computer instanciated.")
                else:
                    print("There is already a watchdog on remote computer running.")
                self.aws.closeSSHconnection()
            else:
                print("Experiment is not running. Did not start watchdog on remote computer.")
        else:
            if self.aws.checkIfExperimentIsStillRunning(self.experiment_script) == 1:
                if not self.aws.checkIfExperimentIsStillRunning(self.watchdog_script): 
                    self.aws.runShellCommandOnRemote(self.watchdog_script + " " + self.experiment_script, enable_logout=True)
                    print("Watchdog on remote computer instanciated.")
                else:
                    print("There is already a watchdog on remote computer running.")
            else:
                print("Experiment is not running. Did not start watchdog on remote computer.")
        
    def killRemoteWatchdog(self, called_manually = False):
        
        if called_manually: # only for the case that user called this function manually
            self.aws = aws_interface.AWSinterface(self.instance_id)
            self.aws.establishSSHconnection(self.username, self.ssh_key) 
            result = self.aws.terminateExperimentOnRemote(self.watchdog_script)
            print("Watchdog on remote computer successfully killed?:", result)
            self.aws.closeSSHconnection()
        else:
            result = self.aws.terminateExperimentOnRemote(self.watchdog_script)
            print("Watchdog on remote computer successfully killed?:", result)
            return result


    def updateAWSinstance(self):
        """
        update the git-repositories of pf_matching and pf_matching_core git repos on
        the remote-computer. compiling catkin_workspace afterwards.
        the whole thing works by uploading to and running a shell-script on the remote-computer

        Note: If git-passwords are needed to proceed, they need to be typed into the CLI
        Note: press Ctrl+c to quit the script on the remote-computer

        Note: Tell the remote computer which repos to update by editing the shell-script
        "AWS_scripts_/installation_scripts/git_actualization_and_compiling_script_remote_side.sh"
        You can also use this script to install additional software etc...
        """

        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # push script to client that tells him how to update
        self.update_script_name='git_actualization_and_compiling_script_remote_side.sh'
        if not self.aws.putFileViaSSH(self.update_script_name,
                                      self.watchdog_path_local+'installation_scripts/',
                                      self.watchdog_path_remote):
            print("ERROR: script-file could not be uploaded to AMI client! Aborting.")
            self.aws.closeSSHconnection()
            return

        # start script on remote computer to update its git repo and do compiling afterwards
        self.aws.runShellCommandOnRemote("sudo chmod +x "+self.watchdog_path_remote+self.update_script_name,
                                         enable_logout=False)
        self.aws.runShellCommandOnRemote(self.watchdog_path_remote+self.update_script_name,
                                         enable_logout=False)

        self.aws.closeSSHconnection()

    def htop(self):
        """
        show the remote-computer's system-status (CPU usage, RAM usage, processes)
        press Ctrl+c to quit

        invokes "htop" command on the remote-computer and shows the result.
        """

        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # start htop on remote computer
        self.aws.runShellCommandOnRemote("htop",
                                         enable_logout=False)

        self.aws.closeSSHconnection()

    def tail(self):
        """
        show the progress of the experiment
        press Ctrl+c to quit displaying the progress

        invokes "tail -f nohub.out" command on the remote-computer and shows the result.
        """

        self.aws = aws_interface.AWSinterface(self.instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # start htop on remote computer
        self.aws.runShellCommandOnRemote("tail -f nohub.out",
                                         enable_logout=False)

        self.aws.closeSSHconnection()


class AWSExperimentInterface_spreaded(AWSExperimentInterface):
    """
    Quick and dirty solution to spread an experiment amongst various AMI-instances
    Methods have mostly same functionality as their counterparts in AWSExperimentInterface-Class
    Plus some extra convenience-functions
    """

    def __init__(self, path_experiment_script, instance_id_list, ssh_private_key_file,
                username, save_path_remote, save_path_local, save_file_name,
                watchdog_filename, watchdog_path_remote, watchdog_path_local):

        self.experiment_script = path_experiment_script  # here we can use ~ to specify path, because we resolve that later.
        self.instance_id_list = instance_id_list  # list of instance IDs of the AMIs
        self.ssh_key = os.path.expanduser(ssh_private_key_file)
        self.save_path_remote = save_path_remote  # always give absolute path, no ~
        self.save_file_name = save_file_name
        self.save_path_local = save_path_local # here we can use ~ to specify path, because we resolve that later.
        self.username = username  # depends on the AMI you are using. e.g. 'ubuntu' if an ubuntu AMI is being used
        self.watchdog_path_remote = watchdog_path_remote  # always give absolute path, no ~
        self.watchdog_path_local = watchdog_path_local  # here we can use ~ to specify path, because we resolve that later.
        self.watchdog_filename = watchdog_filename
        self.watchdog_script = watchdog_path_remote + watchdog_filename


    def startExperiment(self, instance_idx):
        """
        Start the experiment on the remote computer
        Also instantiates the "watchdog" on the remote computer, that will shut down the
        AMI-instance as soon as the experiment-process "experiment-script-file.py" is not
        running anymore. That means, the shutdown will be invoked in at least the following cases:
        - the experiment has finished properly
        - the experiment-script crashed, i.e. because of faulty scripts / typos
        - the experiment crashed due to errors in one of the ROS-nodelets

        Note: never call this function parallel, i.e. for more than one AMI-instance.
        This can mix up the ini-file content. the ini-file is uploaded to the
        remote computer and tells him which part of the experiment he needs to
        conduct. In case of mix-up the AMI-instance will execute the wrong part
        of the experiment!

        Note: the content of the ini-file on the remote-computer can be visulaized
        with the show_ini-function! For manual checking.
        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # define the job by the index of the instance_id in the instance_id_list
        if (self.generate_job_file(instance_id)):
            # push information to client that tells him which part of the experiment he has to do
            try:
                self.aws.putFileViaSSH(self.spreading_info_filename, self.watchdog_path_local,
                                       self.watchdog_path_remote)
            except:
                print("ERROR: ini-file could not be uploaded to AMI client! Aborting.")
                self.aws.closeSSHconnection()
                return

            # check if experiment has already been started
            already_running = self.aws.checkIfExperimentIsStillRunning(self.experiment_script)
            if not already_running:
                # check if roscore has already been started
                while self.aws.checkIfExperimentIsStillRunning("roscore") != 1:
                    self.aws.startROScore()
                    time.sleep(2)
                # start the experiment
                self.aws.runExperimentOnRemote(self.experiment_script, True)

            # push watchdog-file to client
            self.aws.putFileViaSSH(self.watchdog_filename, self.watchdog_path_local,
                                   self.watchdog_path_remote)


            # check if experiment was started
            status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script)
            if status == 1:
                self.startRemoteWatchdog()
                self.aws.closeSSHconnection()
                print("Experiment successfully started.")

            else:
                print("Something went wrong.")
                self.aws.closeSSHconnection()
        else:
            print("Generating local job-id-file went wrong. Aborting.")
            self.aws.closeSSHconnection()

    def downloadExperimentData(self, instance_idx):
        """
        download the experiment-data from the remote computer
        rename the file AFTERWARDS such that the filename holds the instance-idx

        ATTENTION: never call this function for more than one AMI-instance at the same time!
        Otherwise the filenames and contents get mixed-up!

        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)
        reinstanciate_watchdog = self.killRemoteWatchdog()  # if there is one.
        job_id = self.instance_id_list.index(instance_id)
        try:
            self.aws.getFileViaSSH(self.save_file_name, self.save_path_remote,
                                      self.save_path_local)
            file_name = os.path.splitext(self.save_file_name)[0]
            file_ext = os.path.splitext(self.save_file_name)[1]
            os.rename(os.path.expanduser(self.save_path_local)+self.save_file_name,
                      os.path.expanduser(self.save_path_local)+file_name+'_'+str(job_id)+file_ext)
        except:
            print('Download or renaming file went wrong.')
            pass

        if reinstanciate_watchdog:  # set it up again if there was one before download
            self.startRemoteWatchdog()
        self.aws.closeSSHconnection()


    def generate_job_file(self, instance_id):
        """
        this file has to be read by the experiment-file on the remote computer
        it tells the experiment-file which part of the experiment is executed on this AMI-instance
        :param instance_id: AMI EC2 instance id
        :return: True if successful
        """
        config = configparser.ConfigParser()
        job_id = self.instance_id_list.index(instance_id)
        config['DEFAULT'] = {'instance_id': instance_id,
                             'job_id': job_id}
        self.spreading_info_filename = 'spreading_info.ini'
        #print(self.watchdog_path_local+self.spreading_info_filename)
        try:
            with open(os.path.expanduser(self.watchdog_path_local)+self.spreading_info_filename, 'w') as configfile:
                config.write(configfile)
            return True
        except:
            print("ERROR: ini-file could not be generated!")
            pass
            return False

    def stopAMI(self, instance_idx):
        """
        stop the AMI-instance with 'index instance_idx'
        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key)
        reinstanciate_watchdog = self.killRemoteWatchdog()  # if there is one.
        status = self.aws.checkIfExperimentIsStillRunning(self.experiment_script)
        if status == 1:
            if self.confirm('Experiment is not done yet, stop AMI anyway?'):
                self.aws.stopAMI()
            else:
                if reinstanciate_watchdog:
                    self.startRemoteWatchdog()
        elif status == -1:
            if self.confirm('Status of experiment is not known, stop AMI anyway?'):
                self.aws.stopAMI()
            else:
                if reinstanciate_watchdog:
                    self.startRemoteWatchdog()
        else:
            if self.confirm('Experiment is done. Do you want to save the experiment-data locally?'):
                # experiment done. download the results-file to local machine

                job_id = self.instance_id_list.index(instance_id)
                try:
                    self.aws.getFileViaSSH(self.save_file_name, self.save_path_remote,
                                           self.save_path_local)
                    os.rename(os.path.expanduser(self.save_path_local) + self.save_file_name,
                              os.path.expanduser(self.save_path_local) + self.save_file_name + '_' + str(job_id))
                    print('Stopping AMI.')
                    self.aws.stopAMI()
                    return
                except:
                    print('Download or renaming file went wrong. Not stopping AMI.')
                    pass
                    if reinstanciate_watchdog:
                        self.startRemoteWatchdog()
                    return

            print('Stopping AMI.')
            self.aws.stopAMI()


    def killExperiment(self, instance_idx):
        """
        stops experiment on remote-computer
        invokes "SIGKILL"-command on experiment-script on remote-computer

        note: if watchdog was active before kill-command, the AMI-instance
        is likely to shut down after invoking this kill-command.
        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.establishSSHconnection(self.username, self.ssh_key)
        reinstanciate_watchdog = self.killRemoteWatchdog()
        if self.confirm('You are about to kill the experiment. Do you really want to continue?'):
            self.aws.terminateExperimentOnRemote(self.experiment_script)
            self.aws.terminateExperimentOnRemote("roscore")
            self.aws.killExperimentOnRemote(
                self.experiment_script)  # usually the process continues running after SIGTERM
        else:
            print('Kill was cancelled.')
        if reinstanciate_watchdog:
            self.startRemoteWatchdog()
        self.aws.closeSSHconnection


    def updateAWSinstance(self, instance_idx):
        """
        update the git-repositories of pf_matching and pf_matching_core git repos on
        the remote-computer. compiling catkin_workspace afterwards.
        the whole thing works by uploading to and running a shell-script on the remote-computer

        Note: If git-passwords are needed to proceed, they need to be typed into the CLI
        Note: press Ctrl+c to quit the script on the remote-computer

        Note: Tell the remote computer which repos to update by editing the shell-script
        "AWS_scripts_/installation_scripts/git_actualization_and_compiling_script_remote_side.sh"
        You can also use this script to install additional software etc...

        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # push script to client that tells him how to update
        self.update_script_name='git_actualization_and_compiling_script_remote_side.sh'
        try:
            self.aws.putFileViaSSH(self.update_script_name,
                                   self.watchdog_path_local+'installation_scripts/',
                                   self.watchdog_path_remote)
        except:
            print("ERROR: script-file could not be uploaded to AMI client! Aborting.")
            self.aws.closeSSHconnection()
            return

        # start script on remote computer to update its git repo and do compiling afterwards
        self.aws.runShellCommandOnRemote("sudo chmod +x "+self.watchdog_path_remote+self.update_script_name,
                                         enable_logout=False)
        self.aws.runShellCommandOnRemote(self.watchdog_path_remote+self.update_script_name,
                                         enable_logout=False)

        self.aws.closeSSHconnection()

    def htop(self, instance_idx):
        """
        show the remote-computer's system-status (CPU usage, RAM usage, processes)
        press Ctrl+c to quit

        invokes "htop" command on the remote-computer and shows the result.
        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # start htop on remote computer
        self.aws.runShellCommandOnRemote("htop",
                                         enable_logout=False)

        self.aws.closeSSHconnection()

    def tail(self, instance_idx):
        """
        show the progress of the experiment
        press Ctrl+c to quit displaying the progress

        invokes "tail -f nohub.out" command on the remote-computer and shows the result.

        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # start htop on remote computer
        self.aws.runShellCommandOnRemote("tail -f nohub.out",
                                         enable_logout=False)

        self.aws.closeSSHconnection()


    def show_ini(self, instance_idx):
        """
        show the content of the ini-file that was stored on the remote computer
        before experiment was started.
        The job-id showed in the ini-file HAS TO BE THE SAME AS instance_idx
        otherwise the AMI-instance conducts the wrong part of the experiment.
        If startExperiment is called for more than one instance at a time, the
        job-id's can get mixed up!!!

        this can be used to manually check if the instance got assigned with the
        correct job-id, i.e. the correct experiment-part.

        press Ctrl+c to quit displaying the ini-file-content

        :param instance_idx: index of the AMI in the instance_id_list in "instance_config.py"
        :return:
        """

        instance_id = self.instance_id_list[instance_idx]

        self.aws = aws_interface.AWSinterface(instance_id)
        self.aws.startAMI()
        self.aws.establishSSHconnection(self.username, self.ssh_key)

        # start htop on remote computer
        self.aws.runShellCommandOnRemote("tail -f spreading_info.ini",
                                         enable_logout=False)

        self.aws.closeSSHconnection()

    def merge_downloaded_files_of_experiment_parts(self):
        """
        Merging data of n files named like
        "..._0.bin", "..._1.bin" ... "..._n-1.bin"
        into one file "..._merged.bin"
        With "..." representing the experiments save-file-name

        Note:
        The number of files that are merged is taken from the length of
        the list of instance_ids in "instance_config.py".
        That means: If the list holds more IDs than the number of parts
        the experiment was split into, this script will fail.
        """

        print("loading data...")

        data_to_merge = []

        for job_id, instance_id in enumerate(self.instance_id_list):

            file_name = os.path.splitext(self.save_file_name)[0]
            file_ext = os.path.splitext(self.save_file_name)[1]
            file_name_and_path = os.path.expanduser(self.save_path_local) + file_name + '_' + str(job_id) + file_ext

            data_with_job_id = ExperimentDataStructure()
            data_with_job_id.from_pickle(file_name_and_path)
            data_to_merge.append(data_with_job_id)

        data_to_append_to = data_to_merge[0]
        data_to_append = data_to_merge[1:]

        print("removing runs with id '-1'...")
        # remove first (empty) run with id '-1'
        for data in data_to_append:
            del data['runData'][-1]

        print("getting information how many runs are available in each data-file...")
        list_of_n_runs = []
        highest_run_idx_of_data = 0
        for data in data_to_merge:
            for run_idx in data['runData']:
                if highest_run_idx_of_data < run_idx:
                    highest_run_idx_of_data = run_idx
            # increment by 1 and store
            list_of_n_runs.append(highest_run_idx_of_data+1)
            highest_run_idx_of_data = 0

        print('data_to_merge has %s runs' % list_of_n_runs)

        print("merging data...")
        # concatenate runs
        for idx, data in enumerate(data_to_append):
            for run_idx in data['runData']:
                data_to_append_to['runData'][run_idx + sum(list_of_n_runs[:idx+1])] = data['runData'][run_idx]

        print("checking index plausibility...")
        # checking plausibility: continuity of new run-ids
        indices_plausible = True
        prev_idx = -2
        last_idx = 0
        for run_idx in sorted(data_to_append_to['runData']):
            print(run_idx)
            diff = run_idx - prev_idx
            last_idx = run_idx
            if diff != 1:
                print("ERROR: new run-indices are not continuous: %s and %s" % (prev_idx, run_idx))
                indices_plausible = False
                break
            else:
                prev_idx = run_idx
        # checkinf plausibility: total number of new runs
        if last_idx != sum(list_of_n_runs)-1:
            print("ERROR: there are too few new run-indices! is:%s should: %s" % (last_idx, sum(list_of_n_runs)-1))
            indices_plausible = False

        if indices_plausible:
            print("All new run-indices seem fine.")
            user_input = raw_input('Hit <ENTER> to SAVE merged data...')

            file_name = os.path.splitext(self.save_file_name)[0]
            file_name_and_path = os.path.expanduser(self.save_path_local) + file_name
            data_to_append_to.to_pickle(file_name_and_path + '_merged.bin')
            #data_to_append_to.to_yaml(SAVE_PATH_WO_EXTENSION + '_merged.yaml')
        else:
            print("indices were not plausible. Aborting...")