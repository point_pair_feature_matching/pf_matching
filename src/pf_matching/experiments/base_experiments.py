#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke, Sebastian Krone, Markus Franz Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import time
import imp
import sys
import os
from operator import itemgetter
from pf_matching.datasets import DatasetBase
from pf_matching.ros_interface import ExperimentInterface, RuntimeEstimator
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation import data_extraction
from pf_matching.evaluation.data_post_processing import compute_pose_errors

TIME_OUT_S = None  # no timeout


class ExperimentBase:
    """
    base class providing a framework for easy implementation of experiments with the ROS pf_matching
    nodes in a reusable way

    To use, inherit from this class and implement the setup(), looping() and end()-members, then
    start the experiment by calling run(). The interface and experiment data_storage structure are
    provided through member variables.

    This class also implements some convenience functions for commonly required tasks, however, you
    are not required to use these.
    """

    def __init__(self, data_storage, save_path_wo_extension='~/experiment_data'):
        """
        constructor; If you implement your own, make sure to run this one inside it!
        @type data_storage: ExperimentDataStructure
        @param data_storage: the data_storage storage to use
        @param save_path_wo_extension: file-path for storing the experiment results without file
                                       extension
        """

        self.data = data_storage
        self.save_path_wo_extension = save_path_wo_extension
        self.interface = None  # of type ExperimentInterface

    def run(self, wait_before_looping=True):
        """
        run the experiment by setting up required classes and calling the user-defined setup(),
        looping() and end() members
        @param wait_before_looping: if True, wait for a user-input after the setup before looping
        """

        with ExperimentInterface(self.data) as self.interface:
            print('Running setup...')
            self.setup()
            time.sleep(1)  # wait for status messages etc. to come back

            if wait_before_looping:
                self.wait_for_enter()

            print('Looping through experiment...')
            self.looping()

            print('Looping done, performing end-actions...')
            self.end()

        print('Experiment done!')

    @staticmethod
    def wait_for_enter(message='Press enter to start...'):
        """
        wait until the user presses enter
        @param message: message to display
        """
        try:
            input(message)
        except SyntaxError:
            pass

    # ############### convenience members #################################################

    @staticmethod
    def load_dataset(dataset_path):
        """
        load a dataset py pointing to it's python class file
        @param dataset_path: path to the Python file containing the dataset class and a
                             get_dataset() function to get a class instances
        @rtype: DatasetBase
        @return: the dataset class providing information about the dataset
        """
        # find first unique name for the dataset's module
        module_name_format = 'imported_dataset_%d'
        i = 0
        while module_name_format % i in sys.modules.keys():
            i += 1

        # replace '~' by absolute file
        dataset_path = os.path.expanduser(dataset_path)

        return imp.load_source(module_name_format % i, dataset_path).get_dataset()

    def launch_and_connect(self,
                           scene_noisifer=False,
                           pkg_launch_file_tuples=(('pf_matching_tools',
                                                    'pipeline_as_nodes.launch'),
                                                   )):
        """
        launch launch files and connect to them, assuming the most common node names
        @param scene_noisifer: if True, also hook up a dynamic reconfigure interface for the
                                scene noisifer
        @param pkg_launch_file_tuples:
        @return:
        """

        # launch the ROS-nodes
        self.interface.run_launch_files(pkg_launch_file_tuples)
        time.sleep(2)

        # hook up dynamic reconfigure interface
        dyn_reconfigurable_nodes = ['matcher', 'scene_preprocessor', 'model_preprocessor',
                                    'model_loader', 'scene_loader']
        if scene_noisifer:
            dyn_reconfigurable_nodes += ['scene_noisifier']
        self.interface.setup_dyn_reconfigure_clients(dyn_reconfigurable_nodes)

        # set up various other connections
        self.interface.setup_model_path_publisher('/model_load_path')
        self.interface.setup_scene_path_publisher('/scene_load_path')
        self.interface.setup_load_transform_subscriber('/load_transforms')
        self.interface.setup_poses_subscriber()
        time.sleep(1)

        print('Launchfiles running, experiment interface hooked up.')

    def launch_and_connect_with_verifier(self,
                                        pkg_launch_file_tuples=(('pf_matching_tools',
                                                               'pipeline_verifying_as_nodes_with_splitter.launch'),
                                                                )):
        # launch the ROS-nodes
        self.interface.run_launch_files(pkg_launch_file_tuples)
        time.sleep(2)
        
        # hook up dynamic reconfigure interface
        dynamic_reconfigurable_nodes = ['verifier', 'matcher', 'scene_preprocessor',
                                        'model_preprocessor', 'model_loader', 'scene_loader']
        self.interface.setup_dyn_reconfigure_clients(dynamic_reconfigurable_nodes)
        
        # set up various other connections
        self.interface.setup_model_path_publisher('/model_load_path')
        self.interface.setup_scene_path_publisher('/scene_load_path')
        self.interface.setup_load_transform_subscriber('/load_transforms')
        self.interface.setup_poses_subscriber('/posehypotheses')
        #self.interface.setup_papazov_statistics_array_subscriber()
        time.sleep(1)

        print('Launchfiles running, experiment interface hooked up.')

    def launch_and_connect_hinterstoisser(self,
                                         pkg_launch_file_tuples=(('pf_matching_tools',
                                                                  'pipeline_verifying_as_nodes_with_splitter.launch'),
                                                                 )):
        """
        launch launch files and connect to them with subscribers and publishers, assuming the most common node names
        addtional subscriber for /verifiedposes
        @type pkg_launch_file_tuples: tuple of tuples
        @param pkg_launch_file_tuples: ( ('name_of_ros-package_of_the_launch_file', 'name_of_launch_file.launch'),( ... ), ...)
        """
        # launch the ROS-nodes
        self.interface.run_launch_files(pkg_launch_file_tuples)
        time.sleep(2)

        # hook up dynamic reconfigure interface
        dynamic_reconfigurable_nodes = ['verifier', 'matcher', 'scene_preprocessor',
                                        'model_preprocessor', 'model_loader', 'scene_loader']
        self.interface.setup_dyn_reconfigure_clients(dynamic_reconfigurable_nodes)

        # set up various other connections
        self.interface.setup_model_path_publisher('/model_load_path')
        self.interface.setup_scene_path_publisher('/scene_load_path')
        self.interface.setup_load_transform_subscriber('/load_transforms')
        self.interface.setup_poses_subscriber('/posehypotheses')
        self.interface.setup_verifiedposes_subscriber('/verifiedposes')
        time.sleep(1)

        print('Launchfiles running, experiment interface hooked up.')


    def launch_and_connect_with_verifier_without_matcher(self,
                                        pkg_launch_file_tuples=(('pf_matching_tools',
                                                               'pipeline_verifying_without_matcher_as_nodes.launch'),
                                                                )):
        # launch the ROS-nodes
        self.interface.run_launch_files(pkg_launch_file_tuples)
        time.sleep(2)
        
        # hook up dynamic reconfigure interface
        dynamic_reconfigurable_nodes = ['verifier', 'scene_preprocessor',
                                        'model_preprocessor', 'model_loader', 'scene_loader']
        self.interface.setup_dyn_reconfigure_clients(dynamic_reconfigurable_nodes)
        
        # set up various other connections
        self.interface.setup_model_path_publisher('/model_load_path')
        self.interface.setup_scene_path_publisher('/scene_load_path')
        self.interface.setup_poses_publisher('/posehypotheses')
        self.interface.setup_load_transform_subscriber('/load_transforms')
        self.interface.setup_poses_subscriber('/posehypotheses')
        self.interface.setup_verifiedposes_subscriber('/verifiedposes')
        time.sleep(1)

        print('Launchfiles running, experiment interface hooked up.')
        
    
    def setup_dataset_properties(self, dataset):
        """
        for a new dataset, use this function to set it up properly. Calling this will:
        * add the name and units of the dataset to the current run's data_storage
        * set the correct scaling factor for the model loader
        @param dataset: the new dataset
        """

        # add general information to data_storage
        self.interface.add_dataset_info_to_current_run(dataset.get_name(), dataset.get_units())

        # set the right scale factor
        model_loader_settings = {'scale_factor': dataset.get_model_scale_factor()}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

    def add_model(self, dataset, model_index, clear_matcher=True):
        """
        add a model to the matcher by publishing it to the model loader;
        blocks until model is built
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
        @param clear_matcher: if True, erase all models from the matcher prior to adding the new
                              model
        @return: d_dist parameter of the model (absolute value)
        """

        # erase old model(s)
        if clear_matcher:
            self.interface.clear_models('/matcher')

        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model and wait for it to be built
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))
        model_d_dist = self.interface.wait_for_statistics('matcher', 'd_dist_abs', model_name,
                                                          time_out_time=TIME_OUT_S)
        return model_d_dist
    
    def add_model_and_get_d_points_abs(self, dataset, model_index, clear_matcher=True):
        """
        add a model to the matcher by publishing it to the model loader;
        blocks until model is built.
        has the advantage, that it returns the value d_points_abs of the model, not its d_dist_abs.
        this becomes handy if we want to use a different resulution for d_dist than the one we use for d_points.
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
        @param clear_matcher: if True, erase all models from the matcher prior to adding the new
                              model
        @return: model_d_points_abs downsample-distance of the model (absolute value)
        """

        # erase old model(s)
        if clear_matcher:
            self.interface.clear_models('/matcher')

        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model 
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))
        
        # get the d_points_abs value of the downsampled model to ensure, that scene point cloud can be downsampled with the 
        # same d_points_abs like the model.
        # model_d_dist can not be used here, because d_dist and d_points might not be the same
        # especially if we use a higher resolution for d_dist than for d_points (nyquist-criterion)
        model_d_points_abs = self.interface.wait_for_statistics('model_preprocessor', 'd_points_abs', model_name,
                                                                time_out_time=TIME_OUT_S)
        
        # wait for it to be built
        self.interface.wait_for_statistics('matcher', 'd_dist_abs', model_name,
                                           time_out_time=TIME_OUT_S)
        
        return model_d_points_abs

    def add_model_and_get_d_points_abs_hinterstoisser(self, dataset, model_index, clear_matcher=True, clear_verifier=True):
        """
        add a model to the matcher by publishing it to the model loader;
        blocks until model is built. also waits until verifier has pre-processed model.
        has the advantage, that it returns the value d_points_abs of the model, not its d_dist_abs.
        this becomes handy if we want to use a different resulution for d_dist than the one we use for d_points.
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
        @param clear_matcher: if True, erase all models from the matcher prior to adding the new
                              model
        @return: model_d_points_abs downsample-distance of the model (absolute value)
        """

        # erase old model(s)
        if clear_matcher:
            self.interface.clear_models('/matcher')
        if clear_verifier:
            self.interface.clear_models('/verifier')

        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))

        # get the d_points_abs value of the downsampled model to ensure, that scene point cloud can be downsampled with the
        # same d_points_abs like the model.
        # model_d_dist can not be used here, because d_dist and d_points might not be the same
        # especially if we use a higher resolution for d_dist than for d_points (nyquist-criterion)
        model_d_points_abs = self.interface.wait_for_statistics('model_preprocessor', 'd_points_abs', model_name,
                                                                time_out_time=TIME_OUT_S)
        # wait for model to be pre-processed by verifier (should be way faster then model-building, but just in case...)
        self.interface.wait_for_statistics('verifier', 'model_diameter', model_name, time_out_time=TIME_OUT_S)
        # wait for model to be built by matcher
        self.interface.wait_for_statistics('matcher', 'd_dist_abs', model_name, time_out_time=TIME_OUT_S)

        return model_d_points_abs


    def add_model_for_verification(self, dataset, model_index, clear_verifier=True):
        """
        MFZ 04th May 2018: inconsistence of argument names, description, what's the intention of this function etc...
        could be, that this method just waits until the model was subsampled and published by the preprocessor.
        could be, that this method does not return the d_dist_abs, but the number of surface points

        add a model to the matcher by publishing it to the model loader;
        blocks until model subsampled and published 
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
	    @param clear_verifier: if True, erase all models from the verifier prior to adding the new
                               model
        @return: n_points_surface parameter of the downsampled model
        """

        # erase old model(s)
        if clear_verifier:
            self.interface.clear_models('/verifier')
            
        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model and wait for it to be preprocessed
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))
        n_points_surface = self.interface.wait_for_statistics('model_preprocessor', 'n_points_surface', model_name, time_out_time=TIME_OUT_S)
        return n_points_surface

    def add_model_for_verification_and_get_d_points_abs(self, dataset, model_index, clear_verifier=True):
        """
        add a model to the matcher by publishing it to the model loader;
        blocks until model subsampled and published;
        used for verification, and for returning the d_points_abs, to sample the scene to the same d_points_abs
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
        @param clear_verifier: if True, erase all models from the verifier prior to adding the new
                               model
        @return: d_points_abs parameter of the downsampled model
        """

        # erase old model(s)
        if clear_verifier:
            self.interface.clear_models('/verifier')

        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model and wait for it to be preprocessed
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))
        model_d_points_abs = self.interface.wait_for_statistics('model_preprocessor', 'd_points_abs', model_name,
                                                          time_out_time=TIME_OUT_S)
        self.interface.wait_for_statistics('model_preprocessor', 'n_points_surface', model_name,
                                           time_out_time=TIME_OUT_S)
        return model_d_points_abs

        
    def set_scene_preprocessor_d_points(self, d_points, is_relative):
        """
        set the d_points parameter of the scene preprocessor to a value
        @param d_points: value to use
        @param is_relative: if True, d_points is relative to scene cloud diameter, if False, it
                            @permission absolute
        """

        scene_preprocessor_settings = {'d_points': d_points,
                                       'd_points_is_relative': is_relative}
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)
        
    def add_scene(self, dataset, scene_index):
        """
        add a scene to the scene loader and trigger the matching process;
        blocks until matching is done
        @type dataset: DatasetBase
        @param dataset: the dataset of the scene
        @param scene_index: the index of the scene in the dataset
        """

        # set up scene loader
        scene_loader_settings = {'frame_id': dataset.get_scene_name(scene_index)}
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_settings)

        # publish scene and trigger matching
        self.interface.publish_scene(*dataset.get_scene_name_and_path(scene_index))

        # block until matching is done
        self.interface.wait_for_statistics('matcher', 'matched_models', time_out_time=TIME_OUT_S)

    def add_scene_for_verification(self, dataset, scene_index):
        """
        add a scene to the scene loader and trigger the matching and verifying process;
        blocks until verification is done
        @type dataset: DatasetBase
        @param dataset: the dataset of the scene
        @param scene_index: the index of the scene in the dataset
        """

        # set up scene loader
        scene_loader_settings = {'frame_id': dataset.get_scene_name(scene_index)}
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_settings)

        # publish scene and trigger matching
        self.interface.publish_scene(*dataset.get_scene_name_and_path(scene_index))
        # wait until verifier has done its work
        self.interface.wait_for_statistics('verifier', 'verified_n_posehypotheses', dataset.get_scene_name(scene_index), time_out_time=TIME_OUT_S)
        
    def add_scene_for_verification_without_matcher(self, dataset, scene_index):
        """
        add a scene to the scene loader and trigger the matching and verifying process;
        blocks until verification got scenecloud
        @type dataset: DatasetBase
        @param dataset: the dataset of the scene
        @param scene_index: the index of the scene in the dataset
        """

        # set up scene loader
        scene_loader_settings = {'frame_id': dataset.get_scene_name(scene_index)}
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_settings)

        # publish scene and trigger matching
        self.interface.publish_scene(*dataset.get_scene_name_and_path(scene_index))
        # wait until preprocessor has done his work
        self.interface.wait_for_statistics('verifier', 'scene_preparation_time_ms', dataset.get_scene_name(scene_index), time_out_time=TIME_OUT_S)
        
    def copy_equivalentPoses_from_data(self, dataset, iDict, base_data, model_indices):
        
         for model_index in model_indices:
            model_name = dataset.get_model_name(model_index)
            relevant_poses_data = \
                [entry[iDict['equivalentPoses']] for entry in base_data
                 if entry[iDict['model']] == model_name
                 and entry[iDict['datasetName']] == dataset.get_name()]

            #print("relevant_poses_data =", relevant_poses_data)
            #wait = input("PRESS ENTER TO CONTINUE.")

            equivPosesDict = relevant_poses_data[0]

            #print("equivPosesDict = ", equivPosesDict)
            #wait = input("PRESS ENTER TO CONTINUE.")

            try:
                #print(equivPosesDict[model_name])
                self.interface.add_equivalent_poses_to_current_run(equivPosesDict[model_name], model_name)
            except KeyError:
                #print("no equivalent Pose found. Skipping.")
                pass
            
        
    def add_poses_from_data(self, dataset, iDict, base_data, scene_name, model_indices, n_best_poses=10):
        """
        get matched poses from the Experiment data for this specific scene-model combination
        and publish it to the /posehypothes topic
        @type dataset: DatasetBase
        @param dataset: the dataset of the model and scene
        @param iDict: extracted dictionary from ExperimentData
        @param base_data: extracted data from ExperimentData
        @param scene_name: index of the scene in the dataset
        @param model_indices: list with indices of the models in the dataset
        """
        #print("add_poses_from_data, base_data =",base_data)
        for model_index in model_indices:
            model_name = dataset.get_model_name(model_index)
            relevant_data = \
                [entry[iDict['poses']] for entry in base_data
                 if entry[iDict['scene']] == scene_name
                 and entry[iDict['model']] == model_name
                 and entry[iDict['datasetName']] == dataset.get_name()]

            #print ("relevant_data =", relevant_data)
            #wait = input("PRESS ENTER TO CONTINUE.")

            for poses in relevant_data:
                self.interface.publish_n_best_poses(poses, n_best_poses, scene_name, model_name)


    def add_poses_from_data_hinterstoisser(self, dataset, iDict, base_data, scene_name,
                                           model_indices, n_best_poses=2):
        """
        get matched poses from the Experiment data for this specific scene-model combination
        and publish it to the /posehypothes topic

        Note: publish the n best poses of each voting ball

        @type dataset: DatasetBase
        @param dataset: the dataset of the model and scene
        @param iDict: extracted dictionary from ExperimentData
        @param base_data: extracted data from ExperimentData
        @param scene_name: index of the scene in the dataset
        @param model_indices: list with indices of the models in the dataset
        """
        #print("add_poses_from_data, base_data =",base_data)
        for model_index in model_indices:
            model_name = dataset.get_model_name(model_index)
            relevant_data = \
                [entry[iDict['poses']] for entry in base_data
                 if entry[iDict['scene']] == scene_name
                 and entry[iDict['model']] == model_name
                 and entry[iDict['datasetName']] == dataset.get_name()]

            #print ("relevant_data =", relevant_data)
            #wait = input("PRESS ENTER TO CONTINUE.")

            # we need to filter relevant data to get only the n best poses of each voting ball
            # 1. get number of voting balls
            list_of_voting_ball_indices =[]
            for pose in relevant_data[0].values():
                list_of_voting_ball_indices.append(pose['votingBall'])
            number_of_voting_balls = len(set(list_of_voting_ball_indices))
            #print('number of voting balls', number_of_voting_balls)

            for poses in relevant_data:
                #print('poses =', poses)

                # 2. get indices of poses which have to be deleted,
                #    because we want to keep and publish only the n best of each voting ball
                count = []
                poses_to_delete = []
                for voting_ball_index in range(number_of_voting_balls):
                    count.append(0)
                    for pose_idx, pose in sorted(poses.items()):
                        if pose['votingBall'] == voting_ball_index+1:
                            # found a pose for this voting ball
                            #print('pose', pose_idx, ':', pose)
                            count[voting_ball_index] += 1
                            if count[voting_ball_index] > n_best_poses:
                                # pose is too much, mark pose as 'to be deleted'
                                poses_to_delete.append(pose_idx)
                #print('count', count)
                #print('poses_to_delete =', poses_to_delete)
                # 3. delete dispensable poses
                for pose_idx in poses_to_delete:
                    del poses[pose_idx]

                # 4. rename pose indices. they need to run from 0 to ((n_best_poses*number_of_voting_balls) -1)
                #    otherwise publish_n_best_poses() won't work
                for new_index in range(n_best_poses*number_of_voting_balls):
                    if new_index not in poses:
                        counter = -1
                        for old_pose_idx, pose in sorted(poses.items()):
                            counter +=1
                            if counter == new_index:
                                poses[new_index] = poses[old_pose_idx]
                                del poses[old_pose_idx]

                #for pose_idx, pose in sorted(poses.items()):
                #    print('pose', pose_idx,':', pose)

                # 5. publish n best poses of each voting ball
                self.interface.publish_n_best_poses(poses, n_best_poses*number_of_voting_balls, scene_name, model_name)

    def add_info_from_dataset(self, dataset, scene_index, model_indices):
        """
        save information from the dataset for this specific scene-model combination
        saved information:
        * ground truth
        * degree of occlusion
        @type dataset: DatasetBase
        @param dataset: the dataset of the model and scene
        @param scene_index: index of the scene in the dataset
        @param model_indices: list with indices of the models in the dataset
        """

        for model_index in model_indices:
            # save ground truth data_storage and degree of occlusion
            gt = dataset.get_ground_truth(model_index, scene_index)
            self.interface.add_gt_to_current_run(gt, dataset.get_model_name(model_index))

            doo = dataset.get_degree_of_occlusion(model_index, scene_index)
            self.interface.add_occlusion_to_current_run(doo, dataset.get_model_name(model_index))

    # ############### advanced convenience members ########################################

    def loop_scenes(self, dataset, runtime_estimator, model_indices, scene_indices=None):
        """
        simple looping over the scenes of a dataset and matching the models present in the matcher
        for the models defined by model_indices, data_storage from the dataset is added to the run
        data_storage
        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to add information for
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        @return:
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            print ('scene %d: %s' % (scene_index, dataset.get_scene_name(scene_index)))
            runtime_estimator.run_start()

            self.add_scene(dataset, scene_index)
            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(1)  # wait for remaining statistics messages to arrive

            runtime_estimator.run_end()
            self.interface.new_run()

            print('Saving temporary data for this and all previous scenes in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def loop_scenes_for_verification(self, dataset, runtime_estimator, iDict, base_data, model_indices, scene_indices=None):
        """
        simple looping over the scenes of a dataset and matching the models present in the matcher
        for the models defined by model_indices, data_storage from the dataset is added to the run
        data_storage
        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
	    @param iDict: ? it is not used
	    @param base_data: ? it is not used
        @param model_indices: list of indices of the models to add information for
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        @return:
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            scene_name = dataset.get_scene_name(scene_index)
            print ('scene %d: %s' % (scene_index, scene_name))
            runtime_estimator.run_start()
            
            self.add_scene_for_verification(dataset, scene_index)
            
            # just block until verification is done if the matcher found poses ## whats with the "just" here?! what alternative would be there? 
	        # MFZ 04th May 2018: why use wait_for_statistics() here again?? it's already used in add_scene_for_verification()!!!!
            self.interface.wait_for_statistics('verifier', 'verified_n_posehypotheses', scene_name, time_out_time=TIME_OUT_S) 

            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(1)  # wait for remaining statistics messages to arrive

            runtime_estimator.run_end()
            self.interface.new_run()

    def loop_scenes_for_hinterstoisser(self, dataset, runtime_estimator, model_indices, scene_indices=None):
        """
        simple looping over the scenes of a dataset and matching the models present in the matcher
        for the models defined by model_indices, data_storage from the dataset is added to the run
        data_storage
        waiting for the verifier to finish until next pose will be published.
        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to add information for
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        @return:
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            scene_name = dataset.get_scene_name(scene_index)
            print('scene %d: %s' % (scene_index, scene_name))
            runtime_estimator.run_start()

            self.add_scene_for_verification(dataset, scene_index)

            # block until verification is done if the matcher found poses
            # MFZ 04th May 2018: why does Krone use wait_for_statistics() here again?? it's already used in add_scene_for_verification()!!!!
            #self.interface.wait_for_statistics('verifier', 'verified_n_posehypotheses', scene_name,
            #                                   time_out_time=TIME_OUT_S)

            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(1)  # wait for remaining statistics messages to arrive

            print('Saving temporary data for this and all previous scenes in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')

            runtime_estimator.run_end()
            self.interface.new_run()

    def loop_scenes_for_verification_without_matcher(self, dataset, runtime_estimator, iDict, base_data, model_indices, scene_indices=None):
        """
        simple looping over the scenes of a dataset and matching the models present in the matcher
        for the models defined by model_indices, data_storage from the dataset is added to the run
        data_storage
        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to add information for
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        @return:
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            scene_name = dataset.get_scene_name(scene_index)
            print ('scene %d: %s' % (scene_index, scene_name))
            runtime_estimator.run_start()
            
            self.add_scene_for_verification_without_matcher(dataset, scene_index)
            
            print("adding poses")
            self.add_poses_from_data(dataset, iDict, base_data, scene_name, model_indices)
     
            # just block until verification is done if the matcher found poses
            self.interface.wait_for_statistics('verifier', 'verified_n_posehypotheses', scene_name, time_out_time=TIME_OUT_S)

            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(0.2)  # wait for remaining statistics messages to arrive

            runtime_estimator.run_end()
            self.interface.new_run()

    def loop_scenes_for_verification_without_matcher_hinterstoisser(self, dataset, runtime_estimator,
                                                                    iDict, base_data, model_indices,
                                                                    scene_indices=None, n_best_poses=2):
        """
        simple looping over the scenes of a dataset and publishing the models' poses that were previously calculated
        "simulates" matching to save time! Does not use a real matcher but uses the stored poses
        for the models defined by model_indices.

        Note: for publishing the first two best poses of each voting ball. as input for verifier

        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to add poses for
        @param scene_indices: list of indices of the scenes to publish the poses for, or None to use
                              all scenes
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            scene_name = dataset.get_scene_name(scene_index)
            print('scene %d: %s' % (scene_index, scene_name))
            runtime_estimator.run_start()

            self.add_scene_for_verification_without_matcher(dataset, scene_index)

            print("Adding the %s best poses of each voting ball" %n_best_poses)
            self.add_poses_from_data_hinterstoisser(dataset, iDict, base_data, scene_name,
                                                    model_indices, n_best_poses=n_best_poses)

            # just block until verification is done if the matcher found poses
            self.interface.wait_for_statistics('verifier', 'verified_n_posehypotheses', scene_name,
                                               time_out_time=TIME_OUT_S)

            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(0.2)  # wait for remaining statistics messages to arrive

            runtime_estimator.run_end()
            self.interface.new_run()


    def loop_scenes_with_all_models_for_verification_without_matcher(self, 
                                                                    dataset,
                                                                    runtime_estimator,
                                                                    data,
                                                                    model_indices=None,
                                                                    scene_indices=None):
        """
	    **** MFZ 15.05.2018 note: this function may be completely screwed up... no definition of which d_dist to use for
	    matching the scene. ****
	
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_dist
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go
	
        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
	    @param data: ?
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """
        iDict, base_data = \
            data_extraction.extract_data(data,
                                         [['poses']],
                                         [['equivalentPoses'], # why is this key not in the 'model_specific'-list with 'poses'?
                                          ['datasetName']])
        
        
        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        
        self.interface.clear_models('/verifier')        
        # add models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            self.add_model_for_verification(dataset, model_index, clear_verifier=False)
            self.copy_equivalentPoses_from_data(dataset, iDict, base_data, [model_index])
        # MFZ 04th May 2018 here first all models get build, then we loop over all scenes. that differs from the other functions
	    # which d_dist is used here for the scene?
        self.loop_scenes_for_verification_without_matcher(dataset, runtime_estimator, iDict, base_data, model_indices, scene_indices)

    def loop_scenes_with_one_model_for_verification_without_matcher(self,
                                                                     dataset,
                                                                     runtime_estimator,
                                                                     data,
                                                                     model_indices=None,
                                                                     scene_indices=None,
                                                                     use_equivalent_poses=True,
                                                                     n_best_poses=2):
        """
        NOTE: use only with Hinterstoissers Voting Balls

        looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_dist
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param data: Experiment-results/data of matching-process with real matcher. Is necessary to simulate matcher.
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """
        if use_equivalent_poses:
            iDict, base_data = \
                data_extraction.extract_data(data,
                                             [['poses']],
                                             [['equivalentPoses'],
                                              ['datasetName']])
        else:
            iDict, base_data = \
                data_extraction.extract_data(data,
                                             [['poses']],
                                             [['datasetName']])

        #print ("iDict =", iDict)
        #print ("base_data =", base_data)
        #wait = input("PRESS ENTER TO CONTINUE.")

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        self.interface.clear_models('/verifier')
        # add models
        for model_index in model_indices:
            print('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_points_abs = self.add_model_for_verification_and_get_d_points_abs(dataset,
                                                                                      model_index,
                                                                                      clear_verifier=True)

            # set sampling distance d_points_abs of scenes to the same as d_points_abs of the current model
            self.set_scene_preprocessor_d_points(model_d_points_abs, is_relative=False)

            if use_equivalent_poses:
                self.copy_equivalentPoses_from_data(dataset, iDict, base_data, [model_index])

            self.loop_scenes_for_verification_without_matcher_hinterstoisser(dataset,
                                                                             runtime_estimator,
                                                                             iDict, base_data,
                                                                             [model_index],
                                                                             scene_indices,
                                                                             n_best_poses=n_best_poses)

            print('Saving temporary data for this model and all previous models in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def loop_models_and_scenes(self, dataset, runtime_estimator,
                               model_indices=None, scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_dist
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_dist_abs = self.add_model(dataset, model_index, clear_matcher=True)
            self.set_scene_preprocessor_d_points(model_d_dist_abs, is_relative=False)

            self.loop_scenes(dataset, runtime_estimator, [model_index], scene_indices)

            print('Saving temporary data for this and all previous models in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')

    def loop_models_and_scenes_with_same_d_points_for_model_and_scene(self, dataset, runtime_estimator,
                                                                      model_indices=None, scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_points (!)
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_points_abs = self.add_model_and_get_d_points_abs(dataset, model_index, clear_matcher=True)
            self.set_scene_preprocessor_d_points(model_d_points_abs, is_relative=False)

            self.loop_scenes(dataset, runtime_estimator, [model_index], scene_indices)

            print('Saving temporary data for this and all previous models in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')


    def loop_models_and_scenes_with_same_d_points_for_model_and_scene_hinterstoisser(self, dataset, runtime_estimator,
                                                                                     model_indices=None, scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_points (!)
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_points_abs = self.add_model_and_get_d_points_abs_hinterstoisser(dataset, model_index,
                                                                                    clear_matcher=True,
                                                                                    clear_verifier=True)
            self.set_scene_preprocessor_d_points(model_d_points_abs, is_relative=False)

            self.loop_scenes_for_hinterstoisser(dataset, runtime_estimator, [model_index], scene_indices)

            print('Saving temporary data for this and all previous models in case something goes wrong...')
            self.data.to_pickle(self.save_path_wo_extension + '.bin')


    def loop_models_and_scenes_with_constant_d_points(self, dataset, runtime_estimator,
                               model_indices=None, scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * the scene pre-processor's d_points is NOT set here. One has to set it up manually before using this function.
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            self.add_model(dataset, model_index, clear_matcher=True)
            
            self.loop_scenes(dataset, runtime_estimator, [model_index], scene_indices)

    def loop_models_and_scenes_for_verification_without_matcher(self, 
                                                                dataset, 
                                                                runtime_estimator, 
                                                                data, 
                                                                model_indices=None, 
                                                                scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_dist
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """
        iDict, base_data = \
            data_extraction.extract_data(data,
                                         [['poses']],
                                         [['equivalentPoses'],
                                          ['datasetName']])
        
        
        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_dist_abs = self.add_model_for_verification(dataset, model_index, clear_matcher=True, clear_verifier=True)
            self.copy_equivalentPoses_from_data(dataset, iDict, base_data, [model_index])
            #self.set_scene_preprocessor_d_points(model_d_dist_abs, is_relative=False)   ## MFZ May the 4th 2018: why is this commented??? Don't we need to specify the d_dist to subsample the scene correctly?
            
            self.loop_scenes_for_verification_without_matcher(dataset, runtime_estimator, iDict, base_data, [model_index], scene_indices)
            
    # ############### members to be implemented by the child class ########################
    def setup(self):
        """
        user-implemented member function that sets up the pipeline / nodes as required for the
        experiment
        @return: None
        """
        raise NotImplementedError

    def looping(self):
        """
        user-implemented member function that facilitates the actual experiment by looping over
        parameter combinations, datasets etc. and performs the matching and information adding
        @return: None
        """
        raise NotImplementedError

    def end(self):
        """
        (often user-implemented) member function for actions to be performed after looping,
        by default, this calculates the pose errors and saves the data to a binary file
        @return: None
        """
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

        print('Computing pose errors with error-metric from [Kroischke 2016] for all runs...')
        compute_pose_errors(self.data)

        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
