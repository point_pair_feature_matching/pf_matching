#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# noinspection PyShadowingBuiltins
input = raw_input
# noinspection PyShadowingBuiltins
range = xrange

import time
import imp
import sys
import os
from pf_matching.datasets import DatasetBase
from pf_matching.ros_interface import ExperimentInterface, RuntimeEstimator
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation.data_post_processing import compute_pose_errors

TIME_OUT_S = None  # no timeout


class ExperimentBase:
    """
    base class providing a framework for easy implementation of experiments with the ROS pf_matching
    nodes in a reusable way

    To use, inherit from this class and implement the setup(), looping() and end()-members, then
    start the experiment by calling run(). The interface and experiment data_storage structure are
    provided through member variables.

    This class also implements some convenience functions for commonly required tasks, however, you
    are not required to use these.
    """

    def __init__(self, data_storage, save_path_wo_extension='~/experiment_data'):
        """
        constructor; If you implement your own, make sure to run this one inside it!
        @type data_storage: ExperimentDataStructure
        @param data_storage: the data_storage storage to use
        @param save_path_wo_extension: file-path for storing the experiment results without file
                                       extension
        """

        self.data = data_storage
        self.save_path_wo_extension = save_path_wo_extension
        self.interface = None  # of type ExperimentInterface

    def run(self, wait_before_looping=True):
        """
        run the experiment by setting up required classes and calling the user-defined setup(),
        looping() and end() members
        @param wait_before_looping: if True, wait for a user-input after the setup before looping
        """

        with ExperimentInterface(self.data) as self.interface:
            print('Running setup...')
            self.setup()
            time.sleep(1)  # wait for status messages etc. to come back

            if wait_before_looping:
                self.wait_for_enter()

            print('Looping through experiment...')
            self.looping()

            print('Looping done, performing end-actions...')
            self.end()

        print('Experiment done!')

    @staticmethod
    def wait_for_enter(message='Press enter to start...'):
        """
        wait until the user presses enter
        @param message: message to display
        """
        try:
            input(message)
        except SyntaxError:
            pass

    # ############### convenience members #################################################

    @staticmethod
    def load_dataset(dataset_path):
        """
        load a dataset py pointing to it's python class file
        @param dataset_path: path to the Python file containing the dataset class and a
                             get_dataset() function to get a class instances
        @rtype: DatasetBase
        @return: the dataset class providing information about the dataset
        """
        # find first unique name for the dataset's module
        module_name_format = 'imported_dataset_%d'
        i = 0
        while module_name_format % i in sys.modules.keys():
            i += 1

        # replace '~' by absolute file
        dataset_path = os.path.expanduser(dataset_path)

        return imp.load_source(module_name_format % i, dataset_path).get_dataset()

    def launch_and_connect(self,
                           scene_noisifer=False,
                           pkg_launch_file_tuples=(('pf_matching_tools',
                                                    'pipeline_as_nodes_without_matcher_for_debugging.launch'),
                                                   )):
        """
        launch launch files and connect to them, assuming the most common node names
        @param scene_noisifer: if True, also hook up a dynamic reconfigure interface for the
                                scene noisifer
        @param pkg_launch_file_tuples:
        @return:
        """

        # launch the ROS-nodes
        self.interface.run_launch_files(pkg_launch_file_tuples)
        time.sleep(2)

        # hook up dynamic reconfigure interface
        dyn_reconfigurable_nodes = ['matcher', 'scene_preprocessor', 'model_preprocessor',
                                    'model_loader', 'scene_loader']
        if scene_noisifer:
            dyn_reconfigurable_nodes += ['scene_noisifier']
        self.interface.setup_dyn_reconfigure_clients(dyn_reconfigurable_nodes)

        # set up various other connections
        self.interface.setup_model_path_publisher('/model_load_path')
        self.interface.setup_scene_path_publisher('/scene_load_path')
        self.interface.setup_load_transform_subscriber('/load_transforms')
        self.interface.setup_poses_subscriber()
        time.sleep(1)

        print('Launchfiles running, experiment interface hooked up.')

    def setup_dataset_properties(self, dataset):
        """
        for a new dataset, use this function to set it up properly. Calling this will:
        * add the name and units of the dataset to the current run's data_storage
        * set the correct scaling factor for the model loader
        @param dataset: the new dataset
        """

        # add general information to data_storage
        self.interface.add_dataset_info_to_current_run(dataset.get_name(), dataset.get_units())

        # set the right scale factor
        model_loader_settings = {'scale_factor': dataset.get_model_scale_factor()}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

    def add_model(self, dataset, model_index, clear_matcher=True):
        """
        add a model to the matcher by publishing it to the model loader;
        blocks until model is built
        @type dataset: DatasetBase
        @param dataset: dataset of the model
        @param model_index: index of the model in the dataset
        @param clear_matcher: if True, erase all models from the matcher prior to adding the new
                              model
        @return: d_dist parameter of the model (absolute value)
        """

        # erase old model(s)
        if clear_matcher:
            self.interface.clear_models('/matcher')

        # set up model loader
        model_name = dataset.get_model_name(model_index)
        model_loader_settings = {'frame_id': model_name}
        self.interface.set_dyn_reconfigure_parameters('model_loader', model_loader_settings)

        # publish model and wait for it to be built
        self.interface.publish_model(*dataset.get_model_name_and_path(model_index))
        model_d_dist = self.interface.wait_for_statistics('matcher', 'd_dist_abs', model_name,
                                                          time_out_time=TIME_OUT_S)
        return model_d_dist

    def set_scene_preprocessor_d_points(self, d_points, is_relative):
        """
        set the d_points parameter of the scene preprocessor to a value
        @param d_points: value to use
        @param is_relative: if True, d_points is relative to scene cloud diameter, if False, it
                            @permission absolute
        """

        scene_preprocessor_settings = {'d_points': d_points,
                                       'd_points_is_relative': is_relative}
        self.interface.set_dyn_reconfigure_parameters('scene_preprocessor',
                                                      scene_preprocessor_settings)

    def add_scene(self, dataset, scene_index):
        """
        add a scene to the scene loader and trigger the matching process;
        blocks until matching is done
        @type dataset: DatasetBase
        @param dataset: the dataset of the scene
        @param scene_index: the index of the scene in the dataset
        """

        # set up scene loader
        scene_loader_settings = {'frame_id': dataset.get_scene_name(scene_index)}
        self.interface.set_dyn_reconfigure_parameters('scene_loader', scene_loader_settings)

        # publish scene and trigger matching
        self.interface.publish_scene(*dataset.get_scene_name_and_path(scene_index))

        # block until matching is done
        self.interface.wait_for_statistics('matcher', 'matched_models', time_out_time=TIME_OUT_S)

    def add_info_from_dataset(self, dataset, scene_index, model_indices):
        """
        save information from the dataset for this specific scene-model combination
        saved information:
        * ground truth
        * degree of occlusion
        @type dataset: DatasetBase
        @param dataset: the dataset of the model and scene
        @param scene_index: index of the scene in the dataset
        @param model_indices: list with indices of the models in the dataset
        """

        for model_index in model_indices:
            # save ground truth data_storage and degree of occlusion
            gt = dataset.get_ground_truth(model_index, scene_index)
            self.interface.add_gt_to_current_run(gt, dataset.get_model_name(model_index))

            doo = dataset.get_degree_of_occlusion(model_index, scene_index)
            self.interface.add_occlusion_to_current_run(doo, dataset.get_model_name(model_index))

    # ############### advanced convenience members ########################################

    def loop_scenes(self, dataset, runtime_estimator, model_indices, scene_indices=None):
        """
        simple looping over the scenes of a dataset and matching the models present in the matcher
        for the models defined by model_indices, data_storage from the dataset is added to the run
        data_storage
        @type dataset: DatasetBase
        @param dataset: dataset of scenes and models
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to add information for
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        @return:
        """
        # convert input data_storage if required
        if scene_indices is None:
            scene_indices = range(dataset.get_number_of_scenes())

        # loop scenes
        for scene_index in scene_indices:
            print ('scene %d: %s' % (scene_index, dataset.get_scene_name(scene_index)))
            runtime_estimator.run_start()

            self.add_scene(dataset, scene_index)
            self.add_info_from_dataset(dataset, scene_index, model_indices)
            time.sleep(1)  # wait for remaining statistics messages to arrive

            runtime_estimator.run_end()
            self.interface.new_run()

    def loop_models_and_scenes(self, dataset, runtime_estimator,
                               model_indices=None, scene_indices=None):
        """
        simple looping over the models and scenes of a dataset; The following operations are
        performed for each model:
        * erase previously present models and add one
        * set up the scene pre-processor's d_points to the model's d_dist
        * loop over the scenes, matching the model in each scene and adding all required information
          as we go

        After each inner looping, a new run is initialized in the ROS interface.

        @type dataset: DatasetBase
        @param dataset: the dataset of the models and scenes
        @type runtime_estimator: RuntimeEstimator
        @param runtime_estimator: the RuntimeEstimator object to use for measuring run-times
                                  and estimating the remaining time
        @param model_indices: list of indices of the models to match or None to match all models
        @param scene_indices: list of indices of the scenes to match the models in or None to use
                              all scenes
        """

        # convert input data_storage if required
        if model_indices is None:
            model_indices = range(dataset.get_number_of_models())

        # loop models
        for model_index in model_indices:
            print ('model %d: %s' % (model_index, dataset.get_model_name(model_index)))
            model_d_dist_abs = self.add_model(dataset, model_index, clear_matcher=True)
            self.set_scene_preprocessor_d_points(model_d_dist_abs, is_relative=False)

            self.loop_scenes(dataset, runtime_estimator, [model_index], scene_indices)

    # ############### members to be implemented by the child class ########################
    def setup(self):
        """
        user-implemented member function that sets up the pipeline / nodes as required for the
        experiment
        @return: None
        """
        raise NotImplementedError

    def looping(self):
        """
        user-implemented member function that facilitates the actual experiment by looping over
        parameter combinations, datasets etc. and performs the matching and information adding
        @return: None
        """
        raise NotImplementedError

    def end(self):
        """
        (often user-implemented) member function for actions to be performed after looping,
        by default, this calculates the pose errors and saves the data to a binary file
        @return: None
        """
        print('Saving data to make sure we got it if post-processing goes wrong...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')

        print('Computing pose errors for all runs...')
        compute_pose_errors(self.data)

        print('Saving complete data...')
        self.data.to_pickle(self.save_path_wo_extension + '.bin')
