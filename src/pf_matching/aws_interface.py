# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *

import sys
import os

# AWS python SDK
import boto3
from botocore.exceptions import ClientError

# fabric package for ssh stuff with python
from fabric2 import Connection
from subprocess import check_output, CalledProcessError

# http(s) requests       
import requests
from numpy import empty

class AWSinterface:
    """
    class that provides functions to communicate with an AWS EC2 AMI instance 
    (AWS = amazon working space, AMI = amazon machine image)
    combines functionalities from the 'boto3' API to manage AWS EC2 with 
    the SSH interface from 'fabric'-package
    """
    def __init__(self, instance_id_):
        """
        create a boto3-client instance for the AMI instance whose ID is submitted
        @param instance_id: ID of the instance, can be retrieved from AWS web-gui: AWS console --> instances --> id
        @type instance_id: string
        """
        # client object for low level access
        self.client = boto3.client('ec2')
        self.instance_id = instance_id_
        print ("Created boto3 client for starting the EC2, an AMI instance, with ID %s" %self.instance_id)      
        
        # resource object for high level access
        self.resource = boto3.resource('ec2')
        
        # adapt ec2 security group rules such that this local machine
        # can start an SSH session (useful if local ip is not fix)
        if not self.configureSecurityGroupsForSSHconnection():
            raise Exception("no security group existent, or no SSH rule for security group existent")
        
        
    def configureSecurityGroupsForSSHconnection(self):
        """
        this method does not create a security group. it also does not create an SSH rule for an
        existing group, if it does not already have a SSH rule.
        what this method does, is adjusting an existing SSH rule for an existing security group.
        it adjusts the SSH rules in such a way, that 
            1. one rule (inbound + outbound) has the local machine's ip as destination and source 
            2. all other SSH rules of this group are deleted!
        """
        # get local ip with https request
        response = requests.get('https://checkip.amazonaws.com/')
        response.raise_for_status()
        local_ip = response.content.strip() + '/32'
        
        # get list with information about security groups which specify SSH rules ( --> Port 22)        
        ssh_sg_info = self.client.describe_security_groups(Filters=[
                                                          {'Name':'ip-permission.from-port', 
                                                           'Values':['22']},
                                                          {'Name':'ip-permission.to-port', 
                                                           'Values':['22']},
                                                           {'Name':'ip-permission.protocol',
                                                            'Values':['tcp']}
                                                          ])
        if not ssh_sg_info['SecurityGroups']:
            print("It seems as if there might not be a security group that grants SSH access via port 22."\
                  "You need to create one via the AWS console and connect it with your EC2 instance!")
            return False
        #print(ssh_sg_info)
        # compare local ip with the ips that are specified in inbound/outbound rules
        # of security groups with SSH access
        sg_ids_for_ssh_access = {}
        ips_with_ssh_access =[]
        for sg in ssh_sg_info['SecurityGroups']:
            for ippermission in sg['IpPermissions']:
                for iprange in ippermission['IpRanges']:
                    ip_with_ssh_access = iprange['CidrIp']
                    if local_ip is not ip_with_ssh_access:
                        #print (sg['GroupId'], type(sg['GroupId']))
                        #print (ip_with_ssh_access, type(ip_with_ssh_access))
                        ips_with_ssh_access.append(ip_with_ssh_access)
                        sg_ids_for_ssh_access.update({str(sg['GroupId']): ips_with_ssh_access })
            for ippermission in sg['IpPermissionsEgress']:
                for iprange in ippermission['IpRanges']:
                    ip_with_ssh_access = iprange['CidrIp']
                    if local_ip is not ip_with_ssh_access:
                        #print (sg['GroupId'], type(sg['GroupId']))
                        #print (ip_with_ssh_access, type(ip_with_ssh_access))
                        ips_with_ssh_access.append(ip_with_ssh_access)
                        sg_ids_for_ssh_access.update({str(sg['GroupId']): ips_with_ssh_access })
        
        if not sg_ids_for_ssh_access:
            print("At least one security group already granst SSH access to %s" %(local_ip))
            return True
            
        # loop over security groups with SSH access
        for sg_id, ips in sg_ids_for_ssh_access.items():          
            for ip in ips:       
                # local ip is not comlying with inbound/outbound rules
                # delete current inbound/outbound rules of the security group
                security_group = self.resource.SecurityGroup(sg_id)
                try:
                    security_group.revoke_ingress(DryRun = False,
                                                  IpPermissions=[{'FromPort': 22,
                                                                   'ToPort': 22,
                                                                   'IpProtocol': 'tcp',
                                                                   'IpRanges': [{'CidrIp': ip}]
                                                                   }])
                    security_group.revoke_egress(DryRun = False,
                                                 IpPermissions=[{'FromPort': 22,
                                                                  'ToPort': 22,
                                                                  'IpProtocol': 'tcp',
                                                                  'IpRanges': [{'CidrIp': ip}]
                                                                  }])
                except ClientError as e:
                    print(e)
                    pass
                
        import time
        time.sleep(3) # wait, deletion might take a moment
        
        for sg_id in sg_ids_for_ssh_access.keys():     
            # add new inbound and outbound SSH security group rules for the current local ip
            self.client.authorize_security_group_ingress(GroupId = sg_id, 
                                                         IpPermissions=[
                                                        {'IpProtocol': 'tcp',
                                                         'FromPort': 22, # 22 = ssh port
                                                         'ToPort': 22,
                                                         'IpRanges': [{'CidrIp': local_ip}]}
                                                        ])
            self.client.authorize_security_group_egress(GroupId = sg_id, 
                                                         IpPermissions=[
                                                        {'IpProtocol': 'tcp',
                                                         'FromPort': 22,
                                                         'ToPort': 22,
                                                         'IpRanges': [{'CidrIp': local_ip}]}
                                                        ])
            print("Set IP for SSH access to ec2-instance. Security group %s grants access to %s" %(sg_id, local_ip))
        return True
    
        
    def startAMI (self):
        """
        start an existing EC2 AMI ( amazon machine image ) and wait for the starting process to be finished
        """
        # from https://boto3.readthedocs.io/en/latest/guide/ec2-example-managing-instances.html
        # Do a dryrun first to verify permissions
        try:
            self.client.start_instances(InstanceIds=[self.instance_id], DryRun=True)
        except ClientError as e:
            if 'DryRunOperation' not in str(e):
                raise
    
        # Dry run succeeded, run start_instances without dryrun
        try:
            response = self.client.start_instances(InstanceIds=[self.instance_id], DryRun=False)
            print(response)
        except ClientError as e:
            print(e)
            
        # from https://stackoverflow.com/a/46956969
        #print("EC2 waiters:")
        #print (self.client.waiter_names)
        waiter = self.client.get_waiter('instance_running') # instance_status_ok')
        waiter.wait(InstanceIds=[self.instance_id])
        print("The AMI instance %s is now up and running." %self.instance_id)
      
      
    def setROSenvironment(self):
        """
        does not work! do not use that.
        each of those 5 commands need to be executed WITH the runExperimentOnRemote Command in the same command-line
        """
        self.runShellCommandOnRemote('source /opt/ros/indigo/setup.bash', False)
        self.runShellCommandOnRemote('source ~/ROS/catkin_workspace/devel/setup.bash', False)
        self.runShellCommandOnRemote('export LC_ALL="C"', False)
        #self.runShellCommandOnRemote('rospack profile', False)
        self.runShellCommandOnRemote('export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/', False)
        
    def establishSSHconnection(self, username, path_to_ssh_private_key_file):
        """
        establish a ssh connection with fabric. AMI instance must be up and running (--> startAMI() ).
        @param username: the login user for the remote connection
        @type username: str
        @param ip_address: the hosts ip-address or the hostname. is currenty retrieved automatically from boto3-client
                           retrieve that from AWS web-gui: AWS console --> instances --> ip
        @type ip_address: str
        """      
        # get ip addresses of all running instances
        ec2info = self.getInstanceInfo()
        # only the ip-address of the current instance-of-interest is stored:
        self.ip_address = ec2info[self.instance_id]['Public IP']
        #self.security_group_ids = ec2info[self.instance_id]['Security Group ID']
               
        self.username = username
        self.connect_kwargs = {"key_filename": [path_to_ssh_private_key_file]}
        self.SSHconnection = Connection(user=username, host=self.ip_address, port = "22",
                                        connect_kwargs=self.connect_kwargs)
        try: 
            self.SSHconnection.is_connected
            print("Established SSH connection to AMI instance %s@%s." %(self.SSHconnection.user, self.SSHconnection.host))
        except:
            print("Could not establish SSH connection to AMI instance %s@%s!" %(username, self.ip_address))
        
        
    def closeSSHconnection(self):
        self.SSHconnection.close()
        print("SSH connection to AMI instance was shut down.")
        
        
    def stopAMI (self):
        """
        stop an existing AMI ( amazon machine image ) and wait for the stopping process to be finished
        """
        # Do a dryrun first to verify permissions
        try:
            self.client.stop_instances(InstanceIds=[self.instance_id], DryRun=True)
        except ClientError as e:
            if 'DryRunOperation' not in str(e):
                raise
    
        # Dry run succeeded, call stop_instances without dryrun
        try:
            response = self.client.stop_instances(InstanceIds=[self.instance_id], DryRun=False)
            print(response)
        except ClientError as e:
            print(e)
        
        waiter = self.client.get_waiter('instance_stopped')
        waiter.wait(InstanceIds=[self.instance_id])
        print("The AMI instance %s is now stopped." %self.instance_id)
    
        
    def getFileViaSSH(self, filename, remote_directory, local_directory = None):
        """
        Download a file from the remote computer and store it in the specified local directory
        @param remote_directory: Directory of file to download from remote. 
                                 May be absolute, or relative to the remote working directory.
        @type remote_directory: str
        @param local_directory: Path to store the downloaded file in. Can be directory or filename. 
                                If empty, file is stored at current working directory
        @type local_directory: str
        """
        local_directory = os.path.expanduser(local_directory)
        # TODO: source and destination need to be full path incl. filename
        #       this is due to a bug in 'fabric 2.3.1'. bug was fixed but it is not til the next release,
        #       that the fix is active.
        local_filename_and_path = local_directory+filename
        remote_directory = remote_directory+filename
        
        if self.checkRemotePathExists(remote_directory):
            if local_directory is not None:
                if self.checkLocalPathExists(local_directory):
                    result = self.SSHconnection.get(remote_directory, local = local_filename_and_path, preserve_mode = True)
                    print("Downloaded {0.local} from {0.remote}".format(result))
                    return True
                else:
                    print("Specified local directory %s does not exist! Creating it now, to finish copying." %(os.path.expanduser(local_directory)) )
                    os.mkdir(local_directory)
                    result = self.SSHconnection.get(remote_directory, local = local_filename_and_path, preserve_mode = True)
                    print("Downloaded {0.local} from {0.remote}".format(result))
                    return True
            else:
                result = self.SSHconnection.get(remote_directory, local = local_filename_and_path, preserve_mode = True)
                print("Downloaded {0.local} from {0.remote}".format(result))
                return True
        else:
            print("The remote file, that was supposed to be copied, does not exist! Aborting...")
            return False
        
    def putFileViaSSH(self, filename, local_directory, remote_directory = None):
        """
        Uplaod a file from the local computer and store it in the specified remote directory
        @param remote_directory: Directory to upload the file to. must be path, NOT filename.
                                 If empty, file is stored remote home directory
        @type remote_directory: str
        @param local_directory: Local path of file to upload. Can also be a file-like object.
                                
        @type local_directory: str
        """
        local_directory = os.path.expanduser(local_directory)+filename
        remote_filename_and_path = remote_directory
        
        if self.checkLocalPathExists(local_directory):
            if remote_directory is not None:
                if self.checkRemotePathExists(remote_directory):
                    result = self.SSHconnection.put(local_directory, remote = remote_filename_and_path, preserve_mode = True)
                    print("Uploaded {0.local} to {0.remote}".format(result))
                    return True
                else:
                    print("Specified remote directory does not exist! Creating it now, to finish copying.")
                    self.runShellCommandOnRemote('mkdir '+remote_directory)
                    result = self.SSHconnection.put(local_directory, remote = remote_filename_and_path, preserve_mode = True)
                    print("Uploaded {0.local} to {0.remote}".format(result))
                    return True
            else:
                result = self.SSHconnection.put(local_directory, remote = remote_filename_and_path, preserve_mode = True)
                print("Uploaded {0.local} to {0.remote}".format(result))
                return True   
        else :
            print("The local file, that was supposed to be copied, does not exist! Aborting...")
            return False
        
        
    def runExperimentOnRemote(self, script_name, enable_logout = True):
        """
        start a python experiment script on remote computer, e.g. 'MBO_demo.py'
        the script must be located on the remote computer 
        @param enable_logout: true detaches experiment-script from shell to enable a logout. false keeps the experiment-script
               attatched to the shell and therefore the experiment stops upon pressing ctrl+c (which one needs to use, to exit output)
        @type enable_logout: bool
        @param script_name: name of the python file, e.g. 'MBO_demo.py'
        @type script_name: str
        @note this method also calls sourcing-commands before starting the python-experiment script.
              this is due to the problem, that though .run() does call .bashrc but, .bashrc itself does not run the last lines.
              unfortunately it's those last lines that are essectial for setting up the ros-nodes.
              ergo, as a quick and dirty solution, we need to prepend all these sourcing commands to the actual experiment command
        @note if logout enabled, stdout and stderr are redirected to /usr/ubuntu/nohub.out file.
              you can "tail -f nohub.out" when logged in on remote with ssh to monitor the progress
        """ 
        if enable_logout:
            # run on remote bash, using nohup to enable ssh-logout without shutting down the experiment
            # for infos on nohup see https://stackoverflow.com/a/10408906
            self.SSHconnection.run('source /opt/ros/indigo/setup.bash;'\
                                   'source ~/ROS/catkin_workspace/devel/setup.bash;'\
                                   'export LC_ALL="C";'\
                                   'rospack profile;'\
                                   'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/;'\
                                   'nohup python '+script_name + ' >/home/ubuntu/nohub.out </dev/null 2>&1 &', pty=False, echo=True)
            #self.experiment = self.SSHconnection.run('tmux; python '+script_name +'; tmux detach')
        else:
            # run on remote bash, waiting for the experiment to finish
            # Using a pseudo terminal 'pty=True' to get a live-stream of the output of remote console. 
            # I.e. if we press ctrl+c in this pseudo terminal, we abort the experiment
            # If we set 'pty=False', we get no stream, but we and can press ctrl+c in order to get out 
            # of the waiting loop, WITHOUT aborting the experiment.
            self.SSHconnection.run('source /opt/ros/indigo/setup.bash;'\
                                   'source ~/ROS/catkin_workspace/devel/setup.bash;'\
                                   'export LC_ALL="C";'\
                                   'rospack profile;'\
                                   'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/;'\
                                   'python '+ script_name, pty=True, echo=True)
        
    def startROScore(self):
        self.SSHconnection.run('source /opt/ros/indigo/setup.bash;'\
                               'source ~/ROS/catkin_workspace/devel/setup.bash;'\
                               'export LC_ALL="C";'\
                               'rospack profile;'\
                               'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/;'\
                               'nohup roscore >/dev/null </dev/null 2>&1 &', pty=False, echo=True)
    
    def runShellCommandOnRemote(self, shell_command, enable_logout = False):
        """
        execute an arbitrary command in bash of remote computer
        @param enable_logout: true detaches command from shell to enable a logout. false keeps the command
               attatched to the shell and therefore the commands stops upon pressing ctrl+c (which one needs to use, to exit output)
        @type enable_logout: bool
        @param script_name: name of the python file, e.g. 'MBO_demo.py'
        @type script_name: str
        """
        #print ("Running the shell-command '%s' on remote yields: "%shell_command)
        if enable_logout:
            result = self.SSHconnection.run('nohup ' +shell_command+ ' >/dev/null </dev/null 2>&1 &', warn=True, echo=True, hide=('both'))
        else:
            # run command in pseudoterminal to livestream output (pty=True). 
            # without pty the command's output will be printed once it’s done executing
            # pressing CRTL+C will while in a pty will abort command-execution and ssh session
            # not throwing an exception when command is encountering a bad exit (warn=True)
            # echoing command itself in bold letters (echo=True)
            result = self.SSHconnection.run(shell_command, pty=True ,warn=True, echo=True)
        #print ("The output of %s is: %s" %(shell_command, result.stdout.strip()))
        return result
    
    
    def checkIfExperimentIsStillRunning(self, script_name, visualize_remote_output = False):
        process_ids = self.getProcessIDs(script_name)
        if len(process_ids) == 0:
            print("Zero processes with name %s were found. Process might be done." %script_name)
            return 0 #false
        elif len(process_ids) >= 2:
            print("Too many processes with name %s were found" %script_name)
            return -1
        else:    
            # process is found, it has an id. ergo it has not finished yet.
            print("Process with name %s was found. Process is still running!" %script_name)
            #self.SSHconnection.run('tmux attach')
            #self.runShellCommandOnRemote('strace -p%s -s9999 -e write' %str(process_ids[0]))
            if visualize_remote_output:
                self.runShellCommandOnRemote('tail -f /proc/%s/fd/1' %str(process_ids[0]))
            return 1 #true
        
        
    def terminateExperimentOnRemote(self, script_name):
        process_ids = self.getProcessIDs(script_name)
        if len(process_ids) == 1:
            result = self.SSHconnection.run('kill -SIGTERM '+str(process_ids[0]), pty=False, echo=True)
            return True
        else:
            print ('There is no process with name %s running. Not terminating any process.'%script_name)
            return False
        
        
    def killExperimentOnRemote(self, script_name):
        process_ids = self.getProcessIDs(script_name)
        if len(process_ids) == 1:
            result = self.SSHconnection.run('kill -SIGKILL '+str(process_ids[0]), pty=False, echo=True)
            return True
        else:
            print ('There is no process with name %s running. Not killing any process.'%script_name)
            return False
        
        
    def getProcessIDs(self, script_name):
        # separate the name of the script from the path
        if script_name.count('/') > 0:
            script_name = script_name.rsplit('/',1)[1]
        # in parts from https://stackoverflow.com/a/35938503
        try:
            # get output of 'pgrep -f' of remote system to get the process-id
            result = self.runShellCommandOnRemote("pgrep -f "+ script_name)
            result = result.stdout.strip()
            process_ids = list(map(int, result.split()))
        except:
            process_ids = []
        print ('List of PIDs associated with process-name = ' + script_name +': ' + ', '.join(str(e) for e in process_ids))
        return process_ids
        
        
    def checkRemotePathExists(self, path):
        #establish another SSH connection, but now with "fabric 1"
        from fabric.api import env, execute, settings, hide
        from fabric.state import connections
        env.hosts = self.ip_address
        env.user = self.username
        env.key_filename = self.connect_kwargs['key_filename']
        result = execute(self.fabric1CheckRemotePathExists, path)
        for key in connections.keys():
            connections[key].close()
            del connections[key]
        #print ("the result of checkRemotePathExists is %s"%result[self.ip_address])
        return result[self.ip_address]
        
        
    def fabric1CheckRemotePathExists(self, path):
        from fabric.contrib import files
        result = files.exists(path, use_sudo=False, verbose=True)
        if result:
            #print ("the result of fabric1CheckRemotePathExists is: %s" %result)
            return True
        else:
            #print ("the result of fabric1CheckRemotePathExists is: %s" %result)
            return False
        
        
    def fabric2CheckRemotePathExists(self, path):
        print("I was not able to get the check done with fabric2. \
               The command has been migrated to fabric2, but in a strange way \
               that would require the use of patchwork-package. Unfortunately \
               I did not understand the documentation right away. So I chose to \
               stick with fabric1 for that small part. Feel free to implement it.")
        return False
        
        
    def checkLocalPathExists(self, path):
        if os.path.exists(path):
            return True
        else:
            return False
        
        
    def getInstanceInfo(self):
        # from https://gist.github.com/dastergon/b4994c605f76d528d0c4
        running_instances = self.resource.instances.filter(Filters=[{
            'Name': 'instance-state-name',
            'Values': ['running']}])

        from collections import defaultdict
        ec2info = defaultdict()
        for instance in running_instances:
            # Add instance info to a dictionary         
            ec2info[instance.id] = {
                'Type': instance.instance_type,
                'State': instance.state['Name'],
                'Private IP': instance.private_ip_address,
                'Public IP': instance.public_ip_address,
                'Launch Time': instance.launch_time 
                }

        return ec2info
    
    