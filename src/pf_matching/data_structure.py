#!/usr/bin/env python
#  -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *

# other imports
import time
import os
import yaml
import cPickle as pickle


class ExperimentDataStructure(dict):
    """
    dictionary with extended functions and structure for storing ROS pf-matching experiment data
    """

    def __init__(self):

        super(ExperimentDataStructure, self).__init__(self)

        # set up basic datastructure and basic information
        self["runData"] = {}
        self["date"] = time.strftime("%a %d/%m/%y %H:%M")
        self["launchFiles"] = []

    def get_nested(self, key_list):
        """
        get a value from the nested dicts

        e.g.: d.get_nested([1, 2, 3]) is equivalent to d[1][2][3],
        see http://stackoverflow.com/a/14692747

        @param key_list: list of nested keys, all keys must exist
        @return: the value of the deepest nested dict
        """

        return reduce(lambda d, k: d[k], key_list, self)

    def set_nested(self, key_list, value):
        """
        set a value in the nested dicts and create intermediate dicts as required

        e.g.: d.set_nested([1, 2, 3], 4) is equivalent to d[1][2][3] = 4,
        http://stackoverflow.com/a/13688108

        @param key_list: list of nested keys
        @param value: object to save
        @return: None
        """

        # go through / set up intermediate dictionaries
        dic = self
        for key in key_list[:-1]:
            try:
                dic = dic.setdefault(key, {})
            except:
                raise KeyError("Could not set intermediate key '%s'. Object at this place is not a "
                               "dictionary." % key)

        # set value in last dictionary
        try:
            dic[key_list[-1]] = value
        except:
            raise KeyError("Could not set last key '%s'. Object at this place is not a dictionary."
                           % key_list[-1])

    def del_nested(self, key_list):
        """
        delete a value from the nested dicts

        e.g.: d.del_nested([1, 2, 3]) is equivalent to del d[1][2][3],
               see http://stackoverflow.com/a/14692747
        @param key_list: list of nested keys, all keys must exist
        @return: None
        """
        del self.get_nested(key_list[:-1])[key_list[-1]]

    def exists(self, key_list):
        """
        check whether the object at the place specified by the key_list exists or not

        @param key_list: list of nested keys
        @return: True if the object exists, else False
        """
        try:
            self.get_nested(key_list)
            return True
        except KeyError:
            return False

    def from_yaml(self, file_path):
        """
        load data from a YAML-file, if the file does not exist, an IOError exception is thrown and
        self is empty
        @param file_path: path to the file, can contain '~' for user directory
        @return: None
        """

        # replace '~' by absolute file
        file_path = os.path.expanduser(file_path)

        # load data
        self.clear()
        with open(file_path, 'r') as infile:
            self.update(yaml.load(infile))

    def to_yaml(self, file_path):
        """
        save data to a YAML-file
        @param file_path: path to the file, can contain '~' for user directory
        @return: None
        """
        # replace '~' by absolute file
        file_path = os.path.expanduser(file_path)

        # create directory if required
        dir_path = os.path.dirname(file_path)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

        # write to file
        with open(file_path, 'w+') as outfile:
            outfile.write(yaml.dump(self, default_flow_style=False))

    def from_pickle(self, file_path):
        """
        load data from a pickle binary file, if the file does not exist, an IOError exception is
        thrown and self is empty
        @param file_path: path to the file, can contain '~' for user directory
        @return: None
        """

        # replace '~' by absolute file
        file_path = os.path.expanduser(file_path)

        # load data
        self.clear()
        with open(file_path, 'rb') as infile:
            self.update(pickle.load(infile))

    def to_pickle(self, file_path):
        """
        save data to a pickle-binary file
        @param file_path: path to the file, can contain '~' for user directory
        @return: None
        """
        # replace '~' by absolute file
        file_path = os.path.expanduser(file_path)

        # create directory if required
        dir_path = os.path.dirname(file_path)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

        # write to file
        with open(file_path, 'wb') as outfile:
            pickle.dump(self, outfile, protocol=pickle.HIGHEST_PROTOCOL)

    def get_run_data(self, nested_key_lists, copy_missing_from_previous=False, exclude_first_entry=True):
        """
        get the data under each list of keys in nested_key_lists for all available runs in runData
        @param nested_key_lists list of keys or list of lists of keys specifying the data location(s)
                                in the individual run data dicts, e.g.
                                ["a", "b"] to get self["runData"][<run>]["a"]["b"]
                                [["a", "b"], ["c", "d"]] to get self["runData"][<run>]["a"]["b"]
                                and self["runData"][<run>]["c"]["d"]
        @param copy_missing_from_previous: if true, try to replace missing values with the data from
                                           the previous run recursively, starting from the lowest
                                           runID; Note that this does not guarantee None-free return
                                           data. If the initial run is missing the value, it will be
                                           missing in that and all following runs until it was set.
        @param exclude_first_entry: return the list without its first element (usually the initial
                                    configuration)
        @return: list of tuples of the form (<runID>, <data0>, <data1>, ...); Missing values are replaced by None.
        """

        # convert input if the user only supplied a single list instead of a list of lists
        if all(not isinstance(entry, list) for entry in nested_key_lists):
            nested_key_lists = [nested_key_lists]

        if not all(isinstance(entry, list) for entry in nested_key_lists):
            raise TypeError("Expected a list of keys or a list of lists for parameter "
                            "'nested_key_lists'.")

        # get the identifiers of all runs and sort them ascending
        run_IDs = self["runData"].keys()
        run_IDs.sort()

        # make list of (runID, data)-tuples with data=None if there is no such data
        return_data = []
        for runID in run_IDs:
            data = [runID]
            for nestedKeyList in nested_key_lists:
                try:
                    data.append(self.get_nested(["runData", runID] + nestedKeyList))
                except KeyError:
                    data.append(None)
            return_data.append(tuple(data))

        # check that for each key list, the keys really existed in at least one dict
        for tuple_index in range(1, len(nested_key_lists) + 1):
            if all(data[tuple_index] is None for data in return_data):
                raise KeyError("At least one of the nested keys in list with index %d does "
                               "not exist in any of the run data." % (tuple_index - 1))

        # if enabled, copy missing data from the previous run (e.g. dynamic reconfigure settings)
        if copy_missing_from_previous:
            for tuple_index in range(1, len(nested_key_lists) + 1):
                for index, data in enumerate(return_data):
                    if data[tuple_index] is None and index > 0:
                        return_data[index] = data[:tuple_index] + \
                                            (return_data[index - 1][tuple_index], ) + \
                                            data[(tuple_index + 1):]

        if exclude_first_entry:
            return return_data[1:]
        else:
            return return_data

    # def removeRedundantRunData(self):
    #     """
    #     @brief delete those (possibly nested) entries in a run, that are exactly the same as the
    #            entries in the previous run (e.g. unchanged dynamic reconfigure parameters)
    #     """
    #     # get the identifiers of all runs and create run pairs for checking the parameters
    #     runIDs = self["runData"].keys()
    #     runIDs.sort()
    #     checkPairIDs = [(runIDs[i], runIDs[i-1]) for i in range(len(runIDs)-1, 0, -1)]
    #     # (this, previous) starting at the end of the list
    #
    #     def recursiveDelete(thisDict, previousDict):
    #         """
    #         @brief recursively delete from the first dict all identical nested entries of both dicts
    #                or just return true if the first dict could be deleted completely
    #         @param thisDict dict for deletion
    #         @param previousDict dict for checking
    #         @return true if both dicts are completely identical, else false
    #         """
    #
    #         # check complete identity
    #         if thisDict == previousDict:
    #             return True
    #
    #         # if not identical and parameters are dicts, iterate over nested entries
    #         elif isinstance(thisDict, dict) and isinstance(previousDict, dict):
    #             for key in thisDict.keys():
    #                 # check if entries are identical or try to delete nested
    #                 try:
    #                     if recursiveDelete(thisDict[key], previousDict[key]):
    #                         del thisDict[key]
    #                 except KeyError:
    #                     continue
    #             return False
    #
    #     # go through all pairs for checking
    #     for thisRunID, previousRunID in checkPairIDs:
    #         # delete nested or all info, if completely identical
    #         if recursiveDelete(self.get_nested(["runData", thisRunID]),
    #                            self.get_nested(["runData", previousRunID])):
    #             self["runData"][thisRunID] = {}


if __name__ == "__main__":

    d = ExperimentDataStructure()
    for i in range(-1, 2):
        d.set_nested(["runData", i, "matcher", "a"], i + 5)
        d.set_nested(["runData", i, "matcher", "b"], "same")

    d.set_nested(["runData", 1, "matcher", "a"], 5)
