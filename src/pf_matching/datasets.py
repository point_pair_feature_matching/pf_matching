#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# imports
import os


class DatasetBase:
    """
    base-class for all datasets specifying the required information and API to implement plus some reusable member
    functions
    """
    # ############################ to implement by child #############################
    def get_name(self):
        """
        return a sting with the or acronym of the dataset
        """
        raise NotImplementedError

    def get_units(self):
        """
        return a string specifying the distance unit of the dataset, e.g. "mm" or "m"
        """
        raise NotImplementedError

    def get_model_scale_factor(self):
        """
        return a factor with which to scale the model files so that they have the same scale
        as the scenes
        """
        raise NotImplementedError

    def get_number_of_models(self):
        """
        return the total number of models in the dataset
        """
        raise NotImplementedError

    def get_number_of_scenes(self):
        """
        return the total number of scenes in the dataset
        """
        raise NotImplementedError

    def get_model_name_and_path(self, i):
        """
        find name and path of the model
        @param i: model number, 0-based, must be < self.get_number_of_models()
        @return: tuple of (<model name>, <absolute path of model file>)
        """
        raise NotImplementedError

    def get_scene_name_and_path(self, i):
        """
        find name and path of the scene
        @param i: model number, 0-based, must be < self.get_number_of_scenes()
        @return: tuple of (<scene name>, <absolute path of scene file>)
        """
        raise NotImplementedError

    def get_ground_truth(self, model_index, scene_index):
        """
        get a transformation matrix describing the ground truth pose
        @param model_index: 0-based, must be < self.get_number_of_models()
        @param scene_index: 0-based, must be < self.get_number_of_scenes()
        @rtype: list
        @return: None if there is no ground truth pose for that model / scene combination or list
                 with 4x4 homogeneous transformation matrices for each model instance in the scene.
                 The order of the instances is the same as in get_degree_of_occlusion().
        """
        raise NotImplementedError

    def get_degree_of_occlusion(self, model_index, scene_index):
        """
        get the degree of occlusion of the model
        @param model_index: 0-based, must be < self.get_number_of_models()
        @param scene_index: 0-based, must be < self.get_number_of_scenes()
        @rtype: list
        @return: None if there is no occlusion data for that model / scene combination or list with
                 the degrees of occlusion in percent of the model surface for each model instance
                 in the scene. The order of the instances is the same as in get_ground_truth().
        """
        raise NotImplementedError

    def get_error_metric(self):
        """
        get the name of the error metric that shall be used to calculate the pose-errors
        only re-implement this function within your dataset, if you want to use a different one.
        This is simply to allow automatic switching from compute_pose_errors() to
        compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013()
        @return: list of strings, ['Kroischke_2016'] by default
        """
        return ["Kroischke_2016"]

    # ########################### other member #########################################
    def get_model_name(self, i):
        """
        @param i: model index, 0-based, must be < self.get_number_of_models()
        @return: name of the model
        """
        return self.get_model_name_and_path(i)[0]

    def get_model_path(self, i):
        """
        @param i: model index, 0-based, must be < self.get_number_of_models()
        @return: absolute path of the model
        """
        return self.get_model_name_and_path(i)[1]

    def get_scene_name(self, i):
        """
        @param i: scene index, 0-based, must be < self.get_number_of_scenes()
        @return: name of the scene
        """
        return self.get_scene_name_and_path(i)[0]

    def get_scene_path(self, i):
        """
        @param i: scene index, 0-based, must be < self.get_number_of_scenes()
        @return: absolute path of the scene
        """
        return self.get_scene_name_and_path(i)[1]
