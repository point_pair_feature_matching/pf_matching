# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke, Sebstian Krone, Markus Ziegler

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

# package imports
from pf_matching.data_structure import ExperimentDataStructure
from pf_matching.evaluation.data_extraction import extract_data
from pf_matching.pose_tools import *

from operator import itemgetter
from math import floor, pi
import time


def compute_pose_errors(data):
    """
    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The errors are given as the euclidean distance between the positions ("distance") and
    the angle between the quaternion rotations ("angle") and are added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'angle': None, 'distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        error = pose_error(model_test_pose, gt_pose_transformed)
                        error_score = error['distance'] / model_diameter + error['angle'] / pi
                            # considers both distance and angle

                        # the right pose
                        if error_score < min_error_score:
                            min_error = error
                            # min_error['modelPoseIndex'] = pose_index
                            min_error['gtPoseIndex'] = gt_index
                            min_error['equivalentPoseIndex'] = equivalent_pose_index
                            min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of pose errors computed...' % (progress * 100))
            display_steps.remove(progress)


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013(data, dataset):
    """
    Note: This uses a different error-metric than [Kroischke 2016]. It is the error metric for the
    ICCV 2015 dataset of [Krull et al. 2015]. The metric is from [Hinterstoisser, Lepetit et al. 2013].
    It calculates the average distance between the model-vertices under the estimated pose and the ground truth pose.
    It is called ADD

    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The error is added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                #print('got no model load transform')
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                #print('got no model equivalent poses')
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'average distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    #print('ground truth pose %i:' %(gt_index), gt_pose,'\n', pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)
                    #print('*transformed ground truth pose %i:\n' %(gt_index), gt_pose_transformed)
                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)
                        error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized(model_test_pose, gt_pose_transformed,
                                                                                           model_transform ,dataset, model_name)
                        error_score = error['average distance'] / model_diameter
                            # this error metric has no rotational part

                        # the right pose
                        if error_score < min_error_score:
                            min_error = error
                            # min_error['modelPoseIndex'] = pose_index
                            min_error['gtPoseIndex'] = gt_index
                            min_error['equivalentPoseIndex'] = equivalent_pose_index
                            min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of pose errors computed...' % (progress * 100))
            display_steps.remove(progress)


def compute_pose_errors_by_hinterstoisser_lepetit_et_al_2013_ADI(data, dataset):
    """
    Note: This uses a different error-metric than [Kroischke 2016]. It is an error metric for
    rotationally symmetric models. The metric is from [Hinterstoisser, Lepetit et al. 2013].
    It calculates the average distance between the model-vertices and their nearest ground truth
    model-vertices. It is called ADI.

    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The error is added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters(data)

    n_runs = len(data["runData"].keys())
    #display_steps = [i/100 for i in range(100 + 1)]
    time_average = 0

    for i, run_id in enumerate(sorted(data["runData"].keys())):
        start_time = time.time()
        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                #print('got no model load transform')
                model_transform = tft.identity_matrix()

            # try:
            #     model_equivalent_pose_transforms = \
            #         dict(data.get_run_data([['equivalentPoses', model_name]],
            #                                copy_missing_from_previous=True))
            #     model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            # except KeyError:
            #     #print('got no model equivalent poses')
            #     model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'average distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    #print('ground truth pose %i:' %(gt_index), gt_pose,'\n', pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)
                    #print('*transformed ground truth pose %i:\n' %(gt_index), gt_pose_transformed)
                    # equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                    #                            model_equivalent_poses.items()
                    # -1 means identity

                    #for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        #model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)
                        #error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized(model_test_pose, gt_pose_transformed,
                    error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized_ADI(model_pose, gt_pose_transformed,
                                                                                           model_transform ,dataset, model_name)
                    error_score = error['average distance'] / model_diameter
                    #print("model_diameter =",model_diameter, "error =",error['average distance'])
                    #print("error_score =",error_score)
                        # this error metric has no rotational part

                    # the right pose
                    if error_score < min_error_score:
                        min_error = error
                        # min_error['modelPoseIndex'] = pose_index
                        min_error['gtPoseIndex'] = gt_index
                        #min_error['equivalentPoseIndex'] = equivalent_pose_index
                        min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        #progress = floor((i + 1) / n_runs * 100) / 100
        end_time = time.time()
        time_diff = end_time - start_time
        time_average = ((time_average*(i-1)) + time_diff)/i
        remaining_time_estimate = time_average * (n_runs-i)
        print("run took %f sec. %i runs remaining, approx. %f min = %f h left." %( time_diff, n_runs-i,
                                                                                   remaining_time_estimate/60,
                                                                                   remaining_time_estimate/3600))
        #if progress in display_steps:
        #    print ('%d %% of pose errors computed...' % (progress * 100))
        #    display_steps.remove(progress)




def compute_pose_errors_by_hodan_et_al_2018(data, dataset):
    """
    Note: This uses a different error-metric than [Kroischke 2016]. It is yet ANOTHER error metric
    for the ICCV 2015 dataset of [Krull et al. 2015]. The metric is from [Hodan, Michel et al. 2018]
    and was used for the SIXD Challenge 2017 benchmark.

    Because this error metric does not use the model's diameter, there is no need for a
    "_from_verifier_" version of this function.

    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The error is added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                #print('got no model load transform')
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                #print('got no model equivalent poses')
                model_equivalent_poses = {}

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'error score': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    #print('ground truth pose %i:' %(gt_index), gt_pose,'\n', pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)
                    #print('*transformed ground truth pose %i:\n' %(gt_index), gt_pose_transformed)
                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)
                        error = pose_error_by_hodan_et_al_2018(model_test_pose, gt_pose_transformed,
                                                               model_transform ,dataset, model_name,
                                                               scene_name)
                        error_score = error['error score']
                            # this error metric has no rotational part
                            # and is independent of the model diameter
                        #print ('error_score =',error_score)

                        # the right pose
                        if error_score < min_error_score:
                            min_error = error
                            # min_error['modelPoseIndex'] = pose_index
                            min_error['gtPoseIndex'] = gt_index
                            min_error['equivalentPoseIndex'] = equivalent_pose_index
                            min_error_score = error_score



                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of pose errors computed...' % (progress * 100))
            display_steps.remove(progress)


def compute_pose_errors_from_verifier(data):
    """
    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    compared to the "non-verifier" version of this function, we get the model-diameters from the verifier.
    there should be no other differences.

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The errors are given as the euclidean distance between the positions ("distance") and
    the angle between the quaternion rotations ("angle") and are added to each pose entry.
    
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """
    
    #verification_method_dict = get_verification_methods(data)
    """
    # check which verification method was used
    verification_method = [1] * len(data['runData']);
    print("size of runData: %s" % (len(data['runData'])))
    for i, run_id in enumerate(sorted(data['runData'].keys())):
        try:
            verification_method[run_id] = data.get_nested(['runData', run_id, 'dynamicParameters', '/verifier', 'verification_method'])
            print("took new one at run %s" % (run_id))
        except KeyError:
            verification_method[run_id] = verification_method[run_id-1]
            print("took old one at run %s" % (run_id))
        print("verification_method: %s" %(verification_method[run_id]))
        
    """
    diameters_dict = get_model_diameters_from_verifier(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'angle': None, 'distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        error = pose_error(model_test_pose, gt_pose_transformed)
                        error_score = error['distance'] / model_diameter + error['angle'] / pi
                            # considers both distance and angle

                        # the right pose
                        if error_score < min_error_score:
                            min_error = error
                            # min_error['modelPoseIndex'] = pose_index
                            min_error['gtPoseIndex'] = gt_index
                            min_error['equivalentPoseIndex'] = equivalent_pose_index
                            min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of pose errors computed...' % (progress * 100))
            display_steps.remove(progress)

    """
    # check if the pose errors are below the thresholds
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error(pose['error'],
                                                            max_translation_error_relative *
                                                            diameters_dict[model_name],
                                                            max_rotation_error)
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors()?')
                        
                    try:
                        pose['verified'] = _check_papazov_verification(pose['mu'],
                                                                            v=min_mu_v,
                                                                            p=max_mu_p)
                    except KeyError:
                        raise Exception('Found no mu_values for the the pose, '
                                        'did you run order_mu_to_poses()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)
            
    # compute verification numbers
    for run_id, run in data["runData"].items():
        
        run['verification'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = 0
                false_negatives = 0
                true_negatives = len(poses)
                
                # go through poses sorted by index
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):
                    # a pose is a true positive if it's correct and gets verified
                    #print("pose is correct: %s; pose got verified: %s" %(pose['correct'],pose['verified']))
                    if pose['correct'] and pose['verified']:
                        #print("pose_index %d is a true positive" % pose_index)
                        true_positives += 1
                        true_negatives -= 1
                    elif pose['correct'] and not pose['verified']:
                        #print("pose_index %d is a false negative" % pose_index)
                        false_negatives += 1
                        true_negatives -= 1
                    elif not pose['correct'] and pose['verified']:
                        #print("pose_index %d is a false positive" % pose_index)
                        false_positives += 1
                        true_negatives -= 1
                        
                run['verification'][model_name] = {'truePositives': true_positives,
                                                       'falsePositives': false_positives,
                                                       #'trueNegatives': true_negatives, # not needed
                                                       'falseNegatives': false_negatives}
                
        except KeyError:
            # no poses in run, printed output above
            pass 
    # if the parameters are whithin the thresholds of the verification method
    # adds new field 'verified' with values True, False or None
    
    ""
    - go through every run and pose and look if the mu values got set
    - check if 
    ""
    
    ""
    iDict, tuples = extract_data(data,
                                 [['poses']],
                                 [['dynamicParameters', '/verifier', 'verification_method']])
    for run_id, run in data['runData'].items():
        try:
    ""
    """

def compute_pose_errors_from_verifier_by_hinterstoisser_lepetit_et_al_2013(data, dataset):
    """
    Note: This uses a different error-metric than [Kroischke 2016]. It is the error metric for the
    ICCV 2015 dataset of [Krull et al. 2015]. The metric is from [Hinterstoisser, Lepetit et al. 2013].
    It calculates the average distance between the model-vertices under the estimated pose and the ground truth pose.

    compared to the "non-verifier" version of this function, we get the model-diameters from the verifier.
    there should be no other differences.

    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The errors are given as the euclidean distance between the positions ("distance") and
    the angle between the quaternion rotations ("angle") and are added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param dataset: DatasetBase object for the current dataset
    """

    diameters_dict = get_model_diameters_from_verifier(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i / 10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'average distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)

                    equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                               model_equivalent_poses.items()
                    # -1 means identity

                    for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized(model_test_pose, gt_pose_transformed,
                                                                                           model_transform, dataset, model_name)
                        error_score = error['average distance'] / model_diameter
                        # this error metric has no rotational part

                        # the right pose
                        if error_score < min_error_score:
                            min_error = error
                            # min_error['modelPoseIndex'] = pose_index
                            min_error['gtPoseIndex'] = gt_index
                            min_error['equivalentPoseIndex'] = equivalent_pose_index
                            min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print('%d %% of pose errors computed...' % (progress * 100))
            display_steps.remove(progress)


def compute_pose_errors_from_verifier_by_hinterstoisser_lepetit_et_al_2013_ADI(data, dataset):
    """
    Note: This uses a different error-metric than [Kroischke 2016]. It is an error metric for
    rotationally symmetric models. The metric is from [Hinterstoisser, Lepetit et al. 2013].
    It calculates the average distance between the model-vertices and their nearest ground truth
    model-vertices. It is called ADI.

    calculate the differences between the estimated pose and the ground truth pose for all
    models in all runs

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The error is added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters_from_verifier(data)

    n_runs = len(data["runData"].keys())
    #display_steps = [i/100 for i in range(100 + 1)]
    time_average = 0

    for i, run_id in enumerate(sorted(data["runData"].keys())):
        start_time = time.time()
        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                #print('got no model load transform')
                model_transform = tft.identity_matrix()

            # try:
            #     model_equivalent_pose_transforms = \
            #         dict(data.get_run_data([['equivalentPoses', model_name]],
            #                                copy_missing_from_previous=True))
            #     model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            # except KeyError:
            #     #print('got no model equivalent poses')
            #     model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                # go through all ground truth poses in turn and find most likely one
                gts_path = ["runData", run_id, "groundTruth", model_name]
                min_error = {'average distance': None, 'equivalentPoseIndex': None,
                             'gtPoseIndex': None}
                min_error_score = float('inf')
                for gt_index in data.get_nested(gts_path).keys():
                    gt_pose = data.get_nested(gts_path + [gt_index])
                    #print('ground truth pose %i:' %(gt_index), gt_pose,'\n', pose_dict2matrix(gt_pose))
                    #print('model load transform:', model_transform,'\n', pose_dict2matrix(model_transform))
                    #print('scene load transform:\n', scene_transform)
                    gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                       scene_transform)
                    #print('*transformed ground truth pose %i:\n' %(gt_index), gt_pose_transformed)
                    # equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                    #                            model_equivalent_poses.items()
                    # -1 means identity

                    #for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:

                        #model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                        #print('equivalent pose:\n', equivalent_pose)
                        #print('*model test pose:\n', model_test_pose)
                        #error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized(model_test_pose, gt_pose_transformed,
                    error = pose_error_by_hinterstoisser_lepetit_et_al_2013_vectorized_ADI(model_pose, gt_pose_transformed,
                                                                                           model_transform ,dataset, model_name)
                    error_score = error['average distance'] / model_diameter
                    #print("model_diameter =",model_diameter, "error =",error['average distance'])
                    #print("error_score =",error_score)
                        # this error metric has no rotational part

                    # the right pose
                    if error_score < min_error_score:
                        min_error = error
                        # min_error['modelPoseIndex'] = pose_index
                        min_error['gtPoseIndex'] = gt_index
                        #min_error['equivalentPoseIndex'] = equivalent_pose_index
                        min_error_score = error_score

                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'error'],
                                min_error)

        #progress = floor((i + 1) / n_runs * 100) / 100
        end_time = time.time()
        time_diff = end_time - start_time
        time_average = ((time_average*(i-1)) + time_diff)/i
        remaining_time_estimate = time_average * (n_runs-i)
        print("run took %f sec. %i runs remaining, approx. %f min = %f h left." %( time_diff, n_runs-i,
                                                                                   remaining_time_estimate/60,
                                                                                   remaining_time_estimate/3600))
        #if progress in display_steps:
        #    print ('%d %% of pose errors computed...' % (progress * 100))
        #    display_steps.remove(progress)


def compute_pose_errors_of_intermediate_poses(data):
    """
    calculate the differences between the estimated pose of the intermediate pose-cluster and the 
    ground truth pose for all models in all runs.
    Do the calculation for the pose-clusters of the small and the big voting ball. 

    If loadTransforms and / or equivalent poses are contained in the data, these will be used to
    transform the ground truth or model pose respectively while comparing the two.

    The errors are given as the euclidean distance between the positions ("distance") and
    the angle between the quaternion rotations ("angle") and are added to each pose entry.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # get the transform that undoes any rotation / translation from scene loading
        try:
            scene_name = data.get_nested(["runData", run_id, "scene"])
            scene_transform = data.get_nested(["runData", run_id, "loadTransform",
                                               scene_name])
        except KeyError:
            # no loadTransform for the scene, use identity values
            scene_transform = tft.identity_matrix()

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            try:
                model_load_transforms = \
                    dict(data.get_run_data([["loadTransforms", model_name]],
                                           copy_missing_from_previous=True))
                model_transform = model_load_transforms[run_id]
            except KeyError:
                model_transform = tft.identity_matrix()

            try:
                model_equivalent_pose_transforms = \
                    dict(data.get_run_data([['equivalentPoses', model_name]],
                                           copy_missing_from_previous=True))
                model_equivalent_poses = model_equivalent_pose_transforms[run_id]
            except KeyError:
                model_equivalent_poses = {}

            model_diameter = diameters_dict[model_name]

            # go through all found model poses of both voting balls 0 and 1 (small voting ball and large voting ball)
            for voting_ball_idx in range(2):
                poses_path = ["runData", run_id, "Clustered_Poses_of_Voting_Ball_"+str(voting_ball_idx)+"_Poses", model_name]
            
                try: 
                    for pose_index in data.get_nested(poses_path).keys():
                        model_pose = data.get_nested(poses_path + [pose_index])
        
                        # go through all ground truth poses in turn and find most likely one
                        gts_path = ["runData", run_id, "groundTruth", model_name]
                        min_error = {'angle': None, 'distance': None, 'equivalentPoseIndex': None,
                                     'gtPoseIndex': None}
                        min_error_score = float('inf')
                        for gt_index in data.get_nested(gts_path).keys():
                            gt_pose = data.get_nested(gts_path + [gt_index])
                            gt_pose_transformed = transform_gt(gt_pose, model_transform,
                                                               scene_transform)
        
                            equivalent_poses_to_test = [(-1, tft.identity_matrix())] + \
                                                       model_equivalent_poses.items()
                            # -1 means identity
        
                            for equivalent_pose_index, equivalent_pose in equivalent_poses_to_test:
        
                                model_test_pose = concatenate_transforms(model_pose, equivalent_pose)
                                error = pose_error(model_test_pose, gt_pose_transformed)
                                error_score = error['distance'] / model_diameter + error['angle'] / pi
                                    # considers both distance and angle
        
                                # the right pose
                                if error_score < min_error_score:
                                    min_error = error
                                    # min_error['modelPoseIndex'] = pose_index
                                    min_error['gtPoseIndex'] = gt_index
                                    min_error['equivalentPoseIndex'] = equivalent_pose_index
                                    min_error_score = error_score
        
                        # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                        #                     min_error)
                        data.set_nested(["runData", run_id, "Clustered_Poses_of_Voting_Ball_"+str(voting_ball_idx)+\
                                         "_Poses", model_name, pose_index, 'error'],
                                        min_error)

                except KeyError:
                    print('Seems as if voting ball no. %d does not exist.' % voting_ball_idx)
                
        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of posecluster errors computed...' % (progress * 100))
            display_steps.remove(progress)

def compute_recognition(data, max_translation_error_relative, max_rotation_error):
    """
    Check, which and how many poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct. For each run, a field 'recognition' is added
    to store the number of false and true positives and the number of unrecognized ground
    truths that were in the scene.
    If no error data is available, the number of true positives and the value of 'correct' will be
    None.

    @note: The number of true positives can be smaller than the number of correct poses.
           truePositives is counted only until a wrong pose is found (the poses with the lowest
           indices represent the best guess of the algorithm and all guesses after a false pose
           are "worthless").
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    @param max_rotation_error: max. allowed orientation error in rad
    """
    diameters_dict = get_model_diameters(data)

    # check if the pose errors are below the thresholds
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error(pose['error'],
                                                            max_translation_error_relative *
                                                            diameters_dict[model_name],
                                                            max_rotation_error)
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)

    # compute easy recognition numbers
    for run_id, run in data["runData"].items():

        run['recognition'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = len(poses)
                unrecognized_gts = 0
                found_gts = []

                # go through poses sorted by index, pose 0 is the algorithm's best guess
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):

                    # a pose has to be both correct and not be matched to a ground truth pose
                    # that was already found previously
                    if pose['correct'] and pose['error']['gtPoseIndex'] not in found_gts:
                        true_positives += 1
                        false_positives -= 1
                        found_gts.append(pose['error']['gtPoseIndex'])
                    else:

                        # if there is no data, stop checking
                        if pose['correct'] is None and pose_index == 0:
                            true_positives = None

                        # first false pose means all other poses can't be trusted so we cut counting
                        # of true positives here and classify the rest as false positives
                        break

                for gt_index in run['groundTruth'][model_name].keys():
                    if gt_index not in found_gts:
                        unrecognized_gts += 1

                run['recognition'][model_name] = {'truePositives': true_positives,
                                                  'falsePositives': false_positives,
                                                  'unrecognizedGroundTruths': unrecognized_gts}
        except KeyError:
            # same as above, no poses in run
            pass


def compute_recognition_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative):
    """
    Check, which and how many poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct. For each run, a field 'recognition' is added
    to store the number of false and true positives and the number of unrecognized ground
    truths that were in the scene.
    If no error data is available, the number of true positives and the value of 'correct' will be
    None.

    Using the error-metric from [Hinterstoisser, Lepetit et al. 2013]
    Use this for ICCV dataset from [Krull et al. 2015]

    @note: The number of true positives can be smaller than the number of correct poses.
           truePositives is counted only until a wrong pose is found (the poses with the lowest
           indices represent the best guess of the algorithm and all guesses after a false pose
           are "worthless").
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    """
    diameters_dict = get_model_diameters(data)

    # check if the pose errors are below the threshold
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error_hinterstoisser_lepetit_2013(pose['error'],
                                                                                        max_translation_error_relative *
                                                                                        diameters_dict[model_name])
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors_hinterstoisser_lepetit_2013()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)

    # compute easy recognition numbers
    for run_id, run in data["runData"].items():

        run['recognition'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = len(poses)
                unrecognized_gts = 0
                found_gts = []

                # go through poses sorted by index, pose 0 is the algorithm's best guess
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):

                    # a pose has to be both correct and not be matched to a ground truth pose
                    # that was already found previously
                    if pose['correct'] and pose['error']['gtPoseIndex'] not in found_gts:
                        true_positives += 1
                        false_positives -= 1
                        found_gts.append(pose['error']['gtPoseIndex'])
                    else:

                        # if there is no data, stop checking
                        if pose['correct'] is None and pose_index == 0:
                            true_positives = None

                        # first false pose means all other poses can't be trusted so we cut counting
                        # of true positives here and classify the rest as false positives
                        break

                for gt_index in run['groundTruth'][model_name].keys():
                    if gt_index not in found_gts:
                        unrecognized_gts += 1

                run['recognition'][model_name] = {'truePositives': true_positives,
                                                  'falsePositives': false_positives,
                                                  'unrecognizedGroundTruths': unrecognized_gts}
        except KeyError:
            # same as above, no poses in run
            pass


def compute_recognition_with_hodan_et_al_2018(data, theta):
    """
    Check, which and how many poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct. For each run, a field 'recognition' is added
    to store the number of false and true positives and the number of unrecognized ground
    truths that were in the scene.
    If no error data is available, the number of true positives and the value of 'correct' will be
    None.

    Using the error-metric from [Hodan, Michel et al. 2018]
    Use this for ICCV dataset from [Krull et al. 2015] in context with SIXD challenge 2017

    @note: The number of true positives can be smaller than the number of correct poses.
           truePositives is counted only until a wrong pose is found (the poses with the lowest
           indices represent the best guess of the algorithm and all guesses after a false pose
           are "worthless").
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    """

    # check if the pose errors are below the threshold
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error_hodan_et_al_2018(pose['error'], theta)
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors_hodan_et_al_2018()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)

    # compute easy recognition numbers
    for run_id, run in data["runData"].items():

        run['recognition'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = len(poses)
                unrecognized_gts = 0
                found_gts = []

                # go through poses sorted by index, pose 0 is the algorithm's best guess
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):

                    # a pose has to be both correct and not be matched to a ground truth pose
                    # that was already found previously
                    if pose['correct'] and pose['error']['gtPoseIndex'] not in found_gts and \
                        pose['error']['visibility fraction of GT'] >= 0.1:
                        true_positives += 1
                        false_positives -= 1
                        found_gts.append(pose['error']['gtPoseIndex'])
                    else:

                        # if there is no data, stop checking
                        if pose['correct'] is None and pose_index == 0:
                            true_positives = None

                        # first false pose means all other poses can't be trusted so we cut counting
                        # of true positives here and classify the rest as false positives
                        break

                for gt_index in run['groundTruth'][model_name].keys():
                    if gt_index not in found_gts:
                        unrecognized_gts += 1

                #if true_positives != 0:
                #    print (run_id, 'true positives =', true_positives)

                run['recognition'][model_name] = {'truePositives': true_positives,
                                                  'falsePositives': false_positives,
                                                  'unrecognizedGroundTruths': unrecognized_gts}

                #print(run_id,run['recognition'][model_name])

        except KeyError:
            # same as above, no poses in run
            print('No poses in run %d, not computing recognition for this run.' % run_id)
            pass


def compute_recognition_from_verifier_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative):
    """
    Check, which and how many poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct. For each run, a field 'recognition' is added
    to store the number of false and true positives and the number of unrecognized ground
    truths that were in the scene.
    If no error data is available, the number of true positives and the value of 'correct' will be
    None.

    Using the error-metric from [Hinterstoisser, Lepetit et al. 2013]
    Use this for ICCV dataset from [Krull et al. 2015]

    note: this differs only form the 'non-verifier' method in the way the diameters are fetched.
    this method uses the verifier statistics instead of the matcher statistics

    @note: The number of true positives can be smaller than the number of correct poses.
           truePositives is counted only until a wrong pose is found (the poses with the lowest
           indices represent the best guess of the algorithm and all guesses after a false pose
           are "worthless").
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    """
    diameters_dict = get_model_diameters_from_verifier(data)

    # check if the pose errors are below the threshold
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error_hinterstoisser_lepetit_2013(pose['error'],
                                                                                        max_translation_error_relative *
                                                                                        diameters_dict[model_name])
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors_hinterstoisser_lepetit_2013()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)

    # compute easy recognition numbers
    for run_id, run in data["runData"].items():

        run['recognition'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = len(poses)
                unrecognized_gts = 0
                found_gts = []

                # go through poses sorted by index, pose 0 is the algorithm's best guess
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):

                    # a pose has to be both correct and not be matched to a ground truth pose
                    # that was already found previously
                    if pose['correct'] and pose['error']['gtPoseIndex'] not in found_gts:
                        true_positives += 1
                        false_positives -= 1
                        found_gts.append(pose['error']['gtPoseIndex'])
                    else:

                        # if there is no data, stop checking
                        if pose['correct'] is None and pose_index == 0:
                            true_positives = None

                        # first false pose means all other poses can't be trusted so we cut counting
                        # of true positives here and classify the rest as false positives
                        break

                for gt_index in run['groundTruth'][model_name].keys():
                    if gt_index not in found_gts:
                        unrecognized_gts += 1

                run['recognition'][model_name] = {'truePositives': true_positives,
                                                  'falsePositives': false_positives,
                                                  'unrecognizedGroundTruths': unrecognized_gts}
        except KeyError:
            # same as above, no poses in run
            pass



def compute_correctness_from_verifier(data, max_translation_error_relative, max_rotation_error):
    """
    Check, which poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct.
    If no error data is available, the value of 'correct' will be
    None.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    @param max_rotation_error: max. allowed orientation error in rad
    """
    diameters_dict = get_model_diameters_from_verifier(data)

    # check if the pose errors are below the thresholds
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error(pose['error'],
                                                            max_translation_error_relative *
                                                            diameters_dict[model_name],
                                                            max_rotation_error)
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)


def compute_correctness_from_verifier_with_hinterstoisser_lepetit_2013(data, max_translation_error_relative):
    """
    Check, which poses are recognized. A field 'correct' is added to each pose,
    indicating whether the pose was deemed correct.
    If no error data is available, the value of 'correct' will be None.

    Using the error-metric from [Hinterstoisser, Lepetit et al. 2013]
    Use this for ICCV dataset from [Krull et al. 2015]

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    @param max_rotation_error: max. allowed orientation error in rad
    """
    diameters_dict = get_model_diameters_from_verifier(data)

    # check if the pose errors are below the thresholds
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['correct'] = _check_pose_error_hinterstoisser_lepetit_2013(pose['error'],
                                                                                        max_translation_error_relative *
                                                                                        diameters_dict[model_name])
                    except KeyError:
                        raise Exception('Found no precomputed errors, '
                                        'did you run compute_pose_errors_by_hinterstoisser_lepetit_2013()?')
        except KeyError:
            print('No poses in run %d, not computing recognition for this run.' % run_id)

def compute_correctness_of_intermediate_poses(data, max_translation_error_relative, max_rotation_error):
    """
    Check, which poses are recognized. A field 'correct' is added to each pose-cluster,
    indicating whether the pose was deemed correct.
    If no error data is available, the value of 'correct' will be
    None.

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    @param max_translation_error_relative: max. allowed translation error relative to the model's
                                           diameter
    @param max_rotation_error: max. allowed orientation error in rad
    """
    diameters_dict = get_model_diameters(data)

    # check if the pose errors are below the thresholds
    # adds new field 'correct' with values True, False or None
    for run_id, run in data['runData'].items():
        # loop over both voting balls, if existent
        for voting_ball_idx in range(2):
            try:
                for model_name, poses in run["Clustered_Poses_of_Voting_Ball_"+str(voting_ball_idx)+"_Poses"].items():
                    for pose_index, pose in poses.items():
    
                        try:
                            pose['correct'] = _check_pose_error(pose['error'],
                                                                max_translation_error_relative *
                                                                diameters_dict[model_name],
                                                                max_rotation_error)
                        except KeyError:
                            raise Exception('Found no precomputed errors, '
                                            'did you run compute_pose_errors()?')
            except KeyError:
                print('No poses in run %d, or no voting ball no. %d existing, not computing correctness for this run.' % (run_id, voting_ball_idx))

def assign_mu_to_poses(data):
    """
    assigns mu_v and mu_p to the pose it belongs to.
    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            
            # go through all ground truth poses in turn and find most likely one
            mu_path = ["runData", run_id, "statistics", "/verifier", model_name]
            mu_value = {'mu_v': [], 'mu_p': []}
            
            # getting mu_v and mu_p from mu_path
            mu_v = data.get_nested(mu_path + ["mu_v"])
            mu_p = data.get_nested(mu_path + ["mu_p"])
            
            # putting mu_v and mu_p to mu
            mu_value['mu_v'] = mu_v
            mu_value['mu_p'] = mu_p

            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])


                #print ("mu_value: %s" % (mu_value[0]))
                # data.set_nested(["runData", run_id, "poseErrors", model_name, pose_index],
                #                     min_error)
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'mu', 'mu_v'],
                                mu_value['mu_v'][pose_index])
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'mu', 'mu_p'],
                                mu_value['mu_p'][pose_index])

        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of mu values sorted...' % (progress * 100))
            display_steps.remove(progress)
  

def compute_verified(data):
    """
    MFZ: Since Krone did not make much of an effort of documenting his code - I'll try to
    add useful info here:

    This function runs through the experiment-data, checks if a pose got verified, and adds the
    weight of a 'verfied_poses'-pose to its counterpart in 'poses'. The check is based on the fact,
    that the verifier node publishes only verified poses.
    ATTENTION: you can not use this function when you run a script like
    "searchradius_angleDiffThresh_variation_evaluation.py"
    because in those experiments, the verifier node verifies ALL poses. In those cases you want to
    use a combination of "compute_verified_through_mu()" and "add_verifiedWeight_to_poses()" to get
    the same result.

    Result: Now we have information of 'is a pose verified?' and 'how good is its verification-weight?'
    in one place: 'poses'
    {run-id: {poses: {model-name: {pose-number: {verified: True/False, verificationWeight: ... }}}}}

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters_from_verifier(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i/10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print ('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            verified_poses_path = ["runData", run_id, "verified_poses", model_name]
            
            verified_poses_exist = True
            try:
                data.get_nested(verified_poses_path)
            except KeyError:
                verified_poses_exist = False
                
            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])
                
                model_pose_equals_verified_pose_index = -1
                
                if verified_poses_exist:
                    for verified_pose_index in data.get_nested(verified_poses_path).keys():
                        model_verified_pose = data.get_nested(verified_poses_path + [verified_pose_index])
                        
                        if _check_pose_equals_pose(model_pose, model_verified_pose):
                            model_pose_equals_verified_pose_index = verified_pose_index
                            #print("pose %s equals verified_pose %s" %(model_pose, model_verified_pose))
                            break
                    
                    
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'verified'],
                                model_pose_equals_verified_pose_index >= 0)
                    
                if model_pose_equals_verified_pose_index >= 0:
                    verificationWeight = data.get_nested(verified_poses_path + [model_pose_equals_verified_pose_index, 'weight'])
                else:
                    verificationWeight = 0
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'verificationWeight'],
                                verificationWeight)
        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print ('%d %% of verified poses assigned to poses...' % (progress * 100))
            display_steps.remove(progress)


def add_verifiedWeight_to_poses(data):
    """
    This function is only needed when "compute_verified_through_mu()" is used instead of "compute_verified()".
    Background: "compute_verified()" results are made based on the output of verifier-node, i.e. it assumes that every
                published pose of verifier-node is verified. THIS ASSUMPTION CAN NOT BE MADE FOR scripts like
                "searchradius_angleDiffThresh_variation_experiment" and "-evaluation". In those experiments the verifier-node
                "verifies" ALL poses that it received by the matcher node. Thus, "compute_verified()" will result in verifing all poses too.
                In this case we need to use this function here. Apply the following way:
                1. do 'compute_verified_through_mu()'
                2. do 'add_verifiedWeight_to_poses()'

    This function runs through the experiment-data and adds the weight of a 'verfied_poses'-pose to
    its counterpart in 'poses'.
    Result: Now we have inforamtion of 'is a pose verified?' and 'how good is its verification-weight?' in one place: 'poses'
    {run-id: {poses: {model-name: {pose-number: {verified: True/False, verificationWeight: ... }}}}}

    @type data: ExperimentDataStructure
    @param data: the experiment data, the computed values will be written into this data structure
    """

    diameters_dict = get_model_diameters_from_verifier(data)

    n_runs = len(data["runData"].keys())
    display_steps = [i / 10 for i in range(10 + 1)]

    for i, run_id in enumerate(sorted(data["runData"].keys())):

        # find all models that were matched in this run
        try:
            model_names = data.get_nested(["runData", run_id, "poses"]).keys()
        except KeyError:
            print('No models matched in run %d, skipping.' % run_id)
            continue

        for model_name in model_names:

            # go through all found model poses in turn
            poses_path = ["runData", run_id, "poses", model_name]
            verified_poses_path = ["runData", run_id, "verified_poses", model_name]

            verified_poses_exist = True
            try:
                data.get_nested(verified_poses_path)
            except KeyError:
                verified_poses_exist = False

            for pose_index in data.get_nested(poses_path).keys():
                model_pose = data.get_nested(poses_path + [pose_index])

                model_pose_equals_verified_pose_index = -1

                if verified_poses_exist:
                    for verified_pose_index in data.get_nested(verified_poses_path).keys():
                        model_verified_pose = data.get_nested(verified_poses_path + [verified_pose_index])

                        if _check_pose_equals_pose(model_pose, model_verified_pose):
                            model_pose_equals_verified_pose_index = verified_pose_index
                            # print("pose %s equals verified_pose %s" %(model_pose, model_verified_pose))
                            break

                if model_pose_equals_verified_pose_index >= 0:
                    verificationWeight = data.get_nested(
                        verified_poses_path + [model_pose_equals_verified_pose_index, 'weight'])
                else:
                    verificationWeight = 0
                data.set_nested(["runData", run_id, "poses", model_name, pose_index, 'verificationWeight'],
                                verificationWeight)
        progress = floor((i + 1) / n_runs * 10) / 10
        if progress in display_steps:
            print('%d %% of verified poses assigned to poses...' % (progress * 100))
            display_steps.remove(progress)


def compute_verified_through_mu(data, min_mu_v, max_mu_p):
    for run_id, run in data['runData'].items():
        try:
            for model_name, poses in run['poses'].items():
                for pose_index, pose in poses.items():

                    try:
                        pose['verified'] = _check_papazov_verification(pose['mu'],
                                                    v=min_mu_v,
                                                    p=max_mu_p)
                    except KeyError:
                        raise Exception('Found no mu_values for the the pose, '
                                        'did you run order_mu_to_poses()?')
        except KeyError:
            #print('No poses in run %d, not computing verification for this run.' % run_id)
            pass
            
            
def compute_verification(data):
    """
    compute verification numbers

    ATTENTION: THIS (KRONE'S) FUNCTION DOES COUNT POSES AS TRUE POSITIVES
    EVEN IF THEY SHARE THE SAME GROUND TRUTH POSE. This is no issue, if the conflict graph is
    used as well, because it probably resolves all cases where two poses share the same GT-Pose.

    :param data: ExperimentData structure
    """

    for run_id, run in data["runData"].items():
        
        run['verification'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = 0
                false_negatives = 0
                true_negatives = len(poses)
                
                # go through poses sorted by index
                for pose_index, pose in sorted(poses.items(), key=itemgetter(0)):
                    # a pose is a true positive if it's correct and gets verified
                    #print("pose is correct: %s; pose got verified: %s" %(pose['correct'],pose['verified']))
                    if pose['correct'] and pose['verified']:
                        #print("pose_index %d is a true positive" % pose_index)
                        true_positives += 1
                        true_negatives -= 1
                    elif pose['correct'] and not pose['verified']:
                        #print("pose_index %d is a false negative" % pose_index)
                        false_negatives += 1
                        true_negatives -= 1
                    elif not pose['correct'] and pose['verified']:
                        #print("pose_index %d is a false positive" % pose_index)
                        false_positives += 1
                        true_negatives -= 1
                        
                run['verification'][model_name] = {'truePositives': true_positives,
                                                       'falsePositives': false_positives,
                                                       #'trueNegatives': true_negatives, # not needed
                                                       'falseNegatives': false_negatives}
                
        except KeyError:
            # no poses in run, printed output above
            pass

""" NOTE: this is not correct, because it's not finished!
          use the average_rate_...() functions from data_extraction.py instead! They calculate TP's, FP's etc. as well.
          
def compute_hinterstoisser_verification(data):
    # compute verification numbers for use with hinterstoisser extensions
    for run_id, run in data["runData"].items():

        run['verification'] = {}
        try:
            for model_name, poses in run['poses'].items():
                true_positives = 0
                false_positives = 0
                false_negatives = 0
                true_negatives = 0
                unrecognized_gts = 0
                found_gts = []

                max_idx = [-1, -1]
                max_weight = [-1, -1]

                # go through poses sorted by index
                #  find best pose
                for pose_idx, pose in sorted(poses.items(), key=itemgetter(0)):
                    if pose['verificationWeight'] > max_weight[0]:
                        max_idx[0] = pose_idx
                        max_weight[0] = pose['verificationWeight']

                # find second-best pose
                for pose_idx, pose in sorted(poses.items(), key=itemgetter(0)):
                    if pose_idx != max_idx[0]:
                        if pose['verificationWeight'] > max_weight[1]:
                            max_idx[1] = pose_idx
                            max_weight[1] = pose['verificationWeight']

                best_pose = poses[max_idx[0]]
                second_best_pose = poses[max_idx[1]]

                # a pose has to be both correct and verified and not be matched to a ground truth pose
                # that was already found previously
                # the second best pose can only be a true positive if the first pose was correct and verified too
                # (for comparability reasons with Kroischke's results)
                if best_pose['correct'] and best_pose['verified']\
                        and best_pose['error']['gtPoseIndex'] not in found_gts:
                    # first pose is correct and verified
                    true_positives += 1
                    found_gts.append(best_pose['error']['gtPoseIndex'])
                elif not best_pose['correct'] and best_pose['verified']:
                    false_positives += 1
                elif best_pose['correct'] and not best_pose['verified']:
                    false_negatives += 1
                elif not best_pose['correct'] and not best_pose['verified']:
                    true_negatives += 1

                if best_pose['correct'] and best_pose['verified']:
                    if second_best_pose['correct'] and second_best_pose['verified']:
                        if second_best_pose['error']['gtPoseIndex'] not in found_gts:
                            # second pose is correct and verified
                            true_positives += 1
                            found_gts.append(second_best_pose['error']['gtPoseIndex'])
        
                        elif second_best_pose['error']['gtPoseIndex'] in found_gts:
                            false_positives += 1
                    if second_best_pose['correct'] and not second_best_pose['verified']:
                        if second_best_pose['error']['gtPoseIndex'] in found_gts:
                            true_negatives += 1
                        elif second_best_pose['error']['gtPoseIndex'] not in found_gts:
                            false_negatives += 1
                    if not second_best_pose['correct'] and second_best_pose['verified']:
                        false_positives += 1
                    if not second_best_pose['correct'] and not second_best_pose['verified']:
                        true_negatives += 1
                        


                else:

                    # if there is no data, stop checking
                    if pose['correct'] is None and pose_idx == 0:
                        true_positives = None

                    # first false pose means all other poses can't be trusted so we cut counting
                    # of true positives here and classify the rest as false positives
                    break

                for gt_index in run['groundTruth'][model_name].keys():
                    if gt_index not in found_gts:
                        unrecognized_gts += 1

                run['verification'][model_name] = {'truePositives': true_positives,
                                                   'falsePositives': false_positives,
                                                   'trueNegatives': true_negatives,
                                                   'falseNegatives': false_negatives}

        except KeyError:
            # no poses in run, printed output above
            pass
"""

def get_model_diameters(data):
    """
    get the diameters of all models from the experiment data
    @type data: ExperimentDataStructure
    @param data: the experiment data structure
    @return: dictionary of form {model_name: model_diameter}
    """
    name_diameter_pairs = []
    iDict, tuples = extract_data(data, [['statistics', '/matcher']])
    iDict['model_statistics'] = iDict.pop('/matcher')
    for tupl in tuples:
        try:
            name_diameter_pairs.append([tupl[iDict['model']],
                                        tupl[iDict['model_statistics']]['model_diameter']])
        except KeyError:
            continue

    return dict(name_diameter_pairs)


def get_model_diameters_from_verifier(data):
    """
    get the diameters of all models from the verifiers experiment data
    @type data: ExperimentDataStructure
    @param data: the experiment data structure
    @return: dictionary of form {model_name: model_diameter}
    """
    name_diameter_pairs = []
    iDict, tuples = extract_data(data, [['statistics', '/verifier']])
    iDict['model_statistics'] = iDict.pop('/verifier')
    for tupl in tuples:
        try:
            name_diameter_pairs.append([tupl[iDict['model']],
                                        tupl[iDict['model_statistics']]['model_diameter']])
        except KeyError:
            continue

    return dict(name_diameter_pairs)


def _check_pose_error(pose_error, max_translation_error, max_rotation_error):
    """
    check whether a pose_error is within the max allowed error
    @param pose_error: dict of form {"distance": ..., "angle": ...}
    @param max_translation_error:
    @param max_rotation_error: in rad
    @return True if pose_error is within the maximum errors, None if pose_error contains None-data
            and False otherwise
    """
    # no pose_error data -> return none
    if pose_error["distance"] is None or pose_error["angle"] is None:
        return None

    # check that all errors are within bounds
    distance_ok = pose_error["distance"] <= max_translation_error
    angle_ok = pose_error["angle"] <= max_rotation_error
    return distance_ok and angle_ok

def _check_pose_error_hinterstoisser_lepetit_2013(pose_error, max_translation_error):
    """
    check whether a pose_error is within the max allowed error
    @param pose_error: dict of form {"average distance": ...}
    @param max_translation_error: absolute value
    @return True if pose_error is within the maximum errors, None if pose_error contains None-data
            and False otherwise
    """
    # no pose_error data -> return none
    if pose_error["average distance"] is None:
        return None

    # check that error is within bounds
    distance_ok = pose_error["average distance"] <= max_translation_error
    return distance_ok

def _check_pose_error_hodan_et_al_2018(pose_error, theta):
    """
    check whether a pose_error is within the max allowed error
    @param pose_error: dict of form {"error score": ...}
    @param theta: max error score
    @return True if pose_error is within the maximum errors, None if pose_error contains None-data
            and False otherwise
    """
    # no pose_error data -> return none
    if pose_error["error score"] is None:
        return None

    # check that error is within bounds
    error_score_ok = pose_error["error score"] <= theta
    return error_score_ok

def _check_papazov_verification(pose_mu, v, p):
    """
    check whether a mu_value is within the limits of v and p
    @param pose_mu, dict of form {"mu_v": ..., "mu_p": ...}
    @param v: lower limit for mu_v
    @param p: upper limit for mu_p
    @return True if mu_v is higher than v and mu_p is lower than p, 
            None if pose_mu contains None-data and False otherwise
    """
    
    # no pose_mu data -> return none
    if pose_mu["mu_v"] is None or pose_mu["mu_p"] is None:
        print ("something was None")
        return None
    
    # check that all mu-values are within limits
    mu_v_ok = pose_mu["mu_v"] >= v
    mu_p_ok = pose_mu["mu_p"] <= p
    return mu_v_ok and mu_p_ok

def _check_pose_equals_pose(pose1, pose2):
    #print(pose1['quaternion'])
    #print(pose2['quaternion'])
    equalQuaternionValues = len(set(pose1['quaternion']) & set(pose2['quaternion']))
    equalTranslationValues = len(set(pose1['translation']) & set(pose2['translation']))
    equalVotingBallIndices = False
    if pose1['votingBall'] == pose2['votingBall']:
        equalVotingBallIndices = True
    return equalQuaternionValues == 4 and equalTranslationValues == 3 and equalVotingBallIndices
    
    
