# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

# package imports
from pf_matching.data_structure import ExperimentDataStructure

# other
import numpy as np

def get_unique_run_data(data, nested_key_list, **kwargs):
    """
    @brief Get a list of unique entries that reside under the nested keys given in the run datasets
    @param data the data structure returned by the ExperimentInterface
    @param nested_key_list keys to the data (relative to run number)
    @param **kwargs passed directly through to data.get_run_data()
    @return ordered list of unique entries from all runs, note that Python orders capital letters
            first (e.g. ['a', 'B'].sort() -> ['B', 'a'])
    """
    # make sure we get the right data as an input
    assert type(data) is ExperimentDataStructure

    # get data from all runs
    tmp = data.get_run_data(nested_key_list, **kwargs)

    # return the unique values (acquired through "set()" as a list)
    unique_list = list(set([entry[1] for entry in tmp if entry[1] is not None]))
    unique_list.sort()
    return unique_list


def check_pose_error(pose_error, max_translation_error, max_rotation_error):
    """
    check whether a pose_error is within the max allowed error
    @param pose_error: dict of form {"distance": ..., "angle": ...}
    @param max_translation_error:
    @param max_rotation_error: in rad
    @return True if pose_error is within the maximum errors, None if pose_error contains None-data
            and False otherwise
    """
    # no pose_error data -> return none
    if pose_error["distance"] is None or pose_error["angle"] is None:
        return None

    # check that all errors are within bounds
    distance_ok = pose_error["distance"] <= max_translation_error
    angle_ok = pose_error["angle"] <= max_rotation_error
    return distance_ok and angle_ok

def check_papazov_verification(pose_mu, v, p):
    """
    check whether a mu_value is within the limits of v and p
    @param pose_mu, dict of form {"mu_v": ..., "mu_p": ...}
    @param v: lower limit for mu_v
    @param p: upper limit for mu_p
    @return True if mu_v is higher than v and mu_p is lower than p, 
            None if pose_mu contains None-data and False otherwise
    """
    
    # no pose_mu data -> return noe
    if pose_mu["mu_v"] is None or pose_mu["mu_p"] is None:
        return None
    
    # check that all mu-values are within limits
    mu_v_ok = pose_mu["mu_v"] >= v
    mu_p_ok = pose_mu["mu_p"] <= p
    return mu_v_ok and mu_p_ok

def extract_data(data, model_specific_nested_key_lists=None, unspecific_nested_key_lists=None):
    """
    @brief extract information from the data-structure and return it as a list of tuples

    Value extraction for all values uses data.get_run_data() with copyMissingFromPrevious=True!

    @type data: ExperimentDataStructure
    @param data: the data structure returned by the ExperimentInterface
    @param model_specific_nested_key_lists: lists of nested keys to dicts that yield a value for
                                            dict[model_name] or None
    @param unspecific_nested_key_lists: list of nested keys to parameters valid for all models or
                                        None
    @return tuple with:
                   0: a dict for convenient retrieval of the indices for different data fields in
                      the tuples
                   1: list of tuples of form (runID, model, scene,
                                              <model specific values in order of passed key lists>,
                                              <unspecific values in order of passed key lists>
                                              )
                      The tuples are only unique in their combination of runID and model!
    """
    # make sure we get the right data as an input
    assert type(data) is ExperimentDataStructure

    # handle None-data input
    if not model_specific_nested_key_lists:
        model_specific_nested_key_lists = []
    if not unspecific_nested_key_lists:
        unspecific_nested_key_lists = []

    # make list of all key lists
    key_lists = [["models"], ["scene"]] + \
                model_specific_nested_key_lists + \
                unspecific_nested_key_lists
    runID_index = 0
    model_name_index = 1
    scene_name_index = 2
    specific_values_start_index = scene_name_index + 1
    specific_values_end_index = specific_values_start_index +\
                                len(model_specific_nested_key_lists) - 1

    # get the data as tuples
    # element 0 is runID
    data_tuples = data.get_run_data(key_lists, True, True)

    # build the list of tuples
    data_out = []
    for entry in data_tuples:
        for model_name in entry[model_name_index].values():

            # runID, model_name, scene_name
            part1 = (entry[runID_index], model_name, entry[scene_name_index])

            # extract model-specific data
            part2 = []
            for value_dict in entry[specific_values_start_index: specific_values_end_index + 1]:
                part2.append(value_dict[model_name])

            # copy over unspecific values
            part3 = entry[specific_values_end_index + 1:]

            # concatenate all parts
            data_out.append(part1 + tuple(part2) + part3)

    # build the index dictionary
    index_dict = dict([(key_list[-1], i) for i, key_list in enumerate([["runID"]] + key_lists)])
    index_dict["model"] = index_dict.pop("models")  # change key models -> model

    return index_dict, data_out


def nest_and_process_by_values(data_list, indices, fun):
    """
    take the data from a list of data tuples and nest it according to the given indices
    @param data_list: list of data tuples
    @param indices: list of valid indices to the data entries in the data tuples, the entries of
           indices[:-1] are used as the nesting values while indices[-1] (the last index) specifies
           the actual data
    @param fun: function to process the list of data that is found for indices[-1] at the deepest
               nesting, should have the signature fun(<list of data type pointed to by indices[-1]>)
    @return list of tuples of the form x = (nesting_value, list_in_x) or y = (nesting_value, value),
            where list_in_x can be a list of type x or y again
    """

    # check that we have enough indices to work with
    if len(indices) < 2:
        raise Exception("Input indices must have at least length 2.")

    # storage for output
    out_list = []

    # get unique values for the data under the first index
    unique_values = sorted(list(set([entry[indices[0]] for entry in data_list])))

    if len(indices) == 2:
        # indices has only 2 values -> deepest nesting, put in the values
        for value in unique_values:
            #print(value)
            data = [entry[indices[1]] for entry in data_list
                    if entry[indices[0]] == value]
            #print('value =', value)
            out_list.append((value, fun(data)))

    else:
        # we need to go "deeper" -> recursively
        for value in unique_values:
            #print('value =', value)
            out_list.append((value, nest_and_process_by_values(
                [data_tuple for data_tuple in data_list if data_tuple[indices[0]] == value],
                indices[1:],
                fun)))

    return out_list


def nest_and_process_by_values_advanced(data_list, indices, fun, fun_arg):
    """
    Take the data from a list of data tuples and nest it according to the given indices
    Difference to "nest_and_process_by_values"-function: one can pass another argument to "fun"
    This is needed when one wants to pass 'average_key_figure' or 'average_key_figure_per_voting_ball'
    as 'fun', because those functions take arguments.
    Also: creates list of lists, not list of tuples, because we want to enable modifications later on
    @param data_list: list of data tuples
    @param indices: list of valid indices to the data entries in the data tuples, the entries of
           indices[:-1] are used as the nesting values while indices[-1] (the last index) specifies
           the actual data
    @param fun: function to process the list of data that is found for indices[-1] at the deepest
               nesting, should have the signature fun(<list of data type pointed to by indices[-1]>)
    @param fun_arg: argument to pass on to fun-function
    @type fun_arg: string
    @return list of lists of the form x = [nesting_value, list_in_x] or y = [nesting_value, value],
            where list_in_x can be a list of type x or y again
    """

    # check that we have enough indices to work with
    if len(indices) < 2:
        raise Exception("Input indices must have at least length 2.")

    # storage for output
    out_list = []

    # get unique values for the data under the first index
    unique_values = sorted(list(set([entry[indices[0]] for entry in data_list])))

    if len(indices) == 2:
        # indices has only 2 values -> deepest nesting, put in the values
        for value in unique_values:
            #print(value)
            data = [entry[indices[1]] for entry in data_list
                    if entry[indices[0]] == value]

            out_list.append([value, fun(data, fun_arg)])

    else:
        # we need to go "deeper" -> recursively
        for value in unique_values:
            out_list.append([value, nest_and_process_by_values_advanced(
                [data_tuple for data_tuple in data_list if data_tuple[indices[0]] == value],
                indices[1:],
                fun, fun_arg)])

    return out_list


def average_rate_of_best_poses_that_are_correct_and_verified_old_and_buggy(poses_dicts):
    """
    calculate the recognition rate for detecting one instance
    :param poses_dicts: input from nest_and_process_by_values()
    :return: recognition_rate for one-instance-detection
    """
    #print(poses_dicts)
    counter_of_correct_and_verified_best_poses = 0
    for poses_dict in poses_dicts: # loop over scenes
        #print('poses_dict =',poses_dict)
        max_idx = -1
        max_weight = -1 # because not-verified poses have verifictationWeight = 0
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verificationWeight'] > max_weight:
                max_idx = pose_idx
                max_weight = pose['verificationWeight']
        #print('max pose =',max_idx,'=', poses_dict[max_idx])
        if max_idx > -1 and max_weight >= 0 and poses_dict[max_idx]['correct'] and \
                poses_dict[max_idx]['verified']:
            counter_of_correct_and_verified_best_poses += 1

    return counter_of_correct_and_verified_best_poses / len(poses_dicts)


def average_rate_of_best_poses_that_are_correct_and_verified(poses_dicts):
    """
    calculate the recognition rate for detecting one instance
    :param poses_dicts: input from nest_and_process_by_values()
    :return: recognition_rate for one-instance-detection
    """
    #print(poses_dicts)
    counter_of_correct_and_verified_best_poses = 0
    for poses_dict in poses_dicts: # loop over scenes
        #print('poses_dict =',poses_dict)
        max_idx = -1
        max_weight = -1 # because not-verified poses have verifictationWeight = 0
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight:
                    max_idx = pose_idx
                    max_weight = pose['verificationWeight']
        #print('max pose =',max_idx,'=', poses_dict[max_idx])
        if max_idx > -1 and poses_dict[max_idx]['correct'] and \
                poses_dict[max_idx]['verified']:
            counter_of_correct_and_verified_best_poses += 1

    return counter_of_correct_and_verified_best_poses / len(poses_dicts)


def average_verificationWeight_of_best_poses_that_are_correct_and_verified(poses_dicts):
    """
    calculate the average verificationWeight for pose that has highest 'verificationWeight' and
    also is marked as 'correct'
    :param poses_dicts: input from nest_and_process_by_values()
    :return: recognition_rate for one-instance-detection
    """
    #print(poses_dicts)
    sum_verificationWeight_of_correct_and_verified_best_poses = 0
    for poses_dict in poses_dicts: # loop over scenes
        #print('poses_dict =',poses_dict)
        max_idx = -1
        max_weight = -1 # because not-verified poses have verifictationWeight = 0
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight:
                    max_idx = pose_idx
                    max_weight = pose['verificationWeight']
        #print('max pose =',max_idx,'=', poses_dict[max_idx])
        if max_idx > -1 and poses_dict[max_idx]['correct'] and \
                poses_dict[max_idx]['verified']:
            sum_verificationWeight_of_correct_and_verified_best_poses += max_weight

    return sum_verificationWeight_of_correct_and_verified_best_poses / len(poses_dicts)


def average_rate_of_two_best_poses_that_are_correct_and_verified(poses_dicts):
    """
    calculate the recognition rate for detecting two instances
    :param poses_dicts: input form nest_and_process_by_values()
    :return: recognition rate for two-instance-detection
    """
    #print(poses_dicts)
    # TODO: remove after debugging
    # scene = 0
    # sensor = 0
    # models = {0:'cuboid', 1:'cyllinder', 2:'ellipsoid', 3:'hex frustum', 4:'pyramid'}
    # model = 0
    # if len(poses_dicts) ==100: print('************************** %s **************************' % models[model])

    counter_of_correct_and_verified_best_poses = 0
    for poses_dict in poses_dicts: # loop over scenes
        #print('poses_dict =',poses_dict)

        max_idx = [-1,-1]
        max_weight = [-1,-1]
        # find best pose
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight[0]:
                    max_idx[0] = pose_idx
                    max_weight[0] = pose['verificationWeight']
        #print('1st max pose =',max_idx[0],'=', poses_dict[max_idx[0]])

        # find second-best pose
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx[0]:
                if pose['verified']:
                    if pose['verificationWeight'] > max_weight[1]:
                        max_idx[1] = pose_idx
                        max_weight[1] = pose['verificationWeight']
        #print('2nd max pose =',max_idx[1],'=', poses_dict[max_idx[1]])

        # to recognize two instances correctly, they both must be correct and verified,
        # furthermore they have to belong to different ground-thruth-poses
        if max_idx[0] > -1 and max_idx[1] > -1:
            if poses_dict[max_idx[0]]['correct'] and poses_dict[max_idx[0]]['verified'] \
                    and poses_dict[max_idx[1]]['correct'] and poses_dict[max_idx[1]]['verified']:
                if poses_dict[max_idx[0]]['error']['gtPoseIndex'] != poses_dict[max_idx[1]]['error']['gtPoseIndex']:
                    counter_of_correct_and_verified_best_poses += 1

        # # TODO: remove after debugging
        # if poses_dict[max_idx[0]]['error']['gtPoseIndex'] == poses_dict[max_idx[1]]['error']['gtPoseIndex']:
        #     if len(poses_dicts) == 100: print('scene_%s_sensor_%s: both, the second best and the first best pose belong to the same groundtruth' % (scene, sensor))
        # if scene == 4 and sensor == 3:
        #     model += 1
        #     scene = 0
        #     sensor = 0
        #     if model <= 4:
        #         if len(poses_dicts) == 100: print('************************** %s **************************' %models[model])
        # else:
        #     sensor += 1
        #     if sensor > 3:
        #         sensor = 0
        #         scene += 1

    #print('found true positives = ', counter_of_correct_and_verified_best_poses,'\ndivided by = ', len(poses_dicts))
    #wait = input('HIT ENTER TO CONTINUE...')

    # TODO: remove after debugging
    # if len(poses_dicts) == 100: print('***************************************************************')
    # if len(poses_dicts) == 100: print('***************************************************************')

    return counter_of_correct_and_verified_best_poses / len(poses_dicts)


def average_rate_of_best_pose_having_highest_verificationWeight(poses_dicts):

    counter_best_pose_having_highest_verificationWeight = 0
    for poses_dict in poses_dicts:

        # get best pose of verifier-node
        max_idx_verifier = -1
        max_weight_verifier = -1  # because not-verified poses have verifictationWeight = 0
        for pose_idx, pose in poses_dict.items():
            if pose['verificationWeight'] > max_weight_verifier:
                max_idx_verifier = pose_idx
                max_weight_verifier = pose['verificationWeight']
        # print('max pose =',max_idx,'=', poses_dict[max_idx])

        # get best pose of matcher-node
        max_idx_matcher = -1
        max_weight_matcher = -1
        for pose_idx, pose in poses_dict.items():
            if pose['weight'] > max_weight_matcher:
                max_idx_matcher = pose_idx
                max_weight_matcher = pose['weight']

        if max_idx_verifier == max_idx_matcher:
            counter_best_pose_having_highest_verificationWeight += 1

    return counter_best_pose_having_highest_verificationWeight / len(poses_dicts)


def average_rate_of_second_best_pose_having_second_highest_verificationWeight(poses_dicts):

    counter_second_best_pose_having_second_highest_verificationWeight = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verificationWeight'] > max_weight_verifier[0]:
                max_idx_verifier[0] = pose_idx
                max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verificationWeight'] > max_weight_verifier[1]:
                    max_idx_verifier[1] = pose_idx
                    max_weight_verifier[1] = pose['verificationWeight']

        # find best pose of matcher-node
        max_idx_matcher = [-1, -1]
        max_weight_matcher = [-1, -1]
        for pose_idx, pose in poses_dict.items():
            # print('pose',pose_idx, '=',pose)
            if pose['weight'] > max_weight_matcher[0]:
                max_idx_matcher[0] = pose_idx
                max_weight_matcher[0] = pose['weight']

        # find second-best pose of matcher-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_matcher[0]:
                if pose['weight'] > max_weight_matcher[1]:
                    max_idx_matcher[1] = pose_idx
                    max_weight_matcher[1] = pose['weight']

        if max_idx_verifier[1] == max_idx_matcher[1]:
            counter_second_best_pose_having_second_highest_verificationWeight += 1

    return counter_second_best_pose_having_second_highest_verificationWeight / len(poses_dicts)


def average_of_false_positive_second_best_verified_pose_part_one(poses_dicts):
    """
    counts how often the highest verifierWeight-ed pose and the second highest share the same ground truth
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_false_positive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verified']:
                    if pose['verificationWeight'] > max_weight_verifier[1]:
                        max_idx_verifier[1] = pose_idx
                        max_weight_verifier[1] = pose['verificationWeight']
        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified'] \
                    and poses_dict[max_idx_verifier[1]]['correct'] and poses_dict[max_idx_verifier[1]]['verified'] \
                    and poses_dict[max_idx_verifier[0]]['error']['gtPoseIndex'] == poses_dict[max_idx_verifier[1]]['error']['gtPoseIndex']:
                counter_false_positive += 1

    return counter_false_positive / len(poses_dicts)


def average_of_false_positive_second_best_verified_pose_part_two(poses_dicts):
    """
    counts how often the second highest verifierWeight-ed pose is false but got verified
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_false_positive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verified']:
                    if pose['verificationWeight'] > max_weight_verifier[1]:
                        max_idx_verifier[1] = pose_idx
                        max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified'] \
                    and (not poses_dict[max_idx_verifier[1]]['correct']) and poses_dict[max_idx_verifier[1]]['verified']:
                counter_false_positive += 1

    return counter_false_positive / len(poses_dicts)


def average_rate_of_false_positive_second_best_verified_pose(poses_dicts):
    """
    counts how often the second highest verifierWeight-ed pose is falsely verified
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_false_positive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verified']:
                    if pose['verificationWeight'] > max_weight_verifier[1]:
                        max_idx_verifier[1] = pose_idx
                        max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified']:
                if (not poses_dict[max_idx_verifier[1]]['correct']) and poses_dict[max_idx_verifier[1]]['verified']:
                    counter_false_positive += 1
                elif poses_dict[max_idx_verifier[1]]['correct'] and poses_dict[max_idx_verifier[1]]['verified'] \
                        and poses_dict[max_idx_verifier[0]]['error']['gtPoseIndex'] == poses_dict[max_idx_verifier[1]]['error']['gtPoseIndex']:
                    counter_false_positive += 1

    return counter_false_positive / len(poses_dicts)


def average_rate_of_false_negative_second_best_verified_pose(poses_dicts):
    """
    counts how often the second highest verifierWeight-ed pose is falsely not verified
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph

    note: the pose with the second highest verificationWeight that did not got verified has a
    lower verificationWeight than the verified pose with the highest verificationWeight. That has to be made
    sure. Because if it had a higher verificationWeight it would mean, that the best pose was
    falsely not verified (but here we are interested in the second best pose)!
    :param poses_dicts:
    :return:
    """
    counter_false_negative = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                #if not pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[1] and \
                        pose['verificationWeight'] <= max_weight_verifier[0]: # making sure that
                    # the found pose has indeed a lower verificationWeight than the best pose.
                    max_idx_verifier[1] = pose_idx
                    max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified'] \
                    and poses_dict[max_idx_verifier[1]]['correct'] and not poses_dict[max_idx_verifier[1]]['verified'] \
                    and poses_dict[max_idx_verifier[0]]['error']['gtPoseIndex'] != poses_dict[max_idx_verifier[1]]['error']['gtPoseIndex']:
                counter_false_negative += 1

    return counter_false_negative / len(poses_dicts)


def average_rate_of_true_negative_second_best_verified_pose(poses_dicts):
    """
    counts how often the second highest verifierWeight-ed pose is correctly not verified
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_true_neagtive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verificationWeight'] > max_weight_verifier[1] and\
                            pose['verificationWeight'] <= max_weight_verifier[0]: # making sure that
                        # the found pose has indeed a lower verificationWeight than the best pose.
                    max_idx_verifier[1] = pose_idx
                    max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified']:
                if not poses_dict[max_idx_verifier[1]]['correct'] and not poses_dict[max_idx_verifier[1]]['verified']:
                    counter_true_neagtive += 1
                elif poses_dict[max_idx_verifier[1]]['correct'] and not poses_dict[max_idx_verifier[1]]['verified']\
                        and poses_dict[max_idx_verifier[0]]['error']['gtPoseIndex'] == poses_dict[max_idx_verifier[1]]['error']['gtPoseIndex']:
                    counter_true_neagtive += 1

    return counter_true_neagtive / len(poses_dicts)


def average_rate_of_true_negative_second_best_verified_pose_part_one(poses_dicts):
    """
    counts how often the correct second highest verifierWeight-ed pose is not verified, because it
    has the same gt as the first highest verifierWeight-ed pose
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_true_neagtive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if (pose['verificationWeight'] > max_weight_verifier[1]) and \
                        (pose['verificationWeight'] <= max_weight_verifier[0]) : # making sure that
                        # the found pose has indeed a lower verificationWeight than the best pose.
                    max_idx_verifier[1] = pose_idx
                    max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified'] \
                    and poses_dict[max_idx_verifier[1]]['correct'] and not poses_dict[max_idx_verifier[1]]['verified']\
                    and poses_dict[max_idx_verifier[0]]['error']['gtPoseIndex'] == poses_dict[max_idx_verifier[1]]['error']['gtPoseIndex']:
                counter_true_neagtive += 1

    return counter_true_neagtive / len(poses_dicts)


def average_rate_of_true_negative_second_best_verified_pose_part_two(poses_dicts):
    """
    counts how often the second highest verifierWeight-ed pose is correctly not verified,
    because the pose is not correct
    this rate should be influenced by the conflict-threshold of the verifier's conflict graph
    :param poses_dicts:
    :return:
    """
    counter_true_neagtive = 0
    for poses_dict in poses_dicts: # loop over scenes

        # find best pose of verifier-node
        max_idx_verifier = [-1,-1]
        max_weight_verifier = [-1,-1]
        for pose_idx, pose in poses_dict.items():
            #print('pose',pose_idx, '=',pose)
            if pose['verified']:
                if pose['verificationWeight'] > max_weight_verifier[0]:
                    max_idx_verifier[0] = pose_idx
                    max_weight_verifier[0] = pose['verificationWeight']

        # find second-best pose of verifier-node
        for pose_idx, pose in poses_dict.items():
            if pose_idx != max_idx_verifier[0]:
                if pose['verificationWeight'] > max_weight_verifier[1] and\
                            pose['verificationWeight'] <= max_weight_verifier[0]: # making sure that
                        # the found pose has indeed a lower verificationWeight than the best pose.
                    max_idx_verifier[1] = pose_idx
                    max_weight_verifier[1] = pose['verificationWeight']

        if max_idx_verifier[0] > -1 and max_idx_verifier[1] > -1:
            if poses_dict[max_idx_verifier[0]]['correct'] and poses_dict[max_idx_verifier[0]]['verified'] \
                    and not poses_dict[max_idx_verifier[1]]['correct'] and not poses_dict[max_idx_verifier[1]]['verified']:
                counter_true_neagtive += 1

    return counter_true_neagtive / len(poses_dicts)


def recognition_rate_best_poses(recognition_dicts):
    """
    calculate the recognition rate for a list of recognition dictionaries
    If at least one true positive was found, a run is deemed successful. If the number of true
    positives is None, the run will be excluded from calculation
    @param recognition_dicts: list of dicts with at least {'truePositives': value}
    @return: recognition rate in range [0, 1] or None if the list contains only dicts with
             None as true positive values
    """
    #print('recognition_dicts:',recognition_dicts)
    true_positives_list = [recog_dict['truePositives'] for recog_dict in recognition_dicts\
                           if recog_dict['truePositives'] is not None]
    #print('true_positives_list:',true_positives_list)
    n_valid_runs = len(true_positives_list)

    if n_valid_runs > 0:
        return sum(i > 0 for i in true_positives_list) / n_valid_runs
    else:
        return None

def recog_rate_total(recognition_dicts):
    """
    calculate the average recognition rate of all returned poses for a list of recognition
    dictionaries

    ATTENTION: changed this function, to ensure its correct functionality in case that
               #published-poses > #detectable-instances-in-a-scene

    None-values will be ignored.
    @param recognition_dicts: list of dicts with at least
                              {'truePositives': value, 'falsePositives': value}
    @return: recognition rate for all returned poses
    """
    true_positives_list =\
        np.array([recog_dict['truePositives'] for recog_dict in recognition_dicts
                  if recog_dict['truePositives'] is not None])

    # ATTENTION: this only works if one returns n poses, where n is also the number of instances
    #            of the object to detect in one scene.
    #            for #poses>n this formula turns into "precision"
    #false_positives_list =\
    #    np.array([recog_dict['falsePositives'] for recog_dict in recognition_dicts])

    # use instead:
    unrecognized_gts_list = \
        np.array([recog_dict['unrecognizedGroundTruths'] for recog_dict in recognition_dicts])

    n = len(true_positives_list)
    total_poses_list = true_positives_list + unrecognized_gts_list #false_positives_list

    if n > 0:
        return np.mean(true_positives_list / total_poses_list)
    else:
        return None

def average_true_positives(recognition_dicts):
    """
    calculate the average number of recognized objects (true positives) for a list of recognition
    dictionaries
    None-values will be ignored.
    @param recognition_dicts: list of dicts with at least {'truePositives': value}
    @return: average of true postives
    """
    true_positives_list = [recog_dict['truePositives'] for recog_dict in recognition_dicts
                           if recog_dict['truePositives'] is not None]
    n = len(true_positives_list)

    if n > 0:
        return sum(true_positives_list) / n
    else:
        return None

def false_positives_rate(recognition_dicts):
    """
    calculate the average number of false positives for a list of recognition dictionaries
    @param recognition_dicts: list of dicts with at least {'falsePositives': value}
    @return: average number of false positives
    """
    false_positives_list = [recog_dict['falsePositives'] for recog_dict in recognition_dicts]
    n = len(false_positives_list)

    if n > 0:
        return sum(false_positives_list) / n
    else: return None


def average_distance_error(poses_list):
    """
    for a list of pose dictionaries with computed errors, return the average of the distance errors
    """
    return sum([pose['error']['distance'] for pose in poses_list]) / len(poses_list)


def average_angle_error(poses_list):
    """
    for a list of pose dictionaries with computed errors, return the average of the angle errors
    """
    return sum([pose['error']['angle'] for pose in poses_list]) / len(poses_list)


def average_precision(verification_dicts):
    """
    calculate the average precision of all returned poses for a list of verification dictionaries
    None_values will be ignored.
    @param verification_dics: list of dicts with at least
                              {'truePositives': value, 'falsePositives': value}
    @return precision of the verification for all returned poses
    """
    true_positives_list =\
        np.array([verif_dict['truePositives'] for verif_dict in verification_dicts
                  if verif_dict['truePositives'] is not None])
    false_positives_list =\
        np.array([verif_dict['falsePositives'] for verif_dict in verification_dicts])
        
    denominator = sum(true_positives_list) + sum(false_positives_list)

    if denominator > 0:
        return sum(true_positives_list) / denominator
    else:
        return 1

def average_recall(verification_dicts):
    """
    calculate the average recall of all returned poses for a list of verification dictionaries
    None_values will be ignored.
    @param verification_dics: list of dicts with at least
                              {'truePositives': value, 'falseNegatives': value}
    @return precission of the verification for all returned poses
    """
    true_positives_list =\
        np.array([verif_dict['truePositives'] for verif_dict in verification_dicts
                  if verif_dict['truePositives'] is not None])
    false_negatives_list =\
        np.array([verif_dict['falseNegatives'] for verif_dict in verification_dicts])
        
    denominator = sum(true_positives_list) + sum(false_negatives_list)
     
    if denominator > 0:
        return sum(true_positives_list) / denominator
    else:
        return 1
    
def verification_true_positives(verification_dicts):
    """
    for a list of dictionaries with with computed verification, return the sum of true positives
    """
    #print('verification_dicts =', verification_dicts)
    true_positives_list =\
        np.array([verif_dict['truePositives'] for verif_dict in verification_dicts
                  if verif_dict['truePositives'] is not None])
    return sum(true_positives_list) 

def verification_false_positives(verification_dicts):
    """
    for a list of dictionaries with with computed verification, return the sum of false positives
    """
    false_positives_list =\
        np.array([verif_dict['falsePositives'] for verif_dict in verification_dicts
                  if verif_dict['falsePositives'] is not None])
    return sum(false_positives_list) 

def verification_relevants(verification_dicts):
    """
    for a list of dictionaries with with computed verification, return the sum of relevant poses (tp + fn)
    """
    false_negatives_list =\
        np.array([verif_dict['falseNegatives'] for verif_dict in verification_dicts
                  if verif_dict['falseNegatives'] is not None])
    return sum(false_negatives_list) + verification_true_positives(verification_dicts)

def average_match_time(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the matching times (from the field 'match_time_ms')
    """
    return sum([msd['match_time_ms'] for msd in matcher_statistics_dicts]) /\
        len(matcher_statistics_dicts)

def average_model_building_time(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the model building times (from the field 'create_time_ms')
    """

    # somehow the "copy missing from previous run" does not work for this statistic, hence
    summed_create_time = 0
    number_of_models = 0
    for msd in matcher_statistics_dicts:
        try:
            summed_create_time += msd['create_time_ms']
            number_of_models += 1
        except:
            continue
    #print("[data_extraction.average_model_building_time()] number of models:", number_of_models)
    return summed_create_time / number_of_models

def average_number_of_cloud_points(preprocessor_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a preprocessor node, return the
    average number of points (from the field 'n_points_surface')
    """
    #print (preprocessor_statistics_dicts)
    return sum([scene['n_points_surface'] for psd in preprocessor_statistics_dicts for scene in psd.values()]) /\
        len(preprocessor_statistics_dicts)

def average_key_figure_per_voting_ball(statistics_dicts, key_figure):
    """
    for a list of dictionaries with model-specific statistics form a node, return the
    average of the specified key_figure (i.e. from the field 'key_figure').
    A list is returned, with one average per voting ball.
    use this when key_figure has a separate value for each voting ball
    """
    #print('key_figure_data =', statistics_dicts)
    sum_per_voting_ball = []
    # intitialize list of sums with zero
    for voting_ball in statistics_dicts[0][key_figure]:
        sum_per_voting_ball.append(0)

    # sum up
    for sd in statistics_dicts:
        for voting_ball_idx, value in enumerate(sd[key_figure]):
            sum_per_voting_ball[voting_ball_idx] += value

    # calculate average
    result = []
    for sum in sum_per_voting_ball:
        result.append(sum/len(statistics_dicts))
    return result

def average_key_figure(statistics_dicts, key_figure):
    """
    for a list of dictionaries with model-specific statistics form a node, return the
    average of the specified key_figure (i.e. from the field 'key_figure')
    use when key_figure has only ONE value, NOT one for each voting ball!
    """
    #print('key_figure_data =', statistics_dicts)
    return sum([sd[key_figure] for sd in statistics_dicts]) /\
        len(statistics_dicts)

def average_mu_time(verifier_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics from a verifier node, return
    the average of the mu value times (from the field 'mu-value_time_ms')
    """
    return sum([vsd['mu-value_time_ms'] for vsd in verifier_statistics_dicts]) /\
        len(verifier_statistics_dicts)
        
def average_voting_time_wo_VB_and_wo_FA_building(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the voting times (from the field 'voting_time_ms') without the building times of
    an optionally used voting ball or flag array
    """
    return sum([msd['voting_time_ms'] for msd in matcher_statistics_dicts]) /\
        len(matcher_statistics_dicts)

def average_voting_time_wo_VB_and_w_FA_building(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the voting times (from the field 'voting_time_ms_incl_flag_array_building') 
    without the building times of an optionally used voting ball, but WITH the building time 
    of an optionally used flag array
    """
    return sum([msd['voting_time_ms_incl_flag_array_building'] for msd in matcher_statistics_dicts]) /\
        len(matcher_statistics_dicts)
        
def average_voting_time_w_VB_and_w_FA_building(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the voting times (from the field 'voting_time_ms_incl_voting_ball_voxel_grid_building')
    WITH the building times of an optionally used voting ball, AND WITH the building time of 
    an optionally used flag array
    """
    return sum([msd['voting_time_ms_incl_voting_ball_voxel_grid_building'] for msd in matcher_statistics_dicts]) /\
        len(matcher_statistics_dicts)

def average_clustering_time(matcher_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics form a matcher node, return the
    average of the voting times (from the field 'voting_time_ms_incl_voting_ball_voxel_grid_building')
    WITH the building times of an optionally used voting ball, AND WITH the building time of 
    an optionally used flag array
    """
    return sum([msd['match_time_ms'] - msd['voting_time_ms'] for msd in matcher_statistics_dicts]) /\
        len(matcher_statistics_dicts)

def average_edge_adding_time(verifier_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics from a verifier node, return
    the average of the times needed for adding the edges to the conflict graph
    """
    return sum([vsd['edge_adding_time_ms'] for vsd in verifier_statistics_dicts]) /\
        len(verifier_statistics_dicts)

def average_conflic_removing_time(verifier_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics from a verifier node, return
    the average of the times needed for remove the conflicts from the conflict graph
    """
    return sum([vsd['conflic_removing_time_ms'] for vsd in verifier_statistics_dicts]) /\
        len(verifier_statistics_dicts)

def average_model_points(verifier_statistics_dicts):
    """
    for a list of dictionaries with model-specific statistics from a verifier node, return
    the average number of model points
    """
    return sum([vsd['averageModelPoints'] for vsd in verifier_statistics_dicts]) /\
        len(verifier_statistics_dicts)

def average_weight_best_pose_cluster(model_poses_dicts):
    """
    for a list of model poses, return the average weight of the best pose clusters
    """
    return np.average([single_run_poses[0]['weight'] for single_run_poses in model_poses_dicts])

def average_dict_entries(dicts):
    """
    for a list of dicts that all contain the same entries, return one dict with the nummerical
    average of the dict entries
    missing entries will be ignored
    """
    return_dict = {}
    for key in dicts[0].keys():
        return_dict[key] = np.average([d[key] for d in dicts if key in d.keys()])
    return return_dict

def averageModelPoints_verifier(verifier_statistic_dicts):
    """
    for a list of verifier statistic dicts, return the average number of averageModelPoints
    used for zbufferingresolution-tuning
    :param verifier_statistic_dicts:
    :return: average of averageModelPoints
    """
    return sum(run['averageModelPoints'] for run in verifier_statistic_dicts)/len(verifier_statistic_dicts)

if __name__ == "__main__":
    pass
