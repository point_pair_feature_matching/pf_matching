# -*- coding: utf-8 -*-
#!/usr/bin/env python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# imports
import itertools
import matplotlib
import matplotlib.colors
import matplotlib.cm
from matplotlib.figure import Figure
import os.path


def cm2inch(*tupl):
    """
    convert one or more arguments from cm to inches and return them as a tuple
    Code taken from: http://stackoverflow.com/a/22787457
    @param tupl: the cm values
    @return: tuple of inch values
    """
    inch = 2.54  # inch / cm
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)


def save_figure(fig, dir, base_name, extensions=('pdf', 'png'), **kwargs):
    """
    save a matplotlib figure to disk as multiple data types (convenience function)
    @type fig: Figure
    @param fig: the figure to save
    @param dir: directory string, may contain '~' for home directory
    @param base_name: file name without extensions
    @param extensions: file extensions specifying types to save, must be supported by current
                       matplotlib backend, usually out of ('pdf', 'png', 'ps', 'eps', 'svg')
    @param kwargs: keyword arguments to directly pass to fig.savefig()
    @return: None
    """

    for format in extensions:
        full_path = os.path.join(os.path.expanduser(dir), base_name + '.' + format)
        fig.savefig(full_path, format=format, **kwargs)


def get_cmap(n, cmap_name="brg"):
    """
    Get a mapping from index to rgb-color for the given color map
    Taken from from http://stackoverflow.com/a/25628397.
    @n number of distinct colors
    @cmap_name colormap to use
               (see http://matplotlib.org/examples/color/colormaps_reference.html)
    @return function that maps each index in 0, 1, ... n-1 to a color of the color map so that
            0 and n-1 point to the two ends of the color map and all other indices are evenly spaced
            in between them
    """
    color_norm = matplotlib.colors.Normalize(vmin=0, vmax=n - 1)
    scalar_map = matplotlib.cm.ScalarMappable(norm=color_norm, cmap=cmap_name)

    return scalar_map.to_rgba


class PlotColors(object):
    """
    @brief provides different colors for plots, so that next() provides an individual color for
           each plot
    """

    def __init__(self, n, cmap_name="brg"):
        """
        @brief construct for a specified number of plots
        @param n number of different colors to provide, based on the color map
        @param cmap_name valid name of a color-map to create the colors from
               (see http://matplotlib.org/examples/color/colormaps_reference.html)
        """

        self._n = n
        self._current = -1
        self._cmap = get_cmap(n, cmap_name)

    def next(self):
        """
        @brief return the next color
        @return color as rgb-alpha tuple
        """

        self._current += 1
        if self._current >= self._n:
            print("Warning, not enough colors to provide unique colors for each graph. "
                  "Starting at the beginning again.")
            self._current = 0

        return self._cmap(self._current)


class PlotMarkers(object):
    """
    @brief provides different markers for plots, so that next() provides an individual marker for
           each plot
    """

    def __init__(self, marker_symbols=('x', '+', 'v', '^', 'o', 's', 'D', '*')):
        """
        @brief construct by providing marker symbols
        @parm marker_symbols iterable object containing a subset of
              matplotlib.markers.MarkerStyle.markers

        """

        self._marker_cycle = itertools.cycle(marker_symbols)
        self._n = len(marker_symbols)
        self._current = -1

    def next(self):
        """
        @brief return the next marker symbol
        @return marker symbol
        """
        self._current += 1
        if self._current >= self._n:
            print("Warning, not enough marker symbols to provide unique markers for each graph. "
                  "Starting at the beginning again.")

        return self._marker_cycle.next()


class PlotStyle(object):
    """
    @brief provides style settings for plots so that each call to next() gives settings with
           a different color and marker
    """

    def __init__(self, n, cmap_name=None, marker_symbols=None, additional_plot_settings=None):
        """
        @brief construct from settings
        @param n number of evenly spaced colors to provide
        @param cmap_name valid name of a color-map to create the colors from, if None, defaults
               to the default setting of PlotColors
               (see http://matplotlib.org/examples/color/colormaps_reference.html)
        @param marker_symbols valid marker symbols to set in the plot, if None, defaults to the
                              default setting of PlotMarkers
        @parm additional_plot_settings dictionary of keyword arguments to be passed through to the
                                       plot function; Note that setting "marker", "color" or
                                       "markeredgecolor" will override the individual settings for
                                       each plot
        """
        if cmap_name is None:
            self._colors = PlotColors(n)
        else:
            self._colors = PlotColors(n, cmap_name)

        if marker_symbols is None:
            self._markers = PlotMarkers()
        else:
            self._markers = PlotMarkers(marker_symbols)

        if additional_plot_settings is None:
            additional_plot_settings = {}

        self._additional_plot_settings = additional_plot_settings

    def next(self):
        """
        @brief get settings for the next plot
        @return dict of named keyword arguments that can be passed to the plot-function
        """
        # deep copy of additional settings
        out_dict = self._additional_plot_settings.copy()

        # get next marker and color
        m = self._markers.next()
        c = self._colors.next()

        # if the settings for marker and color ar not overriden by the additional settings, set
        # them now
        out_dict["marker"] = out_dict.get("marker", m)
        out_dict["color"] = out_dict.get("color", c)
        out_dict["markeredgecolor"] = out_dict.get("markeredgecolor", c)

        return out_dict


# default plot settings for publishing-quality plots in the thesis,
# set via matplotlib.rcParams.update(thesis_default_settings)
thesis_default_settings = \
    {'font.size': 10,
     'axes.titlesize': 10,
     'lines.markersize': 5,
     'legend.fontsize': 8,
     'savefig.dpi': 300,
     'figure.dpi': 100,
     'figure.autolayout': True,  # use Figure.tight_layout() to adjust subplots
     'savefig.pad_inches': 0,
     'mathtext.default': 'rm'  # upright (roman) fonts for Tex-rendered formulas
     }
    
krone_thesis_default_settings = \
    {'font.size': 11,
     'axes.titlesize': 10,
     'lines.markersize': 5,
     'legend.fontsize': 8,
     'savefig.dpi': 300,
     'figure.dpi': 100,
     'figure.autolayout': True,  # use Figure.tight_layout() to adjust subplots
     'savefig.pad_inches': 0.05,
     #'mathtext.default': 'rm',  # upright (roman) fonts for Tex-rendered formulas
     #'text.default': 'r',
     'font.family': 'serif',
     'font.serif': 'Latin Modern Roman',
     'text.usetex': True,
     'text.latex.unicode': True,
     }

ziegler_thesis_default_settings = \
    {'font.size': 10,
     'axes.titlesize': 10,
     #'axes.labelsize': 7,
     #'xaxis.labelsize': 8,      # gets often overwritten e.g. in bar_plot()
     #'xaxis.labelrotation': 45, # does not work?
     'patch.linewidth': 0.5, # linewidth for patch-objects such as the legend
     'axes.linewidth': 0.5,
     'xtick.top': False,
     'lines.markersize': 5,
     'legend.fontsize': 8,
     'savefig.dpi': 300,
     'figure.dpi': 100,
     'figure.autolayout': False,  # DON'T use Figure.tight_layout() to adjust subplots automatically
     'savefig.pad_inches': 0,
     #'mathtext.default': 'rm',  # upright (roman) fonts for Tex-rendered formulas
     #'text.default': 'r',
     'font.family': 'serif',
     'font.serif': 'Latin Modern Roman',
     'text.usetex': True,
     'text.latex.unicode': True,
     }

ICVS_2019_default_settings = \
    {'font.size': 8,
     'axes.titlesize': 8,
     #'axes.labelsize': 7,
     #'xaxis.labelsize': 8,      # gets often overwritten e.g. in bar_plot()
     #'xaxis.labelrotation': 45, # does not work?
     'patch.linewidth': 0.5, # linewidth for patch-objects such as the legend
     'axes.linewidth': 0.5,
     'xtick.top': False,
     'lines.markersize': 5,
     'legend.fontsize': 6,
     'savefig.dpi': 300,
     'figure.dpi': 100,
     'figure.autolayout': False,  # DON'T use Figure.tight_layout() to adjust subplots automatically
     'savefig.pad_inches': 0,
     #'mathtext.default': 'rm',  # upright (roman) fonts for Tex-rendered formulas
     #'text.default': 'r',
     'font.family': 'serif',
     'font.serif': 'Latin Modern Roman',
     'text.usetex': True,
     'text.latex.unicode': True,
     }

if __name__ == "__main__":

    pass
