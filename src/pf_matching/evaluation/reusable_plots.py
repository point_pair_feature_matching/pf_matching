# -*- coding: utf-8 -*-
#!/usr/bin/env python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *
input = raw_input
range = xrange

# imports
import numpy as np

def bar_by_group(plot_data, ax, legend_title, show_legend=True,
                 group_name_replacements=(('_', '\n'),), bar_width=0.2,
                 colors=('r', 'g', 'b', 'k', 'c')):
    """
    make a bar plot with groups of bars from nested tmp_data
    @param plot_data: nested tmp_data for plotting:
                      ((bar0_name, ((group0_name, g0b0_data), (g1_name, g1b0_data), ...)
                       (bar1_name, (...),
                       ...)
                       all tuples must contain the same tmp_data in the same order
    @type ax: Axes
    @param ax: axis to plot to
    @param show_legend: if True, add lenged, else no legend
    @param legend_title: title to set for the lenged
    @param group_name_replacements: tuple to replace sections of the group name strings
    @param bar_width: width of the bars in relative units
    @param colors: colors for the bars (left to right)
    """

    # get all possible group names as list
    unique_group_names = set()
    for bar_names, group_data in plot_data:
        group_names, values = zip(*group_data)
        for name in group_names:
            unique_group_names.add(name)
    group_names = sorted(list(unique_group_names))

    # locations of the groups in the plots
    group_loc = np.arange(len(group_names))
    group_dict = dict(zip(group_names, group_loc))

    # number of bars per plot
    n_bars = len(plot_data)

    # do plotting for all first bars, second bars etc. in the groups
    for i, (bar_names, group_data) in enumerate(plot_data):
        # replace group name with plot location
        plt_tuples = [(group_dict[entry[0]] + i * bar_width, entry[1]) for entry in group_data]

        ax.bar(*zip(*plt_tuples), width=bar_width, color=colors[i], label=bar_names)

    # replace group name parts if required:
    for i, name in enumerate(group_names):
        for old, new in group_name_replacements:
            name = name.replace(old, new)
        group_names[i] = name

    # set group names under bars
    ax.set_xlim(0)
    ax.set_xticks(group_loc + n_bars / 2 * bar_width)
    # group_names = tuple([name.replace(*group_name_replacements) for name in group_names])
    ax.set_xticklabels(group_names, fontsize=8)

    # misc plot settings
    if show_legend:
        ax.legend(title=legend_title)
    ax.grid(axis='y')
