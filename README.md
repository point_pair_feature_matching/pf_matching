This package contains a toolbox for automating demos and experiments with
the contents of the pf_matching_core and pf_matching_tools package. In addition,
it provides a number of small tools for model conversion etc.

# INSTALLATION

The package was tested with ROS Indigo on Ubuntu 14.04 and with PCL 1.71 and
Meshlab 1.3.2.
To install the package, please follow the steps below.
It is assumed that you are using a catkin
workspace that is located at `~/ROS/catkin_workspace` and that all dependencies
were installed as described in the pf_matching_core package.

1. (install pf_matching_core and pf_matching_tools package)

2. Install additional dependencies:
`
sudo apt-get install meshlab
`

2. Download this Git project into the source folder:
`
mkdir -p ~/ROS/catkin_workspace/src/pf_matching
cd ~/ROS/catkin_workspace/src/pf_matching
git clone https://gitlab.tubit.tu-berlin.de/Master_thesis_Xaver/pf_matching.git .
`
4. Build the catkin workspace and source the changes:
`
cd ~/ROS/catkin_workspace
catkin_make
source ~/ROS/catkin_workspace/devel/setup.bash
rospack profile
`

# CONTENTS

All end-user code is provided under `/scripts`. Run the scripts from a sourced terminal via
`python ~/ROS/catkin_workspace/src/pf_matching/scripts/<...>`

* `scripts/experiments/` contains various scripts to run and evaluate the experiments
  described in Xaver's thesis. Please adjust the parameters and file paths to your
  needs. The experiments need a running `roscore`-instance.
* `scripts/demos/` contains three demo-experiments that show the matching algorithm
  in action. Make sure that you have the required datasets stored in the correct location
  (`~/ROS/datasets/<name>`) or adjust the path in the script. Also make sure
  that a `dataset.py` or `blensor_dataset.py` is present in the root directory of
  the dataset. The demos furthermore need a running `roscore`-instance.
* `scripts/model_conversion/process_models.py` is a small GUI program that automates the
  conversion of meshes to point clouds and estimates normals. First chose the
  appropriate Meshlab batch file, then select the input files and output directory.
* `scripts/bin2yaml.py` is a small GUI script to convert binary experiment data to human
                        readable YAML-files. Note that converting large files may take a while
                        and that the results may be too long for your editor to display them.
* `scripts/example_dataset_scripts/blensor_dataset.py` is an example of how to write
                    a Python script file so that the experiment code can utilize a dataset.

# MISC

* The evaluation of the experiments currently assumes that the equivalent poses
  for rotationally symmetric objects that the matcher-node returns are correct and
  uses them to calculate the pose errors. In the future, this information should be
  provided by the datasets.

* A documentation of the source code can be built via Doxygen. To enable Doxygens special 
  commands like `@param`,`@return`,`@brief`, etc., the filter 'doxypy' needs to be installed first.
`sudo apt-get install doxypy`
`cd ~/ROS/catkin_workspace/src/pf_matching doxygen`
The documentations start page will be located under `/doc/index.html`.

